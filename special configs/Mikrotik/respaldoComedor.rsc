# jul/18/2019 06:23:01 by RouterOS 6.44
# software id = HPAX-4163
#
# model = RouterBOARD 750 r2
# serial number = 58D705D714E9
/interface bridge
add fast-forward=no name=BRIDGE
/interface ethernet
set [ find default-name=ether1 ] advertise=\
    10M-half,10M-full,100M-half,100M-full,1000M-half,1000M-full
set [ find default-name=ether2 ] advertise=\
    10M-half,10M-full,100M-half,100M-full,1000M-half,1000M-full
set [ find default-name=ether3 ] advertise=\
    10M-half,10M-full,100M-half,100M-full,1000M-half,1000M-full
set [ find default-name=ether4 ] advertise=\
    10M-half,10M-full,100M-half,100M-full,1000M-half,1000M-full
set [ find default-name=ether5 ] advertise=\
    10M-half,10M-full,100M-half,100M-full,1000M-half,1000M-full
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/ip hotspot profile
set [ find default=yes ] html-directory=flash/hotspot
/ip ipsec proposal
set [ find default=yes ] enc-algorithms=aes-128-cbc
/system logging action
set 1 disk-file-name=log
/interface bridge port
add bridge=BRIDGE hw=no interface=ether2
add bridge=BRIDGE hw=no interface=ether3
add bridge=BRIDGE hw=no interface=ether4
add bridge=BRIDGE hw=no interface=ether5
/ip address
add address=10.11.120.151/26 interface=ether1 network=10.11.120.128
add address=172.16.0.1/16 interface=BRIDGE network=172.16.0.0
/ip firewall address-list
add address=10.11.120.151 list=ETH1
/ip firewall nat
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde LAN hacia servidor SIGMATION (TCP 3306)" \
    dst-address=172.16.0.1 dst-port=3306 protocol=tcp to-addresses=\
    10.11.206.21
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (TCP 80)" \
    dst-address-list=ETH1 dst-port=80 protocol=tcp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia Ubiquiti (TCP 443)" \
    dst-address-list=ETH1 dst-port=443 protocol=tcp to-addresses=172.16.0.4
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (TCP 8080)" \
    dst-address-list=ETH1 dst-port=8080 protocol=tcp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (TCP 22)" \
    dst-address-list=ETH1 dst-port=22 protocol=tcp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (UDP 137)" \
    dst-address-list=ETH1 dst-port=137 protocol=udp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (UDP 138)" \
    dst-address-list=ETH1 dst-port=138 protocol=udp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (TCP 137)" \
    dst-address-list=ETH1 dst-port=137 protocol=tcp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (TCP 139)" \
    dst-address-list=ETH1 dst-port=139 protocol=tcp to-addresses=172.16.0.2
add action=dst-nat chain=dstnat comment=\
    "Port-forwarding desde red cliente hacia RASPBERRY (TCP 445)" \
    dst-address-list=ETH1 dst-port=445 protocol=tcp to-addresses=172.16.0.2
add action=masquerade chain=srcnat comment="Enmascaramiento de red privada par\
    a conexi\F3n con red cliente y salida a internet" out-interface=ether1
/ip route
add distance=1 gateway=10.11.120.130
/system clock
set time-zone-name=America/Santiago
/tool romon port
add
