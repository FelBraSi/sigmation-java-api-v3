package sigmation.mqtt;
 
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import sigmation.utils.SigmationVarTopic; 

interface MqttEventListener {
    void mqttMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token);
    void mqttSetMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token, String resource);
}

public class pahoMqtt implements MqttCallback
{	
	static ScheduledExecutorService aliveScheduler;
	
	Responder thisresponder;
	SigmationVarTopic stopic;
	
	private List<MqttEventListener> listeners = new ArrayList<MqttEventListener>();
	
	public void addListener(MqttEventListener toAdd) {
        listeners.add(toAdd);
    }
	
	private String SmgSys = "____DRS";
	private String res = "__dev";
	private String param= "__set";
	
	
	private String CONNECTION_URL = "tcp://127.0.0.1:1883";
    private String[] SUBSCRIPTION = {
    													"sigmation/"+SmgSys+"/"+res+"/+/__resourcegroup/+/__dev/+/__group/+/__var/+/"+param,
    													"sigmation/"+SmgSys+"/"+res+"/+/__resourcegroup/+/__dev/+/__var/+/"+param,
    													"sigmation/"+SmgSys+"/"+res+"/+/__group/+/__var/+/"+param,
    													"sigmation/"+SmgSys+"/"+res+"/+/__var/+/"+param,
    													"sigmation/"+SmgSys+"/"+res+"/+/__deploy"
    												};
    private String baseTopic	= "sigmation/"+SmgSys+"/"+res+"/";
    private final String USERNAME = "user";
    private final String PASSWORD = "pass";
    static MqttClient publisher;
    MqttDeliveryToken token; 
    
    String onlineStatusTopic = "sigmation/__system/____backend/java/__var/online/__value";
    byte[] onlineLastWillPayload = {0x30};
    
	public pahoMqtt(String SmgSys, String res, String param) {
			this.SmgSys = SmgSys;
			this.res=res;
			this.param=param;
			redoSUBS();
			System.out.println("paho setup: "+this.SmgSys+" , "+this.res+" , "+this.param);
	}
	
	public String getBaseTopic() {
		return baseTopic;
	}
	
	private void redoSUBS() {
		String[] LOCAL_SUBSCRIPTION = {
				"sigmation/"+SmgSys+"/"+res+"/+/__resourcegroup/+/__dev/+/__group/+/__var/+/"+param,
				"sigmation/"+SmgSys+"/"+res+"/+/__resourcegroup/+/__dev/+/__var/+/"+param,
				"sigmation/"+SmgSys+"/"+res+"/+/__group/+/__var/+/"+param,
				"sigmation/"+SmgSys+"/"+res+"/+/__var/+/"+param,
				"sigmation/"+SmgSys+"/"+res+"/+/__deploy"
			};
		SUBSCRIPTION= LOCAL_SUBSCRIPTION;
		baseTopic	= "sigmation/"+SmgSys+"/"+res+"/";
	}
	
	public void start() {
   	 	mqttClient();
	} 
	
	public void mqttClient() {
		try { 
			publisher = new MqttClient(CONNECTION_URL,MqttClient.generateClientId());
			publisher.setCallback(this);
			MqttConnectOptions options = new MqttConnectOptions();
			options.setAutomaticReconnect(true);
			options.setCleanSession(true);
			options.setConnectionTimeout(10);
			options.setPassword(stringToCharArray(PASSWORD));
			options.setUserName(USERNAME);
			options.setWill(onlineStatusTopic, onlineLastWillPayload, 0, true);
			options.setMaxInflight(1000);
			publisher.connect(options);		
			publisher.subscribe(SUBSCRIPTION); 
			setOnline(); 
			setOnlineSchedule();
			//publisher.disconnect();
		} catch (Exception e) { 
			
		}
	}
	
	
	
	private void setOnline(){
		try {
			byte[] payload = {0x31};
			MqttMessage myMessage= new MqttMessage(payload);
			publisher.publish(onlineStatusTopic, myMessage);
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}
	
	public void publish(String topic,byte[] payload){
		try { 
			MqttMessage myMessage= new MqttMessage(payload);
			publisher.publish(topic, myMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	private static char[] stringToCharArray(String testString) { 
		char[] stringToCharArrayS = testString.toCharArray();
		return stringToCharArrayS;
	}
	
	@Override
	public void connectionLost(Throwable cause) { 
		System.out.println("MQTT connection lost");
		cause.printStackTrace();
	}
	
	@Override
	public void messageArrived(String topic, MqttMessage message)
	        throws Exception {
		System.out.println(topic+"   "+message); 
		processMessage(topic, message);	
	}
	
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) { 
		//System.out.println("message sent");
	}
	
	class MqttObject{
		public SigmationVarTopic stopic;
		
	}
	
	
	private void processMessage(String topic, MqttMessage message) {
		 String[] splitedTopic = topic.split("/__var/"); 		
		 String[] splitedTopic2 = topic.split("/____DRS/__dev/");
		 MqttObject thisMqttObject = new MqttObject();
		 
		 if(splitedTopic.length==2) {
			 //acceptable
			 System.out.println(splitedTopic[1]);
			 String[] splitedVar = splitedTopic[1].split("/");			 
			 if(splitedVar.length==2) {
				 //acceptable
				 thisMqttObject.stopic = new SigmationVarTopic();
				 if(splitedTopic2.length==2) {
					 String[] splitedVar2 = splitedTopic2[1].split("/");
					 System.out.println(splitedVar2[0]);
					 if(splitedVar2.length>0) {
						 thisMqttObject.stopic.setVarTopicId(splitedVar2[0]);
						 if(splitedVar2[1].contentEquals("__group")) {
							 System.out.println(splitedVar2[2]);
							 thisMqttObject.stopic.setGroup(splitedVar2[2]);
						 } else if(splitedVar2[1].contentEquals("__resourcegroup")) {
							 System.out.println(splitedVar2[2]);
							 thisMqttObject.stopic.setResourceGroup(splitedVar2[2]);
							 thisMqttObject.stopic.setSubElement(true);
							 if(splitedVar2[3].contentEquals("__dev")) {
								 System.out.println(splitedVar2[4]);
								 thisMqttObject.stopic.setResourceName(splitedVar2[4]);
								 if(splitedVar2[5].contentEquals("__group")) {
									 System.out.println(splitedVar2[6]);
									 thisMqttObject.stopic.setGroup(splitedVar2[6]);
								 }
							 }
						 } 
					 }			 
				 }
				 thisMqttObject.stopic.setBaseTopic(splitedTopic[0]);
				 thisMqttObject.stopic.setVarName(splitedVar[0]);
				 thisMqttObject.stopic.setCurrentVal(message);
				 System.out.println(splitedVar[0]); 
				 System.out.println(splitedVar[1]); 
				 if(splitedVar[1].contentEquals("__set")) { 
					 try {
						sendMqttMessage(thisMqttObject.stopic, publisher, token);
						//System.out.println("reacted "+stopic.getCurrentTopic()+"  "+ new String(stopic.getCurrentVal()));
					} catch (Exception e) { 
						e.printStackTrace();
					}  
				 } else if(splitedVar[1].contentEquals("__deploy")) { 
					 try {
						sendSetMqttMessage(thisMqttObject.stopic, publisher, token, splitedVar[0]);
						//System.out.println("reacted "+stopic.getCurrentTopic()+"  "+ new String(stopic.getCurrentVal()));
					} catch (Exception e) { 
						e.printStackTrace();
					}  
				 } else {
					 System.out.println("Nothing to do");
				 }
			 } else {
				 //not acceptable
				 System.out.println("Message is not sigmation format-appliant format"); 
			 }
		 } else {
			 //not acceptable
			 System.out.println("Message is not sigmation format-appliant variable"); 
		 }
	}	
	
	private void sendMqttMessage(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token) throws Exception{
		for (MqttEventListener hl : listeners)
            hl.mqttMessageReceived( stopic,  publisher,  token);
		/*MqttTopic mqttTopic = publisher.getTopic(stopic.getCurrentTopic()); 
		 token = mqttTopic.publish(stopic.getCurrentVal(),0,false);*/
	} 
	
	private void sendSetMqttMessage(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token, String resource) throws Exception{
		for (MqttEventListener hl : listeners)
            hl.mqttSetMessageReceived( stopic,  publisher,  token, resource);
		/*MqttTopic mqttTopic = publisher.getTopic(stopic.getCurrentTopic()); 
		 token = mqttTopic.publish(stopic.getCurrentVal(),0,false);*/
	} 
	
	private void setOnlineSchedule(){
		aliveScheduler = Executors.newScheduledThreadPool(1);      
		Long perseconds=LocalDateTime.now().until(LocalDateTime.now(), ChronoUnit.MINUTES);
		aliveScheduler.scheduleAtFixedRate(aliveSet, perseconds, 20, TimeUnit.SECONDS); 
	}
	
	final Runnable aliveSet = new Runnable() {
	       public void run() { setOnline(); }
	}; 
	 
	public class Responder implements MqttEventListener {

		public void mqttMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token) {
			System.out.println(stopic.getCurrentTopic()+" "+stopic.getCurrentVal());
			// TODO Auto-generated method stub
			
		}
		
		public void mqttSetMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token, String resource) {
			System.out.println(stopic.getCurrentTopic()+" "+stopic.getCurrentVal());
			// TODO Auto-generated method stub
			
		}
	} 
	/*
	public class mqttReact{
		public void onreceivedSigmationMessage(MqttEventListener Responder) {
			Responder.mqttMessageReceived(stopic,publisher,token);
		}
	}*/
}


