package sigmation.mail;
 
import java.time.LocalDateTime;
import java.util.ArrayList;

import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;

public class Reporter {

	public static class PersonasFlags{
		public static final int venflag = 0;
		public static final int over_hours=1;
		public static final int time_total=2;
		public static final int idpersonas=3;
		public static final int nombre1=4;
		public static final int nombre2=5;
		public static final int apellido=6;
		public static final int apellido2=7;
		public static final int idn=8;
		public static final int tag=9;
		public static final int email=10;
		public static final int area=11; 
		public static final int ultimazona=13;
		public static final int ultimoingreso=14;
		public static final int ultimasalida=15;
		public static final int vehiculo=20;
		public static final int empresa=25;
		
		
		public static ArrayList<String[]> getPersonasFlags(){
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "get_search_personas_flags";
			cr.connect(); 
			String[] data= {"''"};  
			ArrayList<String[]> result = cr.callValueProcedure(call,data);
			cr.close(); 
			return result;
		}	
		
	}
	
	public static class busLogReport{
		public static final int idbuslog=0;
		public static final int idingreso=1;
		public static final int serielector=2;
		public static final int cantidad=3;
		public static final int tiempo=4;
		public static final int placa=5;  //idn

		public static ArrayList<String[]> getBusList(int turno, String thisdate){
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "getbuslog";
			cr.connect(); 
			String[] data= {"''",""+turno,"'"+thisdate+"'",""+0};  
			ArrayList<String[]> result = cr.callValueProcedure(call,data);
			cr.close(); 
			return result;
		}
	} 

	public static class PersonasReport{ 
		public static final int idpersonas=0;
		public static final int nombre1=1;
		public static final int nombre2=2;
		public static final int apellido=3;
		public static final int apellido2=4;
		public static final int idn=5; 
		public static final int ultimazona=6;
		public static final int ultimoingreso=7;
		public static final int ultimasalida=8; 
		public static final int entrada=9; 
		public static final int nombrezona=10; 
		public static final int ultimaZonaTurno=11; 
		public static final int serial=12; 
		public static final int timeTotal=13; 
		public static final int timeWork=14;   //seconds , can be + or -, if negative, current timestamp should be added 
		public static final int area=15;
		public static final int nombreEmpresa=16;
		public static final int nombreZonaTurno=17;
		public static final int numeroZonaTurno=18;
		public static final int idZonaTurno=19;
		public static final int vehiculo=20;
		public static final int estadoTag=21; 
		public static final int enmina=22;
		public static final int timeTotalN=23;
		public static final int timeTotalA=24; 
		
		public static ArrayList<String[]> getPersonasReport(int turno, LocalDateTime thisdate){
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "turnGet";
			cr.connect(); 
			String[] data= {"''",""+turno,"'"+SysUtils.getDateFormated(thisdate)+"'",""+0};  
			ArrayList<String[]> result = cr.callValueProcedure(call,data);
			cr.close(); 
			return result;
		}
		
		public static ArrayList<String[]> getPersonasReport(int turno, String thisdate){
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "turnGet";
			cr.connect(); 
			String[] data= {"''",""+turno,"'"+thisdate+"'",""+0};  
			ArrayList<String[]> result = cr.callValueProcedure(call,data);
			cr.close(); 
			return result;
		}	
	}
	
	public static class tagFlags{
		public static final int idregpers = 0;
		public static final int idpers=1;
		public static final int tiempo=2;
		public static final int tipoevento=3;
		public static final int empresa=4;
		public static final int zona=5;
		public static final int rfidreader=6;
		public static final int ulzona=7;
		public static final int idzonas=8;
		public static final int numerozona=9;
		public static final int nombrezona=10;
		public static final int descripcionzona=11;
		public static final int mapx=12; 
		public static final int mapy=13;
		public static final int mapid=14;
		public static final int serial=15;
		public static final int idevento=16; 
		public static final int nombrevento=17; 
		public static final int tagserial=18;  
		
		public static ArrayList<String[]> getTagFlags(){
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "get_tnt_alerts_t_a";
			cr.connect(); 
			String[] data= {""};  
			ArrayList<String[]> result = cr.callValueProcedure(call,data);
			cr.close(); 
			return result;
		}	
	}
	
	public static class VehicleFlags{
		public static final int venflag = 0;
		public static final int over_hours=1;
		public static final int time_total=2;
		public static final int idpersonas=3;
		public static final int nombre1=4;
		public static final int nombre2=5;
		public static final int apellido=6;
		public static final int apellido2=7;
		public static final int idn=8;
		public static final int tag=9;
		public static final int email=10;
		public static final int area=11; 
		public static final int ultimazona=13;
		public static final int ultimoingreso=14;
		public static final int ultimasalida=15;
		public static final int vehiculo=20;
		public static final int empresa=25;
		
		
		public static ArrayList<String[]> getVehicleFlags(){
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "get_search_vehicles_flags";
			cr.connect(); 
			String[] data= {"''"};  
			ArrayList<String[]> result = cr.callValueProcedure(call,data);
			cr.close(); 
			return result;
		}	
	}
	
	
}
