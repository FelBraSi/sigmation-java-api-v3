package sigmation.mail;

 
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sigmation.utils.SysUtils; 


public class CsvFileWriter {
	
	//Delimiter used in CSV file
	public static final String COMMA_DELIMITER = ";";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static FileWriter fileWriter = null;
	private static String filepath = "/home/pi/Documents/";
	private static String sigmalogo = "sigmalogo.png";
	private static String minalogo = "minalogo.png";
	private static String sigmalogofull = filepath+sigmalogo;
	private static String minalogofull = filepath+minalogo;
	private static int sigmalogoRow = 0;
	private static int sigmalogoCol = 1;
	private static int minalogoRow = 0;
	private static int minalogoCol = 4;
	private static int coloffset=1;
	private static int rowoffset=1;
	
	public static void writeCsvFile(String fileName, ArrayList<String[]> Reportlist , String header, String Title) {
			
		try {
			fileWriter = new FileWriter(fileName);
			
			fileWriter.append(Title); 
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(header); 
			fileWriter.append(NEW_LINE_SEPARATOR);
			 
			for (String[] subjectData : Reportlist) { 
				for(String thisString: subjectData){
					fileWriter.append(thisString);
					fileWriter.append(COMMA_DELIMITER);
				} 
				fileWriter.append(NEW_LINE_SEPARATOR);
			} 
			System.out.println("CSV file was created successfully !!!");
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
			}
			
		}
	}
	
	public static void writeCsvFile(String fileName, ArrayList<String[][]> Reportlist, int index , String header, String Title) {
		
		try {
			fileWriter = new FileWriter(fileName);
			
			fileWriter.append(Title); 
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(header); 
			fileWriter.append(NEW_LINE_SEPARATOR);
			 
			for (String[][] subjectData : Reportlist) { 
				
				if(SysUtils.returnInt(subjectData[index][0])==1) {
					subjectData[index][0]="";
					String[] localData = subjectData[index];
					for(String thisString: localData){
						fileWriter.append(thisString);
						fileWriter.append(COMMA_DELIMITER);
					} 
					fileWriter.append(NEW_LINE_SEPARATOR);
				}
			} 
			System.out.println("CSV file "+fileName+" was created successfully !!!");
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
			}
			
		}
	}
	
public static void writeExcelFile(String fileName, ArrayList<String[][]> Reportlist, int index , String[] header, String Title) {
		
		try { 
			 Workbook workbook= new HSSFWorkbook();
			 Sheet sheet = workbook.createSheet("test");
			 
			 Font logoFont = workbook.createFont();
			 logoFont.setBold(true);
			 logoFont.setFontHeightInPoints((short)30);
			 logoFont.setColor(IndexedColors.INDIGO.getIndex());
			 
			 CellStyle logosCellStyle = workbook.createCellStyle();
			 logosCellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
			 logosCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			 logosCellStyle.setFont(logoFont);
			 
			 Font titleFont = workbook.createFont();
			 titleFont.setBold(true);
			 titleFont.setFontHeightInPoints((short)8);
			 titleFont.setColor(IndexedColors.INDIGO.getIndex());
			 
			 CellStyle titleCellStyle = workbook.createCellStyle();
			 titleCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			 titleCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			 titleCellStyle.setFont(titleFont);
			 
			 
			 
			 Font headerFont = workbook.createFont();
			 headerFont.setBold(true);
			 headerFont.setFontHeightInPoints((short)17);
			 headerFont.setColor(IndexedColors.WHITE.getIndex());
			 CellStyle headerCellStyle = workbook.createCellStyle();
			 headerCellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
			 headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			 headerCellStyle.setFont(headerFont); 
			 
			 Row headerRow = sheet.createRow(0+rowoffset);
			 headerRow.setHeightInPoints(50);
			 String[] titleCells = SysUtils.splitString(Title, COMMA_DELIMITER);
			 for (int i=0; i< titleCells.length; i++) {  
					Cell cell = headerRow.createCell(i+1);   
					cell.setCellStyle(logosCellStyle);
			 } 
			 
			 	 
			 
			 
			 headerRow = sheet.createRow(1+rowoffset); 
			 for (int i=0; i< titleCells.length; i++) {  
					Cell cell = headerRow.createCell(i+1);  
					cell.setCellValue(titleCells[i]);
					cell.setCellStyle(titleCellStyle);
			} 
			 
			 headerRow = sheet.createRow(2+rowoffset);
			 
			for (int i=1; i< header.length; i++) {  
					Cell cell = headerRow.createCell(i);  
					cell.setCellValue(header[i]);
					cell.setCellStyle(headerCellStyle);
			} 
			
			int rowNum=3+rowoffset;
			
			for (String[][] subjectData : Reportlist) { 
				
				if(SysUtils.returnInt(subjectData[index][0])==1) {
					subjectData[index][0]=""; 
					int cellnum=0;
					String[] localData = subjectData[index];
					Row row = sheet.createRow(rowNum++);
					for(String thisString: localData){ 
						row.createCell(cellnum++).setCellValue(thisString);	 
					}  
				}
			} 
			
			for (int i=0; i< header.length; i++) {  
				sheet.autoSizeColumn(i);
			} 
			
			sheet.addMergedRegion(new CellRangeAddress(0+rowoffset,0+rowoffset,1,4));
			
			InputStream is ;
				try {
					is = new FileInputStream(sigmalogofull);
				} catch (Exception e) {
					is = new FileInputStream("/home/sigmadev/Documents/sigmalogo.png");
				}
			 byte [] bytes = IOUtils.toByteArray(is); 
			 int pictureIndex = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
			 is.close();

			 CreationHelper helper = workbook.getCreationHelper();
			 Drawing drawingPatriarch = sheet.createDrawingPatriarch();
			 ClientAnchor anchor = helper.createClientAnchor();
			 
			 
			 anchor.setCol1(sigmalogoCol);
			 anchor.setCol2(sigmalogoCol+1);
			 anchor.setRow1(sigmalogoRow+rowoffset);
			 anchor.setRow2(sigmalogoRow+rowoffset);
			 anchor.setDx1(150000);
			 anchor.setDy1(125000);  
			 Picture pict = drawingPatriarch.createPicture(anchor, pictureIndex); 
			 pict.resize(); 
			 			 
			 
			 try {
				 is = new FileInputStream(minalogofull);
				} catch (Exception e) {
					is = new FileInputStream("/home/sigmadev/Documents/minalogo.png");
				}
			 bytes = IOUtils.toByteArray(is); 
			 int pictureIndexMina = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
			 is.close();

			 CreationHelper helpermina = workbook.getCreationHelper();
			 Drawing drawingPatriarchmina = sheet.createDrawingPatriarch();
			 ClientAnchor anchormina = helpermina.createClientAnchor(); 
			 anchormina.setCol1(minalogoCol);
			 anchormina.setCol2(minalogoCol+1);
			 anchormina.setRow1(minalogoRow+rowoffset);
			 anchormina.setRow2(minalogoRow+rowoffset);
			 anchormina.setDx1(1600000);
			 anchormina.setDy1(50000);   
			 Picture pictmina = drawingPatriarchmina.createPicture(anchormina, pictureIndexMina); 
			 pictmina.resize(); 
			
			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
			System.out.println("Excel file "+fileName+" was created successfully !!!");
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			
			try { 
				
			} catch (Exception e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
			}
			
		}
	}
}