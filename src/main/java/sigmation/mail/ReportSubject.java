package sigmation.mail;

import java.text.SimpleDateFormat; 
import java.time.LocalDateTime; 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 

import sigmation.utils.SysUtils;

public class ReportSubject { 
	private static String filepath = "/var/www/html/generated/";
	private static String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    private static String Title = ""+date+" ";
    private static final String COMMA_DELIMETER= CsvFileWriter.COMMA_DELIMITER;
	
	public static class PersonFlags{
		static final String FILE_HEADER = "Nombre,IDN,Area,Empresa,Tiempo,Ultima Zona,estado";
        private static String filename="personalReport.csv";
        private static String fullTitle= Title+"Personal Con Documentos Vencidos o exceso de horas: Hoy";
		private String name;
		private String idn;
		private String area;
		private String empresa;
		private String tiempohoy;
		private String uzona;
		private String estado; 
		private String overhours;   
		private static boolean debugThis=false;
		
		public PersonFlags(String name, String idn, String area,String empresa,String tiempohoy,String uzona, String estado, String overhours ) {
			super(); 
			this.name = name;
			this.idn = idn;
			this.area = area;
			this.empresa= empresa;
			this.tiempohoy=tiempohoy;
			this.uzona=uzona;
			this.estado=estado; 
			this.overhours=overhours; 
		}
		
		public static String getTitle(){
			return fullTitle;
		}
		
		public static String getFullPath(){
			return filepath+filename;
		}
		
		public static String getFileName(){
			return filename;
		}
		
		
		public String getEstado( ) {
			int intestado = returnInt(estado);
			int intovrhrs = returnInt(overhours);
			String resume = "Normalizado";
			if(intestado>0 && intovrhrs>0){
				resume=estado+" Doc. vencidos y exceso de horas trabajadas";
			} else if(intestado>0){
				resume=estado+" Doc. vencidos";
			} else if(intovrhrs>0){
				resume="exceso de horas trabajadas";
			}  
			return resume;
		}
		
		public static String getHeader() { 
			return FILE_HEADER;
		}
		
		public String[] toStringArray() {
			String[] reportSubjectData={name,idn,area,empresa,tiempohoy,uzona,getEstado()};
			return reportSubjectData;
		}
		
		public static ArrayList<String[]> resolve(){
			List<ReportSubject.PersonFlags> objectlist = new ArrayList<ReportSubject.PersonFlags>();
			ArrayList<String[]> Report = Reporter.PersonasFlags.getPersonasFlags();
			for(int i=0; i< Report.size();i++){
				String[] subjectArray= Report.get(i);
				if(i==0){
					for(int j=0;j<subjectArray.length;j++){
						if(debugThis) System.out.println("> "+j+" "+subjectArray[j]);
					}
				}
				ReportSubject.PersonFlags subject = new ReportSubject.PersonFlags(
						subjectArray[Reporter.PersonasFlags.nombre1]+" "+subjectArray[Reporter.PersonasFlags.apellido], 
						subjectArray[Reporter.PersonasFlags.idn], 
						subjectArray[Reporter.PersonasFlags.area], 
						subjectArray[Reporter.PersonasFlags.empresa], 
						subjectArray[Reporter.PersonasFlags.time_total],
						subjectArray[Reporter.PersonasFlags.ultimazona], 
						subjectArray[Reporter.PersonasFlags.venflag], 
						subjectArray[Reporter.PersonasFlags.over_hours]); 
				objectlist.add(subject); 
			}
			
			ArrayList<String[]> Reportlist = new ArrayList<String[]>();
			 
			for (ReportSubject.PersonFlags subject : objectlist) {
				String[] subjectData = subject.toStringArray(); 
				Reportlist.add(subjectData);  
			} 
			String[] subjectData = {"Se encontraron "+Reportlist.size()+" Registros"}; 
			Reportlist.add(subjectData);  
			return Reportlist;
		}
	}
		
	
	public static class PersonReport{
		private static final String CD = COMMA_DELIMETER;
		static final String[][] FILE_HEADERS = { 
													{"","Nombre","IDN","Area","Fecha y Hora de Ingreso"},
				  									{"","Nombre","IDN","Area","Fecha y Hora de Salida"},
				  									{"","Nombre","IDN","Area","Ultima Zona"},
				  									{"","Serie Lector","Cantidad","Tiempo","Placa"}
				  };
		static final String[] FILE_HEADER = {  	CD+FILE_HEADERS[0][0]+CD+FILE_HEADERS[0][1]+CD+FILE_HEADERS[0][2]+CD+FILE_HEADERS[0][3],
													CD+FILE_HEADERS[1][0]+CD+FILE_HEADERS[1][1]+CD+FILE_HEADERS[1][2]+CD+FILE_HEADERS[1][3],
													CD+FILE_HEADERS[2][0]+CD+FILE_HEADERS[2][1]+CD+FILE_HEADERS[2][2]+CD+FILE_HEADERS[2][3],
													CD+FILE_HEADERS[3][0]+CD+FILE_HEADERS[3][1]+CD+FILE_HEADERS[3][2]+CD+FILE_HEADERS[3][3]};
		
        private static String[] filename= {"ReporteIngreso.csv","ReporteSalida.csv","ReportePermanencia.csv","ReporteLogBus.csv"}; 
        private static String[] filenameExcel= {"ReporteIngreso.xlsx","ReporteSalida.xlsx","ReportePermanencia.xlsx","ReporteLogBus.xlsx"};
        private static String fullTitle= Title+" Turno: ";
		private String name;
		private String idn;
		private String area;
		private String uingreso; 
		private String usalida; 
		private int enmina;
		private String uzona; 		
		private static boolean debugThis=false;
		
		public PersonReport(String name, String idn, String area, String uingreso,String usalida,String enmina,String uzona, String turno, int turnoint ) {
			super(); 
			this.name = name;
			this.idn = idn;
			this.area = area;
			this.uingreso = uingreso; 
			this.usalida=usalida; 			
			this.enmina=SysUtils.returnInt(enmina);
			this.uzona=uzona; 
		}
		
		public static String getTitle(){
			return PersonReport.fullTitle;
		}
		
		public static String[] getFullPath(){
			String[] localbuf = {filepath+filename[0],filepath+filename[1],filepath+filename[2]};
			return localbuf;
		}
		
		public static String[] getFullPathExcel(){
			String[] localbuf = {filepath+filenameExcel[0],filepath+filenameExcel[1],filepath+filenameExcel[2]};
			return localbuf;
		}
		
		public static String[] getFileName(){
			return filename;
		} 
		
		public static String[] getFileNameExcel(){
			return filenameExcel;
		}
		
		public static String[] getHeader() { 
			return FILE_HEADER;
		}
		
		public static String[][] getHeaders() { 
			return FILE_HEADERS;
		}
		
		public String[][] toStringArray() {
			String[][] reportSubjectData={{"1",name,idn,area,uingreso},{display_1(),name,idn,area,usalida_()},{display_2(),name_(),idn_(),area_(),uzona_()}};
			return reportSubjectData;
		}
		
		public String name_() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return name;}
			else return "";
		}
		
		public String idn_() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return idn;}
			else return "";
		}
		
		public String area_() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return area;}
			else return "";
		}
		
		public String uzona_() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return uzona;}
			else return "";
		}
		
		public String usalida_() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return "--";}
			else return usalida;
		}
		
		public String display_1() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return "0";}
			else return "1";
		}
		
		public String display_2() {
			if(enmina==1 || usalida.equalsIgnoreCase("--")) {return "1";}
			else return "0";
		}
		
		public static ArrayList<String[][]> resolve(int turno, String startDate, String finishDate){
			String sturno = startDate +" - "+finishDate; 
			List<ReportSubject.PersonReport> objectlist = new ArrayList<ReportSubject.PersonReport>(); 
			String thisDate = finishDate;
			String reportDate = finishDate;
			System.out.println("checkpoint "+thisDate);
			PersonReport.fullTitle = "Turno:"+" "+ CD + sturno +CD+ " generado el  "+CD+reportDate+"GMT";
			ArrayList<String[]> Report = Reporter.PersonasReport.getPersonasReport(turno,startDate); 
			
			ArrayList<String[][]> Reportlist = new ArrayList<String[][]>();
			try {
				for(int i=0; i< Report.size();i++){
					String[] subjectArray= Report.get(i);
					if(i==0){
						for(int j=0;j<subjectArray.length;j++){
							if(debugThis) System.out.println("> "+j+" "+subjectArray[j]);
						}
					}
					ReportSubject.PersonReport subject = new ReportSubject.PersonReport(
							subjectArray[Reporter.PersonasReport.nombre1]+" "+subjectArray[Reporter.PersonasReport.apellido], 
							subjectArray[Reporter.PersonasReport.idn], 
							subjectArray[Reporter.PersonasReport.area], 
							subjectArray[Reporter.PersonasReport.ultimoingreso], 
							subjectArray[Reporter.PersonasReport.ultimasalida], 
							subjectArray[Reporter.PersonasReport.enmina],  
							subjectArray[Reporter.PersonasReport.nombrezona], 
							sturno, 
							turno); 
					objectlist.add(subject); 
				} 
				
				
				 
				for (ReportSubject.PersonReport subject : objectlist) {
					String[][] subjectData = subject.toStringArray(); 
					Reportlist.add(subjectData);  
				} 
			} catch (Exception e) {
				e.printStackTrace();
			}
			String[][] subjectData = {{"Se encontraron "+Reportlist.size()+" Registros"},{" "},{" "}}; 
			System.out.println("Se encontraron "+Reportlist.size()+" Registros");
			Reportlist.add(subjectData);  
			return Reportlist;
		}
		
		public static ArrayList<String[][]> resolve(int turno, LocalDateTime startDate, LocalDateTime finishDate){
			String sturno = startDate.getHour()+":"+startDate.getMinute()+" - "+finishDate.getHour()+":"+finishDate.getMinute();//" 7:30-19:00"; 
			List<ReportSubject.PersonReport> objectlist = new ArrayList<ReportSubject.PersonReport>(); 
			String thisDate = startDate.getDayOfMonth()+"-"+startDate.getMonth()+"-"+startDate.getYear()+" ";
			String reportDate = SysUtils.getTodayDateFull();
			System.out.println("checkpoint "+thisDate);
			PersonReport.fullTitle = "Turno:"+thisDate+ CD + sturno +CD+ " generado el  "+CD+reportDate+"GMT";
			ArrayList<String[]> Report = Reporter.PersonasReport.getPersonasReport(turno,startDate);
			ArrayList<String[][]> Reportlist = new ArrayList<String[][]>();
			try {
				for(int i=0; i< Report.size();i++){
					String[] subjectArray= Report.get(i);
					if(i==0){
						for(int j=0;j<subjectArray.length;j++){
							if(debugThis) System.out.println("> "+j+" "+subjectArray[j]);
						}
					}
					ReportSubject.PersonReport subject = new ReportSubject.PersonReport(
							subjectArray[Reporter.PersonasReport.nombre1]+" "+subjectArray[Reporter.PersonasReport.apellido], 
							subjectArray[Reporter.PersonasReport.idn], 
							subjectArray[Reporter.PersonasReport.area], 
							subjectArray[Reporter.PersonasReport.ultimoingreso], 
							subjectArray[Reporter.PersonasReport.ultimasalida], 
							subjectArray[Reporter.PersonasReport.enmina],  
							subjectArray[Reporter.PersonasReport.nombrezona], 
							sturno, 
							turno); 
					objectlist.add(subject); 
				}
				
				
				 
				for (ReportSubject.PersonReport subject : objectlist) {
					String[][] subjectData = subject.toStringArray(); 
					Reportlist.add(subjectData);  
				} 
			} catch (Exception e) {
				e.printStackTrace();
			}
			String[][] subjectData = {{"Se encontraron "+Reportlist.size()+" Registros"},{" "},{" "}}; 
			System.out.println("Se encontraron "+Reportlist.size()+" Registros");
			Reportlist.add(subjectData);  
			return Reportlist;
		}
	}

	public static class BusReport{
		private static final String CD = COMMA_DELIMETER;
		static final String[][] FILE_HEADERS = {  
				  									{"","Serie Lector","Cantidad","Tiempo","Placa"}
				  };
		static final String[] FILE_HEADER = {  	CD+FILE_HEADERS[0][0]+CD+FILE_HEADERS[0][1]+CD+FILE_HEADERS[0][2]+CD+FILE_HEADERS[0][3] };
		
        private static String[] filename= {"ReporteLogBus.csv"}; 
        private static String[] filenameExcel= {"ReporteLogBus.xlsx"};
        private static String fullTitle= Title+" Turno: ";
		private String serie;
		private String cantidad;
		private String tiempo;
		private String placa;  		
		private static boolean debugThis=false;
		
		public BusReport(String serie, String cantidad, String tiempo, String placa) {
			super(); 
			this.serie = serie;
			this.cantidad = cantidad;
			this.tiempo = tiempo;
			this.placa = placa;  
		}
		
		public static String getTitle(){
			return BusReport.fullTitle;
		}
		
		public static String[] getFullPath(){
			String[] localbuf = {filepath+filename[0]};
			return localbuf;
		}
		
		public static String[] getFullPathExcel(){
			String[] localbuf = {filepath+filenameExcel[0]};
			return localbuf;
		}
		
		public static String[] getFileName(){
			return filename;
		} 
		
		public static String[] getFileNameExcel(){
			return filenameExcel;
		}
		
		public static String[] getHeader() { 
			return FILE_HEADER;
		}
		
		public static String[][] getHeaders() { 
			return FILE_HEADERS;
		}
		
		public String[][] toStringArray() {
			String[][] reportSubjectData={{"",serie,cantidad,tiempo,placa}};
			return reportSubjectData;
		} 
		
		public static ArrayList<String[][]> resolve(int turno, String startDate, String finishDate){
			String sturno = startDate +" - "+finishDate; 
			List<ReportSubject.BusReport> objectlist = new ArrayList<ReportSubject.BusReport>(); 
			String thisDate = finishDate;
			String reportDate = finishDate;
			System.out.println("checkpoint "+thisDate);
			PersonReport.fullTitle = "Turno:"+" "+ CD + sturno +CD+ " generado el  "+CD+reportDate+"GMT"; 
			ArrayList<String[]> Report = Reporter.busLogReport.getBusList(turno,startDate);
			
			ArrayList<String[][]> Reportlist = new ArrayList<String[][]>();
			try {
				for(int i=0; i< Report.size();i++){
					String[] subjectArray= Report.get(i);
					if(i==0){
						for(int j=0;j<subjectArray.length;j++){
							if(debugThis) System.out.println("> "+j+" "+subjectArray[j]);
						}
					}
					ReportSubject.BusReport subject = new ReportSubject.BusReport(
							subjectArray[Reporter.busLogReport.serielector], 
							subjectArray[Reporter.busLogReport.cantidad], 
							subjectArray[Reporter.busLogReport.tiempo], 
							subjectArray[Reporter.busLogReport.placa]); 
					objectlist.add(subject); 
				} 				
				
				 
				for (ReportSubject.BusReport subject : objectlist) {
					String[][] subjectData = subject.toStringArray(); 
					Reportlist.add(subjectData);  
				} 
			} catch (Exception e) {
				e.printStackTrace();
			}
			String[][] subjectData = {{"Se encontraron "+Reportlist.size()+" Registros"},{" "},{" "}}; 
			System.out.println("Se encontraron "+Reportlist.size()+" Registros");
			Reportlist.add(subjectData);  
			return Reportlist;
		} 
	}
		
	
		public static class tagAlerts{
			private static String filename="tagReport.csv";
			static final String FILE_HEADER = "Hora-Fecha,Tag Serial,Tipo Evento,Zona";
			private static String fullTitle= Title+" Alertas Tag de hoy";
			private String hora;
			private String tag; 
			private String tipo;
			private String zona;
			private String lector;   
			 
			public tagAlerts(String hora ,String tag,String tipo,String zona,String lector) {
				super(); 
				this.hora = hora;
				this.tag = tag; 
				this.tipo= tipo;
				this.zona=zona;
				this.lector=" "; 
			}
			
			public static String getTitle(){
				return fullTitle;
			}
			 
			public static String getFullPath(){
				return filepath+filename;
			}
			
			public static String getFileName(){
				return filename;
			}
			
			public static String getHeader() { 
				return FILE_HEADER;
			} 
			
			public static ArrayList<String[]> resolve(){
				List<ReportSubject.tagAlerts> objectlist = new ArrayList<ReportSubject.tagAlerts>();
				ArrayList<String[]> Report = Reporter.tagFlags.getTagFlags();
				for(int i=0; i< Report.size();i++){
					String[] subjectArray= Report.get(i);
					if(i==0){
						for(int j=0;j<subjectArray.length;j++){
							if(PersonFlags.debugThis)  System.out.println("> "+j+" "+subjectArray[j]);
						}
					}
					ReportSubject.tagAlerts subject = new ReportSubject.tagAlerts(
							subjectArray[Reporter.tagFlags.tiempo], 
							subjectArray[Reporter.tagFlags.tagserial], 
							subjectArray[Reporter.tagFlags.nombrevento], 
							subjectArray[Reporter.tagFlags.nombrezona], 
							subjectArray[Reporter.tagFlags.serial]) ;
							objectlist.add(subject); 
				}
				
				ArrayList<String[]> Reportlist = new ArrayList<String[]>();
				 
				for (ReportSubject.tagAlerts subject : objectlist) {
					String[] subjectData = subject.toStringArray(); 
					Reportlist.add(subjectData);  
				} 
				String[] subjectData = {"Se encontraron "+Reportlist.size()+" Registros"}; 
				Reportlist.add(subjectData);  
				return Reportlist;
			}
		
		
		public String[] toStringArray() {
			String[] reportSubjectData={hora,tag,tipo,zona,lector};
			return reportSubjectData;
		}
	}
	
	
		public static class vehicleFlags{
			static final String FILE_HEADER = "Marca-Modelo,color,Patente,Tipo,Empresa,Ultima Zona,estado";
	        private static String filename="vehicleReport.csv";
	        private static String fullTitle= Title+"Vehiculos Con Documentos Vencidos: Hoy";
			private String name;
			private String color;
			private String idn;
			private String area;
			private String empresa; 
			private String uzona;
			private String estado;  
			
			public vehicleFlags(String name, String color, String idn, String area,String empresa ,String uzona, String estado ) {
				super(); 
				this.name = name;
				this.color=color;
				this.idn = idn;
				this.area = area;
				this.empresa= empresa;
				this.uzona=uzona;
				this.estado=estado; 
			}
			
			public static String getTitle(){
				return fullTitle;
			}
			
			public static String getFullPath(){
				return filepath+filename;
			}
			
			public static String getFileName(){
				return filename;
			}
			
			
			public String getEstado( ) {
				int intestado = returnInt(estado); 
				String resume = "Normalizado";
				if(intestado>0){
					resume=estado+" Doc. vencidos";
				} 
				return resume;
			}
			
			public static String getHeader() { 
				return FILE_HEADER;
			}
			
			public String[] toStringArray() {
				String[] reportSubjectData={name,color,idn,area,empresa,uzona,getEstado()};
				return reportSubjectData;
			}
			
			public static ArrayList<String[]> resolve(){
				List<ReportSubject.vehicleFlags> objectlist = new ArrayList<ReportSubject.vehicleFlags>();
				ArrayList<String[]> Report = Reporter.VehicleFlags.getVehicleFlags();
				for(int i=0; i< Report.size();i++){
					String[] subjectArray= Report.get(i);
					if(i==0){
						for(int j=0;j<subjectArray.length;j++){
							if(PersonFlags.debugThis)  System.out.println("> "+j+" "+subjectArray[j]);
						}
					}
					vehicleFlags subject = new ReportSubject.vehicleFlags(
							subjectArray[Reporter.VehicleFlags.nombre1]+" "+subjectArray[Reporter.VehicleFlags.nombre2], 
							subjectArray[Reporter.VehicleFlags.apellido], 
							subjectArray[Reporter.VehicleFlags.idn], 
							subjectArray[Reporter.VehicleFlags.area], 
							subjectArray[Reporter.VehicleFlags.empresa], 
							subjectArray[Reporter.VehicleFlags.ultimazona], 
							subjectArray[Reporter.VehicleFlags.venflag]); 
					objectlist.add(subject); 
				}
				
				ArrayList<String[]> Reportlist = new ArrayList<String[]>();
				 
				for (vehicleFlags subject : objectlist) {
					String[] subjectData = subject.toStringArray(); 
					Reportlist.add(subjectData);  
				} 
				String[] subjectData = {"Se encontraron "+Reportlist.size()+" Registros"}; 
				Reportlist.add(subjectData);  
				return Reportlist;
			}
		}
	
	
	
	
	
	public static int returnInt(String evaluateMe){
		return SysUtils.returnInt(evaluateMe);
	}

}
