package sigmation.mail;
 
  
import java.time.LocalDate;
import java.time.LocalDateTime; 
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;

public class SendEmail {
	

	static Multipart multipart = new MimeMultipart();
	static Multipart rMultipart = new MimeMultipart();
	static ArrayList<String[]> options = new ArrayList<String[]>();
	
	static ScheduledExecutorService scheduler;
	static ScheduledExecutorService report_scheduler;
	static ScheduledExecutorService backup_scheduler;
	static ScheduledExecutorService report1_scheduler;
	static ScheduledExecutorService report2_scheduler;
	static ScheduledExecutorService confscheduler;
	
	static LocalDateTime reportDate;
	static LocalDateTime backupDate;
	static LocalDateTime reportStartDate;
	static String reportDateString;
	static String backupDateString;
	static String reportStartDateString;
	static String reportTurn= "0";
	static long reportDelayMinutes = 2;
	
	static String smtp= "smtp.gmail.com";
	static String mailport= "465";
	static String mailAuth= "true";
	static String mailOriginAddress= "felipe@sigma-telecom.com";
	static String mailToAddress= "felipe@sigma-telecom.com";
	static String mailPassword="sigmati0n";
	static String reportTitle= "Sigmation Daily Report";
	static String reportComment= "Automated content from SIGMATION SERVER - Tagging&Tracking App";
	static String hour= "00:00";
	static String generatedString= "Programmed Service";
	
	static int    pHour = 0;
	static int 	  pMinute=0;  
	static Message message;
	static Message rmessage;
	static boolean sentMail=false;
	static boolean sentMailReport=false;
	static boolean sentMailReport1=false;
	static boolean sentMailReport2=false;
	static boolean debugThis=true;
	
 
	
	private static void generateAttachements(){
		try {
			options = getOptions();
			for (String[] object : options) {
				String subobject = object[0]; 
				int subobjectInt = SysUtils.returnInt(subobject);
				
				String fullpath ="";
				ArrayList<String[]> resolve = new ArrayList<String[]>();
				String header ="";
				String title ="";
				String filename =""; 
				
				switch(subobjectInt){
				
					case 0:		fullpath = ReportSubject.PersonFlags.getFullPath();
								resolve=ReportSubject.PersonFlags.resolve();
								header=ReportSubject.PersonFlags.getHeader();
								title=ReportSubject.PersonFlags.getTitle();
								filename=ReportSubject.PersonFlags.getFileName();
								addAttachment(fullpath, resolve, header, title, filename);
								if(debugThis)System.out.println("attached personf.");
								break;
					case 1:		fullpath = ReportSubject.tagAlerts.getFullPath();
								resolve=ReportSubject.tagAlerts.resolve();
								header=ReportSubject.tagAlerts.getHeader();
								title=ReportSubject.tagAlerts.getTitle();
								filename=ReportSubject.tagAlerts.getFileName();
								addAttachment(fullpath, resolve, header, title, filename);
								if(debugThis) System.out.println("attached tagal.");
								break;	
					case 2:		fullpath = ReportSubject.vehicleFlags.getFullPath();
								resolve=ReportSubject.vehicleFlags.resolve();
								header=ReportSubject.vehicleFlags.getHeader();
								title=ReportSubject.vehicleFlags.getTitle();
								filename=ReportSubject.vehicleFlags.getFileName();
								addAttachment(fullpath, resolve, header, title, filename);
								if(debugThis) System.out.println("attached vehf.");
								break;	
					default:	break;
				}
				
		         
			} 
		} catch (Exception e) { 
		}
	}
	
	private static void generateReportAttachements(int turno){
		try {
			options = getOptions();   
				resetMultipart();
				String[] fullpath ={"","",""};
				ArrayList<String[][]> resolve = new ArrayList<String[][]>();
				String[][] headers = {{""},{""},{""}};
				String title ="";
				String[] filename ={"","",""}; 
				
				fullpath = ReportSubject.PersonReport.getFullPathExcel();
				resolve=ReportSubject.PersonReport.resolve(turno,reportStartDate, reportDate);
				headers=ReportSubject.PersonReport.getHeaders();
				title=ReportSubject.PersonReport.getTitle();
				filename=ReportSubject.PersonReport.getFileNameExcel();
				for(int i=0;i<3;i++){
					addAttachment(fullpath[i], resolve, i , headers[i], title, filename[i],true);
					if(debugThis)System.out.println("attached "+filename[i]);
				}				
				
				fullpath = ReportSubject.BusReport.getFullPathExcel();
				resolve=ReportSubject.BusReport.resolve(turno,reportStartDateString, reportDateString);
				headers=ReportSubject.BusReport.getHeaders();
				title=ReportSubject.BusReport.getTitle();
				filename=ReportSubject.BusReport.getFileNameExcel();
				for(int i=0;i<filename.length;i++){
					addAttachment(fullpath[i], resolve, i , headers[i], title, filename[i],true);
					if(debugThis)System.out.println("attached "+filename[i]);
				}	
				 
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
	private static void generateReportAttachements(int turno, String reportStartDateString, String reportDateString){
		try {
			options = getOptions();   
				resetMultipart();
				String[] fullpath ={"","",""};
				ArrayList<String[][]> resolve = new ArrayList<String[][]>();
				String[][] headers = {{""},{""},{""}};
				String title ="";
				String[] filename ={"","",""}; 
				
				fullpath = ReportSubject.PersonReport.getFullPathExcel();
				resolve=ReportSubject.PersonReport.resolve(turno,reportStartDateString, reportDateString);
				headers=ReportSubject.PersonReport.getHeaders();
				title=ReportSubject.PersonReport.getTitle();
				filename=ReportSubject.PersonReport.getFileNameExcel();
				for(int i=0;i<3;i++){
					addAttachment(fullpath[i], resolve, i , headers[i], title, filename[i],true);
					if(debugThis)System.out.println("attached "+filename[i]);
				}
				
				fullpath = ReportSubject.BusReport.getFullPathExcel();
				resolve=ReportSubject.BusReport.resolve(turno,reportStartDateString, reportDateString);
				headers=ReportSubject.BusReport.getHeaders();
				title=ReportSubject.BusReport.getTitle();
				filename=ReportSubject.BusReport.getFileNameExcel();
				for(int i=0;i<filename.length;i++){
					addAttachment(fullpath[i], resolve, i , headers[i], title, filename[i],true);
					if(debugThis)System.out.println("attached "+filename[i]);
				}	
				
				 
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
	private static void resetMultipart() {
		//multipart = new MimeMultipart();
	} //rMultipart
	
	private static void addAttachment(String fullpath, ArrayList<String[]> resolve, String header,String title,String filename) throws MessagingException{
		addAttachment(fullpath, resolve, header, title, filename, false);
	}
	
	private static void addAttachment(String fullpath, ArrayList<String[]> resolve, String header,String title,String filename, boolean useOtherMultipart) throws MessagingException{
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(fullpath);
        CsvFileWriter.writeCsvFile(fullpath,resolve,header, title);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);
        if (useOtherMultipart) rMultipart.addBodyPart(messageBodyPart); 
        else multipart.addBodyPart(messageBodyPart); 
	}
	
 	
	private static void addAttachment(String fullpath, ArrayList<String[][]> resolve, int index, String[] header,String title,String filename, boolean useOtherMultipart) throws MessagingException{
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(fullpath);
        CsvFileWriter.writeExcelFile(fullpath,resolve,index,header, title);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);
        if (useOtherMultipart) rMultipart.addBodyPart(messageBodyPart); 
        else multipart.addBodyPart(messageBodyPart); 
	}
	
	public static void init() { 
		    	try {
		    		getMailVars();
		    		getNextReport("startup");
		    		generateReportAttachements(1);
		    		scheduleConfigCheck();
			    	String Reason="App Startup";  
				} catch (Exception e) {
					e.printStackTrace();
				} 
	} 
	
	//Transport.send(message);
	
	private static void setReportTime(){ 
		setDailyMailSchedule(hour);		
	} 
	
	private static void reportScheduler(){ 
		setMailSchedule(reportDateString, 1, reportStartDateString, reportDateString, SysUtils.returnInt(reportTurn));
	}
	  
	
	private static void scheduleConfigCheck(){
		confscheduler = Executors.newScheduledThreadPool(1);      
		Long perseconds=LocalDateTime.now().until(LocalDateTime.now(), ChronoUnit.MINUTES);
		confscheduler.scheduleAtFixedRate(configCheck, perseconds, 60, TimeUnit.SECONDS); 
	}
	
	final static Runnable configCheck = new Runnable() {
	       public void run() { checkConfig(); }
	}; 
	
	private static void checkConfig(){ 
		if(SysUtils.getconfigRefresh("tntmail","refreshconfig")){ 
			getMailVars();
			getNextReport("tntmail refresh");
			checkSchedule();
		}
		if(SysUtils.getconfigRefresh("tntturnrp","refreshconfig")){ 
			getMailVars();
			getNextReport("turn system update");
			checkSchedule();
		} 	
		getNextReport();
		checkSchedule();
	}
	
	private static void checkSchedule() {
		ArrayList<String[]> scheduleList = getScheduleMail();
		if(scheduleList.isEmpty()) {
			//if(debugThis) System.out.println("No tasks available");
			return;
		}
		try {
			if(debugThis) System.out.println("Executing "+scheduleList.size()+" Tasks");
			for(String[] listElement : scheduleList){		
				executeSchedule(listElement);
				if(debugThis) System.out.println("Executed a task");
			}
		} catch (Exception e) {
			if(debugThis) System.out.println("Failed to follow schedule");
		}	
	}
	
	private static void executeSchedule(String[] listElement) {
		String id_ = listElement[0];
		String tiempo_ = listElement[1];
		String tipo_mail_ =listElement[2];
		int tipo_mail_int = SysUtils.returnInt(tipo_mail_);
		String estado_ =listElement[3];
		int estado_int = SysUtils.returnInt(estado_);
		String comienzo_ =listElement[4];
		String fin_ =listElement[5];
		String param_ =listElement[6];
		int param_int = SysUtils.returnInt(param_);
		
		if(tipo_mail_int ==1) {
			backupAndRestart(fin_);
			doSendReportMail(generatedString,param_int, comienzo_, fin_);
		} else if (tipo_mail_int == 2) {
			doSendMail(generatedString);
		}
	}
	  
	public static void doSendMail(String whoGeneratedMe){ 
		Properties props = new Properties(); 
		BodyPart messageBodyPart = new MimeBodyPart(); 
		Session session; 
		props.put("mail.smtp.host", smtp);
		props.put("mail.smtp.socketFactory.port", mailport);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", mailAuth);
		props.put("mail.smtp.port", mailport);

		session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailOriginAddress,mailPassword);
				}
			});

		try {
			messageBodyPart = new MimeBodyPart();
			multipart = new MimeMultipart();
		    message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@sigma-telecom.com"));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(mailToAddress));
			// Set Subject: header field
	         message.setSubject(reportTitle);  

	         // Fill the message
	         messageBodyPart.setText(reportComment+" | Generated by:"+whoGeneratedMe+" | @ "+ SysUtils.getTodayDateFull()); 

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	         // Part two is attachment  
	         generateAttachements();

	         // Send the complete message parts
	         message.setContent(multipart );

	         // Send message
	         Transport.send(message);
	         if(debugThis) System.out.println("Sent message successfully....");
	         sentMail=true;
	         

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void doSendReportMail(String whoGeneratedMe, int turno, String startDateString, String finishDateString){ 
		Properties props = new Properties(); 
		 
		props.put("mail.smtp.host", smtp);
		props.put("mail.smtp.socketFactory.port", mailport);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", mailAuth);
		props.put("mail.smtp.port", mailport);
		 
		BodyPart rmessageBodyPart = new MimeBodyPart(); 
		
		Session session;
		session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailOriginAddress,mailPassword);
				}
			});

		try {
			rmessageBodyPart = new MimeBodyPart();
			rMultipart = new MimeMultipart(); 
			multipart = new MimeMultipart();
			rmessage = new MimeMessage(session);
			rmessage.setFrom(new InternetAddress("noreply@sigma-telecom.com"));
			rmessage.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(mailToAddress));
			// Set Subject: header field
			rmessage.setSubject("Sigmation: Reporte por turno");  

	         // Fill the message
			rmessageBodyPart.setText(reportComment+" | Generated by:"+whoGeneratedMe+" | @ Turno "+turno+" - "+ reportDate); 

	         // Set text message part
	         rMultipart.addBodyPart(rmessageBodyPart);

	         // Part two is attachment  
	         generateReportAttachements(turno,startDateString,finishDateString);

	         // Send the complete message parts
	         rmessage.setContent(rMultipart);

	         // Send message
	         Transport.send(rmessage);
	         sentMailReport=true;
	         if(debugThis) System.out.println("Sent Report mail successfully....");  
	         SysUtils.printDate();
	         getNextReport("automated report");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void doSendReportMail(String whoGeneratedMe, int turno){
		getMailVars();
		Properties props = new Properties(); 
		 
		props.put("mail.smtp.host", smtp);
		props.put("mail.smtp.socketFactory.port", mailport);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", mailAuth);
		props.put("mail.smtp.port", mailport);
		 
		BodyPart rmessageBodyPart = new MimeBodyPart(); 
		
		Session session;
		session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailOriginAddress,mailPassword);
				}
			});

		try {
			rmessageBodyPart = new MimeBodyPart();
			rMultipart = new MimeMultipart(); 
			multipart = new MimeMultipart();
			rmessage = new MimeMessage(session);
			rmessage.setFrom(new InternetAddress("noreply@sigma-telecom.com"));
			rmessage.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(mailToAddress));
			// Set Subject: header field
			rmessage.setSubject("Sigmation: Reporte por turno");  

	         // Fill the message
			rmessageBodyPart.setText(reportComment+" | Generated by:"+whoGeneratedMe+" | @ Turno "+turno+" - "+ reportDate); 

	         // Set text message part
	         rMultipart.addBodyPart(rmessageBodyPart);

	         // Part two is attachment  
	         generateReportAttachements(turno);

	         // Send the complete message parts
	         rmessage.setContent(rMultipart);

	         // Send message
	         Transport.send(rmessage);
	         sentMailReport=true;
	         if(debugThis) System.out.println("Sent Report mail successfully....");  
	         SysUtils.printDate();

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static ArrayList<String[]> getOptions(){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "getReportOptions";
		cr.connect(); 
		String[] data= {""};  
		ArrayList<String[]> result = cr.callValueProcedure(call,data);
		cr.close(); 
		return result;
	}
	
	public static ArrayList<String[]> getScheduleMail(){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "getschedulemail";
		cr.connect(); 
		String[] data= {""};  
		ArrayList<String[]> result = cr.callValueProcedure(call,data);
		cr.close(); 
		return result;
	}
	
	public static ArrayList<String[]> setMailSchedule(String tiempo_, int tipo_mail_, String comienzo_, String fin_, int param_){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "schedulemail";
		cr.connect(); 
		String[] data= {"'"+tiempo_+"'",""+tipo_mail_,"'"+comienzo_+"'","'"+fin_+"'",""+param_};  
		ArrayList<String[]> result = cr.callValueProcedure(call,data);
		cr.close(); 
		return result;
	}
	
	public static ArrayList<String[]> setDailyMailSchedule(String tiempo_){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "scheduledailymail";
		cr.connect(); 
		String[] data= {"'"+tiempo_+"'"};  
		ArrayList<String[]> result = cr.callValueProcedure(call,data);
		cr.close(); 
		return result;
	}
	
	public static void getMailVars(){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "get_tnt_mailvars";
		cr.connect(); 
		String[] data= {""};  
		ArrayList<String[]> result = cr.callValueProcedure(call,data); 
		cr.close(); 
		try {
			for(String[] subresult : result){		
				smtp= subresult[1];
				mailport= subresult[2];
				mailAuth= subresult[3];
				mailOriginAddress= subresult[4];
				mailToAddress= subresult[5];
				mailPassword=subresult[6];
				reportTitle= subresult[7];
				reportComment= subresult[8];
				hour= subresult[9];
				setReportTime();
				if(debugThis) System.out.println(subresult[1]+" , "+subresult[2]+" , "+subresult[3]+" , "+subresult[4]+" , "+subresult[5]+" , "+subresult[6]+" , "+subresult[7]+" , "+subresult[8]);
			}
		} catch (Exception e) {
			System.out.println("Failed to get mail stats");
		}		
	} 
	
	private static void getNextReport(String cause){    
			try {
				if(debugThis) System.out.println(" get next report by "+ cause);
				MySQL cn = new MySQL();
				int startt=0;
				int finishtt=1;
				int thisturn = 2;
				cn.database="ampdb";		
				String call = "currentturngettimesstats";
				cn.connect();   
				ArrayList<String[]> result = cn.callValueProcedure(call); 
				if(!result.isEmpty()) {
					String[] lconfig= result.get(0);  
					reportDate= SysUtils.getLocalDateStr(lconfig[finishtt]);
					reportStartDate= SysUtils.getLocalDateStr(lconfig[startt]);
					reportDateString=lconfig[finishtt];
					reportStartDateString=lconfig[startt];
					reportTurn= lconfig[thisturn];
					reportScheduler();
				}				
				cn.close(); 
			} catch (Exception e) {
				e.printStackTrace();
			}		
	}
	
	private static void getNextReport(){    
		try { 
			MySQL cn = new MySQL();
			int startt=0;
			int finishtt=1;
			int thisturn = 2;
			cn.database="ampdb";		
			String call = "currentturngettimesstats";
			cn.connect();   
			ArrayList<String[]> result = cn.callValueProcedure(call); 
			if(!result.isEmpty()) {
				String[] lconfig= result.get(0);  
				reportDate= SysUtils.getLocalDateStr(lconfig[finishtt]);
				reportStartDate= SysUtils.getLocalDateStr(lconfig[startt]);
				reportDateString=lconfig[finishtt];
				reportStartDateString=lconfig[startt];
				reportTurn= lconfig[thisturn];
				reportScheduler();
			}				
			cn.close(); 
		} catch (Exception e) {
			e.printStackTrace();
		}		
}
	
	public static void backupAndRestart(String fin_){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "backupAndRestartTime";
		String[] data= {"'"+fin_+"'"};
		cr.connect();  
		cr.callVoidProcedure(call,data); 
		cr.close(); 
		System.out.println("Backup and Restart done!");
		SysUtils.printDate();
	}	
	
}