package sigmation.utils;

import java.util.ArrayList;

import sigmation.modbus.Modbus;



public class PM130EH {
	
	/*INITIAL OPTIONS*/
	public int[] wiring;
	public int[] nominalfreq;
	public int[] ptratiomultfactor;
	public int[] powerblockdemandperiod;
	
	/*OTHER OPTIONS*/
	public int[] phaseEnergyCalcMode;
	int[] startingVoltage ; //max 50
	public int[] deviceResolution;
	
	/*VARIABLES*/
	public int address;
	public double Vmax;
	public double Imax;
	public double Pmax;
	public double Fmax;
	public int[] VoltageScale;
	public int[] CurrentScale;
	public int[] CTprimaryCurrent;
	public int[] PTratio;
	public int[] powercalcmode;
	public int[] phaseenergycalc;
	public int[] energytestmode;
	public int[] ampscale;
	public int[] startingvoltagefs;
	public int[] voltampdemandperiod;
	public int[] blocksinsliding;
	
	/*setup (config)*/
	public ArrayList<int[]> configsetup;
	
	/*post-setup*/
	public ArrayList<int[]> setpointsetup;
	
	/*meassurement parameters*/
	ArrayList<String[]> specialPars;  //address,quantity,parameters
	
	
	/*UNITS*/
	
	double U1;
	double U2;
	double U3;
	
	enum PowerCalcMode {REACTIVE, NON_ACTIVE}
	enum PnergyRollValue { EXP4, EXP5, EXP6, EXP7, EXP8, EXP9}
	enum PhaseEnergyCalcMode {DISABLED, ENABLED}
	enum DeviceResolution {LOW_RESOLUTION, HIGH_RESOLUTION}
	
	final int SETPOINT_MAXLENGTH=8;

	public enum WIRING { WIRING_4LN3,
		                 WIRING_3LN3,
		                 WIRING_3BLN3, 
		                 WIRING_4LL3,
		                 WIRING_3LL3,
		                 WIRING_3BLL3, 
		                 WIRING_3OP2, 
		                 WIRING_3OP3, 
		                 WIRING_3DIR2}
	
	public enum NominalFreq {   F25HZ,
								F50HZ,
								F60HZ, 
								F400HZ}
	
	public enum PowerBlockDemandPeriod {    MIN1,
											MIN2,
											MIN3, 
											MIN5,
											MIN10,
											MIN15,
											MIN20,
											MIN30,
											MIN60,
											EXTERNAL_SYNC}
	
	public enum PTRAtioMultFactor { x1, x10}
	

public PM130EH(ArrayList<String[]> parametersList, byte address){
	try {
		int[]     ptratiox={336,0},
				ptratiomultiplier={2305,1},
				ctprimarycurrent={2324,300},
				nominalfrequency={2306,50},
				powercalcmode={2315,1},
				phaseenergycalc={2378,0},
				energytestmode={2386,0},
				deviceresolution={2390,1},
				voltscale={242,144},
				ampscale={234,100},
				startingvoltagefs={2387,15},
				wiringmode={2304,1},
				voltampdemandperiod={2308,900},
				blocksinsliding={2312,1},
				pwblockdmpd={2307,15};
		for(int i=0;i<parametersList.size() ;i++){
			String[] deviceValues= parametersList.get(i);			
			int[] localvalues= {Integer.parseInt(deviceValues[1]),Integer.parseInt(deviceValues[5])};					
			switch(deviceValues[0]){
				
			case "PT Ratio" : ptratiox = localvalues;
			break;
			case "PT Ratio Multiplier" : ptratiomultiplier = localvalues;
			break;
			case "CT Primary Current" : ctprimarycurrent = localvalues;
			break;
			case "Nominal Frequency" : nominalfrequency = localvalues;
			break;
			case "Power Calc. Mode" : powercalcmode = localvalues;
			break;
			case "Phase Energy Calc." : phaseenergycalc =localvalues;
			break; 
			case "Energy Test Mode" : energytestmode = localvalues;
			break; 			
			case "Device Resolution" : deviceresolution =localvalues;
			break; 			
			case "Volts Scale" : voltscale = localvalues;
			break; 			
			case "Amps Scale" : ampscale = localvalues;
			break;
			case "Starting Voltage %FS" : startingvoltagefs = localvalues;
			break;			
			case "Wiring Mode" : wiringmode =localvalues;
			break;			
			case "Volt/Ampere Demand Period" : voltampdemandperiod =localvalues; 
			break;				
			case "Blocks In Sliding Demand" : blocksinsliding = localvalues;
			break;			
			case "Pw. Block Demand Period" : pwblockdmpd = localvalues;
			break;
			}
		}
		configsetup = new ArrayList<int[]>();
		this.address = address;
		this.PTratio = ptratiox;
		configsetup.add(this.PTratio);
		this.ptratiomultfactor = ptratiomultiplier;
		configsetup.add(this.ptratiomultfactor);
		this.CTprimaryCurrent = ctprimarycurrent;
		configsetup.add(this.CTprimaryCurrent);
		this.nominalfreq = nominalfrequency;
		configsetup.add(this.nominalfreq);
		this.powercalcmode = powercalcmode;
		configsetup.add(this.powercalcmode);
		this.phaseenergycalc = phaseenergycalc;
		configsetup.add(this.phaseenergycalc);
		this.energytestmode = energytestmode;
		configsetup.add(this.energytestmode);
		this.deviceResolution = deviceresolution;
		configsetup.add(this.deviceResolution);
		this.VoltageScale = voltscale;
		configsetup.add(this.VoltageScale);
		this.ampscale = ampscale;
		configsetup.add(this.ampscale);
		this.startingvoltagefs = startingvoltagefs;
		configsetup.add(this.startingvoltagefs);
		this.wiring=wiringmode;
		configsetup.add(this.wiring);
		this.voltampdemandperiod=voltampdemandperiod;
		configsetup.add(this.voltampdemandperiod);
		this.blocksinsliding=blocksinsliding;
		configsetup.add(this.blocksinsliding);
		this.powerblockdemandperiod = pwblockdmpd;
		configsetup.add(this.powerblockdemandperiod);
		
		int maxpowerfactor=2;
		if (wiring[1]==1 || wiring[1]==5 || wiring[1]==8 ) maxpowerfactor=3;
		double ptfv = 0.1 , ptfa = 0.01, ptfp = 1000;
		if (this.PTratio[1]==10 || this.deviceResolution[1]==0) {ptfv=1 ; ptfa = 1;}
		if (this.deviceResolution[1]==1 && PTratio[1]==10) ptfp =1; 
		Vmax =  this.VoltageScale[1] * this.PTratio[1] * 0.1 / this.ptratiomultfactor[1];
		Imax =  this.ampscale[1] * 0.1 * this.CTprimaryCurrent[1] /5; // *
		
		Pmax = Vmax * Imax * maxpowerfactor;
		
		if(nominalfreq[1]>100) Fmax= 500;
		else Fmax=100;
		
		U1 = ptfv;
		U2 = ptfa;
		U3 = ptfp;
	} catch (Exception e) {
		System.out.println("error inside builder");
		e.printStackTrace();
	}
	
	
}	
	
	public void setSpecialPars(ArrayList<String[]> specialPars){
		this.specialPars=specialPars;
	}

	public void rebuild(){
		int maxpowerfactor=2;
		if (wiring[1]==1 || wiring[1]==5 || wiring[1]==8 ) maxpowerfactor=3;
		Vmax =  VoltageScale[1] * PTratio[1];
		Imax = CTprimaryCurrent[1] * 2 ;
		Pmax = Vmax * Imax * maxpowerfactor;		
	}
	
	public String getString(){
		return "\nDevice:"+address+"\nConfig pars:"+configsetup.size()+"\nSetpoint pars:"+setpointsetup.size();		
	}
	
	public void setPostSetup(ArrayList<String[]> postSetupList){
		setpointsetup = new  ArrayList<int[]>();
		for(int i=0; i<postSetupList.size();i++){
			String[] pstvalues= postSetupList.get(i);
			int[] deviceid = {Integer.parseInt(pstvalues[0]),Integer.parseInt(pstvalues[1])};
			setpointsetup.add(deviceid);
		}
	}
	

	public double scaleToReal(double x, double hi, double lo){
		double y = (x*(hi-lo)/9999) + lo;
		if (y>hi) y=hi;
		else if(y<lo) y=lo;
		return y;
	}
	
	public int u32ToInt(int lowerRegister, int higherRegister){
		int val = ((higherRegister*65536)+lowerRegister);
		return val;
	}
	
	public int s32ToInt(int lowerRegister, int higherRegister){
		int sign = 0;
		
		if(higherRegister>32767) sign = higherRegister-65536;
		else sign = higherRegister;
		
		int val = ((sign*65536)+lowerRegister);
		return val;
	}
	
	public int Mod10kToInt(int lowerRegister, int higherRegister){
		int val = (higherRegister*10000) + lowerRegister;
		return val;
	}
	
	public double mapValues(int address, int value){ //ArrayList<String[]> specialPars;  //address,quantity,parameters
		String par1="";
		String par2="";
		int quantity=0;
		for (int i=0; i<specialPars.size();i++){
			String[] localstring = specialPars.get(i);
			if (Integer.parseInt(localstring[0])==address){
				quantity=Integer.parseInt(localstring[1]);
				String[] parts = localstring[2].split(",");
				if(parts.length==2){
					par1=parts[0];
					par2=parts[1];
				} else {
					par1="";
					par2="";
				}
				
			}
		}
		
		double result =value;

		if("0-Vmax".equals(par1) && "U1".equals(par2)){
			
			result=voltReading(value);
			result=result*U1;	
		} else if( "0-Imax".equals(par1) && "U2".equals(par2)){
			result=currentReading(value);
			result=result*U2;			
		} else if( "-Pmax-Pmax".equals(par1) &&  "U3".equals(par2)){
			result=(double)powerReading(value);
			result=result*U3;			
		}  else if( "0-Pmax".equals(par1) &&  "U3".equals(par2)){
			result=(double)maxPowerReading(value);
			result=result*U3;			
		} else if(( "-1.000-1.000".equals(par1)  ||  "-1000-1000".equals(par1) ) &&  "0.001".equals(par2)){
			result=(double)powerFactorReading(value);
			result=result*1;			
		} else if( "45.00-65.00".equals(par1) &&  "0.01Hz".equals(par2)){
			result=(double)nomfrequencyReading(value);
		} else if( "0-999.9".equals(par1) &&  "0.1%".equals(par2)){
			result=(double)per1KReading(value);
			result=result*0.1;			
		} else if(( "0-100.0".equals(par1) &&  "0.1%".equals(par2)) || ( "0-1000".equals(par1) &&  "0.1%".equals(par2))){
			result=(double)percentReading(value);		
		} else if( "1.0-999.9".equals(par1) &&  "0.1".equals(par2)){
			result=(double)oneto999p9Reading(value);	
		} else if( "10-9999".equals(par1) &&  "U1".equals(par2)){
			result=(double)oneto999p9Reading(value);
			result=result*0.1;		
		} else if( "0-1.000".equals(par1) &&  "0.001".equals(par2)){
			result=(double)factor1Reading(value);			
		} else if( "0-9999".equals(par1) && ( "x10MWh".equals(par2) ||  "x10Mvarh".equals(par2) )){
			result=result*10;			
		} else if( "0-9999".equals(par1) &&  "0.1°".equals(par2)){
			result=angleReading(value);			
		} else if( "0-9999".equals(par1) &&  "x0.1%".equals(par2)){
			result=per1KReading(value);			
		} else if( "0-Fmax".equals(par1) &&  "0.01Hz".equals(par2)){
			result=per1KReading(value);	
			result=result*0.1;
		} else if( "0-109-1".equals(par1) && ( "1 kWh".equals(par2) ||  "1 kwh".equals(par2) )){
			System.out.println("Energy reading: "+value);
			result=energyReading(value);			
		} else if( "0-9999".equals(par1) && ( "1 MWh".equals(par2) ||  "1 Mwh".equals(par2) )){
			System.out.println("High import reading: "+value);
			result=importReadingHigh(value);			
		}  else if( "0-9999".equals(par1) && ( "1kWh".equals(par2) ||  "1kwh".equals(par2) || "1 kWh".equals(par2) ||  "1 kwh".equals(par2))){
			System.out.println("Low Import reading: "+value);
			result=importReadingLow(value);			
		}      
		//frequencyReading
		return result;
	}
	
	public double importReadingLow(int rawData){		
			double HI_ENG = 9999;
			double LO_ENG = 0;		
			return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double importReadingHigh(int rawData){
		
		double HI_ENG = 9999;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double energyReading(int rawData){
		double HI_ENG = 1000000000-1;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double voltReading(int rawData){
		
		double HI_ENG = Vmax;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double currentReading(int rawData){
		double HI_ENG = Imax;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double powerReading(int rawData){
		double HI_ENG = Pmax;
		double LO_ENG = -Pmax;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double maxPowerReading(int rawData){
		double HI_ENG = Pmax;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double powerFactorReading(int rawData){
		double HI_ENG = 1;
		double LO_ENG = -1;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double nomfrequencyReading(int rawData){
		double HI_ENG = 45;
		double LO_ENG = 65;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double frequencyReading(int rawData){
		double HI_ENG = Fmax;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double percentReading(int rawData){
		double HI_ENG = 100;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double oneto999p9Reading(int rawData){
		double HI_ENG = 999.9;
		double LO_ENG = 1.0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double per1KReading(int rawData){
		double HI_ENG = 999.9;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double factor1Reading(int rawData){
		double HI_ENG = 1;
		double LO_ENG = 0;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public double angleReading(int rawData){
		double HI_ENG = 180;
		double LO_ENG = -180;		
		return scaleToReal(rawData, HI_ENG, LO_ENG);
	}
	
	public byte[] getSetpointStatusBuffer(){
		Modbus myModbus = new Modbus();
		myModbus.readInputRegisters((byte)address,11616,1);
		return myModbus.data;
	}
	
	public byte[] setpointStatusBuffer(int[] datax){
		Modbus myModbus = new Modbus();
		if(datax.length>SETPOINT_MAXLENGTH) return null;
		myModbus.writeMultipleRegisters((byte)address,2576,datax);
		return myModbus.data;
	}
	
	public byte[] setConfig(int index){
		Modbus myModbus = new Modbus();
		int[] localint = configsetup.get(index);
		int localaddress = localint[0];
		int[] localvalue = {localint[1]};
		//myModbus.writeSingleRegister((byte)address, localaddress, localvalue);
		//myModbus.writeMultipleRegisters((byte)address,localaddress,new int[]{localvalue});
		myModbus.writeMultipleRegisters((byte)address, localaddress, localvalue);
		return myModbus.data;
	}
	
	public byte[] setConfig(int index, boolean noaddress){
		Modbus myModbus = new Modbus();
		int[] localint = configsetup.get(index);
		int localaddress = localint[0];
		int[] localvalue = {localint[1]};
		myModbus.writeMultipleRegisters((byte)0x01, localaddress, localvalue);
		//myModbus.writeMultipleRegisters((byte)address, localaddress, localvalue);
		return myModbus.data;
	}
	
	public byte[] setSetPoints(int index){
		Modbus myModbus = new Modbus();
		int[] localint = setpointsetup.get(index);
		int localaddress = localint[0];
		int[] localvalue = {localint[1]};
		myModbus.writeMultipleRegisters((byte)address, localaddress, localvalue);
		//myModbus.writeSingleRegister((byte)address, localaddress, localvalue);
		//myModbus.writeMultipleRegisters((byte)address,2576,localint);
		return myModbus.data;
	}
	
	public byte[] setSetPoints(int index, boolean noadd){
		Modbus myModbus = new Modbus();
		int[] localint = setpointsetup.get(index);
		int localaddress = localint[0];
		int[] localvalue = {localint[1]};
		myModbus.writeMultipleRegisters((byte)0x01, localaddress, localvalue);
		//myModbus.writeSingleRegister((byte)0x01, localaddress, localvalue);
		//myModbus.writeMultipleRegisters((byte)address, localaddress, localvalue);
		//myModbus.writeMultipleRegisters((byte)address,2576,localint);
		return myModbus.data;
	}
}


