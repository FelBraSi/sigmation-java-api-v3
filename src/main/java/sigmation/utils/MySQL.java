package sigmation.utils;


import java.sql.*;
import java.util.ArrayList;
 
import com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException;

import sigmation.main.Sigmation;;


public class MySQL {
	
    public String host = "localhost";
    public String database = "ampdb";
    public String user = Sigmation.user;
    public String pass = Sigmation.pass;
    public Statement s;
    public Connection conexion;
    public CallableStatement cStmt;
    
    boolean mysqldebug=false;

   
    public MySQL() 
    {

        try
        {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
 
    public void connect() 
    {
    	connect(host);
    }
    
    public void connect(String thisHost) 
    {
    	String conectString="jdbc:mysql://"+thisHost+"/"+database;
        try
        { 
             conexion = DriverManager.getConnection (
            		 conectString,user, pass);
            s = conexion.createStatement();
        }
        catch (Exception e)
        {
            System.out.println("Could not connect to MYSQL server: "+conectString+"  USER: "+user+" PASS " +pass+" :::");
        }
    }
    
    public boolean isClosed() 
    {
        try
        {
             return conexion.isClosed();
        }
        catch (NullPointerException e)
        { 
        	return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }
    }
    
    public void close() 
    {
        try
        {
             conexion.close();
        }
        catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("MySQL close failed, no connections open");
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void rawUpdateMySQLCommand(String sentence) {
        try {
            s.executeUpdate(sentence);
            if(mysqldebug)System.out.println("sentence done");
        }
        catch (Exception e ) {
        	if(mysqldebug)System.out.println("An error has occurred while executing this MySQL task: "+sentence);
        }
    } 
    
    public ArrayList<String[]> rawSelect(String sentence){
        try
        {
        	ArrayList<String[]> results = new ArrayList<String[]>();
            ResultSet rs = s.executeQuery (sentence);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            
            while (rs.next())
            {   String[] currentRow= new String[columnsNumber];
	            for(int i = 1;i<=currentRow.length;i++){
	            	currentRow[i-1]=rs.getString(i);
	            }
                results.add(currentRow); 
            }
            conexion.close();
            return results;
        }
        catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("rawSelect failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        	return null;
    }
    
    public ArrayList<String[]> select(String table, String argument, String condition){
        try
        {
        	ArrayList<String[]> results = new ArrayList<String[]>();
        	if (mysqldebug)  System.out.println("select "+argument+" from "+table+ " where " + condition);
            ResultSet rs = s.executeQuery ("select "+argument+" from "+table+ " where " + condition);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            
            while (rs.next())
            {   String[] currentRow= new String[columnsNumber];
	            for(int i = 1;i<=currentRow.length;i++){
	            	currentRow[i-1]=rs.getString(i);
	            }
                results.add(currentRow);
                
            }
            conexion.close();
            return results;
        }
        catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("Select failed, connection down");
        	return null;
        } catch (Exception e) {
        	e.printStackTrace();
        }
        	return null;
    }
    
    private String getUpVals(String[] argument){          // used to get values for stored procedure calls
    	int argsize=argument.length;
    	String args= " ";
    	for(int i=0;i<argsize;i++){
    		if(i!=0) args= args + " , ";
    		args= args+argument[i];
    	}
    	return args;
    }
    
    private String getUpArg(String[] argument,  String[] values){
    	int argsize=argument.length;
    	String args= " ";
    	for(int i=0;i<argsize;i++){
    		if(i!=0) args= args + " , ";
    		args= args+argument[i]+"="+values[i];
    	}
    	return args;
    }
    
    private String getUpANDArg(String[] argument,  String[] values){
    	int argsize=argument.length;
    	String args= " ";
    	for(int i=0;i<argsize;i++){
    		if(i!=0) args= args + " AND ";
    		args= args+argument[i]+"="+values[i];
    	}
    	return args;
    }
  

    private String getLessArg(String[] argument,  String[] values, boolean delete ){
    	int argsize=argument.length;
    	String args= " ";
    	String union = ",";
    	if(delete) union=" AND ";
    	for(int i=0;i<argsize;i++){
    		if(i!=0) args= args + union;
    		args= args+argument[i]+"<"+values[i];
    	}
    	return args;
    }
    
    
    
    private String getInArg(String[] argument, String[] values){
    	int argsize=argument.length;
    	String args1= "( ";
    	String args2= " values(";
    	for(int i=0;i<argsize;i++){
    		if(i!=0) {
    					args1= args1 + ",";
    					args2= args2 + ",";
    				}
    		args1= args1 + argument[i];
    		args2= args2 + values[i];
    	}
    	 args1= args1+") ";
    	 args2= args2+") ";
    	return args1+args2;
    }
    
    public void callVoidProcedure(String procedure , String[] argument){
    	String command_e= new String();
    	
    	try
        {   
    		
        	command_e = "{call "+procedure+"( "+getUpVals(argument)+" )}";
        	cStmt = conexion.prepareCall(command_e);
        	cStmt.execute();
        	if (mysqldebug)  System.out.println(command_e);
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("callvoid failed, connection down");
        	return;
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        }    	
    }
    
    public void callVoidProcedure(String procedure){
    	String command_e= new String();
    	
    	try
        {   
    		
        	command_e = "{call "+procedure+"()}";
        	cStmt = conexion.prepareCall(command_e);
        	cStmt.execute();
        	if (mysqldebug)  System.out.println(command_e);
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("callvoid failed, connection down");
        	return;
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        }    	
    }
    
    public ArrayList<String[]> callValueProcedure(String procedure){
    	try
        {	String command_e= new String();
	        command_e = "{call "+procedure+"(  )}";
	        if (mysqldebug) System.out.println(command_e);
	    	cStmt = conexion.prepareCall(command_e);
        	ArrayList<String[]> results = new ArrayList<String[]>();
            ResultSet rs = cStmt.executeQuery ();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount(); 
            while (rs.next())
            {   String[] currentRow= new String[columnsNumber];
	            for(int i = 1;i<=currentRow.length;i++){
	            	currentRow[i-1]=rs.getString(i);
	            }
                results.add(currentRow); 
            }
            conexion.close();
            return results;
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("callvalue failed, connection down");
        	return null;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        	return null; 
    }
    
    public ArrayList<String[]> callValueProcedure(String procedure , String[] argument){
    	return callValueProcedure ( procedure , argument, true);
    }
    
    public ArrayList<String[]> callValueProcedure(String procedure , String[] argument, boolean closeCon){
    	int trials=0;
    	try {
    		while(trials<1) {
        		try
                {	String command_e= new String();
        	        command_e = "{call "+procedure+"( "+getUpVals(argument)+" )}";
        	        if (mysqldebug) System.out.println(command_e);
        	    	cStmt = conexion.prepareCall(command_e);
                	ArrayList<String[]> results = new ArrayList<String[]>();
                    ResultSet rs = cStmt.executeQuery ();
                    ResultSetMetaData rsmd = rs.getMetaData();
                    int columnsNumber = rsmd.getColumnCount(); 
                    while (rs.next())
                    {   String[] currentRow= new String[columnsNumber];
        	            for(int i = 1;i<=currentRow.length;i++){
        	            	currentRow[i-1]=rs.getString(i);
        	            }
                        results.add(currentRow); 
                    }
                    if (closeCon) conexion.close();
                    return results;
                }
            	catch (NullPointerException e)
                {
                	if (mysqldebug)  System.out.println("callvalue failed, connection down");
                	return null;
                } catch (MySQLTransactionRollbackException e)
                {
                	trials++;
                	if (mysqldebug && trials <3)  System.out.println("Deadlock "+(trials)+"/3, retrying ...");
                	else if(mysqldebug)  {
                		System.out.println("Deadlock 3/3, error, can't try anymore... check DB");
                		return null;}
                    
                } catch (Exception e)
                {
                	trials++;
                	if (mysqldebug && trials <3)  {
                		System.out.println("Deadlock "+(trials)+"/3, retrying ...");
                		Thread.sleep(SysUtils.getRandom(5000));
                	}
                	else if(mysqldebug)  {
                	System.out.println("Deadlock 3/3, error, can't try anymore... check DB");
                	return null;}
                }
        	}
		} catch (Exception e)
        {
        	if (mysqldebug)  System.out.println("general exception on callValueProcedure");
            e.printStackTrace();
            return null;
        }
    	
    	
        return null; 
    }
    
    public void addUpdate(String table, String[] argument, String condition, String[] values){
        String command_e= new String();
    	try
        {   String command= "select * from "+table+ " where " + condition;
        	ResultSet rs = s.executeQuery(command);
        	rs.last();
        	if (mysqldebug)  System.out.println(command_e); 
        	if(rs.getRow()>0){
        		String args= getUpArg(argument, values);
        		command = "UPDATE "+table+ " SET "+args+" where " + condition;
        		command_e=command;
        		s.executeUpdate(command);        		
        	} else {
        		String args= getInArg(argument, values);
        		command="INSERT INTO "+table+ " "+args;
        		command_e=command;
        		s.executeUpdate(command); 
        	}
                if (mysqldebug)  System.out.println(command_e);    
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("addupdate failed, connection down");
        	return;
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        }
    }  
    
    public void update(String table, String[] argument, String condition, String[] values){
        String command_e= new String();
    	try
        {   
        		String args= getUpArg(argument, values);
        		String command = "UPDATE "+table+ " SET "+args+" where " + condition;
        		command_e=command;
        		s.executeUpdate(command);     		
                if (mysqldebug)  System.out.println(command_e);    
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("update failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        }
    }  
    
    public void updateCase(String table, String variable, String condition, String thenValue, String elseValue, String whereCondition){
        String command_e= new String();
    	try
        {   
        		String command = "UPDATE "+table+ " SET "+variable+" = CASE  WHEN " + condition + "THEN "+thenValue+" ELSE "+elseValue+" END WHERE "+whereCondition;
        		command_e=command;
        		s.executeUpdate(command);     		
                if (mysqldebug)  System.out.println(command_e);    
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("updateCase failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        }
    } 
    
    public void ifExistsUpdate(String table, String[] argument, String condition, String[] values){
        String command_e= new String();
    	try
        {   String command= "select * from "+table+ " where " + condition;
        	ResultSet rs = s.executeQuery(command);
        	rs.last();
        	if(rs.getRow()>0){
        		String args= getUpArg(argument, values);
        		command = "UPDATE "+table+ " SET "+args+" where " + condition;
        		command_e=command;
        		s.executeUpdate(command);        		
        	} else {
        		//do nothing
        	}
                    

        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("ifexistupdate failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        }
    }   

    public boolean exists(String table,String condition){
        String command_e= new String();
    	try
        {   String command= "select * from "+table+ " where " + condition;
        	ResultSet rs = s.executeQuery(command);
        	rs.last();
        	if(rs.getRow()>0){
        		return true;        		
        	} else {
        		return false;
        	}   
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("exist failed, connection down");
        	return false;
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
            return false;
        }
    }   
    
    public void insert(String table, String[] argument, String[] values){
    	String command_e= new String();
        try
        {
        	String args= getInArg(argument, values);
        	String command="INSERT INTO "+table+ " "+args;
        	command_e=command;
        	s.executeUpdate(command); 
        	if (mysqldebug)  System.out.println(command_e);
        }
        catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("Insert failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println(command_e);
        } 
    }   

    public void delete(String table, String[] argument, String[] values){
    	String args= getUpANDArg(argument, values);
    	String command="DELETE FROM "+table+ " WHERE "+args;
    	try
        {
        	
        	s.executeUpdate(command);         
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("delete failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println("< "+command+" >");
        }

    }       

    public void deleteLess(String table, String[] argument, String[] values){
    	String args= getLessArg(argument, values, true);
    	String command="DELETE FROM "+table+ " WHERE "+args;
    	if (mysqldebug)  System.out.println("< "+command+" >");
    	try
        {
        	
        	s.executeUpdate(command);         
        }
    	catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("deleteless failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
            if (mysqldebug)  System.out.println("< "+command+" >");
        }

    }   
    
    public void delete(String table, String argument, String value){
        try
        {
        	s.executeUpdate("DELETE FROM "+table+ " WHERE "+argument+"="+value);         
        }
        catch (NullPointerException e)
        {
        	if (mysqldebug)  System.out.println("deleteT failed, connection down");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    } 
    public void printer(ArrayList<String[]> output){
    	
        for (int index=0; index< output.size();index++){
        	String[] s_out=output.get(index);
        	for(int i=0; i<s_out.length;i++){
        		//if(s_out[i]!=null)System.out.print (s_out[i]+" ");       		
        	}
        	if (mysqldebug)  System.out.println ("");	
        }
    	
    }	

    public String[][]  organize(ArrayList<String[]> output){
    	int columns = output.get(0).length;
    	String[][] result = new String[output.size()][columns];
        for (int index=0; index< output.size();index++){
        	String[] s_out=output.get(index);
        	result[index]=s_out;
        }
    	return result;
    }
 /*   
    public static void main(String[] args) 
    {
        MySQL cn = new MySQL();
        cn.database="config";
        cn.connect();
        ArrayList<String[]> output = cn.select("sys_config","*","1=1");
        cn.printer(output);
        cn.organize(output);       
    }*/
    
}

