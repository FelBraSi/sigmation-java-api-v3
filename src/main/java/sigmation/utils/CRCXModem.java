package sigmation.utils;

import com.github.snksoft.crc.CRC;

/**
 * 
 *  CRC XMODEM library
 *  Extracted from the internet
 *  Minor changes to get the new crc as soon as it updates
 *  
 *  -FBS-
 * 
 * */
 

public class CRCXModem {
	public final static int polynomial = 0x1021;	// Represents x^16+x^12+x^5+1
	int crc;
	
	public CRCXModem(){
		crc = 0x0000;
	}
	
	public int getCRC(){
		return crc;
	}
	
	public String getCRCHexString(){
		String crcHexString = Integer.toHexString(crc);
		return crcHexString;
	}
	
	public void resetCRC(){
		crc = 0x0000;
	}
	
	public void printCRC(){
		System.out.println("CRC XMODEM: "+crc);
	}
	
	public int update(byte[] args) { 
		resetCRC();
        /*for (byte b : args) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b   >> (7-i) & 1) == 1);
                boolean c15 = ((crc >> 15    & 1) == 1);
                crc <<= 1;
                // If coefficient of bit and remainder polynomial = 1 xor crc with polynomial
                if (c15 ^ bit) crc ^= polynomial;
             }
        }*/
		CRC tableDriven = new CRC(CRC.Parameters.XMODEM);
		long xmodemCrc = tableDriven.calculateCRC(args);
        crc = (int)xmodemCrc & 0xFFFF;
        return getCRC();
    }	
}