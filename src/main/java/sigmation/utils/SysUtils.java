package sigmation.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit; 

import com.pi4j.system.SystemInfo;
 
import sigmation.io.LcdDevice;
import sigmation.io.LedDisplay;
import sigmation.main.Sigmation;
import sigmation.tagTcp.MessageHandler;


public class SysUtils {

	public static String currentTask="Sigmation";
	public static Random rand = new Random();
	
	public static byte intToHexFormatOnly(int toFormat) {
		try {
			String sToFormat= ""+toFormat;
			byte inFormat = (byte)Integer.parseInt(sToFormat, 16);
			return inFormat;
		} catch (Exception e) {
			return 0;
		}
	}
	
	public static String jsonOptionBuilder(String[][] args, String type) {
		try {
			String bufferString="{ \"type\":\""+type+"\",\"elements\": [";
			boolean startcoma = false;
			if(args==null) return bufferString + "{\"label\":\"jsonBuilderError:nullargs\",\"value\":\"0\"}]}";
			else if(args[0].length!=2) return bufferString + "{\"label\":\"jsonBuilderError:argLengthError\",\"value\":\"0\"}]}";
			for (String[] var:args) {
				String label=var[0];
				String value=var[1];
				if (startcoma) bufferString=bufferString+",";
				bufferString=bufferString+"{\"label\":\""+label+"\",\"value\":\""+value+"\"}";
				startcoma=true;
			}
			bufferString=bufferString+"]}";
			return bufferString;
		} catch (Exception e) {
			return "{ \"type\":\"select\",\"elements\": [{\"label\":\"jsonBuilderError:Exception\",\"value\":\"0\"}]}";
		}
	}
	 
	
	public static byte[] intToByteArrayMSB2LSB( final int i ) {
	    BigInteger bigInt = BigInteger.valueOf(i);      
	    return bigInt.toByteArray();
	}
	
	public static byte[] bigIntToByteArray( final BigInteger toBytes , int fixedSize) {
	    byte[] intBytes = toBytes.toByteArray();
	    byte[] inverseIntBytes = inverseBytes(intBytes);
	    byte[] finalBytes = new byte[fixedSize];
	    for(int i=0; i<fixedSize && i < inverseIntBytes.length; i++) {
	    	finalBytes[i]=inverseIntBytes[i];
	    }
	    return finalBytes;
	}
	
	/**
	 * 
	 * int = 0x0001  >> bytes : {0x00,0x00,0x00,0x01}
	 * int = 0x10000001  >> bytes : {0x10,0x00,0x00,0x01}
	 * 
	 * */
	public static byte[] intToByteArrayMSB2LSB( final int toBytes , int fixedSize) {
		BigInteger bigInt = BigInteger.valueOf(toBytes);    
	    byte[] intBytes = bigInt.toByteArray();
	    byte[] finalBytes = new byte[fixedSize];
	    int intByteSize=intBytes.length;
	    for(int i=0; i<fixedSize && i < intByteSize; i++) {
	    	finalBytes[fixedSize-i-1]=intBytes[intByteSize-i-1];
	    }
	    return finalBytes;
	}
	
	/**
	 * 
	 * int = 0x0001  >> bytes : {0x01,0x00,0x00,0x00}
	 * int = 0x10000001  >> bytes : {0x01,0x00,0x00,0x10}
	 * */
	public static byte[] intToByteArrayIndexOrder( final int toBytes , int fixedSize) {
		BigInteger bigInt = BigInteger.valueOf(toBytes);    
	    byte[] intBytes = bigInt.toByteArray();
	    byte[] inverseIntBytes = inverseBytes(intBytes);
	    byte[] finalBytes = new byte[fixedSize];
	    for(int i=0; i<fixedSize && i < inverseIntBytes.length; i++) {
	    	finalBytes[i]=inverseIntBytes[i];
	    }
	    return finalBytes;
	}
	
	/**
	 * 
	 * int = 0x0001  >> bytes : {0x01,0x00,0x00,0x00}
	 * int = 0x10000001  >> bytes : {0x01,0x00,0x00,0x10}
	 * */
	public static int[] bigIntToIntArrayIndexOrder( final BigInteger toBytes , int fixedSize) {
	    byte[] intBytes =  toBytes.toByteArray();
	    byte[] inverseIntBytes = inverseBytes(intBytes);
	    int[] finalInt = new int[fixedSize];
	    for(int i=0; i<fixedSize && i < inverseIntBytes.length; i++) {
	    	finalInt[i]=byteToUnsignedInt(inverseIntBytes[i]);
	    }
	    return finalInt;
	}
	
	public static byte[] inverseBytes(byte[] toInverse) {
		int bytelength =toInverse.length;
		byte[] inversedBytes = new byte[bytelength];
		for(int i=0;i<bytelength;i++) {
			inversedBytes[i]=toInverse[bytelength-i-1];
		}
		return inversedBytes;
	}
	
	public static String jsonBuilder(String[][] args, String type) {
		try {
			String bufferString="{ \"type\":\""+type+"\",\"elements\": {";
			boolean startcoma = false;
			if(args==null) return bufferString + "\"label\":\"jsonBuilderError:nullargs\",\"value\":\"0\"}}";
			else if(args[0].length!=2) return bufferString + "\"label\":\"jsonBuilderError:argLengthError\",\"value\":\"0\"}}";
			for (String[] var:args) {
				String label=var[0];
				String value=var[1];
				if (startcoma) bufferString=bufferString+",";
				bufferString=bufferString+"\""+label+"\":\""+value+"\"";
				startcoma=true;
			}
			bufferString=bufferString+"}}";
			return bufferString;
		} catch (Exception e) {
			return "{ \"type\":\"select\",\"elements\": [{\"label\":\"jsonBuilderError:Exception\",\"value\":\"0\"}]}";
		}
	}
	
	public class jsonArray{
		String name;
		ArrayList<jsonObject[]>  items;
		
		public jsonArray(String name, ArrayList<jsonObject[]> items) {
			this.name=name;
			this.items=items;
		}
	}
	
	public class jsonObject{
		String label;
		String value;	
		
		public jsonObject(String label, String value) {
			this.value=value;
			this.label=label;
		}
	}
	
	public static String jsonMetaBuilder(String[][] args, String type, ArrayList<jsonArray> listArgs) {
		try {
			String bufferString="{ \"type\":\""+type+"\",\"elements\": {";
			boolean startcoma = false;
			if(args==null) return bufferString + "\"label\":\"jsonBuilderError:nullargs\",\"value\":\"0\"}}";
			else if(args[0].length!=2) return bufferString + "\"label\":\"jsonBuilderError:argLengthError\",\"value\":\"0\"}}";
			for (String[] var:args) {
				String label=var[0];
				String value=var[1];
				if (startcoma) bufferString=bufferString+",";
				bufferString=bufferString+"\""+label+"\":\""+value+"\"";
				startcoma=true;
			}
			bufferString=bufferString+"}";
			
			for(jsonArray array : listArgs) {
				
				boolean startArrayComa = false;
				if (startcoma) bufferString=bufferString+",";
				bufferString=bufferString+ "\""+array.name+"\": [";
				
				for(jsonObject[]  items : array.items ) {
					
					boolean startArrayElementsComa = false;
					if (startArrayComa) bufferString=bufferString+",";
					bufferString=bufferString+"{";
					
					for(jsonObject item: items) {
						
						String label=item.label;
						String value=item.value;
						if (startArrayElementsComa) bufferString=bufferString+",";
						bufferString=bufferString+"\""+label+"\":\""+value+"\"";
						startArrayElementsComa = true;
					}	
					
					bufferString=bufferString+"}";	 
					startArrayComa = true;
				}
				
				bufferString=bufferString+"]";	
				startcoma=true;
			}
			
			bufferString=bufferString+"}";
			return bufferString;
		} catch (Exception e) {
			return "{ \"type\":\"select\",\"elements\": [{\"label\":\"jsonBuilderError:Exception\",\"value\":\"0\"}]}";
		}
	}
	
	
	public static boolean getconfigRefresh(){
		String argument= "value";
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="module_config";
		String condition = " param = 'refresconfig' ";
		ArrayList <String[]> localConfig = cn.select(table, argument,condition);
		if(localConfig==null | localConfig.isEmpty()){
			cn.close(); 
			return false;
		} else {
			String[] lconfig= localConfig.get(0);  
			int intconfig= Integer.parseInt(lconfig[0]);
			if (intconfig == 0) {
				cn.close(); 
				return false;
			} else {  
				cn.connect();
				String[] args1 = {argument};
				String[] data={"0"};  
			    cn.addUpdate(table, args1, condition, data);
			    cn.close(); 
				return true;
			}
		}
	}
	
	public static int[] stringToTime(String myTime){ 
        String[] tempArray;
 
        /* delimiter */
        String delimiter = ":";
 
        /* given string will be split by the argument delimiter provided. */
        tempArray = myTime.split(delimiter);
        if(tempArray.length==2){
        	return new int[]{returnInt(tempArray[0]),returnInt(tempArray[1])};
        }
        else return new int[]{0,0};
	}
	
	public static boolean getconfigRefresh(String module, String param){
		String argument= "value";
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="module_config";
		String condition = " param = '"+param+"' AND module='"+module+"'";
		ArrayList <String[]> localConfig = cn.select(table, argument,condition);
		if(localConfig==null | localConfig.isEmpty()){
			cn.connect();
			String[] args1 = {"module","param","value"};
			String[] data={"'"+module+"'","'"+param+"'","0"};  
		    cn.addUpdate(table, args1, condition, data); 
		    cn.close();
			return false;
		} else {
			String[] lconfig= localConfig.get(0);  
			int intconfig= Integer.parseInt(lconfig[0]);
			if (intconfig == 0) {
				return false;
			} else {  
				cn.connect();
				String[] args1 = {argument};
				String[] data={"0"};  
			    cn.addUpdate(table, args1, condition, data);
			    cn.close();
				return true;
			}
		}
	} 
	
	public static LocalDateTime getLocalDateStr(String thisdate) {  
		String [] arrOfStr = thisdate.split("\\.");  
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
		return LocalDateTime.parse(arrOfStr[0], formatter);
	}
	
	public static String getTodayDateFull() {
		return getDateMinusDaysFull(0);
	}
	
	public static String getDateMinusDaysFull(int days) {
		OffsetDateTime odt = OffsetDateTime.now( ZoneId.systemDefault() ) ;
		LocalDateTime date = LocalDateTime.now().minusDays(days); 
		DateTimeFormatter f = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG).withZone(odt.getOffset());
		return date.format(f);
	}
	
	public static String getTodayDate() {
		return getDateMinusDays(0);
	}
	
	public static void printDate() {
		System.out.println("Server Java Date: "+getDateMinusDaysFull(0));
	}
	
	public static String getDateFormated(LocalDateTime thisdate) {  
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return thisdate.format(formatter);
	}
	
	public static String getDateMinusDays(int days) {
		LocalDate date = LocalDate.now().minusDays(days); 
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
		return date.format(formatter);
	}
	
	public static int getCurrentTimeStamp(){
		Date currentdate= new Date();
		long currentTime = currentdate.getTime();
		return (int)(currentTime/1000);
	}
	
	public static byte[] getCurrentTimeStampBytes(){
		Date currentdate= new Date();
		long currentTime = currentdate.getTime();
		return intTo4Bytes((int)(currentTime/1000));
	}
	
	public static String thisDate() {
		java.util.Date dt = new java.util.Date();

		java.text.SimpleDateFormat sdf = 
		     new java.text.SimpleDateFormat("yyyy-MM-dd");

		return sdf.format(dt);
	}
	
	public static void exitApi(){
		System.out.println("Api: exit. End of program.\n ------------------- Adios --------------------");
		System.exit(1);
	}
	
	public static byte[] intTo4Bytes(int toConvert){
		byte[] byteArray = {
				(byte)(toConvert >>> 24),
	            (byte)(toConvert >>> 16),
	            (byte)(toConvert >>> 8),
	            (byte) toConvert
	    };
		return byteArray;
	}
	
	public static byte[] stringToByteArray(String toArray){
		byte[] b = toArray.getBytes(StandardCharsets.UTF_8); 
		return b;
	}
	
	public static byte[] intTo2Bytes(int toConvert){
		byte[] byteArray = { 
	            (byte)(toConvert >>> 8),
	            (byte) toConvert
	    };
		return byteArray;
	}
	
	public static Integer tryParse(String text) {
		  try {
			  text=text.replaceAll("\\s", "");
		    return Integer.parseInt(text);
		  } catch (NumberFormatException e) {
		    return 0;
		  }
		}
	
	public static Double tryParseDouble(String text) {
		  try {
			  text=text.replaceAll("\\s", "");
		    return Double.parseDouble(text);
		  } catch (NumberFormatException e) {
		    return (double) 0;
		  }
		}
	
	public static Integer tryParseHex(String text) {
		  try {
			  text=text.replaceAll("\\s", "");
		    return Integer.parseInt(text,16);
		  } catch (NumberFormatException e) {
		    return 0;
		  }
		}
	
	public static Long tryParseLong(String text) {
		  try { 
			text=text.replaceAll("\\s+", "");
		    return Long.parseLong(text);
		  } catch (NumberFormatException e) {
			e.printStackTrace();
		    return (long) 0;
		  }
		}
	
	public static int returnInt(String evaluateMe){
		int eval =0;
		if(evaluateMe==null){
			return 0;
		} else{
			try {
				evaluateMe=evaluateMe.replaceAll("\\s", "");
				eval= tryParse(evaluateMe);
			} catch (Exception e) {
				return 0;
			}
		}	
		return eval;
	}
	
	public static double returnDouble(String evaluateMe){
		Double eval =(double) 0;
		if(evaluateMe==null){
			return 0;
		} else{
			try {
				evaluateMe=evaluateMe.replaceAll("\\s", "");
				eval= tryParseDouble(evaluateMe);
			} catch (Exception e) {
				return (double) 0;
			}
		}	
		return eval;
	}
	
	public static int returnIntHex(String evaluateMe){
		int eval =0;
		if(evaluateMe==null){
			return 0;
		} else{
			try {
				evaluateMe=evaluateMe.replaceAll("\\s", "");
				eval= tryParseHex(evaluateMe);
			} catch (Exception e) {
				return 0;
			}
		}	
		return eval;
	}
	
	public static long returnLong(String evaluateMe){
		long eval =0;
		System.out.println("evaluating "+evaluateMe);
		if(evaluateMe==null){
			System.out.println("evaluation is null ");
			return 0;
		} else{
			try {
				evaluateMe=evaluateMe.replaceAll("\\s", "");
				eval= tryParseLong(evaluateMe);
			} catch (Exception e) {
				return 0;
			}
		}	
		return eval;
	}
	
	public static void indev(String sysname){
		System.out.println(" ==>> "+sysname+" System in development ");
		SysUtils.exitApi();
	}
	
	public static void noPiModeWarning(){
		System.out.println("\n\n////////////////////////////////////////////////////////////////////////////////////\n---------[ LOCAL DEBUG MODE (noPiMode=true), this is just a soft warning ]----------\n -Pi4J libraries are DEACTIVATED: \n If you are using a Rpi, THIS IS PROBABLY NOT INTENTED!! \n If so, set noPiMode to false!!!!\n Else just ignore this warning and have a nice day :)\n------------------------------------------------------------------------------------\n////////////////////////////////////////////////////////////////////////////////////\n\n");
	}
	
	public static int getRandom(int maxval){
		int  n = rand.nextInt(maxval) + 1;
		return n;
	}
	
	public static int byteToUnsignedInt(byte b) {
	    return 0x00 << 24 | b & 0xff;
	  }
	
	public static byte[] WMXDemoBytes(){
		//0xfe 0x 1 0xff 0x 2 0x 2 0x 2 0xa7 0xa7 0xfc
		byte[] thisbyte   = {(byte)0xfe,(byte)0x01 ,(byte)0xff ,(byte)0x04 ,(byte)0x04 ,(byte)0x02 ,(byte)0x00 ,(byte)0x00 ,(byte)0xfc,(byte)0xfd };
		byte[] thisbyte2  = {(byte)0xfe,(byte)0x01 ,(byte)0xff ,(byte)0x04 ,(byte)0x04 ,(byte)0x02 ,(byte)0xa7 ,(byte)0xa7 ,(byte)0xfc,(byte)0xfd };
		byte[] thisbyte3  = {(byte)0xfe,(byte)0x01 ,(byte)0xff ,(byte)0x04 ,(byte)0x04 ,(byte)0x0a ,(byte)0x01 ,(byte)0x01 ,(byte)0x0a,(byte)0x02,(byte)0x01,(byte)0xfe,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x0fe,(byte)0xfc,(byte)0xfd };
		byte[] thisbyte4  = {(byte)0xfe,(byte)0x01 ,(byte)0xff ,(byte)0x04 ,(byte)0x04 ,(byte)0x0a ,(byte)0x01 ,(byte)0x01 ,(byte)0x0a,(byte)0x02,(byte)0x01,(byte)0xfd,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x0fd,(byte)0xfc,(byte)0xfd };
		byte[] thisbyte5  = 
			{ (byte)0xfe,(byte)0x01 ,(byte)0xff ,(byte)0x04 ,(byte)0x04 ,

			(byte)0x28 ,
			(byte)0xa0 ,

			(byte)0x30 ,(byte)0x31,(byte)0x30 ,(byte)0x32,(byte)0x30 ,(byte)0x31,(byte)0x30 ,(byte)0x32,(byte)0x30 ,(byte)0x31,(byte)0x30 ,(byte)0x38,
			(byte)0x30 ,(byte)0x31,(byte)0x30 ,(byte)0x32,(byte)0x30 ,(byte)0x31,(byte)0x30 ,(byte)0x32,(byte)0x30 ,(byte)0x31,(byte)0x30 ,(byte)0x38,

			(byte)0x5B ,(byte)0xBC,(byte)0xFE ,(byte)0xF1,

			(byte)0x5B ,(byte)0xBC,(byte)0xFE ,(byte)0xF1,

			(byte)0x01 ,

			(byte)0x00,(byte)0x01,

			(byte)0x01 ,(byte)0xa0 ,(byte)0xff ,(byte)0x28 ,

			(byte)0x00,(byte)0xfd 

			};
		//,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x0fe 
		if(getRandom(100)<10)return thisbyte5;
		else if(getRandom(100)<20)return thisbyte5;
		else if(getRandom(100)<60)return thisbyte5;
		else return thisbyte5;
	}
	
	public void debugintarray(ArrayList<int[]> parametersList){
		System.out.println("\nArray -------");
		for(int i=0;i<parametersList.size();i++){			
			int[] localstring = parametersList.get(i);
			System.out.println("\nLine:"+i+ "  size : "+localstring.length);
			for(int j=0; j < localstring.length; j++){
				System.out.print(" - "+localstring[j]);
			}		
		}
		System.out.println("\nEND -------");
	}
	
	public static String getTime(){
		return new SimpleDateFormat("dd/MM/yyy HH:mm:ss").format(Calendar.getInstance().getTime());
	}
	
	public static String getEthernetAddress(){
		
		try {
			String interfaceName = "eth0";
		    NetworkInterface networkInterface;
			networkInterface = NetworkInterface.getByName(interfaceName);
			Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
		    InetAddress currentAddress;
		    currentAddress = inetAddress.nextElement();
		    while(inetAddress.hasMoreElements())
		    {
		        currentAddress = inetAddress.nextElement();
		        String ScurrentAddress = currentAddress.toString();
		        if(currentAddress instanceof Inet4Address && !currentAddress.isLoopbackAddress() && ScurrentAddress.length()!=0)
		        {
		            return ScurrentAddress;		            
		        }
		    } 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return "100.100.100.1WLAN";
		}
	    return "100.100.100.1WLAN";
	}	
	
	public static float roundnumber(float notrounded){
		BigDecimal rounded = new BigDecimal(Float.toString(notrounded));
		rounded = rounded.setScale(1,BigDecimal.ROUND_HALF_UP);
		return rounded.floatValue();
	}
	
	public static int[] bytestoInts16(byte[] bytearray){
		int buffersize = bytearray.length/2;
		int[] intBuffer = new int[buffersize];
		for(int i=0;i<buffersize;i=i+2){
			intBuffer[i/2] = ((bytearray[i] & 0xff) << 8) | (bytearray[i+1] & 0xff);
		} 		
		return intBuffer;
	}
	
	//short s = (short) ((bytes[0] & 0xff) | (bytes[1] << 8));
	
	public static short bytestoSignedInts16(byte[] bytearray){
		short s = (short) ((bytearray[0] & 0xff) | (bytearray[1] << 8));
		return s;
	}
	
	public static int[] bytestoInts32(byte[] bytearray){
		int[] intBuffer = new int[bytearray.length/4];
		for(int i=0;i<bytearray.length;i=i+4){
			intBuffer[i/4] = (((bytearray[i] & 0xff ) << 24 ) | ((bytearray[i+1] & 0xff ) << 16 ) | ((bytearray[i+2] & 0xff ) << 8 ) | (bytearray[i+3] & 0xff ));
		} 		
		return intBuffer;
	}
	
	public static boolean[] toBooleanArray(byte[] bytes) {
	    BitSet bits = BitSet.valueOf(bytes);
	    boolean[] bools = new boolean[bytes.length * 8];
	    for (int i = bits.nextSetBit(0); i != -1; i = bits.nextSetBit(i+1)) {
	        bools[i] = true;
	    }
	    return bools;
	}

	public static byte[] toByteArray(boolean[] bools) {
	    BitSet bits = new BitSet(bools.length);
	    for (int i = 0; i < bools.length; i++) {
	        if (bools[i]) {
	            bits.set(i);
	        }
	    }

	    byte[] bytes = bits.toByteArray();
	    if (bytes.length * 8 >= bools.length) {
	        return bytes;
	    } else {
	        return Arrays.copyOf(bytes, bools.length / 8 + (bools.length % 8 == 0 ? 0 : 1));
	    }
	}
	
	
	public static void sysInfo(){
		
		if(!Sigmation.noPiMode){
			String stat1= new String(), stat2 = new String();
			try{stat1= "CPU:" + roundnumber(SystemInfo.getCpuVoltage()) +" V";}
	        catch(Exception ex){
	        	currentTask=">ER 0x006       ";
				  sysInfo();
	        }
			try{stat1= stat1 + "-"+ roundnumber(SystemInfo.getCpuTemperature())+" C";}
	        catch(Exception ex){
	        	currentTask=">ER 0x007       ";
				  sysInfo();
	        }
			stat2=currentTask;
			int size1 = stat1.length();
			int size2 = stat2.length();
			if (size1>17) size1=17;
			if (size2>17) size2=17;
			
			LcdDevice.setStats(stat1.substring(0, size1), stat2.substring(0, size2));	
		}	
	}
	
	public static boolean piRFlag= false;

	public static int returnArrayIndex(ArrayList<String[]> array, String comparedToThis){	
		
		for(int i = 0 ; i< array.size(); i++){
			String[] thisArrayItem= array.get(i);
			if (thisArrayItem[0].contentEquals(comparedToThis)) return i;
		}
		return -1;
	}

	public static int printhex(byte[] toprint, int size){
		return printhex(toprint, size, false);
	}	
	
	public static int printhex(byte[] toprint, int size, boolean noheader){
		int i=0;
		for (i=0;i<size;i++){           		
    		if(noheader) System.out.print(String.format("%02x ", toprint[i]));
    		else  System.out.print(String.format("0x%02x ", toprint[i])); 
    		if(i>0){
    				if(toprint[i]==0x0D & toprint[i-1]==0x0A){
    			
    					System.out.print("\r\n");
    					return 0;
    				}
    		}
		}
		return 0;
	}
	
	public static int printchar2hex(char[] toprint, int size){
		int i=0;
		for (i=0;i<size;i++){           		
    		System.out.print(String.format("0x%2x ", toprint[i]));
    		if(i>0){
    				if(toprint[i]==0x0D & toprint[i-1]==0x0A){
    			
    					System.out.print("\r\n");
    					return 0;
    				}
    		}}
		return 0;
	}
	
	public static int printbyte2hex(byte toprint){
       		
    		System.out.print(String.format("0x%2x ", toprint));
		return 0;
	}
	
	public static String byteToStringHex(byte toFormat){
		return String.format("%1$02X", toFormat); 
	}
	
	public static float h2v(byte hextoconvert){
		float value= (((float)hextoconvert)*5/255);
		return value;
	}
	
	public static float cutDec(float tocut){
		BigDecimal bd = new BigDecimal(tocut).setScale(2, RoundingMode.HALF_EVEN);
		float value = bd.floatValue();
		return value;
	}
	
	public static boolean getRandomBoolean(boolean debug) {
		   double result = Math.random();
		   System.out.println("simple random "+result);
	       return  result < 0.5; 
	}
	
	public static boolean getRandomBoolean(double probability) {
		   double result = Math.random();
		   System.out.println("simple random "+result+" vs "+probability);
	       return  result < probability; 
	}
	
	public static boolean getRandomBoolean(long seed) {
		return getRandomBoolean(seed, 0.5);
	}
	
	public static boolean getRandomBoolean(long seed, double probability) {
		   Random rnd = new Random();
		   rnd.setSeed(seed);
		   double result = rnd.nextFloat();
		   System.out.println("seed random "+result+" < "+probability);
	       return result < probability; 
	}
	
	public static long  fromByteArray(byte[] bytes){
		return bytes[0] << 16 | (bytes[1] & 0xFF) <<0 ;
	}
	
	public static void piReset(){
		 	if(piRFlag){
				
					try {
						LcdDevice.setReboot();
						Runtime rt = Runtime.getRuntime(); 
						System.out.println("Reset ...");
						String[] cmd = {"sudo","sync"};	
						rt.exec(cmd);
						String[] cmd2 = {"sudo","reboot"};	
						rt.exec(cmd2);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}				
				System.exit(0);
			}
		}
	
	public static void Syscheck(String SerialBound){
    	try {
    		if(SystemInfo.getSerial().contentEquals(SerialBound)){
    			System.out.println("Serial check "+SystemInfo.getSerial()+" OK");
    			LcdDevice.setTask("Serial check OK");
    		} else {
    			System.out.println("Serial check "+SystemInfo.getSerial()+" Fail. Do not use this software in other computers. Distribution is prohibited");
    			System.out.println("Rebooting in 7 second");
    			LcdDevice.setTask("Serial check FAIL");
    			Thread.sleep(1000);
    			LcdDevice.setTask(" Do not use this    ");
    			Thread.sleep(1000);
    			LcdDevice.setTask(" software in other  ");
    			Thread.sleep(1000);
    			LcdDevice.setTask(" computers.         ");
    			Thread.sleep(1000);
    			LcdDevice.setTask(" Distribution is    ");
    			Thread.sleep(1000);
    			LcdDevice.setTask(" prohibited.        ");
    			Thread.sleep(1000);
    			piRFlag=true;	//Comentar para evitar reset
    			piReset();		//Comentar para evitar reset
    			System.exit(-1);//Comentar para evitar reset
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public static long currentTimeStamp() {
		return System.currentTimeMillis(); 
	}
	
	public static String currentTimeStampString() {
		return ""+System.currentTimeMillis(); 
	}
	
	public static String getUniqueId(byte headbyte) {
		return getUniqueId(byteToUnsignedInt(headbyte));
	}
	
	public static String getUniqueId(int head) {
		return head+"-"+currentTimeStamp()+"__"+getRandom(100);
	}
	
	public static String[] splitString(String toSplit, String whereToSplit){
		String[] thisStringArray = toSplit.split(whereToSplit);
		return thisStringArray;
	}
	
	public static class HardCheckConfig extends TimerTask {
		
		String module="system";
		String parname="refreshconfig";
		
		public HardCheckConfig(String module){
			this.module=module;
		}
		
		public HardCheckConfig(String module, String parname){
			this.module=module;
			this.parname=parname;
		}
		
	    public void run() {
	    	if(SysUtils.getconfigRefresh(module,parname)){
	    		//RemoteDAppi.finishThread();
	    		MessageHandler.setResetMessage();	    		
	    		LedDisplay.stopScreen();
	    		System.exit(1);
			}  
	    }
	}
	
	public static class systemScheduler{
		static ScheduledExecutorService scheduler;
		static long pHour;
		static long pMinute;
		static boolean hardReset=false;
		
		public static void init(long pHour, long pMinute, boolean hardReset){
			systemScheduler.pHour=pHour;
			systemScheduler.pMinute=pMinute;
			systemScheduler.hardReset=hardReset;
			midnightScheduler();
		} 
		
		public static void init(String pHour, String pMinute, boolean hardReset){
			systemScheduler.pHour=returnInt(pHour);
			systemScheduler.pMinute=returnInt(pMinute);
			midnightScheduler();
		} 
		
		final static Runnable runThis = new Runnable() {
		       public void run() { 
		    	   	String message = "Reset";
		    	   	if(hardReset)message = "Reboot";
		    	   	MessageHandler.setMessage(" "+message+" by     System");    		
		    		LedDisplay.stopScreen();
		    		if(hardReset)SysUtils.piReset();
		    		else System.exit(1);
		       }
		}; 
		
		private static void midnightScheduler(){  
			scheduler = Executors.newScheduledThreadPool(1);  
			Long midnight=LocalDateTime.now().until(LocalDate.now().plusDays(1).atStartOfDay().plusMinutes(1).plusHours(pHour).plusMinutes(pMinute), ChronoUnit.MINUTES); 
			scheduler.schedule(runThis, midnight, TimeUnit.MINUTES);
			System.out.println("Scheduled backup for  @"+LocalDate.now().plusDays(1).atStartOfDay().plusMinutes(1).plusHours(pHour).plusMinutes(pMinute).toString()+" - FBS");
		}
	}
	
	
	
}
