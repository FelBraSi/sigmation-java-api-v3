package sigmation.utils;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
 

public class RunShellCommandFromJava extends Thread{
	
	String[] args;
	boolean keepAlive=false;
	boolean terminate=false;
	boolean debugThis=false;
	
	public RunShellCommandFromJava(String[] args){
		this.args=args;
	}
	
	public RunShellCommandFromJava(String[] args, boolean keepAlive){
		this.args=args;
		this.keepAlive=keepAlive;
	}
	
	public void unblock(){
		keepAlive=false;
		terminate=true;
	}

    public void run(){
    	try { 
    		boolean KeepAlive = true;
    		while(KeepAlive){
    			Runtime thisRuntime = Runtime.getRuntime();
                if(debugThis) System.out.print("Starting command: '"+args[0] + "'\n");
                if(debugThis) if(args.length>1) System.out.print("Path: '"+args[1] + "'\n");
                if(debugThis) if(keepAlive) System.out.print("This process will be kept alive \n");
                Process proc;
                if(args.length>1) proc = thisRuntime.exec(args[0],null, new File(args[1])); 
                else proc = thisRuntime.exec(args[0]); 
                String line = "";
                BufferedReader reader =  
                        new BufferedReader(new InputStreamReader(proc.getInputStream()));
                /*while(proc.isAlive()) {
                	if(debugThis) while((line = reader.readLine()) != null) System.out.print(line + "\n");
                }*/
                //proc.waitFor(1,TimeUnit.SECONDS); 
                while(!terminate){
                	proc.waitFor(1,TimeUnit.SECONDS); 
                	if(debugThis) if(keepAlive) System.out.print("runner tick \n");
                }
                proc.destroy();
                //proc.waitFor(); 
                KeepAlive=keepAlive;
    		}
             
		} catch ( Exception e) {
			if(debugThis) e.printStackTrace();
		}
    	 

    }
} 