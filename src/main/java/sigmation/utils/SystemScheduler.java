package sigmation.utils; 

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

public class SystemScheduler {
	
	static BodyPart messageBodyPart = new MimeBodyPart();
	static Multipart multipart = new MimeMultipart();
	static ArrayList<String[]> options = new ArrayList<String[]>();
	
	static ScheduledExecutorService scheduler;
	static ScheduledExecutorService confscheduler;
	
	static String ftpAddress= "localhost"; 
	static String username= "user";
	static String originfilename= "sigmation.sql";
	static String password="pass";
	static String originpath= "/var/www/html/generated/";
	static String destinypath= "/";
	static String hour= "00:00";
	static int enabled= 0;
	static String generatedString= "ServerFault";
	static int    pHour = 0;
	static int 	  pMinute=0;
	static Session session;
	static Message message;
	static boolean sentFile=false;  
	
	public static void init() {
		Thread thread = new Thread(){
		    public void run(){
		    	getConfVars();
		    	if(enabled>0) todayScheduler();
		    	scheduleConfigCheck(); 
		    }
		  };
		thread.start();
	} 
	
	//Transport.send(message);
	
	private static void setReportTime(){
		int[] timeToSet=SysUtils.stringToTime(hour);
		pHour=timeToSet[0];
		pMinute=timeToSet[1];
	}
	
	private static void todayScheduler(){
		sentFile=false;
		scheduler = Executors.newScheduledThreadPool(1);  
		generatedString="System Programmed Report";
		Long midnight=LocalDateTime.now().until(LocalDate.now().atStartOfDay().plusMinutes(1).plusHours(pHour).plusMinutes(pMinute), ChronoUnit.MINUTES);
		scheduler.schedule(backupMaker, midnight, TimeUnit.MINUTES);
		System.out.println("Scheduled backup for  @"+LocalDate.now().atStartOfDay().plusMinutes(1).plusHours(pHour).plusMinutes(pMinute).toString()+" - FBS");
	}
	
    private static void midnightScheduler(){
    	sentFile=false;
    	generatedString="System Programmed Report";
		scheduler = Executors.newScheduledThreadPool(1);     
		Long midnight=LocalDateTime.now().until(LocalDate.now().plusDays(1).atStartOfDay().plusMinutes(1).plusHours(pHour).plusMinutes(pMinute), ChronoUnit.MINUTES);
		scheduler.schedule(backupMaker, midnight, TimeUnit.MINUTES);
		System.out.println("Scheduled backup for  @"+LocalDate.now().plusDays(1).atStartOfDay().plusMinutes(1).plusHours(pHour).plusMinutes(pMinute).toString()+" - FBS");
	}

	
	private static void scheduleNow(String Reason){ 
		//doSendMail(Reason);
		todayScheduler();
	}
	
	private static void scheduleConfigCheck(){
		confscheduler = Executors.newScheduledThreadPool(1);      
		Long perseconds=LocalDateTime.now().until(LocalDateTime.now(), ChronoUnit.MINUTES);
		confscheduler.scheduleAtFixedRate(configCheck, perseconds, 20, TimeUnit.SECONDS); 
	}
	
	final static Runnable backupMaker = new Runnable() {
	       public void run() { 
	    	   	DumpDB.export();
	    	   	FtpFile.Upload(ftpAddress, username, password, originfilename, originpath, destinypath,enabled);
	    	   	sentFile=true;
	    	   }
	}; 
	
	final static Runnable configCheck = new Runnable() {
	       public void run() { checkConfig(); }
	}; 
	
	private static void checkConfig(){ 
		if(SysUtils.getconfigRefresh("sys_backup","refreshconfig")){
			getConfVars();
			restartScheduleNow(); 
		}
		if(sentFile && enabled>1){
			System.out.println("daily configured");
			midnightScheduler();
		}
	}
	
	private static void restartScheduleNow(){
		scheduler.shutdownNow();
		while(!scheduler.isShutdown()){}
		scheduleNow("New Configuration");
	}
 
	
	public static void getConfVars(){
		MySQL cr = new MySQL();
		cr.database="config";	
		String table="backupconf";
		String condition = " 1 LIMIT 1";
		cr.connect();  
		String argument= " * ";
		ArrayList<String[]> result = cr.select(table, argument, condition);
		//FtpFile.Upload(ftpAddress, username, password, originfilename, originpath, destinypath);
		try {
			for(String[] subresult : result){		
				ftpAddress= subresult[1];
				username= subresult[2];
				password= subresult[3];
				originfilename= subresult[4];
				originpath= subresult[5];
				destinypath=subresult[6];
				enabled= SysUtils.returnInt(subresult[7]); 
				hour= subresult[8];
				setReportTime();
				System.out.println(subresult[1]+" , "+subresult[2]+" , "+subresult[3]+" , "+subresult[4]+" , "+subresult[5]+" , "+subresult[6]+" , "+subresult[7]+" , "+subresult[8]);
			}
		} catch (Exception e) {
			System.out.println("failed to get scheduler vars, MYSQL conection is probably down");
		}
	} 
}