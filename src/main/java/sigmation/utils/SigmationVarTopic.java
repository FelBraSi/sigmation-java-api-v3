package sigmation.utils;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public class SigmationVarTopic {
	private String baseTopic="sigmation";
	private String varname="variable"; //group
	private String group="nogroup"; 
	private String resourceGroup="nogroup";
	private String resourceName="noName";
	private boolean isSubElement=false;
	private MqttMessage setval;
	private MqttMessage currentval;
	private MqttMessage valueval;
	private String varTopicId = null;
	
	public SigmationVarTopic() {}
	
	public void setBaseTopic(String baseTopic){
		this.baseTopic=baseTopic;
	}
	
	public void setSubElement(boolean isSubElement){
		this.isSubElement=isSubElement;
	}
	
	public void setVarTopicId(String varTopicId){
		this.varTopicId=varTopicId;
	}
	public void setResourceGroup(String resourceGroup){
		this.resourceGroup=resourceGroup;
	}
	public void setGroup(String group){
		this.group=group;
	}
	public void setVarName(String varname){
		this.varname=varname;
	}//resourceName
	public void setResourceName(String resourceName){
		this.resourceName=resourceName;
	}
	public void setVarSet(MqttMessage setval){
		this.setval=setval;
	}
	public void setCurrentVal(MqttMessage currentval){
		this.currentval=currentval;
	}
	public void setValueVal(MqttMessage valueval){
		this.valueval=valueval;
	}
	
	public String getBaseTopic(){
		return baseTopic;
	}
	public String getResourceGroup(){
		return resourceGroup;
	}
	public String getGroup(){
		return group;
	}
	public String getVarName(){
		return varname;
	}
	public MqttMessage getVarSet(){
		return setval;
	}
	public String getResourceName(){
		return resourceName;
	}
	public boolean isSubElement( ){
		return isSubElement;
	}
	
	public byte[] getCurrentVal(){
		return currentval.getPayload();
	}
	
	public String getCurrentValString(){
		return new String(currentval.getPayload());
	}
	
	public MqttMessage getValueVal(){
		return valueval;
	}
	
	public String getSetTopic() {
		return baseTopic+"/__var/"+varname+"/"+"__set";
	}
	
	public String getVarTopicId(){
		return varTopicId;
	}
	
	public String getCurrentTopic() {
		return baseTopic+"/__var/"+varname+"/"+"__current";
	}
	
	public String getValueTopic() {
		return baseTopic+"/__var/"+varname+"/"+"__value";
	}
}