package sigmation.utils;

public class Definitions {
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 	SYSTEM PARAMETERS, DONT CHANGE THIS UNLESS YOU KNOW WHAT YOU ARE DOING	//
	 *      -FBS-																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 */
	
	// Sigmation server systems
	public final static int ApiSystem_tag  		= 1;     	// sistema tag
	public final static int ApiSystem_msg  		= 2;		// sistema mensajes
	public final static int ApiSystem_vent 		= 3;		// ventilacion
	public final static int ApiSystem_drfp 		= 4;		// diagnostico fuentes
	public final static int ApiSystem_dram 		= 5;		// diagnostico amp.
	public final static int ApiSystem_fsys 		= 6;		// firewatcher
	public final static int ApiSystem_mb_dolf  	= 7;		// dolf
	public final static int ApiSystem_drs		= 8;		// DRS
	public final static int ApiSystem_mb_dolfv2	= 9;		// dolf version 2
	// Other systems, start from 100+
	public final static int ApiSystem_r_limit  	= 100;		//limite inferior para servicios remotos
	public final static int ApiSystem_r_tgsrv  	= 101;		//Remote Tag Server
	
	// Sigmation server versions (hardware)
	public final static int sversion_stel_1  	= 0;     	// Sigma telecom v1
	public final static int sversion_stel_2  	= 1;		// Sigma telecom v2
	public final static int sversion_stel_3_wmx	= 2;		// Sigma telecom v3 + wmx, using Raveon modems
	public final static int sversion_ssa_2 		= 3;		// Sigma sudafrica v2 
	//Other systems, start from 100+
	public final static int sversion_r_tag_1   	= 101;		//Remote Tag Server	
	
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 		MySQL Sentences													    //
	 *           																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 */	
	
	public class MySQLSentences{
		
		public static final String createSystemIniDatabase = "CREATE TABLE IF NOT EXISTS `config`.`init_config` (`parameter` VARCHAR(20) NOT NULL,`value` VARCHAR(20) NOT NULL DEFAULT '0', `det` VARCHAR(200) NULL DEFAULT NULL, PRIMARY KEY (`parameter`),UNIQUE INDEX `parameter` (`parameter` ASC))";
		public static final String insertSystemIni ="INSERT IGNORE INTO `config`.`init_config` (`parameter`, `value`, `det`) VALUES ('ApiSystem', '1', 'Aplicacion Activa'),('sversion', '2', 'Version Hardware' ),('wmx', '1', 'Modo RF WMX'),('nopimode', '1', 'Desactivar uso GPIO')";
		
		public static final String createSystemDefinitions = "CREATE TABLE IF NOT EXISTS `config`.`sys_definitions` (   `parameter` VARCHAR(20) NOT NULL,  `value` VARCHAR(20) NOT NULL DEFAULT '0',  `det` VARCHAR(200) NULL DEFAULT NULL,  `module` VARCHAR(45) NULL,  PRIMARY KEY (`parameter`), UNIQUE INDEX `parameter` (`parameter` ASC))";
		public static final String insertSystemDefinitions ="INSERT IGNORE INTO `config`.`sys_definitions` (`parameter`, `value`, `det`, `module`) VALUES ('ApiSystem_tag', '"+ApiSystem_tag+"', 'Sistema Tag', 'apiSystem'),('ApiSystem_msg', '"+ApiSystem_msg+"', 'Sistema Mensajes', 'apiSystem'),('ApiSystem_vent', '"+ApiSystem_vent+"', 'Sistema Ventilacion', 'apiSystem'),('ApiSystem_drfp', '"+ApiSystem_drfp+"', 'Diagnostico Remoto Fuentes', 'apiSystem'),('ApiSystem_dram', '"+ApiSystem_dram+"', 'Diagnostico Remoto Amp.', 'apiSystem'),('ApiSystem_fsys', '"+ApiSystem_fsys+"', 'Firewatcher', 'apiSystem'),('ApiSystem_mb_dolf','"+ApiSystem_mb_dolf+"', 'DOLF', 'apiSystem'),('ApiSystem_r_tgsrv', '"+ApiSystem_r_tgsrv+"', 'Remote Tag Server', 'apiSystem'),('sversion_stel_1', '"+sversion_stel_1+"', 'Sigma Telecom V1.0', 'hwver'),('sversion_stel_2', '"+sversion_stel_2+"', 'Sigma Telecom V2.0', 'hwver'),('sversion_stel_3_wmx', '"+sversion_stel_3_wmx+"', 'Sigma Telecom V3.0 WMX', 'hwver'),('sversion_ssa_2', '"+sversion_ssa_2+"', 'Sigma Sudafrica V2.0', 'hwver'),('sversion_r_tag_1', '"+sversion_r_tag_1+"', 'Remote Tag Server V1.0', 'hwver')";
	}
	
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 		Tag System															//
	 *           																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 */	
	
	public class TNT{
		public static final byte serverResquestsTag   	= (byte) 0x00;
		public static final byte serverNextTag	  	 	= (byte) 0x01;  
		public static final byte serverNextBusTag  	 	= (byte) 0x02;
		public static final byte serverPing		  		= (byte) 0x03;
		public static final byte serverBusTag		  	= (byte) 0x04;
		
		// Same commands but received through Ethernet, the system acts differently when working through Ethernet
		public static final byte serverResquestsTag_R  	= (byte) 0xA0;
		public static final byte serverNoTag_R		  	= (byte) 0xA1;
		public static final byte serverPing_R		  	= (byte) 0xA2;
		
		public static final byte serverBusTag_R		  	= (byte) 0xA3;
		
		public static final byte thisAppid				= (byte) ApiSystem_tag;
		
		public static final byte MasterAddress= (byte)0xff;
		
		public static final String thisDeviceTable="rfidreaders";
		public static final String thisDeviceIndex="serial";
	}

	
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 		MODBUS																//
	 *           																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 */	
	 	
	public static class Modbus {
		/**======== FUNCTIONS =================================================================*/
		
		public static final byte READ_COIL_STATUS  			= (byte)0x01;
		public static final byte READ_INPUT_STATUS 			= (byte)0x02;
		public static final byte READ_HOLDING_REGISTERS		= (byte)0x03;
		public static final byte READ_INPUT_REGISTERS		= (byte)0x04;
		public static final byte FORCE_SINGLE_COIL			= (byte)0x05;
		public static final byte PRESET_SINGLE_REGISTER 	= (byte)0x06;
		public static final byte READ_EXCEPTION_STATUS  	= (byte)0x07;
		public static final byte DIAGNOSTICS				= (byte)0x08;
		public static final byte PROGRAM_484				= (byte)0x09;
		public static final byte POLL_484					= (byte)0x0A;
		public static final byte FETCH_COM_EVENT_CMTR		= (byte)0x0B;
		public static final byte FETCH_COM_EVENT_LOG		= (byte)0x0C;
		public static final byte PROGRAM_CONTROLLER			= (byte)0x0D;
		public static final byte POLL_CONTROLLER 			= (byte)0x0E;
		public static final byte FORCE_MULTIPLE_COILS 		= (byte)0x0F;
		public static final byte PRESET_MULTIPLE_REGISTERS 	= (byte)0x10;
		public static final byte REPORT_SLAVE_ID		    = (byte)0x11;
		public static final byte PROGRAM_884_M84	 		= (byte)0x12;
		public static final byte RESET_COMM_LINK 			= (byte)0x13;
		public static final byte READ_GENERAL_REFERENCE    	= (byte)0x14;
		public static final byte WRITE_GENERAL_REFERENCE   	= (byte)0x15;	
		
		/**======== ERROR RESPONSES =================================================================*/
		
		public static final byte E_READ_COIL_STATUS  		= READ_COIL_STATUS			+(byte)0x80;
		public static final byte E_READ_INPUT_STATUS 		= READ_INPUT_STATUS			+(byte)0x80;
		public static final byte E_READ_HOLDING_REGISTERS	= READ_HOLDING_REGISTERS	+(byte)0x80;
		public static final byte E_READ_INPUT_REGISTERS		= READ_INPUT_REGISTERS		+(byte)0x80;
		public static final byte E_FORCE_SINGLE_COIL		= FORCE_SINGLE_COIL			+(byte)0x80;
		public static final byte E_PRESET_SINGLE_REGISTER 	= PRESET_SINGLE_REGISTER	+(byte)0x80;
		public static final byte E_READ_EXCEPTION_STATUS  	= READ_EXCEPTION_STATUS		+(byte)0x80;
		public static final byte E_DIAGNOSTICS				= DIAGNOSTICS				+(byte)0x80;
		public static final byte E_PROGRAM_484				= PROGRAM_484				+(byte)0x80;
		public static final byte E_POLL_484					= POLL_484					+(byte)0x80;
		public static final byte E_FETCH_COM_EVENT_CMTR		= FETCH_COM_EVENT_CMTR		+(byte)0x80;
		public static final byte E_FETCH_COM_EVENT_LOG		= FETCH_COM_EVENT_LOG		+(byte)0x80;
		public static final byte E_PROGRAM_CONTROLLER		= PROGRAM_CONTROLLER		+(byte)0x80;
		public static final byte E_POLL_CONTROLLER 			= POLL_CONTROLLER			+(byte)0x80;
		public static final byte E_FORCE_MULTIPLE_COILS 	= FORCE_MULTIPLE_COILS		+(byte)0x80;
		public static final byte E_PRESET_MULTIPLE_REGISTER	= PRESET_MULTIPLE_REGISTERS +(byte)0x80;
		public static final byte E_REPORT_SLAVE_ID		    = REPORT_SLAVE_ID			+(byte)0x80;
		public static final byte E_PROGRAM_884_M84	 		= PROGRAM_884_M84			+(byte)0x80;
		public static final byte E_RESET_COMM_LINK 			= RESET_COMM_LINK			+(byte)0x80;
		public static final byte E_READ_GENERAL_REFERENCE  	= READ_GENERAL_REFERENCE	+(byte)0x80;
		public static final byte E_WRITE_GENERAL_REFERENCE 	= WRITE_GENERAL_REFERENCE	+(byte)0x80;	
		
		/**======== CUSTOM FUNCTIONS =================================================================*/

		public static final byte GET_AND_SET_TIMESTAMP    = 0x40;
		public static final byte GET_NEXT_DELETE_LAST     = 0x41;
		
		/**======== CUSTOM ERROR RESPONSES =================================================================*/
		
		public static final byte E_GET_AND_SET_TIMESTAMP  = GET_AND_SET_TIMESTAMP	+(byte)0x80;
		public static final byte E_GET_NEXT_DELETE_LAST   = GET_NEXT_DELETE_LAST	+(byte)0x80;
		
		/**======== ERROR CODES =================================================================*/
		
		public static final byte EC_ILLEGAL_FUNCTION		= (byte) 0x01;
		public static final byte EC_ILLEGAL_DATA_ADDRESS	= (byte) 0x02;
		public static final byte EC_ILLEGAL_DATA_VALUE		= (byte) 0x03;
		public static final byte EC_SLAVE_DEVICE_FAILURE	= (byte) 0x04;
		public static final byte EC_ACKNOWLEDGE				= (byte) 0x05;
		public static final byte EC_SLAVE_DEVICE_BUSY		= (byte) 0x06;  
		public static final byte EC_NEGATIVE_ACKNOWLEDGE	= (byte) 0x07;
		public static final byte EC_MEMORY_PARITY_ERROR		= (byte) 0x08;
		public static final byte EC_GATEWAY_PTH_UNAVAIL		= (byte) 0x0A;
		public static final byte EC_GTW_TG_DEV_FAIL_RESP	= (byte) 0x0B;
		
		/**======== CUSTOM ERROR CODES =================================================================*/
		public static final byte EC_REMOTE_DEVICE_OFFLINE	= (byte) 0x0C;
	}
}
