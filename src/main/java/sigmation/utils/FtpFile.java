package sigmation.utils;


import org.apache.commons.net.ftp.FTPClient;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
 
public class FtpFile {
	
    public static void Upload(String ftpAddress, String username, String password, String originfilename, String originpath, String destinypath, int enabled) {

    	Date dateNow = new Date();
		SimpleDateFormat dateformatyyyyMMdd = new SimpleDateFormat("yyyyMMdd");
		String date_to_string = dateformatyyyyMMdd.format(dateNow); 
    	FTPClient client = new FTPClient();
        FileInputStream fis = null;
        try {
        		client.connect(ftpAddress);
        		client.login(username, password); 
        		// Create an InputStream of the file to be uploaded 
        		String fullname= originpath+originfilename;
        		fis = new FileInputStream(fullname);
        		// Store file on server and logout 
        		client.changeWorkingDirectory(destinypath);
        		if(enabled>2 || enabled==1) client.storeFile(date_to_string+"_"+originfilename, fis); 
        		else client.storeFile(originfilename, fis); 
        		client.logout(); 
        		
        } catch (Exception e) { 
        	System.out.println("\n COULD NOT CONNECT TO FTP SERVER");

        } finally {
        	try {
        			if (fis != null) {
        				 fis.close();
        			} 
        			client.disconnect(); 
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }

        }
        System.out.println("\n File Sent to Destination");
    }
    
    public static void main(String[] args) {
    	Upload("localhost","sdevelopment","sigmati0n", "sigmation.sql","/var/www/html/","Escritorio",1);
    }
    
}