package sigmation.utils;
 
import sigmation.main.Sigmation;
import sigmation.modbus.ModbusOEth;

public class tagTcpMain {
	
	//static byte[] commanding = {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x06,(byte)0x01,(byte)0x03,(byte)0x00,(byte)0x6B,(byte)0x00,(byte)0x03,(byte)0x17,(byte)0x74};
	//static byte[] commanding = {(byte)0xbb,(byte)0x17,(byte)0x02,(byte)0x00,(byte)0x00,(byte)0x19,(byte)0x0D,(byte)0x0A};
	static byte[] commanding = {(byte)0x30,(byte)0x31,(byte)0x32,(byte)0x33,(byte)0x34};
	static String noResponse = "No response received from source";
	static String noData = "No data received from source";
	
	// tag "192.168.60.235" 8000, modbus "192.168.60.205" 502

	public static void main(String[] args) {
		
		while(!Sigmation.resetSignal){
			try { 
				/*
				TcpIpHandler myconnection = new TcpIpHandler("192.168.60.228", 9001);
				byte[] response = myconnection.askAndClose(commanding);	
				if(response!= null){
					System.out.println("Received "+ response.length + "bytes of data");
				} else if (myconnection.getData_received()){
					System.out.println(noData);
				} else {
					System.out.println(noResponse);
				}*/
				
				ModbusOEth mymodbus = new ModbusOEth("192.168.60.228", 9001,false);
				//mymodbus.readHoldingRegisters((byte)1, 0x006b, 1);
				//mymodbus.writeSingleRegister((byte)1, 242, 500);
				mymodbus.readHoldingRegisters((byte)1, 242, 1);
				byte[] mybytes = mymodbus.askAndClose();
				if(mymodbus.hasData()){
					System.out.println("----------PACKET----------- ");
					System.out.println("address "+mymodbus.responseAddress);
					System.out.println("function "+mymodbus.responseFunction);
					System.out.println("quantity "+mymodbus.responsedataQuantity);
					System.out.println("--------------------- ");
				}
				
			} catch (Exception e) {
				System.out.println("CANT CONNECT: RETRYING");
			}
			
		}
		
	}

}
