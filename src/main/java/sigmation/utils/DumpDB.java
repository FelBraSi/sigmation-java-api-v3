package sigmation.utils;

import java.io.File;
import java.io.InputStream;
import java.io.PrintStream; 

import java.util.Date; 
import java.text.SimpleDateFormat; 
public class DumpDB {

	private static String ip="localhost";
//	private static String port="3306";
	private static String databases="ampdb config userdb";
	private static String user="root";
	private static String pass="orca";
	private static String path="/var/www/html/generated/";
	
	public void export(String ip, String port, String databases, String user, String pass, String path){
		DumpDB.ip=ip;
//		DumpDB.port=port; 
		DumpDB.path=path;
		export();
	}
	
	public static void export()
	{
		Date dateNow = new Date();
		SimpleDateFormat dateformatyyyyMMdd = new SimpleDateFormat("yyyyMMdd");
		String date_to_string = dateformatyyyyMMdd.format(dateNow);
		System.out.println("date into yyyyMMdd format: " + date_to_string);
		String ss="sigmation.sql";
		String fullName  = path + ss;
		String dumpCommand = "mysqldump --databases " + databases + " -h " + ip + " -u " + user +" -p" + pass;
		Runtime rt = Runtime.getRuntime();
		File test=new File(fullName);
		PrintStream ps;
		try{
			Process child = rt.exec(dumpCommand);
			ps=new PrintStream(test);
			InputStream in = child.getInputStream();
			int ch;
			while ((ch = in.read()) != -1) {
				ps.write(ch);
				//System.out.write(ch); //to view it by console
			}
			
			InputStream err = child.getErrorStream();
			while ((ch = err.read()) != -1) {
			System.out.write(ch);
			}
		}catch(Exception exc) {
		exc.printStackTrace();
		}
	} 
}