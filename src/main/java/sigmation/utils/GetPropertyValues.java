package sigmation.utils;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL; 
import java.util.Properties;
import sigmation.utils.PromptColors;
import sigmation.main.Sigmation;
  
public class GetPropertyValues {
	String result = "";
	InputStream inputStream;
	Properties prop = new Properties();
 
	public String getPropValues() throws IOException {
 
		try {
			
			String propFileName = "config.properties"; 
			URL url = this.getClass().getProtectionDomain().getCodeSource().getLocation();
			File file = new File( url.getPath( ) );
	        // get the parent of the file
	        String parentPath = file.getParent( ); 			
			String totalpath= parentPath+"/"+propFileName;
			System.out.println("path "+totalpath);
			inputStream = new FileInputStream(totalpath);
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			} 
			printProps();
			// get the property value and print it out
			String SysVer 		= prop.getProperty("SysVer");  
			result = "SysVer  " + SysVer  ;
			SysVer 		= prop.getProperty("nopimode");  
			result = "Nopimode  " + SysVer  ;
			
			boolean debugLoad=false;
			if(prop.getProperty("DebugLoad","false").equalsIgnoreCase("true")){
				debugLoad=true;
			} else debugLoad=false;
			
			boolean defaultConf=false;
			if(prop.getProperty("DefaultConfig","false").equalsIgnoreCase("true")){
				defaultConf=true;
			} else defaultConf=false;
			
			if(!defaultConf){
				Sigmation.user		=prop.getProperty("MySQLUserLocal",Sigmation.user);  
				Sigmation.pass		=prop.getProperty("MySQLPasswordLocal",Sigmation.pass);
				Sigmation.remoteuser	=prop.getProperty("MySQLUserRemote",Sigmation.remoteuser);
				Sigmation.remotepass	=prop.getProperty("MySQLPasswordRemote",Sigmation.remotepass);
				Sigmation.midnightReset = getBoolean("midnightReset", false);
				if (Sigmation.midnightReset){
					Sigmation.hardReset = getBoolean("hardReset", false);
					Sigmation.hour[0]=getInt("reset_hour", 0);
					Sigmation.hour[1]=getInt("reset_minute", 0);
					if (Sigmation.hour[0]>23) Sigmation.hour[0]=23;
					if (Sigmation.hour[1]>59) Sigmation.hour[1]=59;
				}
				
				if(prop.getProperty("nopimode","false").equalsIgnoreCase("true")){
					Sigmation.noPiMode=true;
				} else Sigmation.noPiMode=false;
				
				Sigmation.ApiSystem = SysUtils.returnInt(prop.getProperty("ApiSystem",""+Sigmation.ApiSystem));
				Sigmation.serverVersion=SysUtils.returnInt(prop.getProperty("serverhw",""+Sigmation.serverVersion));
				
				if(prop.getProperty("WMX","true").equalsIgnoreCase("true")){
					Sigmation.WMX=true;
				} else Sigmation.WMX=false;
				
				if(prop.getProperty("rs485Ctrl","true").equalsIgnoreCase("true")){
					Sigmation.rs485Ctrl=true;
				} else Sigmation.rs485Ctrl=false;
			} else System.out.println("File config has been ignored");
						
			if(debugLoad){
				System.out.println(" user:"+Sigmation.user+" \n pass:"+Sigmation.pass+" \n remoteuser:"+Sigmation.remoteuser+" \n remotepass:"+Sigmation.remotepass+" \n noPiMode:"+Sigmation.noPiMode+" \n ApiSystem:"+Sigmation.ApiSystem+" \n"+" \n serverVersion:"+Sigmation.serverVersion+" \n"+" \n WMX:"+Sigmation.WMX+" \n");
			}
			System.out.println(result);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return result;
	}
	
	public void printProps() {
		//System.out.println(PromptColors.ANSI_BLUE_BACKGROUND+" "+PromptColors.ANSI_WHITE+" "+prop.stringPropertyNames());
		System.out.println(PromptColors.ANSI_WHITE+" "+prop.stringPropertyNames());
	}
	
	public boolean getBoolean(String varname, boolean defaultValue){
		boolean thisBoolean=false;
		String thisBool=""+defaultValue;
		if(prop.getProperty(varname,thisBool).equalsIgnoreCase("true")){
			thisBoolean=true;
		} else thisBoolean=false;
		return thisBoolean;
	}
	
	public int getInt(String varname, int defaultValue){
		return SysUtils.returnInt(prop.getProperty(varname,""+defaultValue));
	}
	
	public long getLong(String varname, long defaultValue){
		return SysUtils.returnLong(prop.getProperty(varname,""+defaultValue));
	}
	
	public double getDouble(String varname, double defaultValue){
		return SysUtils.returnDouble(prop.getProperty(varname,""+defaultValue));
	}
	
	public String getString(String varname, String defaultValue){
		return prop.getProperty(varname,defaultValue).replaceAll("\\s+","");
	}
}