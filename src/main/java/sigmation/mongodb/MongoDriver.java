package sigmation.mongodb; 

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.eclipse.paho.client.mqttv3.MqttClient; 
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import sigmation.utils.ReadConfig;
import sigmation.utils.SysUtils;

/**
 * 
 *
 */
public class MongoDriver  
{	 
	private static String mongoServer="mongodb://127.0.0.1:3001/meteor";
	private static String mongoHost="127.0.0.1";
	private static int mongoPort=3001;
	private static String databaseName= "meteor"; 
	private static String collectionName= "mqttcollection";  
    static MqttClient publisher;
    MqttDeliveryToken token; 
    
    static Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    
    /**
     * 
     * SmgSys: Sigla del sistema (3 letras)
     * res: Recurso a utilizar (dev por defecto)
     * param: parametro a escuchar (set por defecto)
     * 
     * */ 
    
    public static void start(String mongoServer, String databaseName,String collectionName) {
    	MongoDriver.mongoServer = mongoServer;
    	MongoDriver.databaseName = databaseName;
    	MongoDriver.collectionName = collectionName; 
    	getProperties();
    	start();
    }
    
    private static void getProperties(){
		String prefix="mongo";
		//ReadConfig.properties.printProps();
		String thisMode = ReadConfig.properties.getString(prefix+"Mode", "development");
		if(thisMode.toLowerCase().contentEquals("deploy")) {
			mongoHost=ReadConfig.properties.getString(prefix+"Address", mongoHost);
			mongoPort=SysUtils.returnInt(ReadConfig.properties.getString(prefix+"Port", ""+mongoPort));
			databaseName=ReadConfig.properties.getString(prefix+"DB", databaseName);
			String thisMongoAddress = "mongodb://"+mongoHost+":"+mongoPort+"/"+databaseName;
			MongoDriver.mongoServer = thisMongoAddress;
		}
		System.out.println("Mongodb "+MongoDriver.mongoServer);
	}
	
	public static void start() { 
    	MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);
    	MongoDatabase database = mongoClient.getDatabase(databaseName); 
    	MongoCollection<Document> collection = database.getCollection(collectionName);
    	FindIterable<Document> cursor = collection.find();
    	Block<Document> thisblock= new Block<Document>() {

			public void apply(Document t) {
				Object thisstring= t.get("topic");
				System.out.println("topic "+thisstring);
			}
    		
		};
   	 	cursor.forEach(thisblock);  
    	mongoClient.close();
	}
	
	private static class MongoBuffer{
		public static ArrayList<String[]> result = new ArrayList<String[]>();
	}
	
	public List<Document> getObjects(String collectionName, final String[] items) { 
		try {
			MongoBuffer.result.clear();
			MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);
	    	MongoDatabase database = mongoClient.getDatabase(databaseName); 
	    	MongoCollection<Document> collection = database.getCollection(collectionName); 
	    	FindIterable<Document> cursor = collection.find();
	    	List<Document> thisItems = (List<Document>) cursor.into(new ArrayList<Document>());    
	    	mongoClient.close();
	    	return thisItems;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	public List<Document> getObjects(String collectionName, final String[] items, String filterName, Object filterValue) { 
		try {
			MongoBuffer.result.clear();
			MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);
	    	MongoDatabase database = mongoClient.getDatabase(databaseName); 
	    	MongoCollection<Document> collection = database.getCollection(collectionName);
	    	Bson bsonFilter = Filters.eq(filterName, filterValue);
	    	FindIterable<Document> cursor = collection.find(bsonFilter);
	    	List<Document> thisItems = (List<Document>) cursor.into(new ArrayList<Document>());    
	    	mongoClient.close();
	    	return thisItems;
		} catch (Exception e) {
			return null;
		}
	}
	
	public boolean updateOneDocument(String _id, String collectionName, String item, String value, String type) {
		try {
			MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);
			MongoDatabase database = mongoClient.getDatabase(databaseName); 
			MongoCollection<Document> collection = database.getCollection(collectionName);
			BasicDBObject newDocument = new BasicDBObject();
			if(type.toLowerCase().contentEquals("string")) newDocument.append("$set", new BasicDBObject().append(item,value));	
			else if(type.toLowerCase().contentEquals("boolean")) {
				boolean boolval= false;
				if(value.toLowerCase().contentEquals("true")) boolval=true;
				newDocument.append("$set", new BasicDBObject().append(item,boolval));
			} else 	if(type.toLowerCase().contentEquals("int")) {
				int intval= SysUtils.returnInt(value);
				newDocument.append("$set", new BasicDBObject().append(item,intval));
			} else 	if(type.toLowerCase().contentEquals("bigint")) {
				String intval= value;
				newDocument.append("$set", new BasicDBObject().append(item,intval));
			}
			BasicDBObject searchQuery = new BasicDBObject().append("_id", _id);
			collection.updateOne(searchQuery, newDocument);
			mongoClient.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 *  Object array List element = # {nameOfVariable, variableValue}
	 * 
	 * */
	public boolean insertOneDocument(String collectionName, ArrayList<Object[]> objectToInsert) {
		try {
			Document documentToInsert = new Document();
			if(objectToInsert.size()==0) return false;
			for(Object[] thisObject : objectToInsert) {
				documentToInsert.append(thisObject[0].toString(), thisObject[1]);
			}
			return insertOneDocument(collectionName, documentToInsert);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean insertOneDocument(String collectionName, Document documentToInsert) {
		try {
			MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);
			MongoDatabase database = mongoClient.getDatabase(databaseName); 
			MongoCollection<Document> collection = database.getCollection(collectionName);
			collection.insertOne(documentToInsert);
			mongoClient.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	private static void nologs() {
		root.setLevel(Level.ERROR);
	}
	
	static {
		nologs();
	}
	
	public MongoDriver() {
		
	}    
	
	public MongoDriver(String mongoServer, String databaseName,String collectionName) {
		MongoDriver.mongoServer = mongoServer;
    	MongoDriver.databaseName = databaseName;
    	MongoDriver.collectionName = collectionName; 
    	getProperties();
	}   
	
	public MongoDriver(String mongoServer, String databaseName) {
		MongoDriver.mongoServer = mongoServer;
    	MongoDriver.databaseName = databaseName; 
    	getProperties();
	}   
}
