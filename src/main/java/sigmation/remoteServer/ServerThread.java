package sigmation.remoteServer;

import java.io.BufferedOutputStream; 
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException; 
import java.math.BigInteger; 
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.xml.bind.DatatypeConverter;

import sigmation.io.ComLayer;
import sigmation.utils.SysUtils;
import sigmation.main.Sigmation;
import sigmation.tagTcp.RFThread;

public class ServerThread extends Thread{  

    String line=null;
    DataOutputStream os = null;
    DataInputStream is = null;
    Socket s=null;
    Socket localSocket = null;
    String localsname;
    int localp;
    BufferedOutputStream out;
    boolean tx = false;
	
    public String groupId="0";


    /** 
     * Packethandler( String ip/url, int port number);
     * default timeout is 20 ms.
     * Packet is init() at creation, you don't need to init() the object unless
     * you close() it.
     * use Packethandler( String ip/url, int port number, int timeout )
     * to setup a custom timeout in ms.
     * Or use the setTimeOut functions to set up the timeout in
     * seconds, milliseconds, microseconds or nanoseconds. 
     * */

    private boolean initialized = false;
    private int debug_level =0;
    public  boolean debugthis = true;
    private int system_type=0;
        
    private String noinitMessage= "PacketHandler must be init() first";
        
    byte[] databuffer;
    private boolean data_received=false;
        
    private long start_time = 0;
    private long deltatime_ns = 1000 * 1000000;	//default value for timeout, *1000000 if milliseconds, default = 20 milliseconds
    
    private long start_time_beacon = 0;
    private long deltatime_ms_beacon = 60000 * 1000;	//default value for timeout, *1000000 if milliseconds, default = 20 milliseconds
    
    private String thisClient ="";
    private int mode=0;
    
    private int nextmove=0;
        
    /*----PROTOCOL ----*/
    byte header = (byte)0xbb;
    byte tail1 = (byte)0x0d;
    byte tail2 = (byte)0x0a;
    
    int timeoutMiliSeconds = 20000;
        
    public ServerThread(Socket s, String thisClient){
    	this.thisClient=thisClient;
        this.s=s;
        try {
			s.setSoTimeout(timeoutMiliSeconds);
			if(debugthis) System.err.println("\nNew Server Thread Starting ...  ");
			if(debugthis) System.err.println("\n---started with no mode | Client: "+thisClient+"---  ");
		} catch (SocketException e) { 
			e.printStackTrace();
		}
    } //mode
    
    public ServerThread(Socket s, String thisClient, int mode){
    	this.thisClient=thisClient;
    	this.mode=mode;
        this.s=s;
        try {
			s.setSoTimeout(timeoutMiliSeconds);
			if(debugthis) System.err.println("\nNew Server Thread Starting ...  ");
			if(debugthis) System.err.println("\n---Mode:"+mode+" | Client: "+thisClient+"---  ");
		} catch (SocketException e) { 
			e.printStackTrace();
		}
    }  
    
    public void nextMove(int nextmove){
    	this.nextmove=nextmove;
    }

    public void run() {
	    try{
	    	groupId = SysUtils.getUniqueId(ComLayer.getThisAddress());
	    	os = new DataOutputStream(s.getOutputStream());
	        is = new DataInputStream(s.getInputStream());
	        initialized=true;
	
	    }catch(IOException e){
	        if(debugthis)System.out.println("IO error in server thread");
	    }
	
	    try { 
	    	if(debugthis) System.err.println("\nServer Thread "+Thread.currentThread().getId()+": Ready  ");
	        while(!Sigmation.resetSignal){ 
	        	write();
	        	Thread.sleep(1); 
	        }   
	    } catch (Exception e) { 
	        line=this.getName(); //reused String line for getting thread name
	        if(debugthis) System.out.println("IO Error/ Client "+line+" terminated abruptly");
	    } 
	
	    finally{    
		    try{
		    	if(debugthis) System.out.print("Server Thread "+Thread.currentThread().getId()+": Closing -> ");
		        if (is!=null){
		            is.close(); 
		            if(debugthis) System.out.print("[Socket Input Stream]");
		        }		
		        if(os!=null){
		            os.close();
		            if(debugthis) System.out.print("[Socket Out]");
		        }
		        if (s!=null){
		        	s.close();
		        	if(debugthis) System.out.print("[Socket]");
		        }
		
		    	} catch(IOException ie){
		    		if(debugthis) System.out.println("<<(!)Socket Close Error>>");
		    	}
		    	if(debugthis) System.out.println("::: Connection Terminated");
	    }//end finally
    }
    
    public void readSocket(){ 
		int length;
		try {
			length = is.available();
			if(length>0) {
	            byte[] message = new byte[length];
	            try {
					is.readFully(message, 0, message.length);
				} catch (IOException e) { 
					e.printStackTrace();
				} // read the message
	            String hex = DatatypeConverter.printHexBinary(message);
	            if(debugthis) System.out.printf("Received (%d): ",length);
	            if(debugthis) System.out.println(hex); 	
	            databuffer=message;
	            data_received=true;  
	            RFThread rfthread = new RFThread(databuffer,thisClient,mode,this);
				rfthread.start();
	        }
		} catch (IOException e1) { 
			e1.printStackTrace();
		}                     
        
    }
    
    
    public boolean gotData(){
    	return data_received;
    }
    
    public byte[] getData(){
    	data_received=false;
    	return databuffer;
    }
    
    /** Set timeout in seconds */
    public void setTimeOut_s(int seconds){
    	
    	deltatime_ns = seconds * 1000000000;
    }
    
    /** Set timeout in milliseconds */
    public void setTimeOut_ms(int milliseconds){
    	
    	deltatime_ns = milliseconds * 1000000;
    }
    
    /** Set timeout in microseconds */
    public void setTimeOut_us(int microseconds){
    	
    	deltatime_ns = microseconds * 1000;
    }
    
    /** Set timeout in nanoseconds */
    public void setTimeOut_ns(long nanoseconds){
    	
    	deltatime_ns = nanoseconds;
    }
    
    
    /** get timeout in seconds */
    public long getTimeOut_s(){
    	
    	return deltatime_ns / 1000000000;
    }
    
    /** get timeout in milliseconds */
    public long getTimeOut_ms(){
    	
    	return deltatime_ns / 1000000;
    }
    
    /** get timeout in microseconds */
    public long getTimeOut_us(){
    	
    	return deltatime_ns / 1000;
    }
    
    /** get timeout in nanoseconds */
    public long getTimeOut_ns(){
    	
    	return deltatime_ns;
    }
    
	
	/** Set debugging level of this object*/
	public void setDebug(int level){
		
		debug_level=level;
	}
	
	/** get debugging level of this object*/
	public int getDebug(){
		
		return debug_level;
	}
	
	/** Set System type of this object 
	 * Default is 0
	 * 0 means no system applied, so no data treatment is done
	 * */
	public void setSystemType(int level){
		
		system_type=level;
	}
	
	/** Get System type of this object*/	
	public int getSystemType(){

		return system_type;
	}
	
	public void getRFID(byte[] packetData,int pointer ){
		int RFID_BYTES=12;
		byte[] RFID_N = new byte[12];
		
		for(int i=0;i<RFID_BYTES; i++){
			if(pointer+5+i<packetData.length) RFID_N[i]=packetData[pointer+5+i];			
		}
		int antenna= 0;
		if(pointer+19<packetData.length) {
			antenna= packetData[pointer+19];	
			String hex = DatatypeConverter.printHexBinary(RFID_N); 
			if(debugthis) System.out.printf("RFID (antena %d):", antenna );
			if(debugthis) System.out.println(hex); 
        }
	}
	
	public void rfid_sys(byte[] packetData,int[] pointers, int packetCounter, int counter){
		switch(packetData[pointers[counter]+1]){
		
		case (byte)0x97 	: 	getRFID(packetData,pointers[counter]);
								break;
	
		default				:    
								break;
	
		}
	}
	
	public void modbus_sys(byte[] packetData,int[] pointers, int packetCounter, int counter){
		//do something choda dog
		
	}
	
	public void packetTreatment(byte[] packetData,int[] pointers, int packetCounter){
				
				
		
		
				for (int i=0; i< packetCounter ; i++){
					
					
					switch (system_type){
						case 0 				:	//do nothings
												break;
						case 1 				:	rfid_sys(packetData, pointers, packetCounter, i);
												break;
						case 2				:	modbus_sys(packetData, pointers, packetCounter, i);
												break;
						default				:	break;					
					
					}	
				}
	}
	
	public void protocolChecker(byte[] packetData, int length){
			
		    int pointer = 0;
		    int packetCounter=0;
		    int[] pointers= new int[10000];
		    while(pointer+2<length){
		    	if(packetData[pointer]==header){
					int localsize = packetData[2+pointer] + 6;				
					pointers[packetCounter]=pointer;
					packetCounter++;
					pointer=pointer+localsize;
				}
		    	else pointer = length;
		    }
		    //System.out.printf("\n >>> (%d) packets ",packetCounter);
		    packetTreatment(packetData, pointers, packetCounter);
	}

	
	public void writeSocket(byte[] commanding) throws Exception {
			String hex = DatatypeConverter.printHexBinary(commanding);
			if(debugthis) System.out.printf("sent ");
			if(debugthis) System.out.println(hex); 
			os.write(commanding);
			checktimer(true);
	}

	public void writeAndRead() {
			//writeSocket(commanding);
			readSocket();			
	}

	public void write() throws Exception {
		
	
		// The capital string before each colon has a special meaning to SMTP
		// you may want to read the SMTP specification, RFC1822/3
	        	 
		        	writecommand();	
		        	if(mode==0 || mode==6 || mode==7) checkBeaconTimer(true);	 
		        		while (true) {			        		
			        		writeAndRead(); 
			        		if(mode==0)checkBeaconTimer();
			        		else doNextMove();
			        		
			        	}
	}
	
	private void doNextMove() throws Exception{
		switch(nextmove){
				case 1		:   requestNext();
								break;
				case 2		:   requestConfirm();
								break;
				default		: 	break;
		}
		nextmove=0;
	}

	public void writecommand() throws Exception {
					byte[] commanding = {(byte)0xbb,(byte)0x17,(byte)0x02,(byte)0x00,(byte)0x00,(byte)0x19,(byte)0x0D,(byte)0x0A};
					writeSocket(commanding);	
	}
	
	public void requestNext() throws Exception {
		byte[] commanding = {(byte)0xbb,(byte)0x51,(byte)0x02,(byte)0x01,(byte)0x00,(byte)0x54,(byte)0x0D,(byte)0x0A};
		writeSocket(commanding);	
	}
	
	public void requestConfirm() throws Exception {
		byte[] commanding = {(byte)0xbb,(byte)0x51,(byte)0x02,(byte)0x02,(byte)0x00,(byte)0x55,(byte)0x0D,(byte)0x0A};
		writeSocket(commanding);	
	}
	
	
	public boolean checktimer(){
		return checktimer(false);
	}

	public boolean checktimer(boolean start){
		if(start){
			start_time = System.nanoTime();
			return true;
		}
		else {
			long delta_time = System.nanoTime() - start_time;
			if(delta_time>deltatime_ns) {
					databuffer=null;
					return false;
			}
			else if (data_received) return false;
			else return true;
		}
	}
	
	public boolean checkBeaconTimer() throws Exception{
		return checkBeaconTimer(false);
	}
	
	public boolean checkBeaconTimer(boolean start) throws Exception{
		if(start){
			start_time_beacon = System.nanoTime();
			return false;
		}
		else {
			long delta_time = (System.nanoTime() - start_time_beacon)/1000;
			if(delta_time>deltatime_ms_beacon) { 
					writecommand();
					start_time_beacon = System.nanoTime();
					return true;
			} 
			return true;
		}
	}

	public void close(){
		if(initialized){
			try {
				localSocket.close();
			   	os.close();
			   	is.close();
			   	initialized=false;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println(noinitMessage);
		}	
	}

	/** 
	 * Sends a byte array data packet expecting a response,
	 * if no response is received before the timeout
	 * it returns a null byte array
	 * else it returns the data received in byte array format.
	 * Use setTimeOut functions to set up a custom timeout
	 * */
	public byte[] ask(byte[] toSend) throws IOException {
		
		if(initialized){
			try {
				if (localSocket != null && os != null && is != null) {     	
			       	writeSocket(toSend);
				   	while(checktimer()){
				   		readSocket();
				   	}				   	
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else{
			System.out.println(noinitMessage);
		}
		
		return databuffer;
	}

	/** object must not be used after this function, 
	  * this is intented to wait for an answer then terminate the connection.
	  * if you need to use the object after this, use ask() instead.
	  * You can also use init() after this function
	  * to start a new connection with the same object. */
	public byte[] askAndClose(byte[] toSend) throws IOException {	
		 
		
		byte[] localbuffer = ask(toSend);
		close();
		return localbuffer;
	}

	public static String toHex(String arg) {
		return String.format("%040x", new BigInteger(1, arg.getBytes()));
	}

	 


	public void write2(){
	if (localSocket != null && os != null && is != null) {
	    try {
	// The capital string before each colon has a special meaning to SMTP
	// you may want to read the SMTP specification, RFC1822/3
	    	
	        try {
	        	byte[] commanding = {(byte)0xbb,(byte)0x16,(byte)0x00,(byte)0x16,(byte)0x0D,(byte)0x0A};
		        os.write(commanding); 
				Thread.sleep(2000);
			        os.write(commanding); 
				Thread.sleep(2000);
			        os.write(commanding); 
				Thread.sleep(2000);
			        os.write(commanding); 
				Thread.sleep(2000);
			        os.write(commanding); 
				Thread.sleep(2000);
			        os.write(commanding); 
				Thread.sleep(2000);
			} catch (InterruptedException e) { 
				e.printStackTrace();
			}
	        
	        os.close();
	        is.close();
	        localSocket.close();   
	    } catch (UnknownHostException e) {
	    	if(debugthis) System.err.println("Trying to connect to unknown host: " + e);
	    } catch (IOException e) {
	    	if(debugthis) System.err.println("IOException:  " + e);
	    }
	}

}
}