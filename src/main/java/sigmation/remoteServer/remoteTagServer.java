package sigmation.remoteServer;
 
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import sigmation.io.ComLayer;
import sigmation.utils.Definitions;
import sigmation.utils.MySQL; 
import sigmation.utils.SysUtils;
import sigmation.utils.SysUtils.HardCheckConfig;
import sigmation.main.Sigmation;
import sigmation.tagTcp.MessageHandler;
import sigmation.tagTcp.PacketHandler;

public class remoteTagServer {
	
	
	
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 		SYSTEM FUNCTIONS, DONT CHANGE THIS UNLESS YOU KNOW WHAT YOU         //
	 *      ARE DOING.		                                                    //
	 *      -FBS-																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 *
	 *
	 * 	serverResquestsTag: server request the first tag available to register,
	 * 						if there's one, this remote server will respond with 
	 * 						tag data, tag index (in this remote server) and a flag 
	 * 						indicating if there are more tags available.
	 *  
	 *  serverNextTag	  : Server requests another tag, adding the last registered
	 *  					tag, so this server can mark it as registered and not 
	 *  					as available, until a new reading.
	 *  					If there's a tag, this remote server will respond with 
	 * 						tag data, tag index (in this remote server) and a flag 
	 * 						indicating if there are more tags available.
	 * 
	 * 	serverAck		  :	First command sent to open communication, this includes
	 * 						the current system's time stamp. This remote server
	 * 						will use the time stamp provided to set up its time, 
	 * 						and will answer with a flag indicating if there are
	 * 						tags available to send. USED ON RF MODE ONLY.
	 * 
	 * */
	
		static final byte serverResquestsTag   	= Definitions.TNT.serverResquestsTag;
		static final byte serverNextTag	  	 	= Definitions.TNT.serverNextTag;
		static final byte serverNextBusTag		= Definitions.TNT.serverNextBusTag;
		static final byte serverPing		  	= Definitions.TNT.serverPing;
		
		// Same commands but received through Ethernet, the system acts differently when working through Ethernet
		static final byte serverResquestsTag_R  = Definitions.TNT.serverResquestsTag_R;
		static final byte serverNoTag_R		  	= Definitions.TNT.serverNoTag_R;
		static final byte serverPing_R		  	= Definitions.TNT.serverPing_R;
		static final byte serverBusTag_R  		= Definitions.TNT.serverBusTag_R;
		
		static final byte thisAppid				= Definitions.TNT.thisAppid;
		
		static final int entryMode = 1;       //define puertos y modo de lecturas para registro de subida a bus
		static final int exitMode = 2;        //define puertos y modo de lecturas para registro de bajada a bus
		static final int specialModeA = 4;       //define puertos y modo de lecturas para registro lectura especial Zona A
		static final int specialModeB = 5;        //define puertos y modo de lecturas para registro lectura especial Zona B
		static final int MonoAntenaModeA = 6;       //define puertos y modo de lecturas para registro lectura antena unica Zona A
		static final int MonoAntenaModeB = 7;        //define puertos y modo de lecturas para registro lectura antena unica Zona B
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   CONFIG										//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
		static boolean debugThis			= true;        //activar/desactivar debug
		static boolean WMX					= false;       	//activar/desactivar WMX, falso por defecto, remotos no necesitan WMX, solo master
		static int	   thisRemoteAddress	= 1;  			//direccion de este equipo, SOLO PARA RF 
		static boolean ethernetCom			= false;	  	//comunicacion ethernet, si es falso se usa comunicacion RF
		static int	   port					= 8000;			//puerto de escucha del servidor local, para lectores RFID
		static final int	   BusEntryPort			= 8001;			//puerto de escucha del servidor local, para lectores RFID ENTRADA ABUS
		static final int	   BusExitPort			= 8002;			//puerto de escucha del servidor local, para lectores RFID SALIDA BUS
		static final int	   SpecialAPort			= 8004;			//puerto de escucha del servidor local, para lectores RFID Especiales Zona A
		static final int	   specialBPort			= 8005;			//puerto de escucha del servidor local, para lectores RFID Especiales Zona B
		static final int	   MonoAntenaAPort			= 8006;			//puerto de escucha del servidor local, para lectores RFID Especiales Zona A
		static final int	   MonoAntenaBPort			= 8007;			//puerto de escucha del servidor local, para lectores RFID Especiales Zona B
		
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   System variables								//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */		
		static byte 	rtaddress;
		static byte 	rtzone; 
		static byte[] 	rtdata;
		static byte[] 	rtdatabuffer; 
		static InetAddress MasterIpAddress;
		static byte MasterAddress=(byte)0;
		
		static long lastTime = System.currentTimeMillis();
		static long messagetime = 1000; //milliseconds
		static long tagTimeSeconds= 30; //seconds
		static long tagTime = tagTimeSeconds*1000; //milliseconds
		static long currentmessagetime=500;
		static long shortMessageTime=200;
		
		static MySQL crTag = new MySQL();
		static MySQL crBusTag = new MySQL();
		
		static String userMysql=Sigmation.user;
		static String passMysql=Sigmation.pass;
		
		static boolean TagCheckBusy = false; 
		static boolean busTagCheckBusy = false; 
		
		static public int[] cardcounter = new int[4];
		
	 /**
	  * 
	  * 	Data packet frame components
	  * 
	  * */	
		
		static byte[] tagdata = new byte[12];
		static byte[] unix_timestamp = new byte[4];
	
	 /**
	  * 
	  * 	System internal buffers
	  * 
	  * */	
		
		private static ArrayList<String[]> TagList;
		private static ArrayList<String[]> BusTagList;
		private static long timeDifference=0;
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   INITIALIZE									//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
	public static void init(boolean WMX){
		/**
		 * 		Remote Tag Server
		 * 
		 * -Tareas de inicio
		 * 1) Obtener parametros de funcionamiento del sistema desde base de datos local
		 * 2) iniciar variables de este sistema
		 * 3) abrir un hilo para aceptar conexiones como servidor, y operar como tal
		 *  
		 * */
 
		if(debugThis)System.out.println("remotetagserver: init");
		getThisMode();
		getMasterIp();
		getThisPort();
		remoteTagServer.WMX = WMX; 
		if(!ethernetCom)ComLayer.init(WMX,thisAppid);
		thisRemoteAddress=SysUtils.byteToUnsignedInt(ComLayer.getThisAddress());
		startMessageThread();
		PacketHandler.multiclientServerStart(port);
		PacketHandler.multiclientServerStart(BusEntryPort,entryMode);
		PacketHandler.multiclientServerStart(BusExitPort,exitMode); 
		PacketHandler.multiclientServerStart(SpecialAPort,specialModeA);
		PacketHandler.multiclientServerStart(specialBPort,specialModeB); 
		PacketHandler.multiclientServerStart(MonoAntenaAPort,MonoAntenaModeA);
		PacketHandler.multiclientServerStart(MonoAntenaBPort,MonoAntenaModeB); 
		hardCheckStart();
		Sigmation.startSignal=true;
	}
	
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   TIMERS   									//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
		
	public static void hardCheckStart(){
		Timer timer = new Timer();
		timer.schedule(new HardCheckConfig("rm_tnt"), 0, 1000);
	}
	
	public static void timeStampCheckStart(){
		Timer timer = new Timer();
		timer.schedule(new timeStampCheck(), 0, 3600000);
	}
	
	public static void busTagCheckStart(){
		Timer timer = new Timer();
		timer.schedule(new busTagCheck(), 0, tagTime);
	}
	
	public static void tagCheckStart(){
		Timer timer = new Timer();
		timer.schedule(new tagCheck(), 0, tagTime);
	}
	
	public static void onlineCheckStart(){
		Timer timer = new Timer();
		timer.schedule(new onlineCheck(), 0, 60000);
	}
	
	
	private static class timeStampCheck extends TimerTask {
		
		public void run() { 
			setMasterTimeStamp();		
		}
	}
	
	private static class onlineCheck extends TimerTask {
			
			public void run() { 
				setOnlineAtMaster();		
			}
    }
	
	private static class busTagCheck extends TimerTask {
		
		public void run() {  
			if(!TagCheckBusy) {
				//if(checkServerCon()) {
					if(getAvailableTags()) {
						TagCheckBusy=true;
						saveTagsOnMaster();
					}
				//}				
			}
		}
	}
	
	private static class tagCheck extends TimerTask {
			
			public void run() { 
				if(!busTagCheckBusy) {
					//if(checkServerBusCon()) {
						if(getAvailableBusTags()) {
							busTagCheckBusy=true;
							saveBusTagOnMaster(); 
						}	
					//}
				}
			}
    }
	 

	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   FUNCTIONS									//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
	private static void startMessageThread(){
		MessageHandler mesThread = new MessageHandler();
		mesThread.start(); 
	}
	
	private static boolean checkServerCon() {
		try {
			if(crTag.isClosed()) {
				if(debugThis) System.out.println(":::tag::: creating connection");
				crTag.user=userMysql;
				crTag.pass=passMysql;
				crTag.database="ampdb"; 
				crTag.connect(MasterIpAddress.getHostAddress());
				if(debugThis) System.out.println(":::tag::: connected to master");
			}
			return true;
		} catch (Exception e) {
			if(debugThis) System.out.println(":::tag::: connection problems! cannot connect.");
			return false;
		}
	}
	

	private static boolean checkServerBusCon() {
		try {
			if(crTag.isClosed()) {
				if(debugThis) System.out.println(":::bustag::: creating connection");
				crBusTag.user=userMysql;
				crBusTag.pass=passMysql;
				crBusTag.database="ampdb"; 
				crBusTag.connect(MasterIpAddress.getHostAddress());
				if(debugThis) System.out.println(":::bustag::: connected to master");
			}
			return true;
		} catch (Exception e) {
			if(debugThis) System.out.println(":::bustag::: connection problems! cannot connect.");
			return false;
		}
	}
	 
	
	private static boolean setMasterTimeStamp(){
		try {
			MySQL cr = new MySQL();
			cr.user=userMysql;
			cr.pass=passMysql;
			cr.database="ampdb";		
			String sentence = "SELECT UNIX_TIMESTAMP(NOW())";
			cr.connect(MasterIpAddress.getHostAddress());   
			ArrayList<String[]> getTime = cr.rawSelect(sentence);
			if(getTime.size()>0) {
					long timestamp = Long.parseLong(getTime.get(0)[0]);
					setUpTime(timestamp);
					return true;
					}
			else return false;	
		} catch (Exception e) {
			return false;
		}
			
	}
 
	public static void setUpTime(long timeStamp){ // 
		/***
		 * 
		 *   Solo usar si este servidor no tiene acceso a una red
		 * 
		 * 
		 * */ 
		Date currentdate= new Date();
		long currentTime = currentdate.getTime();
		Date time=new java.util.Date(timeStamp*1000L);
		long lastTime = time.getTime();
		timeDifference=(lastTime-currentTime)/1000;
			SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	        //to convert Date to String, use format method of SimpleDateFormat class.
			String formattedDate = sdf.format(time);
	        if(debugThis) System.out.println(formattedDate+" to change");
			try {
				Process p=Runtime.getRuntime().exec(new String[]{"date","--set",formattedDate}); 
				p.waitFor();
				int exitCode = p.exitValue();
				if(exitCode == 0) { // success 
					if(debugThis) System.out.println("time changed");
					//refreshTimestamps(timeDifference);
				}
				else { // failed 
					if(debugThis) System.out.println("time setup failed");
				}
			} catch (Exception e) { 
				if(debugThis)e.printStackTrace();
			}
	}
	
	public static void exit(){
		SysUtils.exitApi();
	}
	
	private static void getMasterIp(){
		try {
			MasterIpAddress =InetAddress.getLocalHost();
			getMasterIpAddress();
		} catch (Exception e) {
			if(debugThis) e.printStackTrace();
		}
	}
	
	
	private static boolean packetExecuter(){
		if(debugThis) System.out.println("[executing packet]");
		boolean eof=packetAnalizer();
		if(eof){
			if(debugThis) System.out.println("[valid packet]");
			//ComLayer.deviceKeepAlive[SysUtils.byteToUnsignedInt(rtaddress)]=false;
			tagSystem(rtdatabuffer,rtaddress,rtzone); 
			// responder aca
		}
		else if(debugThis) System.out.println("[Bad packet]");
		return eof;
	}
	
	private static boolean packetAnalizer(){
		byte[] localdata = ComLayer.getPacketData();
		if(localdata==null){
			ComLayer.resetProtocol();
			return false;
		} 
		rtdata=localdata;
		rtaddress= ComLayer.getCurrentAddress();
		if(SysUtils.byteToUnsignedInt(rtaddress)>255 || SysUtils.byteToUnsignedInt(rtaddress)<1){ 
			System.out.println("RemoteTagServer: failed reading! -- \n rtaddress="+SysUtils.byteToUnsignedInt(rtaddress)+"\n");
			return false;
		}
		rtzone = ComLayer.getCurrentZone();
		rtdatabuffer= ComLayer.getDataBuffer();
		if(debugThis) {
				System.out.println(" --- packet stats --- \n address:"+SysUtils.byteToUnsignedInt(rtaddress)+"\n zone"+rtzone+"\n Databuffer:");
				SysUtils.printhex(rtdatabuffer, rtdatabuffer.length);
				System.out.println("\n--------");
		}
		ComLayer.resetProtocol(); 		
		return true;
	}
	
	private static void sendingCheck(){
		ComLayer.sendingCheck();
	}
	
	public static boolean getEOF(){
		boolean localeof= ComLayer.getEOF();
		if(localeof) {if(debugThis) System.out.println("EOF!");} 
		//else {if(debugThis) System.out.print("*");} 
		return localeof;
	}
	
	
	public static void rfMode(){
		/**
		 * 		Remote Tag Server RF MODE 
		 * 
		 * -Tareas de loop
		 * 1) revisar paquetes de datos que han llegado por serial
		 * 2) ejecutar paquetes si han llegado
		 * 3) enviar respuesta a paquetes ejecutados
		 * 
		 * */
		try { 
			if (Sigmation.noPiMode) ComLayer.demo();
			if(debugThis) System.out.println("remotetagserver: rf loop");
			while(!Sigmation.resetSignal){	//welcome to the loop!
				
					if(getEOF())packetExecuter();  			
					sendingCheck();  
					Thread.sleep(10); 
			}
			//exit();
		} catch (Exception e) {
			if(debugThis)e.printStackTrace();
		}
	}
	
	private static boolean getAvailableTags(){
		return getAvailableTags(false);
	}
	
	private static boolean getAvailableTags(boolean limitOne){  
			String argument= "id, tagnum, zone, UNIX_TIMESTAMP(last_mod) ";
			MySQL cn = new MySQL();
			cn.database="remotedb";
			cn.connect();
			String table="rfid";
			String limitString=" LIMIT 100 "; 
			if(limitOne) limitString=" LIMIT 1 ";
			TagList = cn.select(table, argument, "  display=3 "+ limitString);
			cn.close();
			if(TagList==null | TagList.isEmpty()) return false;
			else return true;
	}	
	
	private static boolean getAvailableBusTags(){
		return getAvailableBusTags(false,false);
	}
	
	private static boolean getAvailableBusTags(boolean groupByReader){
		return getAvailableBusTags(false,groupByReader);
	}
	
	private static boolean getAvailableBusTags(boolean limitOne, boolean groupByReader){  
		String argument= "id, tagnum, readerid, UNIX_TIMESTAMP(last_mod), groupnum ";
		MySQL cn = new MySQL();
		cn.database="remotedb";
		cn.connect();
		String table="busrfid";
		String limitString=" ";
		String groupString=" ";
		if(limitOne) limitString=" LIMIT 1 ";
		if(groupByReader) groupString=" GROUP BY readerid ";
		BusTagList = cn.select(table, argument, "  score=1 AND last_mod < CURRENT_TIMESTAMP - INTERVAL 1 MINUTE "+groupString+ limitString );
		cn.close();
		if(BusTagList==null | BusTagList.isEmpty()) return false;
		else return true;
	}	
	
	private static int getAvailableTagNumber(){  
		int localcounter=0;
		String argument= "count(*) ";
		MySQL cn = new MySQL();
		cn.database="remotedb";
		cn.connect();
		String table="rfid";
		String limitString=" "; 
		TagList = cn.select(table, argument, "  display=3"+ limitString);
		cn.close();
		if(TagList==null | TagList.isEmpty()) localcounter= 0;
		else {
			String localString= TagList.get(0)[0]; 
			localcounter= SysUtils.returnInt(localString);
		}
		cn = new MySQL();
		cn.database="remotedb";
		cn.connect();
		table="busrfid";
		limitString=" "; 
		BusTagList = cn.select(table, argument, "  score>0"+ limitString);
		cn.close();
		if(BusTagList==null | BusTagList.isEmpty()) localcounter= localcounter+0;
		else {
			String localString= TagList.get(0)[0]; 
			localcounter= SysUtils.returnInt(localString)+localcounter;
		}
		return localcounter;
	}	
	
	private static void saveTagsOnMaster(){  
		try {
			checkServerCon();
			if(debugThis) System.out.println(":::tag::: saving "+TagList.size()+"tags on master");
			for(int i=0; i< TagList.size();i++){
				if(debugThis) System.out.println(":::tag::: "+(i+1)+" tags of "+TagList.size()+"on master");
				String[] tagData= TagList.get(i);
				String tagnum       = tagData[1];
				String tagZoneString= tagData[2];
				String tagId		= tagData[0];
				int    tagZone 		= SysUtils.returnInt(tagZoneString);
				if(debugThis) System.out.println(":::tag::: connecting to master");
				if(setTag(crTag,tagnum, tagZone)) {
													if(debugThis) System.out.println(":::tag::: registry finished");
													tagRegistered(tagId);
												}
				if(debugThis) System.out.println(":::tag::: "+(i+1)+" tags finished of "+TagList.size()+"on master");
			}
			if(debugThis) System.out.println(":::tag::: tags to master finished");
			TagCheckBusy=false;
			crTag.close();
		} catch (Exception e) {
			if(debugThis) e.printStackTrace();
			TagCheckBusy=false;
		}
	}
	
	private static void saveBusTagOnMaster(){  
		try {
			checkServerBusCon();
			if(debugThis) System.out.println("saving on master"); 
			for(int i=0; i< BusTagList.size();i++){
				String[] tagData= BusTagList.get(i);
				String tagId       = tagData[0];
				String tagnum       = tagData[1];
				String readerId     = tagData[2];
				String tmstmp		= tagData[3]; 
				String groupid		= tagData[4]; 
				if(saveBusTagOnMaster(crBusTag,tagnum, readerId, tmstmp, groupid)) tagRegistered(tagId,true);
			}
			busTagCheckBusy=false;
			crBusTag.close();
		} catch (Exception e) {
			if(debugThis) e.printStackTrace();
			busTagCheckBusy=false;
		}
		
	}
	
	private static void tagRegistered(String tagId){
		tagRegistered (tagId,  false);
	}
	
	private static void tagRegistered (String tagId, boolean busTag){
		String[] argument= {"display"};
		if(busTag) argument[0]="score";
		MySQL cn = new MySQL();
		cn.database="remotedb";
		cn.connect();
		String table="rfid"; 
		if (busTag) table="busrfid";
		String[] data= {"0"};  
		String condition = " id= '"+tagId+"' ";
		cn.update(table, argument, condition, data); 
		cn.close();
	}
	
	private static void refreshTimestamps (long timedifference){
		String[] argument= {"last_mod"};
		MySQL cn = new MySQL();
		cn.database="remotedb";
		cn.connect();
		String table="rfid"; 
		String plus="+";
		if(timedifference<0) plus=" ";
		String[] data= {"DATE(UNIX_TIMESTAMP(last_mod)"+plus+""+timedifference+")"};  
		String condition = " WHERE 1 ";
		cn.update(table, argument, condition, data); 
	}
	
	private static void tagRegistered (int tagId, boolean busTag){
		tagRegistered (""+ tagId,busTag); 
	}
	
	private static boolean setTag(MySQL cr, String tagnum, int tagZone){
		try {
				
			String call = "registro_tag";
			
			int readernum=thisRemoteAddress;
			String[] data= {"'"+tagnum+"'", Integer.toString(tagZone), Integer.toString(readernum)};  
			if(cr.callValueProcedure(call,data,false).size()>0) {	return true;}
			else {	
					return false;}	
		} catch (Exception e) {
			return false;
		}
			
	}
	
	private static boolean setOnlineAtMaster(){
		try {
			String[] argument= {"online","isethernet","last_mod"}; 
			MySQL cr = new MySQL();
			cr.user=userMysql;
			cr.pass=passMysql;
			cr.database="ampdb";
			cr.connect(MasterIpAddress.getHostAddress());
			int readernum=thisRemoteAddress;
			String table="rfidreaders";  
			String[] data= {"1","1","CURRENT_TIMESTAMP"};  
			String condition = " serial= '"+readernum+"' ";
			cr.update(table, argument, condition, data); 
			return true; 
		} catch (Exception e) {
			return false;
		}
		
	}
	
	private static boolean saveBusTagOnMaster(MySQL cr,String tagnum, String readerId, String tmstmp, String groupid){
 
		try {  		
			String call = "saveTagBus"; 
			int readernum=thisRemoteAddress;
			String[] data= {"'"+tagnum+"'","'"+readerId+"'", "1" , ""+readernum, tmstmp, "'"+groupid+"'"}; 
			if(cr.callValueProcedure(call,data,false).size()>0) return true;
			else return false;	
		} catch (Exception e) {
			return false;
		}
	}	
	
	private static boolean isItTime(){
		long thisTime= System.currentTimeMillis();
		if (thisTime-lastTime>=currentmessagetime) {currentmessagetime=shortMessageTime ; lastTime= thisTime;return true;}
		return false;
	} 
	
	public static void ethernetMode(){
		/**
		 * 		Remote Tag Server ETHERNET MODE
		 * 
		 * -Tareas de loop
		 * 1) revisar tags listos para guardar (registro de cambio de zona)
		 * 2) si hay tags listos, guardarlos en base de datos remota 
		 * 
		 * */
		onlineCheckStart();
		setMasterTimeStamp();
		timeStampCheckStart();
		busTagCheckStart();
		tagCheckStart();
		if(debugThis) System.out.println("remotetagserver: ethernet loop");
		try {
			while(!Sigmation.resetSignal){ 			
				Thread.sleep(1);
			}
		} catch (Exception e) {
			if(debugThis) e.printStackTrace();
		} 
		if(debugThis) System.out.println("remotetagserver: nothing to do. Fin.");
	}
	
	private static void getThisMode(){         //used to get an address for RF from the database. REMOTE DEVICES ONLY!
		String argument= "value";
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="sys_config"; 
		ArrayList<String[]> localList= cn.select(table, argument, " parameter='ethernet_mode'");
		if(localList.size()>0){
			String[] localStringMatrix= localList.get(0);   //get the item array where the item is stored
			String localString = localStringMatrix[0];		//get the item from the item array
			int localnumber = SysUtils.returnInt(localString);//get the number from the item string
			if(localnumber==1) ethernetCom=true;
			else ethernetCom=false;			
		}
		if(debugThis) System.out.println("Remote Tag Server: got my ethernet mode, it's "+ethernetCom);
	}
	
	private static void getThisPort(){         //used to get an address for RF from the database. REMOTE DEVICES ONLY!
		String argument= "value";
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="sys_config"; 
		ArrayList<String[]> localList= cn.select(table, argument, " parameter='remoteserver_port'");
		if(localList.size()>0){
			String[] localStringMatrix= localList.get(0);   //get the item array where the item is stored
			String localString = localStringMatrix[0];		//get the item from the item array
			int localnumber = SysUtils.returnInt(localString);//get the number from the item string
			port = localnumber;
		}
		if(debugThis) System.out.println("Remote Tag Server: got my server port, it's "+port);
	}
	
	private static void getMasterIpAddress() throws Exception{         //used to get an address for Ethernet mode. REMOTE DEVICES ONLY!
		String argument= "value";
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="sys_config"; 
		ArrayList<String[]> localList= cn.select(table, argument, " parameter='master_ip'");
		if(localList.size()>0){
			String[] localStringMatrix= localList.get(0);   //get the item array where the item is stored
			String localString = localStringMatrix[0];		//get the item from the item array 
			if(!localString.equalsIgnoreCase("localhost")) {
				userMysql=Sigmation.remoteuser;
				passMysql=Sigmation.remotepass;
			}
			MasterIpAddress = InetAddress.getByName(localString); 
		}
		if(debugThis) System.out.println("Remote Tag Server: got my master address, it's "+MasterIpAddress.getHostAddress());
	}
	 
	
	public static void loop(){ 
		if(ethernetCom)ethernetMode();
		else rfMode();
	}
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   TASK FUNC. CLASS								//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
		private static class remoteTasks{
			
			static byte[] datax;
			
			/**
			 * Estructura data
			 * 
			 * Recepcion:
			 * 	-Ping
			 * 		data[0]= command (serverPing)
			 * 		data[1(MSB)][2][3][4(LSB)]= Timestamp
			 *  
			 *  -Request tag (serverResquestsTag)
			 *  	data[0]=command
			 *  
			 *  -Next tag
			 *  	data[0]=command (serverNextTag) 
			 *  	data[1(MSB)][2][3][4(LSB)]= Last Tag index
			 *  
			 * Transmision:
			 * 	-send Tag
		 	 * 		data[0]=command (serverResquestsTag_R)
		 	 * 		data[1(MSB)]...[24(LSB)]= Tag number
		 	 * 		data[25(MSB)][26][27][28(LSB)]= Timestamp
		 	 * 		data[29(MSB)][30][31][32(LSB)]= Tag index
		 	 * 		data[33]=zone
		 	 * 		data[34(MSB)][35(LSB)]= Tags left
		 	 * 
		 	 *  -No tags
		 	 *  	data[0]=command (serverNoTag_R)   
			 */
			
			public static void serverResquestsTag(){
				if(getAvailableTags(true)){
					
					String[] localStringBuf	=TagList.get(0);
					String idString			=localStringBuf[0];
					String tagString		=localStringBuf[1];
					String zoneString		=localStringBuf[2];
					String tmstmpString		=localStringBuf[3]; 
					
					byte[] command 	= {serverResquestsTag_R};
					byte[] tagBytes	= SysUtils.stringToByteArray(tagString);
					byte[] tmByte 	= SysUtils.intTo4Bytes(SysUtils.returnInt(tmstmpString));
					byte[] idByte 	= SysUtils.intTo4Bytes(SysUtils.returnInt(idString));
					byte[] zoneByte = {(byte)SysUtils.returnInt(zoneString)};
					byte[] tagsleft	= SysUtils.intTo2Bytes(getAvailableTagNumber());
					
					byte[] allData	= new byte[command.length+tagBytes.length+tmByte.length+idByte.length+zoneByte.length+tagsleft.length];
					int[] lengths ={command.length,
									command.length+tagBytes.length,
									command.length+tagBytes.length+tmByte.length,
									command.length+tagBytes.length+tmByte.length+idByte.length,
									command.length+tagBytes.length+tmByte.length+idByte.length+zoneByte.length};
					System.arraycopy(command , 0, allData	, 0			, command.length	);
					System.arraycopy(tagBytes, 0, allData	, lengths[0], tagBytes.length	);
					System.arraycopy(tmByte	 , 0, allData	, lengths[1], tmByte.length		);
					System.arraycopy(idByte	 , 0, allData	, lengths[2], idByte.length		);
					System.arraycopy(zoneByte, 0, allData	, lengths[3], zoneByte.length	);
					System.arraycopy(tagsleft, 0, allData	, lengths[4], tagsleft.length	);
					ComLayer.serialsend(MasterAddress, allData);
				} else if(getAvailableBusTags()){ 
					String[] localStringBuf	=BusTagList.get(0);
					String idString			=localStringBuf[0];
					String tagString		=localStringBuf[1];
					String readerString		=localStringBuf[2];
					String tmstmpString		=localStringBuf[3]; 
					
					byte[] command 	  = {serverBusTag_R};
					byte[] tagBytes	  = SysUtils.stringToByteArray(tagString);
					byte[] tmByte 	  = SysUtils.intTo4Bytes(SysUtils.returnInt(tmstmpString));
					byte[] idByte 	  = SysUtils.intTo4Bytes(SysUtils.returnInt(idString));
					byte[] readerBytes= SysUtils.intTo4Bytes(SysUtils.returnInt(readerString));
					byte[] tagsleft	  = SysUtils.intTo2Bytes(getAvailableTagNumber());
					
					byte[] allData	= new byte[command.length+tagBytes.length+tmByte.length+idByte.length+readerBytes.length+tagsleft.length];
					int[] lengths ={command.length,
									command.length+tagBytes.length,
									command.length+tagBytes.length+tmByte.length,
									command.length+tagBytes.length+tmByte.length+idByte.length,
									command.length+tagBytes.length+tmByte.length+idByte.length+readerBytes.length};
					System.arraycopy(command 	, 0, allData	, 0			, command.length	);
					System.arraycopy(tagBytes	, 0, allData	, lengths[0], tagBytes.length	);
					System.arraycopy(tmByte	 	, 0, allData	, lengths[1], tmByte.length		);
					System.arraycopy(idByte	 	, 0, allData	, lengths[2], idByte.length		);
					System.arraycopy(readerBytes, 0, allData	, lengths[3], readerBytes.length);
					System.arraycopy(tagsleft	, 0, allData	, lengths[4], tagsleft.length	);
					ComLayer.serialsend(MasterAddress, allData);
				} else noTags();
			}
			
			public static void serverNextTag(){
				registerMasterRead();
				serverResquestsTag();
			}
			
			public static void serverNextBusTag(){
				boolean busTag=true;
				registerMasterRead(busTag);
				serverResquestsTag();
			}
			
			public static void serverPing(){
				saveTimestamp();
				serverResquestsTag();
			}
			
			public static void noTags(){				
				byte[] command 	= {serverNoTag_R};
				ComLayer.serialsend(MasterAddress, command);
			}
			
			public static void registerMasterRead(){
				registerMasterRead(false);
			}
			
			public static void registerMasterRead(boolean BusTag){
				byte[] tagIdBytes= {datax[1],datax[2],datax[3],datax[4]};
				int tagId= ByteBuffer.wrap(tagIdBytes).getInt();
				tagRegistered(tagId, BusTag);
			}
			
			public static void saveTimestamp(){
				byte[] timeBytes= {0,0,0,0,datax[1],datax[2],datax[3],datax[4]};
				long timestamp= ByteBuffer.wrap(timeBytes).getLong();
				setUpTime(timestamp);
				serverResquestsTag();
			}
		}
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   TASK FUNC.									//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
		
	public static void tagSystem(byte[] datax, byte series, byte zone){
		if(debugThis) System.out.println("[TNTR:command "+SysUtils.byteToUnsignedInt(datax[0])+"]");
		ComLayer.deviceInitTime(SysUtils.byteToUnsignedInt(series));
		remoteTasks.datax=datax;
		switch(datax[0]){   //comandos del sistema
			case serverResquestsTag	:	remoteTasks.serverResquestsTag();
										break;
			case serverNextTag		:	remoteTasks.serverNextTag();
										break;		
			case serverNextBusTag	:	remoteTasks.serverNextBusTag();
										break;	
			case serverPing			:	remoteTasks.serverPing();
										break;					
			default		   			: 	//do nothing!
										break;				
		
		}
		
	}
}
