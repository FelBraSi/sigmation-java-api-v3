package sigmation.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
 
import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;

import sigmation.utils.SysUtils;
import sigmation.main.Sigmation;
import sigmation.modbus.Modbus;
import sigmation.modbus.ModbusDefinitions;


public class WMXModCom {
	public static byte[] out_buffer = new byte[1] ;
	public static byte[] in_buffer = new byte[300] ;
	public static byte[] buffer = new byte[300] ;
	public static byte[] databuffer = new byte[300] ;
	public static byte currentError=0;
	public static byte currentFunction = 0 ;
	public static byte destAddress = 0 ;
	public static byte currentAddress = 0 ; 
	public static boolean head_received=false;
	public static boolean eof_received=false;
	public static boolean data_received=false;
	public static int currentDataSize=0;  
	public static int packet_state=0;
	public static int in_b_counter=0;
	public static long date1= System.nanoTime();
	public static long date2= System.nanoTime();
	public static Serial serial;
	public static int[] ResponseData;
	public static boolean dummysend=false;
	
	static boolean wmxdebug=true;
	
	public final static  byte[] dummymes = {0x01,0x40,0x30,0x30,0x30,0x32,0x21,0x30,0x30,0x30,0x31,0x23,0x30,0x02,(byte)0xaa,(byte)0xaa,0x10,0x03,0x04};
	
	
	
	/*-------------------------------------------------------------------------------
	 *---------------------------- WMX ----------------------------------------------
	 *-------------------------------------------------------------------------------
	 *
	 *	Header (1 byte): 0x01 
	 *		Control Field (1 byte): 0x40-0xFF
	 *		Destination Address (0-4 ASCII): 0000-FFFF
	 *		Separator 1 (1 byte): 0x21 (! ascii char) 
	 *		Source Address (0-4 ASCII): 0000-FFFF * optional when transmiting
	 *		Separator 2 (1 byte): 0x22 (" ascii char) * ommitted if group is not used
	 *		group number (0-4 ASCII): 0000-FFFF * optional when transmiting
	 *		Separator 3 (1 byte): 0x23 (# ascii char) * ommitted if sequence is not used
	 *		sequence (0-4 ASCII): 0-255 * optional when transmiting, 0 or NULL=Automatic
	 *		Separator 4 (1 byte): 0x23 ($ ascii char) * ommitted if RSSI FIELD not used
	 *		RSSI (0-16 ASCII): ascii digits * optional 
	 * 	Start of text SOT (1 byte): 0x02 
	 * 		Data Field (1-500 bytes): see data notes
	 * 	End of Text (2 bytes): 0x10 0x03
	 * 		Checksum (0-4 ASCII): 0-FFFF
	 * 	End of Transmission EOT (1 byte): 0x04
	 * 
	 *  -------------- DATA NOTES -----------------
	 *  
	 *  	WHEN TRANSMITING, the following bytes are replaced
	 *  	for their 0xFF exor pair equivalents, to prevent protocol 
	 *  	flags triggering:
	 *  
	 * 			original 	- replacement
	 *  		0xFF 		- 0xFF 0x00
	 *  		0x03		- 0xFF 0xFC
	 *  		0x04		- 0xFF 0xFB
	 *  		0x0D		- 0xFF 0xF2
	 *  	
	 *  	WHEN Receiving, the following byte pairs are replaced
	 *  	for their exor result. No 0xFF byte must be taken as it is, they always
	 *  	come paired and should be replaced:
	 *  
	 * 			replace for	- if original pair is
	 *  		0xFF 		- 0xFF 0x00
	 *  		0x03		- 0xFF 0xFC
	 *  		0x04		- 0xFF 0xFB
	 *  		0x0D		- 0xFF 0xF2
	 *  
	 *
	 *------------------------------------------------------------------------------- 
	 * */
	
	//WMX variables
	
	// ----- variable buffers
	public static byte[] destination_address_b = new byte[4]; 
	public static int destination_address_counter = 0;
	
	public static byte[] source_address_b= new byte[4]; 
	public static int source_address_counter = 0;
	
	public static byte[] group_number_b = new byte[4];
	public static int group_number_counter = 0;
	
	public static byte[] sequence_b = new byte[4];
	public static int sequence_counter = 0;
	
	public static byte[] RSSI_b = new byte[16];
	public static int RSSI_counter = 0;
	
	public static byte[] CHCKS_b = new byte[4];
	public static int CHCKS_counter = 0;
	
	public static byte[] DATA_b = new byte[500];
	public static int DATA_counter = 0;
	
	
	
	//----- final received variables
	public static String 	destination_address;
	public static String   	source_address;
	public static String  	group_number;
	public static String	sequence;
	public static String  	RSSI;
	public static String    CHCKS;
	public static byte[] 	STORED_DATA = new byte[500];
	public static int 		STORED_DATA_counter = 0;
	public static boolean 	ack_control=false;
	
	
	//----- WMX protocol commands and separators
	public final static byte SOH 			= (byte)0x01;
	public final static byte SOT		 	= (byte)0x02;
	public final static byte ETX		 	= (byte)0x03;
	public final static byte EOT		 	= (byte)0x04;
	
	public final static byte DLE		 	= (byte)0x10;
	public static boolean DLE_state 		= false;         //false: nothing, true: on hold.  
	
	public final static byte Separator1 	= (byte)0x21;
	public final static byte Separator2 	= (byte)0x22;
	public final static byte Separator3 	= (byte)0x23;
	public final static byte Separator4 	= (byte)0x24;
	
	//----- output variables
	public static String 	o_destination_address;
	public static String   	o_source_address="0001";
	public static String   	o_sequence="0";
	public static int   	o_sequenceint=Integer.parseInt(o_sequence);
	public static String   	o_group="0";
	public static byte 		o_header = (byte) 0x01;
	public static byte 		o_control_field= (byte) 0x50; //(byte) 0x40;
	public static byte 		o_control_ack= (byte) 0x43;
	
	//---- Timings
	
	public static int serialDelay = 0;
	
	/*------------------------------------------------------------
	 *---------------------------MODBUS --------------------------
	 *------------------------------------------------------------ 
	 * */
	//-----modbus output variables
	
	public static byte mAddress = 0;
	public static int mFunction =0;
	public static int mDataAddress=0;
	public static int mCoils=0;
	public static int mRegs =0;
	
	//-----modbus input variables
	
	public static byte iAddress = 0;
	public static byte iFunction =0;
	public static byte iError=0;
	public static int iDataSize =0;	
	public static boolean iEOF =false;	
	
	public static final int COIL_ON   = 0xFF00;
	public static final int COIL_OFF  = 0x0000;
	
	private static final long timeoutMBExtended=1000;
	
	private static final long timeoutMB=250;
	
	private static boolean extendedWait = false;
	
	public static boolean onHold = false;
	
	public static class modFunction{
		
		private static byte[] arrangeData(Modbus modbusOutput){
			byte[] mbdata=modbusOutput.data;
			return arrangeData(mbdata);
		}
		
		private static byte[] arrangeData(byte[] mbdata){
			int bytecounter=0;
			for(int i=0; i<mbdata.length; i++){
				switch(mbdata[i]){
					case 	(byte)0xFF: 	bytecounter++; break;
					case	(byte)0x03:		bytecounter++; break;
					case	(byte)0x04:		bytecounter++; break;
					case	(byte)0x0D:		bytecounter++; break;
					default:				break;
				
				}
				bytecounter++;
			}
			byte[] arrangedData = new byte[bytecounter];
			bytecounter=0;
			for(int i=0; i<mbdata.length; i++){
				switch(mbdata[i]){
					case 	(byte)0xFF: 	arrangedData[bytecounter]=(byte)0xFF; bytecounter++; arrangedData[bytecounter]=(byte)0x00; break;
					case	(byte)0x03:		arrangedData[bytecounter]=(byte)0xFF; bytecounter++; arrangedData[bytecounter]=(byte)0xFC; break;
					case	(byte)0x04:		arrangedData[bytecounter]=(byte)0xFF; bytecounter++; arrangedData[bytecounter]=(byte)0xFB; break;
					case	(byte)0x0D:		arrangedData[bytecounter]=(byte)0xFF; bytecounter++; arrangedData[bytecounter]=(byte)0xF2; break;
					default:				arrangedData[bytecounter]=mbdata[i];break;
				
				}
				bytecounter++;
			}
			return arrangedData;
		}
		
		public static byte[] getChecksum(byte[] o_data,byte[] dest_address ,byte[] src_address,byte[] seq,byte[] grp){
			int localchecksum=0; 
			localchecksum= localchecksum+SysUtils.byteToUnsignedInt(o_header);
			localchecksum= localchecksum+SysUtils.byteToUnsignedInt(o_control_field); 
			for(int i=0;i<dest_address.length; i++){
				localchecksum= localchecksum+SysUtils.byteToUnsignedInt(dest_address[i]); 
			}
			localchecksum= localchecksum+SysUtils.byteToUnsignedInt(Separator1); 
			for(int i=0;i<src_address.length; i++){
				localchecksum= localchecksum+SysUtils.byteToUnsignedInt(src_address[i]); 
			}
			localchecksum= localchecksum+SysUtils.byteToUnsignedInt(Separator2); 
			for(int i=0;i<grp.length; i++){
				localchecksum= localchecksum+SysUtils.byteToUnsignedInt(grp[i]); 
			}
			localchecksum= localchecksum+SysUtils.byteToUnsignedInt(Separator3); 
			for(int i=0;i<seq.length; i++){
				localchecksum= localchecksum+SysUtils.byteToUnsignedInt(seq[i]); 
			}
			localchecksum= localchecksum+SysUtils.byteToUnsignedInt(SOT);	 
			for(int i=0;i<o_data.length; i++){
				localchecksum= localchecksum+SysUtils.byteToUnsignedInt(o_data[i]); 
			} 
			String localcString = Integer.toHexString(localchecksum);
			byte[] localbytes = localcString.getBytes(StandardCharsets.UTF_8);
			byte[] localbyteaux=localbytes; 
			if(localbytes.length>4){
				localbyteaux = new byte[4];
				localbyteaux[0]=localbytes[0];
				localbyteaux[1]=localbytes[1];
				localbyteaux[2]=localbytes[2];
				localbyteaux[3]=localbytes[3];
				
			} 
			return localbyteaux;
		}
		 
		 
		public static void sendMBD(byte[] mbdata){
			byte[] o_data = arrangeData(mbdata);
			sendMB(o_data);
		}
		
		public static void sendMB(Modbus modbusOutput){
			byte[] o_data = arrangeData(modbusOutput);
			sendMB(o_data);
		}
		
		public static void sendMB(byte[] o_datax){
			
			byte[] o_data = o_datax;
			byte[] dest_address = o_destination_address.getBytes(StandardCharsets.UTF_8);
			byte[] src_address = o_source_address.getBytes(StandardCharsets.UTF_8);
			byte[] seq = o_sequence.getBytes(StandardCharsets.UTF_8);
			byte[] grp = o_group.getBytes(StandardCharsets.UTF_8);  
			byte[] chck = getChecksum(o_data, dest_address, src_address, seq, grp);
			out_buffer = new byte[9+dest_address.length+src_address.length+o_data.length+seq.length+grp.length+chck.length];
			int bytecounter=0;
			out_buffer[bytecounter]=o_header;
			bytecounter++;
			out_buffer[bytecounter]=o_control_field;
			bytecounter++;
			for(int i=0;i<dest_address.length; i++){
				out_buffer[bytecounter]=dest_address[i];
				bytecounter++;
			}
			out_buffer[bytecounter]=Separator1;
			bytecounter++;
			for(int i=0;i<src_address.length; i++){
				out_buffer[bytecounter]=src_address[i];
				bytecounter++;
			}
			out_buffer[bytecounter]=Separator2;
			bytecounter++;
			for(int i=0;i<grp.length; i++){
				out_buffer[bytecounter]=grp[i];
				bytecounter++;
			}
			out_buffer[bytecounter]=Separator3;
			bytecounter++;
			for(int i=0;i<seq.length; i++){
				out_buffer[bytecounter]=seq[i];
				bytecounter++;
			}
			out_buffer[bytecounter]=SOT;			
			bytecounter++;
			for(int i=0;i<o_data.length; i++){
				out_buffer[bytecounter]=o_data[i];
				bytecounter++;
			} 
			out_buffer[bytecounter]=DLE;			
			bytecounter++;
			out_buffer[bytecounter]=ETX;			
			bytecounter++;
			for(int i=0;i<chck.length; i++){
				out_buffer[bytecounter]=chck[i];
				bytecounter++;
			}
			out_buffer[bytecounter]=EOT;			
			bytecounter++;
			serialSend();
			startMBT();
			onHold=true;
			o_sequenceint++;
			if(o_sequenceint>100) o_sequenceint=1;
			o_sequence=""+o_sequenceint;
		}
		
		
		
		private static void singleMB(byte fAddress, int fDataAddress, int fCoils){
			mAddress = fAddress;
			mDataAddress = fDataAddress;
			if(mFunction<3 || mFunction ==5)mCoils = fCoils;
			else mRegs = fCoils;
			
			Modbus modbusOutput = new Modbus(); 
			if(mFunction==1) modbusOutput.readCoilStatus(fAddress, fDataAddress, fCoils);
			if(mFunction==2) modbusOutput.readDiscreteInputs(fAddress, fDataAddress, fCoils);
			if(mFunction==3) modbusOutput.readHoldingRegisters(fAddress, fDataAddress, fCoils);
			if(mFunction==4) modbusOutput.readInputRegisters(fAddress, fDataAddress, fCoils);
			if(mFunction==5) modbusOutput.writeSingleCoil(fAddress, fDataAddress, fCoils);
			if(mFunction==6) modbusOutput.writeSingleRegister(fAddress, fDataAddress, fCoils);
			sendMB(modbusOutput);
		}
		
		public static void f1(byte fAddress, int fDataAddress, int fCoils){
			mFunction = 0x01;
			singleMB(fAddress, fDataAddress, fCoils);
		}
		
		public static void f2(byte fAddress, int fDataAddress, int fCoils){
			mFunction = 0x02; 
			singleMB(fAddress, fDataAddress, fCoils);
		}
		
		public static void f3(byte fAddress, int fDataAddress, int fRegs){
			mFunction = 0x03; 
			singleMB(fAddress, fDataAddress, fRegs);
		}
		
		public static void f4(byte fAddress, int fDataAddress, int fRegs){
			mFunction = 0x04; 
			singleMB(fAddress, fDataAddress, fRegs);
		}
		
		public static void f5(byte fAddress, int fDataAddress, boolean coilState){
			mFunction = 0x05; 
			int coilValue = COIL_OFF;
			if(coilState) coilValue = COIL_ON;
			singleMB(fAddress, fDataAddress, coilValue);
		}
		
		public static void f6(byte fAddress, int fDataAddress, int regValue){
			mFunction = 0x06; 
			singleMB(fAddress, fDataAddress, regValue);
		}
		
		public static void f15(byte fAddress, int fDataAddress, int fCoils, int[] datax){
			mFunction = 0x0F; 
			mAddress = fAddress;
			mDataAddress = fDataAddress;
			mCoils = fCoils;
			Modbus modbusOutput = new Modbus(); 
			modbusOutput.writeMultipleCoils(fAddress, fDataAddress, fCoils, datax);
			sendMB(modbusOutput);
		}
		
		public static void f16(byte fAddress, int fDataAddress, int fRegs, int[] datax){
			mFunction = 0x10; 
			mAddress = fAddress;
			mDataAddress = fDataAddress;
			mRegs = fRegs;
			Modbus modbusOutput = new Modbus(); 
			modbusOutput.writeMultipleRegisters(fAddress, fDataAddress, fRegs, datax);
			sendMB(modbusOutput);
		}
	}
	
	public static void setDestinationInt(int destin){
		o_destination_address= String.format("%04d", destin);
	}
	
	public static void setBroadcast(){
		o_destination_address= "FFFF";
	}
	
	public static boolean confirmACK(){
		boolean localack = ack_control & eof_received;
		if(localack){
			if(wmxdebug) System.out.println("ACK received, waiting for response");
			ack_control=false;
			resetProtocol();
			waitlonger();
		}
		return localack;
	}
	
	public static int iAddressInt(){
		return iAddress & 0xFF;
	}
	
	public static int mAddressInt(){
		return mAddress & 0xFF;
	}
	
	public static boolean gotResponse(){
		if(iAddress==mAddress && iFunction==mFunction) return true;
		return false;
	}

	public static void flush_in_buffer(){
		int i=0;
		for (i=0;i<in_buffer.length;i++){
			in_buffer[i]=0;
		}		
	}	
	public static void flush_in_databuffer(){
		int i=0;
		for (i=0;i<databuffer.length;i++){
			databuffer[i]=0;
		}		
	}		

	
	public static void buffercopy(){
		for(int i=0; i<in_buffer.length;i++){
			buffer[i]=in_buffer[i];
		}
		
	}
	
	public static boolean chcks(int value){
		int chs =0;
		for(int i=1;i<value;i++){
			chs= chs ^ in_buffer[i];			
		}		
		if(chs==(int)in_buffer[value]) { 
					if(wmxdebug) System.out.print("[chk:ok]");
			       return true;
		}
		else {
			  if(wmxdebug) System.out.print("[chk:fail] ");
			  return false;}
	}	
	
	private static void waitlonger(){
		extendedWait=true;
		startMBT();
	}
	
	private static void MBdata(byte[] response, int size){
		if(wmxdebug) SysUtils.printhex(response,size);
		int res_s = size;
		  	int i=0;
		  	byte[] localbuffer = new byte[500];
		  	int local_b_counter=0;
		  	int localstate=0;
		  	for (i=0;i<res_s;i++){ 
		  		startMBT();
		  		localbuffer[local_b_counter]=(byte)response[i];
		  		//serialDebug();
		  		Byte value = (byte)response[i];
		  		Byte value2 =0;
		  		if(localstate==5) { 
		  				
		  				value2 = (byte)response[i+1];
		  				local_b_counter++;
		  			    i++;
		  			}
		  		
		  		switch(localstate){
		  			case	0:	localstate=getAddress(value,localstate);
	  							break;
		  			case	1:	localstate=getFunction(value,localstate);
		  						break;
	  		  		case	2:	localstate=getByteSize(value,localstate);
	  		  					break;
	  		  		case	3:	localstate=getDataBytes(value,localstate,local_b_counter);
	  							break;
	  		  		case	4:	localstate=getErrorCode(value,localstate);
	  							break;
	  		  		case	5:	localstate=getCRC(value, value2,localstate);
	  							break;
	  		  		case	6:	localstate=getCRC(value, value2,localstate);
					break;
		  			default	 :	break; 
		  		}
		  		local_b_counter++;
		  	}	
	}
	
	public static int getAddress(Byte data, int localstate){
		currentAddress= data; ///
		localstate=1;
		if(wmxdebug) System.out.println("[State:"+localstate+"] "); 
		return localstate;
	}

	public static int getFunction(Byte data, int localstate){
		currentFunction=data; 
		if(SysUtils.byteToUnsignedInt(currentFunction)>SysUtils.byteToUnsignedInt((byte)0x81)){
			byte[] toprint = {currentFunction};
		  	    if(wmxdebug) System.out.print("[currentFunction is Error] ");
		  	  if(wmxdebug) SysUtils.printhex(toprint, 1);
		  		localstate=4;
			if(wmxdebug) System.out.println("[State:"+localstate+"] ");
		} else if(currentFunction==ModbusDefinitions.PRESET_MULTIPLE_REGISTERS){
			localstate=5;
			if(wmxdebug) System.out.println("[State:"+localstate+"] ");
		} else
		{
			if(wmxdebug) System.out.println("[function:"+SysUtils.byteToUnsignedInt(currentFunction)+"] ");
			localstate=2;
			if(wmxdebug) System.out.println("[State:"+localstate+"] ");
		}
		return localstate;
		 
	}	
	
	public static int getByteSize(Byte data, int localstate){ 
		currentDataSize=SysUtils.byteToUnsignedInt(data);  
		localstate=3;
		if(wmxdebug) System.out.println("[State:"+localstate+" size "+currentDataSize+"] ");
		return localstate;
	}

	public static int getDataBytes(Byte data, int localstate, int currentcount){
		if(wmxdebug) System.out.println("[State:extracting data ]");
		if(currentcount<=currentDataSize+2){
			databuffer[currentcount-3]=(byte)data;
			localstate=3; 
			if(wmxdebug) System.out.println("[State:extracting data"+(currentcount-3)+"]");
		}else{
			localstate=5;
			if(wmxdebug) System.out.println("[State:"+localstate+"] "); 
		} 
		return localstate;
	}
	
	public static int getErrorCode(Byte data, int localstate){
		currentError=(byte)data;
		localstate=5;
		//debug_modbus_error(); 
		return localstate;
	}	
	
	public static void getIntData(){
		ResponseData = new int[currentDataSize/2];
		for(int i=0; i<currentDataSize; i=i+2){
			ResponseData[i/2]= ((databuffer[i] & 0xff) << 8 ) | (databuffer[1+i] & 0xff);
		}
	}
	
	public static int getCRC(Byte data, Byte data2, int localstate){ ////////////////////
		//hacer algo con CRC
		iAddress = currentAddress;
		iFunction =currentFunction;
		iError=currentError;
		iDataSize =currentDataSize;	
		if(wmxdebug) SysUtils.printbyte2hex(iAddress);  
		if(wmxdebug) System.out.println(" : iAddress"); 
		if(wmxdebug) SysUtils.printbyte2hex(iFunction); 
		if(wmxdebug) System.out.println(" : iFunction");
		if(wmxdebug) SysUtils.printbyte2hex(iError);
		if(wmxdebug) System.out.println(" : iError");
		if(wmxdebug) System.out.print(iDataSize);
		if(wmxdebug) System.out.println(" : iDataSize");  
		if ((iFunction==(byte)0x03 || iFunction==(byte)0x04 )&& iDataSize>0){
			getIntData();   
			if(wmxdebug) System.out.println(" --int data received --");
		} 
		iEOF=true; 
		return localstate+1; 
		
	}	

	public static void resetProtocol(){
		eof_received=false;
		data_received=false;
		extendedWait =false;
		packet_state=0;
		in_b_counter=0; 

		destination_address_counter=0;
		group_number_counter = 0;
		RSSI_counter = 0;
		CHCKS_counter=0;
		DATA_counter=0;
		sequence_counter=0;
		source_address_counter=0;
		
		destination_address_counter = 0;
		source_address_counter = 0;
		group_number_counter = 0;
		sequence_counter = 0;
		RSSI_counter = 0;
		CHCKS_counter = 0;
		DATA_counter = 0;
		if(wmxdebug) System.out.println("[MB RST]");
		flush_in_buffer();
		startMBT();
		onHold=false;
		// 	queda en estado 8 hasta no ser reseteado por checkpacket
	}	
	
	public static void serialSend(){
		if(dummysend){
			out_buffer=dummymes;
		}  
		try {
			if(wmxdebug) System.out.println(":::::::outbuffer");
			rs485.tx();
			Thread.sleep(serialDelay);
			if(!Sigmation.noPiMode) serial.write(out_buffer);
			/*for (int i=0; i<out_buffer.length;i++){
				if(!RemoteDAppi.noPiMode) serial.write(out_buffer[i]);
				Thread.sleep(5);
			}
			for (int i=0; i<10;i++){
				if(!RemoteDAppi.noPiMode) serial.write((byte)0x00);
				Thread.sleep(5);
			}*/
			Thread.sleep(serialDelay);
			rs485.rx();
			if(wmxdebug) SysUtils.printhex(out_buffer, out_buffer.length, true);
			if(wmxdebug) System.out.println("::::::: ");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if(wmxdebug) e.printStackTrace();
		}		
	}
	
	public static void resetModem(){
		/*try { 
			if(!RemoteDAppi.noPiMode) serial.write("exit \r\n exit \n\r");
			Thread.sleep(50);
			if(wmxdebug) System.out.println("reset modem");
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			if(wmxdebug) e.printStackTrace();
		}*/
	}
	
	public static void debug_modbus_error(){
		if(wmxdebug) System.out.println("Modbus Error:");
		if(wmxdebug) System.out.println("[State <error>:"+packet_state+"] ");
	}
	
	public static void startMBT(){
		//comenzar timers aca
		date1= System.nanoTime();
		date2= System.nanoTime();
	}
	
	public static boolean hasDataPacket(){
		checkMBT();
		return eof_received;
	}
	
	public static boolean checkMBT(){
		//comparar timers aca
		date2= System.nanoTime();
		long localtimeout =0;
		if(extendedWait) localtimeout = timeoutMBExtended;
		else localtimeout = timeoutMB;
		long difference = date2- date1;
		if(difference<localtimeout*1000000) return false;
		resetProtocol();
		return true;
	}
	
	private static void serialDebug(){
		byte[] toprint = {in_buffer[in_b_counter]};
	  	    if(wmxdebug) System.out.print("["+in_b_counter+"] ");
	  	  if(wmxdebug) SysUtils.printhex(toprint, 1);
	}
	
	//placeholders-----------------------
	
	private static void getHeader(){
		if(in_buffer[in_b_counter]==SOH){
			head_received=true;			
			in_b_counter++;
			packet_state=1;
		}
	}
	
	private static void getControlField(){
		if(in_buffer[in_b_counter]==(byte)0x42){
			if(wmxdebug) System.out.println(" Data Received");
			ack_control= false;
			packet_state=2;
		} else if (in_buffer[in_b_counter]==o_control_ack){
			if(wmxdebug) System.out.println(" ACK Received");
			ack_control= true;
			packet_state=2;
		}
	}
	
	private static void getDestinationAddress(){
		if(in_buffer[in_b_counter]!=Separator1 && destination_address_counter<4){
			destination_address_b[destination_address_counter]=in_buffer[in_b_counter];
			destination_address_counter++;
			in_b_counter++;
		} else if(in_buffer[in_b_counter]!=Separator1 && destination_address_counter>3){
			resetProtocol();
		} else {
			packet_state=3;
			if(wmxdebug) System.out.println(" Separator1");
			in_b_counter++;
		}
	}
	
	private static boolean checkBorders(byte[] buffer, int counter){
		if (counter>=buffer.length) {
			resetProtocol();
			return true;
		} return false;
	}
	
	private static void getSourceAddress(){
		if(checkBorders(source_address_b,source_address_counter)) return;
		source_address_b[source_address_counter]=in_buffer[in_b_counter];
		source_address_counter++;
		in_b_counter++;
	}
	
	private static void getGroupNumber(){
		if(checkBorders(group_number_b,group_number_counter)) return;
		group_number_b[group_number_counter]=in_buffer[in_b_counter];
		group_number_counter++;
		in_b_counter++;
	}
	
	private static void getSequence(){
		if(checkBorders(sequence_b,sequence_counter)) return;
		sequence_b[sequence_counter]=in_buffer[in_b_counter];
		sequence_counter++;
		in_b_counter++;
	}
	
	private static void getRSSI(){
		if(checkBorders(RSSI_b,RSSI_counter)) return;
		RSSI_b[RSSI_counter]=in_buffer[in_b_counter];
		RSSI_counter++;
		in_b_counter++;
	}
	
	private static void getPacketStats(){
		if(packet_state>2 && packet_state<7){
			if(in_buffer[in_b_counter]==Separator2){
				packet_state=4;
				if(wmxdebug) System.out.println(" separator2");
				in_b_counter++;
			} else if(in_buffer[in_b_counter]==Separator3){
				packet_state=5;
				if(wmxdebug) System.out.println(" separator3");
				in_b_counter++;
			} else if(in_buffer[in_b_counter]==Separator4){
				packet_state=6;
				if(wmxdebug) System.out.println(" separator4");
				in_b_counter++;
			} else if(in_buffer[in_b_counter]==SOT){
				packet_state=7;
				if(wmxdebug) System.out.println(" SOT");
				in_b_counter++;
			} else {
				
				switch(packet_state){
					
					case 3: getSourceAddress();
							break;
					
					case 4: getGroupNumber();
							break;
							
					case 5: getSequence();
							break;
					
					case 6: getRSSI();
							break; 				
				}
				
			}
			
			
		}
	}
	
	private static void getData(){
		if(in_buffer[in_b_counter]!=DLE && !DLE_state){
			if (!ack_control) DATA_b[DATA_counter]=in_buffer[in_b_counter];
			if (!ack_control) DATA_counter++;
			in_b_counter++;
		} else if(DLE_state){
			if(in_buffer[in_b_counter]==ETX){
				in_b_counter++;
				if(wmxdebug) System.out.println(" ETX");
				DLE_state= false;
				packet_state=8;
			} else {
				DLE_state= false;  //false DLE ETX state, proceed to continue to store data
				if (!ack_control) DATA_b[DATA_counter]=in_buffer[in_b_counter-1];
				if (!ack_control) DATA_counter++;
				if (!ack_control) DATA_b[DATA_counter]=in_buffer[in_b_counter];
				if (!ack_control) DATA_counter++;
				in_b_counter++;
			}			
		} else if(in_buffer[in_b_counter]==DLE ){
			if (ack_control) DATA_counter=0;
			DLE_state= true;  //possible DLE ETX command incoming, data storage on hold
			in_b_counter++;		
		}
	}
	
	private static void getCHCK(){
		if(in_buffer[in_b_counter]==EOT ){
			if(wmxdebug) System.out.println(" EOT");
			EOTP();
			
		} else {
			CHCKS_b[CHCKS_counter]=in_buffer[in_b_counter];
			CHCKS_counter++;
			in_b_counter++;
		}
	}
	
	private static void storeData(){
		STORED_DATA_counter=0;
		byte fullByte= (byte) 0xff;
		for(int i=0; i<DATA_counter ; i++){
			if(DATA_b[i]==fullByte){
				STORED_DATA[STORED_DATA_counter]=(byte)((DATA_b[i] ^ DATA_b[i+1]) & 0xFF);
				i++;
				STORED_DATA_counter++;
			}else{
				STORED_DATA[STORED_DATA_counter]=DATA_b[i];
				STORED_DATA_counter++;
			}
		}
	}
	
	public static byte[] getStoredData(){
		byte[] localstoredData = new byte[STORED_DATA_counter];
		for(int i=0; i<STORED_DATA_counter ; i++){
			localstoredData[i]=STORED_DATA[i];
		}
		return localstoredData;
	}
	
	public static void setStoredData(byte[] storedata){
		STORED_DATA_counter=storedata.length;
		for(int i=0;i<storedata.length;i++){
			STORED_DATA[i]=storedata[i];
		} 
	}
	
	private static void getStats(){
		byte[] localByteArray= new byte[destination_address_counter];
		for(int i=0; i<destination_address_counter; i++){
			localByteArray[i]=destination_address_b[i];
		}
		destination_address= new String( localByteArray, StandardCharsets.UTF_8);
		
		localByteArray= new byte[source_address_counter];
		for(int i=0; i<source_address_counter; i++){
			localByteArray[i]=source_address_b[i];
		}
		source_address=new String( localByteArray, StandardCharsets.UTF_8);
		
		localByteArray= new byte[group_number_counter];
		for(int i=0; i<group_number_counter; i++){
			localByteArray[i]=group_number_b[i];
		}
		group_number=new String( localByteArray, StandardCharsets.UTF_8);
		
		localByteArray= new byte[sequence_counter];
		for(int i=0; i<sequence_counter; i++){
			localByteArray[i]=sequence_b[i];
		}
		sequence=new String( localByteArray, StandardCharsets.UTF_8);
		
		localByteArray= new byte[RSSI_counter];
		for(int i=0; i<RSSI_counter; i++){
			localByteArray[i]=RSSI_b[i];
		}
		RSSI=new String( localByteArray, StandardCharsets.UTF_8);
		
		localByteArray= new byte[CHCKS_counter];
		for(int i=0; i<CHCKS_counter; i++){
			localByteArray[i]=CHCKS_b[i];
		}
		CHCKS=new String( localByteArray, StandardCharsets.UTF_8);
	}
	
	private static void EOTP(){
		in_b_counter++;
		storeData();
		getStats();
		if(wmxdebug) debugPacket();
		resetProtocol2();		
	}
	
	private static void resetProtocol2(){
		packet_state=0;
		eof_received=true;
		if(DATA_counter>0) data_received=true;
		else data_received = false;
		in_b_counter=0;	
	}
	
	private static void debugPacket(){
		System.out.println(" ");
		System.out.println("  | Packet received");
		System.out.println("destination_address "+destination_address);
		System.out.println("source_address "+source_address);
		System.out.println("group_number "+group_number);
		System.out.println("sequence "+ sequence);
		System.out.println("RSSI -"+RSSI+" db");
		System.out.println("CHCKS "+CHCKS);
		System.out.println("STORED_Data_counter "+STORED_DATA_counter); 

	}
	
	private static void dataModbus(byte[] response){
		int res_s = response.length;
		  	int i=0;
		  	for (i=0;i<res_s;i++){ 
		  		startMBT();
		  		in_buffer[in_b_counter]=(byte)response[i];
		  		serialDebug();
		  		
		  		switch(packet_state){
		  			case	0:	getHeader();
	  							break;
		  			case	1:	getControlField();
		  						break;
	  		  		case	2:	getDestinationAddress();
	  		  					break;
	  		  		case	3:	getPacketStats();
	  							break;
	  		  		case	4:	getPacketStats();
								break;
	  		  		case	5:	getPacketStats();
								break;
	  		  		case	6:	getPacketStats();
								break;
	  		  		case	7:	getData();
	  							break;
	  		  		case	8:	getCHCK();
	  							break;
		  			default	 :	break; 
		  		}
		  	}	
		  	if(wmxdebug) System.out.println("|*|");
	}
	/*
	private static void dataSerial(byte[] response){
		int res_s = response.length;
		  	int i=0;
		  	for (i=0;i<res_s;i++){ 
		  		startMBT();
		  		in_buffer[in_b_counter]=(byte)response[i];
		  		//serialDebug();
		  		Byte value = (byte)response[i];
		  		Byte value2 =0;
		  		if(packet_state==5) { 
		  				
		  				value2 = (byte)response[i+1];
		  			    in_b_counter++;
		  			    i++;
		  			}
		  		
		  		switch(packet_state){
		  			case	0:	getAddress(value);
	  						break;
		  			case	1:	getFunction(value);
		  						break;
	  		  		case	2:	getByteSize(value);
	  		  					break;
	  		  		case	3:	getDataBytes(value);
	  							break;
	  		  		case	4:	getErrorCode(value);
	  							break;
	  		  		case	5:	getCRC(value, value2);
	  							break;
		  			default	 :	break; 
		  		}
		  	}	
	}
	*/
	
	private static class rs485{
		public static void rx(){
			SystemIO.rs485.tx();
		}		
		public static void tx(){
			SystemIO.rs485.tx();
		}
	}
	
	
	public static void modbusIni(){
		/*
		 *   PROTOCOLO MODBUS
		 *   
		 * */
		if(!Sigmation.noPiMode){
			startMBT();
			rs485.tx();
			serial = SerialFactory.createInstance();
	      	try {
	      		SerialConfig config = new SerialConfig();
	            config.device("/dev/serial0")
	                  .baud(Baud._9600)
	                  .dataBits(DataBits._8)
	                  .parity(Parity.NONE)
	                  .stopBits(StopBits._1)
	                  .flowControl(FlowControl.NONE);
	            if(wmxdebug) System.out.println("WMXCom: Started serial /dev/serial0");
	            // open the default serial device/port with the configuration settings
	            serial.open(config);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
	
			serial.addListener(new SerialDataEventListener() {
	            @Override
	            public void dataReceived(SerialDataEvent event) {
	          	  try {
	          		  	byte[] response = event.getBytes();
	          		    //dataSerial(response);
	          		  	dataModbus(response);
	          		  				
					} catch (Exception e) {
						head_received=false;
						in_b_counter=0;
						packet_state=0;
						if(wmxdebug) e.printStackTrace();
						flush_in_buffer();
					}
	   	
	                   
	            	
	            }            
	        });		
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				if(wmxdebug) e.printStackTrace();
			}
		}
		
		
	}
	
	public static void retrieveData(){
		MBdata(STORED_DATA,STORED_DATA_counter);
	}
	
	public static void testCom(){
		modbusIni();
		while(!Sigmation.resetSignal){
			//loop waiting for data
			checkMBT();
			if(eof_received){
				MBdata(STORED_DATA,STORED_DATA_counter);
				if(iEOF){
					if(wmxdebug) System.out.println("iEOF received");
					iEOF=false;
					o_destination_address="5678";
					o_source_address="1234";
					modFunction.f3((byte)0x01, 0x006b, 0x0003);
					//out_buffer=dummymes;
					//serialSend();
				}
				eof_received=false;
			}
			
		}
	}
	
}
