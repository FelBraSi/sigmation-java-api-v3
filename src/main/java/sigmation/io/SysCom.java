package sigmation.io;

import java.io.IOException;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;

import sigmation.utils.SysUtils;


public class SysCom {
	public static byte[] out_buffer = new byte[1] ;
	public static byte[] in_buffer = new byte[300] ;
	public static byte[] buffer = new byte[300] ;
	public static byte[] databuffer = new byte[300] ;
	public static byte appId=0;
	public static byte origAddress = 0 ;
	public static byte destAddress = 0 ;
	public static byte currentAddress = 0 ;
	public static byte currentZone = 0 ;
	public static boolean head_received=false;
	public static boolean eof_received=false;
	public static int pkt_size=0;  
	public static int packet_state=0;
	public static int in_b_counter=0;
	public static GpioPinDigitalOutput modemEnable ;
	public static Serial serial;
	
	static boolean syscomdebug=true;
	
	public static void enableModem(){
		if (modemEnable.isLow()) modemEnable.high();
	}
	public static void flush_in_buffer(){
		int i=0;
		for (i=0;i<in_buffer.length;i++){
			in_buffer[i]=0;
		}		
	}	
	public static void flush_in_databuffer(){
		int i=0;
		for (i=0;i<databuffer.length;i++){
			databuffer[i]=0;
		}		
	}		
	public static void output_buffer(int command, int dest, int orig){
		byte[] comm = {(byte)command};
		output_buffer(1,dest, orig,1, comm);		
	}
	
	public static void buffercopy(){
		for(int i=0; i<in_buffer.length;i++){
			buffer[i]=in_buffer[i];
		}		
	}
	
	public static boolean chcks(int value){
		int chs =0;
		for(int i=1;i<value;i++){
			chs= chs ^ in_buffer[i];			
		}		
		if(chs==SysUtils.byteToUnsignedInt(in_buffer[value])) { 
											if(syscomdebug) System.out.print("[chk:ok] ");
			                               return true;}
		else {
			if(syscomdebug) System.out.print("[chk:fail] ");
			  return false;}
	}	
	
	public static void output_buffer(int app_id,int dest, int orig,int largoData, byte[] data){
		
		int chcks=0;
		int i=0;
		byte[] l_out_buffer= new byte[largoData+8];
		l_out_buffer[0]=  (byte) 0xFE;			//cabecera
		l_out_buffer[1]=  (byte) app_id;      	//app id
		chcks=chcks ^ (int)l_out_buffer[1] ;
		l_out_buffer[2]=  (byte) dest;			//destino
		chcks=chcks ^ (int)l_out_buffer[2] ;
		l_out_buffer[3]=  (byte) orig;			//origen
		chcks=chcks ^ (int)l_out_buffer[3] ;
		l_out_buffer[4]=  0x00;    				//Zona
		chcks=chcks ^ (int)l_out_buffer[4] ;
		l_out_buffer[5]=  (byte) largoData;		//largo data
		chcks=chcks ^ (int)l_out_buffer[5] ;
		for(i=0;i<largoData;i++){
			l_out_buffer[6+i]=  (byte) data[i];   //data
			chcks=chcks ^ (int)l_out_buffer[6+i] ;
		}	
		l_out_buffer[6+largoData]=  (byte) chcks;		    //chksm
		l_out_buffer[7+largoData]=  (byte) 0xFD;		    //fin
		if(syscomdebug) System.out.println("<out>");
		if(syscomdebug) SysUtils.printhex(l_out_buffer,l_out_buffer.length);
		if(syscomdebug) System.out.println("</out>");
		out_buffer=l_out_buffer;
	}
	
	public static byte[] packetCheck(){
		return packetCheck(buffer);
	}
	
	public static byte[] packetCheck(byte[] dpack){
		
		/*================================================
		 *  pData[0] = estados de error 
		 *  
		 *  cabecera: 								0x03
		 *  direccion: 								0x05
		 * 	checksum: 								0x07
		 *  cabecera + direccion : 					0x08
		 *  cabecera + checksum : 					0x0A
		 * 	end: 									0x0B
		 *  direccion + checksum : 					0x0C
		 * 	cabecera + end : 						0x0E
		 *  cabecera + direccion + checksum : 		0x0F
		 *  direccion + end : 						0x10
		 *  checksum + end : 						0x12
		 *  cabecera + direccion + end : 			0x13
		 *  cabecera + direccion + checksum + end : 0x1A
		 *  cabecera + checksum + end : 			0x15 
		 *  direccion + checksum + end : 			0x17
		 * 
		 *==============================================*/
		try {
			resetProtocol();
			int chcks=0;
			int i=0;
			byte[] pData= new byte[5];
			pData[0]=0x00;
			if(dpack[0]!=(byte) 0xFE) pData[0]=(byte)0x03;
			pData[1]=dpack[1];       			//app_id
			chcks=chcks ^ (int)dpack[1] ;
			if(dpack[2]!=(byte) 0xFF) pData[0]=(byte)(pData[0]+0x05);  //si no va dirigido al servidor (0xFF), desechar
			pData[2]=dpack[2]; 				// dest
			chcks=chcks ^ (int)dpack[2] ;
			pData[3]=dpack[3]; 				// orig
			currentAddress=pData[3];
			chcks=chcks ^ (int)dpack[3] ;
			pData[4]=dpack[4]; 				// zona
			currentZone=pData[4];
			chcks=chcks ^ (int)dpack[4] ;
			int largoData = (int) dpack[5];
			chcks=chcks ^ (int)dpack[5] ;
			
			if(largoData>0) {
				byte[] dummy = {dpack[6]};
				if(syscomdebug) SysUtils.printhex(dummy, 1);}
			else{
				byte[] dummy = {(byte)0xAB};
				if(syscomdebug) SysUtils.printhex(dummy, 1);
			}
			if(largoData>0) databuffer= new byte[largoData];
			for(i=0;i<largoData;i++){
				databuffer[i] = dpack[6+i] ;
				chcks=chcks ^ (int)dpack[6+i] ;
			}		
			if (dpack[6+largoData]!=  (byte) chcks) pData[0]=(byte)(pData[0]+0x07);  //checksum fail
			if (dpack[7+largoData]!=  (byte) 0xFD) pData[0]=(byte)(pData[0]+0x0B);	//end fail
			if(syscomdebug) System.out.println("[ chck: og- "+SysUtils.byteToUnsignedInt(dpack[6+largoData])+"  vs calc- "+SysUtils.byteToUnsignedInt((byte) chcks)+" ]"); 
			return pData;
		} catch (Exception e) {
			if(syscomdebug) e.printStackTrace();
			resetProtocol();
			return null;
		}
		
	}
	
	public static void checkHeader(Byte data){
		//if(syscomdebug) System.out.print("[entry] ");
		//if(syscomdebug) SysUtils.printbyte2hex((byte)data);
	    //if(syscomdebug) System.out.print("[match] ");
	    //if(syscomdebug) SysUtils.printbyte2hex((byte)0xFE);
		if(data==(byte)0xFE){
			in_buffer[0]=(byte)data;
			packet_state=1;
			in_b_counter=1;
			head_received=true;
		}	
	}

	public static void getAppId(Byte data){
		if(in_buffer[0]==(byte)0xFE){
			in_buffer[in_b_counter]=(byte)data;
			appId=in_buffer[in_b_counter];
			packet_state=2;
			in_b_counter=2;
		}	
	}
	
	public static void getDest(Byte data){ 
			in_buffer[in_b_counter]=(byte)data;
			destAddress=in_buffer[in_b_counter];
			packet_state=3;
			in_b_counter=3;
	}
	
	public static void getOrig(Byte data){ 
			in_buffer[in_b_counter]=(byte)data;
			origAddress=in_buffer[in_b_counter];
			packet_state=4;
			in_b_counter=4; 
	}

	public static void getZone(Byte data){
		if(data!=(byte)0xFF){
			in_buffer[in_b_counter]=(byte)data;
			currentZone=in_buffer[in_b_counter];
			packet_state=5;
			in_b_counter=5;
		} else {
			packet_state=0;
			in_b_counter=0;
		}
	}	
	
	public static void getDataLength(Byte data){
			in_buffer[in_b_counter]=(byte)data;
			pkt_size=SysUtils.byteToUnsignedInt(in_buffer[in_b_counter]);
			packet_state=6;
			in_b_counter=6;
	}

	public static void fillData(Byte data){
		if(in_b_counter<=SysUtils.byteToUnsignedInt(in_buffer[5])+5){
			in_buffer[in_b_counter]=(byte)data;
			packet_state=6;
			in_b_counter++;
		} 
		
		if(in_b_counter>SysUtils.byteToUnsignedInt(in_buffer[5])+5){
			packet_state=7;
		}
	}
	
	public static void getChecksum(Byte data){
		in_buffer[in_b_counter]=(byte)data;
		packet_state=8;
		in_b_counter++;
	}	
	
	public static void checkEnd(Byte data){
		in_buffer[in_b_counter]=(byte)data;
		buffercopy();
		packet_state=0;
		eof_received=true;
		in_b_counter=0;
		if(syscomdebug) System.out.println(">> received !");
	}	

	public static void resetProtocol(){
		eof_received=false;
		// 	queda en estado 8 hasta no ser reseteado por checkpacket
	}	
	
	public static void serialSend(){
		try {
			Thread.sleep(50);
			serial.write(out_buffer);
		} catch (Exception e) {
			if(syscomdebug) e.printStackTrace();
		}
		;
		
	}
	
	private static void serialDebug(){
		byte[] toprint = {in_buffer[in_b_counter]};
	  	    if(syscomdebug) System.out.print("["+in_b_counter+"] ");
	  	  if(syscomdebug) SysUtils.printhex(toprint, 1);
	}
	
	public static void serialReceive(byte[] response){
		int res_s = response.length;
		  	int i=0;
		  	for (i=0;i<res_s;i++){ 
		  		in_buffer[in_b_counter]=(byte)response[i];
		  		serialDebug();		  		
		  		switch(packet_state){
		  			case	0:	checkHeader((byte)response[i]);
		  						//serial.write((byte)0xA0);
		  						break;
		  			case	1:	getAppId((byte)response[i]);
		  		                //serial.write((byte)0xA1);
		  						break;		
		  			case	2:	getDest((byte)response[i]);
		  		                //serial.write((byte)0xA2);
		  			            break;
		  			case	3:	getOrig((byte)response[i]);
		  						//serial.write((byte)0xA3);
		  						break;
		  			case	4:	getZone((byte)response[i]);
	  							//serial.write((byte)0xA3);
	  							break;			
		  			case	5:	getDataLength((byte)response[i]);
								//serial.write((byte)0xA4);
	  							break;		
		  			case	6:	fillData((byte)response[i]);
								//serial.write((byte)0xA5);
	  							break;
		  			case	7:	getChecksum((byte)response[i]);
								//serial.write((byte)0xA6);
	  						break;	
		  			case	8:	checkEnd((byte)response[i]);
								//serial.write((byte)0xA7);
	  						break;
	  			default	 :	break; 
		  		}
		  	}				
	}
	
	public static void serialIni(){
		/*
		 *   PROTOCOLO SERIAL
		 * 
		 *  CAB x1		:	0xFE
		 *  APP_ID x1	:	alarma incendio=0x01
		 *  DEST x1		:	0xFF / alarm_dir (0->254)
		 *  ORIG x1		:	alarm_dir (0->254) / 0xFF
		 *  L_DATA x2	:	# Data Bytes
		 *  DATA xN (0 - 1024)		: 	Data Bytes
		 *  CHCK x [APP_ID->DATA(N)]:	APP_ID XOR DEST XOR ORIG XOR L_DATA XOR DATA  
		 *  FIN			:	0xFD
		 *  
		 * */
		modemEnable = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.GPIO_27);
		modemEnable.low();

		serial = SerialFactory.createInstance();
		//serial.open(Serial.DEFAULT_COM_PORT, 2400); // RPI-2 = /dev/ttyAMA0
      	try {
      		SerialConfig config = new SerialConfig();

            // set default serial settings (device, baud rate, flow control, etc)
            //
            // by default, use the DEFAULT com port on the Raspberry Pi (exposed on GPIO header)
            // NOTE: this utility method will determine the default serial port for the
            //       detected platform and board/model.  For all Raspberry Pi models
            //       except the 3B, it will return "/dev/ttyAMA0".  For Raspberry Pi
            //       model 3B may return "/dev/ttyS0" or "/dev/ttyAMA0" depending on
            //       environment configuration.
            config.device("/dev/serial0")
                  .baud(Baud._9600)
                  .dataBits(DataBits._8)
                  .parity(Parity.NONE)
                  .stopBits(StopBits._1)
                  .flowControl(FlowControl.NONE);
            if(syscomdebug) System.out.println("SysCom: Starting Serial Port /dev/serial0");
            // open the default serial device/port with the configuration settings
            serial.open(config);
		} catch (IOException e1) {
			if(syscomdebug) e1.printStackTrace();
		} 
      	
      	
      	/*serial.addListener(new SerialDataEventListener() {
            @Override
            public void dataReceived(SerialDataEvent event) {

                // NOTE! - It is extremely important to read the data received from the
                // serial port.  If it does not get read from the receive buffer, the
                // buffer will continue to grow and consume memory.

                // print out the data received to the console
                try {
                	System.out.println("[HEX DATA]   " + event.getHexByteString());
                	System.out.println("[ASCII DATA] " + event.getAsciiString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });*/
		serial.addListener(new SerialDataEventListener() {
            @Override
            public void dataReceived(SerialDataEvent event) {
          	  try {
          		  	byte[] response = event.getBytes();
          		  	serialReceive(response);			
				} catch (Exception e) {
					head_received=false;
					in_b_counter=0;
					packet_state=0;
					flush_in_buffer();
				}
   	
                   
            	
            }            
        });		
		output_buffer(0,0,0);
		serialSend();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			if(syscomdebug) e.printStackTrace();
		}
		
	}	
	
}
