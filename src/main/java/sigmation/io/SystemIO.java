package sigmation.io;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.system.SystemInfo;

import sigmation.utils.SysUtils;
import sigmation.main.Sigmation;

/**
 * Raspberry GPIO
 * 				 Func			GPIO						GPIO		Func
 * 											-----------
 *               ------  	    3.3V  		| 01 | 02 |     5V			LCD Vcc & Serial Vcc
 *               ------		    08  		| 03 | 04 |     5V			5VD REG
 *               ------         09  		| 05 | 06 |     GND			Serial GND
 *               Reset COM	    07  		| 07 | 08 |     15			Serial Tx
 *               ------		    GND  		| 09 | 10 |     16			Serial Rx
 *               ------		    00  		| 11 | 12 |     01			RS485 Ctrl
 *               ------		    02  		| 13 | 14 |     GND			GND
 *               ------		    03  		| 15 | 16 |     04			--------
 *               Serial Vcc     3.3V  		| 17 | 18 |     05			--------
 *               ------		    12  		| 19 | 20 |     GND			PWR led
 *               Serial Vcc     13  		| 21 | 22 |     06			LCD bit 2
 *               Serial Vcc     14  		| 23 | 24 |     10			LCD bit 4
 *               Serial Vcc     GND  		| 25 | 26 |     11			LCD RS
 *               Serial Vcc     30  		| 27 | 28 |     31			--------
 *               ------		    21  		| 29 | 30 |     GND			--------
 *               ------		    22  		| 31 | 32 |     26			LCD Bklt
 *               Serial Vcc     23  		| 33 | 34 |     GND			--------
 *               ------		    24  		| 35 | 36 |     27			--------
 *               ------		    25  		| 37 | 38 |     28			--------
 *               ------		    GND  		| 39 | 40 |     29			--------    
 * 											-----------
 * 
 * */

public class SystemIO {
	public static int serverVersion= 3;  //0 - Sigma Telecom V1 , 1 - Sigma Telecom V2 , 2 - Sigma Telecom V3 (WMX), 3 - Sigma South Africa Server V2.
	public static boolean noPiMode = false;
    public static boolean piRFlag=false; 
    public static boolean rs485Ctrl=true; //use RS 485 output control
    
	static GpioController gpio = null; 
	static GpioPinDigitalInput resetButton = null; 
	static GpioPinDigitalOutput rs485CtrlPin = null;
	// Sigma South Africa Server Leds
	static GpioPinDigitalOutput blueHeartBeat = null; 
	public static GpioPinDigitalOutput yellowWarning = null; 
	public static GpioPinDigitalOutput redCritical = null; 
	
	static Thread hearBeat;
	
	public static String currentTask="Sigmation";
	
	public static class rs485{
		public static void rx(){
			if(rs485Ctrl && !Sigmation.noPiMode) rs485CtrlPin.low();
		}
		
		public static void tx(){
			if(rs485Ctrl && !Sigmation.noPiMode) rs485CtrlPin.high();
		}
	}
	
	public static void initGpio(int serverVersion, boolean noPiMode, boolean piRFlag, boolean rs485Ctrl){
		SystemIO.serverVersion= serverVersion;  
		SystemIO.noPiMode = noPiMode;
		SystemIO.piRFlag=piRFlag; 
		SystemIO.rs485Ctrl=rs485Ctrl; 
		if(!Sigmation.noPiMode && serverVersion<100){
			gpio = GpioFactory.getInstance(); 
			if (serverVersion>100) return;
			lcdIni();
			if(serverVersion== 3) { 
					resetButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_29, PinPullResistance.PULL_UP);
					blueHeartBeat = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26,PinState.HIGH); 
					yellowWarning = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27,PinState.LOW);  
					redCritical = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28,PinState.LOW);  
					startHeartBeat();             	
			 }
			else {
				resetButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_07, PinPullResistance.PULL_DOWN);
				rs485CtrlPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01,PinState.LOW);
			}
		}
	}
	
	public static void startHeartBeat(){
		hearBeat = new Thread() {
		    public void run() {
		        try {
		        	SystemIO.redCritical.high();
		        	Thread.sleep(500);
		        	SystemIO.redCritical.low();
		            while(!Sigmation.resetSignal){
		            	SystemIO.blueHeartBeat.toggle(); 
		            	Thread.sleep(500);
		            }				             
		        } catch(InterruptedException e) {
		            System.out.println(e);
		        }
		    }  
		};

		hearBeat.start();
	}
	
	public static void lcdIni(){
		if(serverVersion>100) return;
		LcdDevice.serverVersionSelect(serverVersion);
		LcdDevice.setLogo();
	}
	
	public static void startResetButton(){
		
		
        if(!noPiMode){
        	// create and register gpio pin listener
            resetButton.addListener(new GpioPinListenerDigital() {
                @Override
                public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                    // display pin state on console
                	try {
                		boolean buttonPressed = resetButton.isHigh();                    	
                    	int buttoncounter=0;
                    	for (buttoncounter = 0; buttoncounter < 5 && buttonPressed ; buttoncounter++) {
                    		buttonPressed = resetButton.isHigh();
							Thread.sleep(100); 
						}  
                    	if(buttoncounter==5) buttonPressed=true;
                    	if(buttonPressed) SysUtils.piRFlag=true;
                    	if(buttonPressed) System.out.println("<<<<  Reset button high >>>>");
                    	
                    	else System.out.println("<<<<  Reset button low >>>>");
                    	SysUtils.piReset();
                		Thread.sleep(1000);  	            		
    				} catch (Exception e) {
    					currentTask=">ER 0x003       ";
    					  sysInfo();
    				}
                	
                } 
            });	
            System.out.println("<<<<  Reset button Ready >>>>");
        }		
	}
	
	public static void sysInfo(String message){
		if(!Sigmation.noPiMode){
			currentTask = message;
			String stat1= new String(), stat2 = new String();
			try{stat1= "CPU:" + SysUtils.roundnumber(SystemInfo.getCpuVoltage()) +" V";}
	        catch(Exception ex){
	        	currentTask=">ER 0x006       ";
				  sysInfo();
	        }
			try{stat1= stat1 + "-"+ SysUtils.roundnumber(SystemInfo.getCpuTemperature())+" C";}
	        catch(Exception ex){
	        	currentTask=">ER 0x007       ";
				  sysInfo();
	        }
			stat2=currentTask;
			int size1 = stat1.length();
			int size2 = stat2.length();
			if (size1>17) size1=17;
			if (size2>17) size2=17;
			
			LcdDevice.setStats(stat1.substring(0, size1), stat2.substring(0, size2));	
		}
			
	}
	
	public static void sysInfo(){		
		sysInfo(currentTask);		
	}
	
}
