package sigmation.io;
 

/*
 * sin uso -
 * 
 * import java.io.UnsupportedEncodingException;
 * import java.text.SimpleDateFormat;
 * import java.util.Date;
 * 
 * 
 * */


import com.pi4j.wiringpi.Lcd;
import com.pi4j.wiringpi.SoftPwm;

import sigmation.utils.SysUtils;


public class LcdDevice {

    public static int LCD_ROWS = 2;
    public static int LCD_COLUMNS = 16;
    public static int LCD_BITS = 4;
    public static int lcdHandle=-1;
    
    public static int RSPin = 11;
    public static int strobePin = 30;
    public static int[] dataPins= {13,6,14,10,0,0,0,0};
    
    public static int contrast = 50;
    public static int brightness = 30;
    
    public static boolean enableExtraData=false;
    public static boolean lcdPowerControl=true;
    
    public static void ini(int LCD_ROWS,int LCD_COLUMNS,int LCD_BITS){
    	LcdDevice.LCD_ROWS=LCD_ROWS;
    	LcdDevice.LCD_COLUMNS=LCD_COLUMNS;
    	LcdDevice.LCD_BITS=LCD_BITS;
    }
    
    public static void serverVersionSelect(int serverVersion){
    	/**
    	 * 0 - Sigma Telecom V1 , 1 - Sigma Telecom V2 , 2 - Sigma Telecom V3 (WMX), 3 - Sigma South Africa Server V2.
    	 * */
    	if(serverVersion>100) return;
    	switch(serverVersion){
    		case	0	:	int[] thisdataPins0= {13,6,14,10,0,0,0,0};
							RSPin= 11;
							strobePin = 30;
							dataPins=thisdataPins0;
							ini2x40x4b();
							break;
    		case	3	:	int[] thisdataPins3= {1,4,5,6,0,0,0,0};
							RSPin= 10;
							strobePin = 11;
							dataPins=thisdataPins3;
							ini2x40x4b();
							lcdPowerControl=false;
    						break;
    		default		:	int[] thisdataPinsd= {13,6,14,10,0,0,0,0};
    						RSPin= 11;
    						strobePin = 30;
    						dataPins=thisdataPinsd;
    						ini2x16x4b();
    						break;
    	}
    }
    
    public static void ini2x40x4b(){
    	/** 
    	 * <p>
    	 * 2 x 40 LCD Display, 4 bits
    	 * </p>
    	 * */
    	LcdDevice.LCD_ROWS=4;
    	LcdDevice.LCD_COLUMNS=20;
    	LcdDevice.LCD_BITS=4;
    	enableExtraData=true;
    	ini();
    }
    
    public static void ini2x16x4b(){
    	/** 
    	 * <p>
    	 * 2 x 16 LCD Display, 4 bits
    	 * </p>
    	 * */
    	LcdDevice.LCD_ROWS=2;
    	LcdDevice.LCD_COLUMNS=16;
    	LcdDevice.LCD_BITS=4;
    	ini();
    }
    
    public static void ini2x20x4b(){
    	/** 
    	 * <p>
    	 * 2 x 20 LCD Display, 4 bits
    	 * </p>
    	 * */
    	LcdDevice.LCD_ROWS=2;
    	LcdDevice.LCD_COLUMNS=20;
    	LcdDevice.LCD_BITS=4;
    	ini();
    }
    
    
    public static void lcdPWControl(){
    	getProperties();
    	SoftPwm.softPwmCreate(26, 0, 100);
        SoftPwm.softPwmWrite(26, brightness );
        SoftPwm.softPwmCreate(23, 0, 100);
        SoftPwm.softPwmWrite(23, contrast );
    }
    
    private static void getProperties(){
		String prefix="LCD_DISPLAY_";
		//sigmation.utils.ReadConfig.properties.printProps(); 
		contrast=sigmation.utils.ReadConfig.properties.getInt(prefix+"contrast", contrast);
		brightness=sigmation.utils.ReadConfig.properties.getInt(prefix+"brightness", brightness); 
	}
    
    
    public static void ini(){
    	try {
	    		/*if (Gpio.wiringPiSetup() == -1) {
	                System.out.println(" ==>> GPIO SETUP FAILED");
	                return;
	            }   */
	    		System.out.println(" ==>> GPIO SETUP SUCCESS");
	            lcdHandle= Lcd.lcdInit(LCD_ROWS,     // number of row supported by LCD
	                    LCD_COLUMNS,  // number of columns supported by LCD
	                    LCD_BITS,     // number of bits used to communicate to LCD 
	                    RSPin,           // LCD RS pin   4
	                    strobePin,           // LCD strobe pin  6
	                    dataPins[0],          // LCD data bit 1
	                    dataPins[1],           // LCD data bit 2
	                    dataPins[2],           // LCD data bit 3
	                    dataPins[3],            // LCD data bit 4
	                    dataPins[4],            // LCD data bit 5 (set to 0 if using 4 bit communication)
	                    dataPins[5],            // LCD data bit 6 (set to 0 if using 4 bit communication)
	                    dataPins[6],            // LCD data bit 7 (set to 0 if using 4 bit communication)
	                    dataPins[7]);           // LCD data bit 8 (set to 0 if using 4 bit communication)
	
	
	    		// verify initialization
	    		if (lcdHandle == -1) {
	    		System.out.println(" ==>> LCD INIT FAILED");
	    		return;
	    		}
	    		if(lcdPowerControl) lcdPWControl();	    		
	    		// clear LCD
	    		Lcd.lcdClear(lcdHandle);
	    		Thread.sleep(1000);
	    		
	    		// write line 1 to LCD
	    		Lcd.lcdHome(lcdHandle);
	    		System.out.println(" ==>> LCD READY");
		} catch (Exception e) {
			e.printStackTrace();
		} 
    }
    
    public static void setlcdbright(int ap, int bp){
    	if(lcdPowerControl){
    		SoftPwm.softPwmWrite(26, ap);
        	SoftPwm.softPwmWrite(23, bp);
    	}    	
    }
    
    public static void setLogo(){
    	 Lcd.lcdHome(lcdHandle);
    	 //Lcd.lcdPuts (lcdHandle, "-----SIGMATION-----|") ;
    	 try {
    		 Thread.sleep(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 Lcd.lcdPosition (lcdHandle, 1, 1) ; 
         Lcd.lcdPuts (lcdHandle, "|") ; 
         Lcd.lcdHome(lcdHandle);
    	 try {
    		 Thread.sleep(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    
    public static void setReboot(){
    	setTask("Rebooting...");
   		Lcd.lcdHome(lcdHandle);
   	 	Lcd.lcdPuts (lcdHandle, "     SIGMATION      ") ;
   	 try {
   		 Thread.sleep(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
   	 	Lcd.lcdPosition (lcdHandle, 0, 0) ; 
        Lcd.lcdPuts (lcdHandle, ">Rebooting Server...") ; 
        Lcd.lcdPosition (lcdHandle, 0, 1) ; 
        Lcd.lcdPuts (lcdHandle, "Please wait...      ") ; 
   	 try {
   		 Thread.sleep(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
   }     
    
    public static void setTask(String task){
    	Lcd.lcdPosition (lcdHandle, 0, 1) ; 
        Lcd.lcdPuts (lcdHandle, task) ; 
   	 try {
			 Thread.sleep(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }


    public static void setStats(String stat1, String stat2){
    	try {
        	
            Lcd.lcdPosition (lcdHandle, 0, 1) ; 
            //Thread.sleep(50);
            Lcd.lcdPuts (lcdHandle, stat2) ;
            //Thread.sleep(50);
            Lcd.lcdPosition (lcdHandle, 0, 0) ; 
        	//Thread.sleep(50);
            Lcd.lcdPuts (lcdHandle, stat1) ;
            
            if(enableExtraData){
            	String ipAddr = "IP:"+SysUtils.getEthernetAddress(); 
            	Lcd.lcdPosition (lcdHandle, 0, 2) ; 
            	//Thread.sleep(50);
                Lcd.lcdPuts (lcdHandle, ipAddr) ;
                String timeStamp = SysUtils.getTime();
                Lcd.lcdPosition (lcdHandle, 0, 3) ; 
            	//Thread.sleep(50);
                Lcd.lcdPuts (lcdHandle, timeStamp) ;
            }
		} catch (Exception e) {
			e.printStackTrace();
		}

   	
    }
    

}
