package sigmation.io;

import java.util.Timer;
import java.util.TimerTask;

import sigmation.utils.RunShellCommandFromJava; 
import sigmation.utils.ReadConfig;
import sigmation.main.Sigmation; 

public class LedDisplay {
	
	public static String 	mapping		= "regular";
	public static String 	sequence	= "RBG";
	public static String 	exeFile		= "clock";
	public static String 	Folder		= "/home/pi/Documents/"; 
	
	public static int 		rows		= 32;
	public static int 		cols		= 64;
	public static int 		chain		=  4;
	public static int 		pwmBits		=  8;
	public static int 		pwmLsNanoSec= 500;
	public static int 		slowDownGPIO= 2;
	public static int 		scanMode	= 1; 
	
	public static long 		screenTimerSeconds = 3600;
	
	private static String[] command = new String[2];
	
	public static void init(){
		
		startScreen();
		screenRestart();
	}
	
	private static void setCommand(){
		command[1]=Folder;
		command[0]=	"./"+exeFile+
					" --led-gpio-mapping="+mapping+
					" --led-rows="+rows+
					" --led-cols="+cols+
					" --led-chain="+chain+
					" --led-pwm-bits="+pwmBits+
					" --led-pwm-lsb-nanoseconds="+pwmLsNanoSec+
					" --led-scan-mode="+scanMode+
					" --led-rgb-sequence="+sequence+
					" --led-slowdown-gpio="+slowDownGPIO+
					" -r 300 -R 1 ";
		System.out.println("folder is "+command[1]);
		System.out.println("command is "+command[0]);
	}
	
	private static void getProperties(){
		String prefix="LED_DISPLAY_";
		ReadConfig.properties.printProps();
		mapping=ReadConfig.properties.getString(prefix+"mapping", mapping);
		sequence=ReadConfig.properties.getString(prefix+"sequence", sequence);
		exeFile=ReadConfig.properties.getString(prefix+"exeFile", exeFile);
		Folder=ReadConfig.properties.getString(prefix+"Folder", Folder);
		rows=ReadConfig.properties.getInt(prefix+"rows", rows);
		cols=ReadConfig.properties.getInt(prefix+"cols", cols);
		chain=ReadConfig.properties.getInt(prefix+"chain", chain);
		pwmBits=ReadConfig.properties.getInt(prefix+"pwmBits", pwmBits);
		pwmLsNanoSec=ReadConfig.properties.getInt(prefix+"pwmLsNanoSec", pwmLsNanoSec);
		slowDownGPIO=ReadConfig.properties.getInt(prefix+"slowDownGPIO", slowDownGPIO);
		scanMode=ReadConfig.properties.getInt(prefix+"scanMode", scanMode);
		screenTimerSeconds=ReadConfig.properties.getLong(prefix+"screenTimerSeconds", screenTimerSeconds);
	}
	
	public static void startScreen(){
		//String[] command = {"./clock   --led-gpio-mapping=regular --led-rows=32 --led-cols=64 --led-chain=4 --led-pwm-bits=8 --led-pwm-lsb-nanoseconds=500 --led-scan-mode=1  --led-rgb-sequence=RBG --led-slowdown-gpio=2  -r 2.2 -R 1 ",Folder};
		getProperties();
		setCommand();
		boolean keepAlive=true;
		Sigmation.runner= new RunShellCommandFromJava(command,keepAlive); 
		Sigmation.runner.start();
	}
	
	public static void screenRestart(){
		Timer timer = new Timer();
		long thistimer= screenTimerSeconds*(long)1000;
		if (thistimer<0) { 
			System.out.println("timer is "+thistimer+" and it is not positive, aborting screen restart schedule, please check the code");
			return;
		} else {
			System.out.println("timer is "+thistimer+" and it is positive, proceeding");
		}
		timer.schedule(new ScreenRestarter(), 0, screenTimerSeconds*(long)1000);
	}
	
	public static class ScreenRestarter extends TimerTask {
		
		public void run() { 
			restartScreen();
		}
	}
	
	private static void restartScreen(){
		stopScreen();
		startScreen();
	}
	
	public static void stopScreen(){
		try {
			Thread.sleep(1000);
			System.out.println("now I will stop RUNNER thread");
			if(Sigmation.runner.isAlive()) {
				Sigmation.runner.unblock(); 
				System.out.println("sent the interrupt signal, now i wait ...");
			}
			Sigmation.runner.join();
			System.out.println("and the thread stopped");
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}
	}
	 

}
