package sigmation.io;

import java.util.ArrayList;

import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;
import sigmation.main.Sigmation;

public class ComLayer {
	
	public static boolean WMX=false;
	public static boolean debugThis=true;
	public static byte thisappid=0; 
	public static byte thisAddress=0;  
	public static String thisDeviceTable="undefined";
	public static String thisDeviceIndex="series";
	
	static java.util.Date [] deviceTimers =  new java.util.Date[256];
	public static boolean[] deviceActive= new boolean[256];
	public static boolean[] deviceKeepAlive= new boolean[256];  //logica invertida, falso es keepalive, verdadero significa que no ha mostrado actividad, candidato a offline
	
	private static java.util.Date pingInit= new java.util.Date();
	private static java.util.Date pingCurrent= new java.util.Date();
	
	
	static java.util.Date packetInitTime= new java.util.Date();
	static java.util.Date packetCurrentTime= new java.util.Date();
	public static long packetDelay = 500;   		//frecuencia de envio de paquetes, evita saturar la red
	public static long offlineTime = 120000; 	//tiempo para declarar offline un MLA
	public static long pingTime= 20000;	//tiempo de frecuencia de pings
	
	//logica de comunicacion
	static final boolean doBroadcast=true;	
	public static boolean pingNextEnabled=true;  //habilitado cada 20 segundos, desabilitado al iniciar ping
	
	public static boolean sendingPacket=false; 
	
	public static void init(boolean WMX, byte appid){
		getThisAddress();
		init(WMX,"notused","notused",appid,thisAddress);
	}
	
	public static void init(boolean WMX, String deviceTable, String deviceIndex, byte appid){
		getThisAddress();
		init(WMX,deviceTable,deviceIndex,appid,thisAddress);
	}
	
	public static void init(boolean WMX, String deviceTable, String deviceIndex, byte appid, byte myaddress){
		thisDeviceTable = deviceTable;
		thisDeviceIndex= deviceIndex;
		thisappid=appid;
		thisAddress=myaddress;
		ComLayer.WMX=WMX;
		pingInit= null;
		pingCurrent= null; 
		if(!Sigmation.noPiMode){
			if(WMX){
				WMXModCom.modbusIni();
			} else {
				SysCom.serialIni();
			}
		}
	}
	
	public static void resetProtocol(){
		if(WMX){
			WMXModCom.resetProtocol();
			SysCom.resetProtocol();
		}else{
			SysCom.resetProtocol();
		}
	}
	
	public static void serialSendData(int appid, int dest , int orig, int largodata, byte[] localdata, boolean broadcast){
		sendingStart(dest,broadcast);
		if(WMX){
			byte bytedest=0;
			WMXModCom.resetModem();
			if(broadcast) bytedest= (byte)0x00;
			else bytedest =(byte)dest;
			SysCom.output_buffer(appid, bytedest, orig, largodata, localdata);
			if(broadcast) WMXModCom.setBroadcast();
			else WMXModCom.setDestinationInt(dest);
			WMXModCom.modFunction.sendMBD(SysCom.out_buffer);
		}else{
			SysCom.output_buffer(appid, dest, orig, largodata, localdata);
			SysCom.serialSend();	
		}			
	}
	
	public static void sendingCheck(){	
		packetCurrentTime= new java.util.Date();
		long difference = packetCurrentTime.getTime() - packetInitTime.getTime();
		if (difference > packetDelay) sendingPacket = false;
		else sendingPacket= true;
	}
	
	public static void pingCheck(){	
		if(pingInit==null){
			pingNextEnabled= true; 
			pingStart();
		}else{
			pingCurrent= new java.util.Date();
			long difference = pingCurrent.getTime() - pingInit.getTime();
			if (difference > pingTime) pingNextEnabled= true;
		}
	}
	
	public static void sendingStart(int dest, boolean broadcast){		
		sendingPacket= true;
		packetInitTime= new java.util.Date();
		packetCurrentTime=packetInitTime;
		if(dest<1) return;
		if(!broadcast){
			if(!deviceKeepAlive[dest]) deviceTimers[dest]= packetInitTime;    //si es falso, quiere decir que ha mostrado actividad, se renueva el tiempo
			deviceKeepAlive[dest]=true;										  //reseteo de flag, remoto debe cambiar estado contestando ping o enviando alguna data
		}		
	}
	
	public static void sendingExtend(){		
		sendingPacket= true;
		packetInitTime= new java.util.Date();
		packetCurrentTime=packetInitTime;
	}
	
	public static void pingStart(){	 
		pingInit= new java.util.Date();
		pingCurrent=pingInit; 
		pingNextEnabled=false;
	}
	
	public static void deviceInitTime(int dest){
		deviceTimers[dest]= new java.util.Date(); 
	}
	
	public static void serialsend(byte localdest,byte[] localdata, boolean datalength ){
		serialSendData(thisappid, localdest, thisAddress, localdata.length, localdata); 		
	}
	
	public static void serialsend(byte localdest,byte[] localdata){
		serialSendData(thisappid, localdest, thisAddress, localdata.length, localdata); 		
	}
	
	public static void serialSendData(int appid, int dest , int orig, int largodata, byte[] localdata){
		serialSendData(appid,dest ,orig, largodata, localdata, false);
	}
	
	public static boolean getEOF(){
		if(WMX){   
			return WMXModCom.eof_received;
		}
		else{ 
			return SysCom.eof_received;
		}
	}
	
	public static void checkOffline(){	
		java.util.Date localCurrentTime= new java.util.Date();
		for (int i=0; i< 255; i++){
			if(deviceActive[i]){
				if(deviceTimers[i]==null) deviceTimers[i]= new java.util.Date();
				long difference = localCurrentTime.getTime() - deviceTimers[i].getTime();
				if (difference > offlineTime){						  //si ha pasado mas del tiempo esperado
					if(deviceKeepAlive[i]) deviceSetOffline((byte)i);  //y si dispositivo no ha mostrado actividad, seteo offline
					deviceTimers[i]= new java.util.Date();
				} 
			}			
		}
	}
	
	public static void deviceSetOffline(int series){
		String[] args1= {"series","online"};
		String[] data= new String[2];
		
		data[0]= ""+series;
		data[1]= ""+0;
		
		String condition = " series= '"+data[0]+"' ";
		
		String table= thisDeviceTable;
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.ifExistsUpdate(table, args1,condition, data);			
	}
	
	public static void deviceSetOffline(byte series){
		int seriesint= SysUtils.byteToUnsignedInt(series);
		String[] args1= {thisDeviceIndex,"online"};
		String[] data= new String[2];
		
		data[0]= ""+seriesint;
		data[1]= ""+0;
		
		String condition = " "+thisDeviceIndex+"= '"+data[0]+"' ";
		
		String table= thisDeviceTable;
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.ifExistsUpdate(table, args1,condition, data);			
	}
	
	public static void deviceSetOnline(byte series){
		int seriesint= SysUtils.byteToUnsignedInt(series);
		String[] args1= {thisDeviceIndex,"online"};
		String[] data= new String[2];
		
		data[0]= ""+seriesint;
		data[1]= ""+1;
		
		String condition = " "+thisDeviceIndex+"= '"+data[0]+"' ";
		
		String table= thisDeviceTable;
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.ifExistsUpdate(table, args1,condition, data);			
	}
	
	public static byte getCurrentAddress(){
		if(WMX){ 
			return (byte)Integer.parseInt(WMXModCom.source_address);
		}
		else{
			return SysCom.currentAddress;
		} 
	}
	
	public static byte[] getDataBuffer(){
		if(WMX){
			return SysCom.databuffer;
		}
		else{
			return SysCom.databuffer;
		} 
	}
	
	public static byte getCurrentZone(){
		if(WMX){
			return SysCom.currentZone;
		}
		else{
			return SysCom.currentZone;
		} 
	}
	
	public static byte[] getPacketData(){
		if(WMX){
			return SysCom.packetCheck(WMXModCom.getStoredData());
		}
		else{
			return SysCom.packetCheck();
		} 
	}
	
	public static byte getThisAddress(){         //used to get an address for RF from the database. REMOTE DEVICES ONLY!
		String argument= "value";
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="sys_config"; 
		ArrayList<String[]> localList= cn.select(table, argument, " parameter='device_address'");
		if(localList.size()>0){
			String[] localStringMatrix= localList.get(0);   //get the item array where the item is stored
			String localString = localStringMatrix[0];		//get the item from the item array
			int localnumber = SysUtils.returnInt(localString);//get the number from the item string
			byte localbyte  = (byte)localnumber;
			thisAddress=localbyte;
			if(debugThis) System.out.println("Com Layer: got my address, it's "+localbyte);
		}
		return thisAddress;
	}
	
	public static void demoFunction(){ 
			while(!Sigmation.resetSignal){
				try {
					if(SysUtils.getRandom(1000)==1 && !sendingPacket){
						 if(WMX){
							 WMXModCom.source_address="0004";
								WMXModCom.currentDataSize=10;
								WMXModCom.data_received=true;
								WMXModCom.eof_received=true;
								WMXModCom.setStoredData(SysUtils.WMXDemoBytes()); 
						 } else {
							 SysCom.serialReceive(SysUtils.WMXDemoBytes()); 
							 sendingStart(0,true);
						 }						
					} 
					Thread.sleep(1);
				} catch (Exception e) {
					if(debugThis) e.printStackTrace();
				}
			}
	}
	
	public static void demo(){
		if(debugThis) System.out.println("Com Layer: started demo");
		Thread thread = new Thread(){
		    public void run(){
		    	if(Sigmation.noPiMode)demoFunction();
		    }
		  };
		  thread.start();
	}
}
