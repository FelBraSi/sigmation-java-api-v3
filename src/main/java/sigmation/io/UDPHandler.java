package sigmation.io;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sigmation.utils.SysUtils;

interface UDPInterface {
    void UDPMessageReceived(byte[] received, String currentAddress, int port ); 
}

public class UDPHandler extends Thread {
 
    private DatagramSocket socket;
    private boolean running; 
    private int localPort = 4445;
    private InetAddress address;
    private int destPort=4445;
    
    private List<UDPInterface> listeners = new ArrayList<UDPInterface>();
    
    public void addListener(UDPInterface toAdd) {
        listeners.add(toAdd);
    }
    
    private void messageReceived(byte[] received,String currentAddress,int port ) throws Exception{
		for (UDPInterface hl : listeners)
            hl.UDPMessageReceived(received, currentAddress,port );
		/*MqttTopic mqttTopic = publisher.getTopic(stopic.getCurrentTopic()); 
		 token = mqttTopic.publish(stopic.getCurrentVal(),0,false);*/
	} 
    
    public class UDPReceive implements UDPInterface {
		public void UDPMessageReceived(byte[] received,String currentAddress,int port ) {
		}
	} 
    
    public UDPHandler() {
        try {
			socket = new DatagramSocket(localPort);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public UDPHandler(int port) {
        try {
        	localPort=port;
			socket = new DatagramSocket(localPort);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    public void run() {
        running = true;
 
        while (running) {
        	byte[] buf = new byte[1024]; 
            DatagramPacket packet 
              = new DatagramPacket(buf, buf.length);
            try {            	
				socket.receive(packet);
				InetAddress address = packet.getAddress();
	            int port = packet.getPort();
	            byte[] buf2 = Arrays.copyOf(buf,packet.getLength());
	            packet = new DatagramPacket(buf2, buf2.length, address, port); 
	            byte[] received = packet.getData();	   
	            String currentAddress = packet.getAddress().getHostAddress();
	            int fromPort = packet.getPort();
	   		    messageReceived(received,currentAddress,fromPort);
			} catch (Exception e) { 
				e.printStackTrace();
			}         
        }
        socket.close();
    }
    
/*
    public String sendCommand(String msg) {
        try {
        	byte[] outBuf  = msg.getBytes();
            DatagramPacket packet 
              = new DatagramPacket(outBuf, outBuf.length, address, destPort);
            socket.send(packet);
            System.out.println("Packet sent to "+address); 
            return "done";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
 */   
    public String sendCommand(byte[] msg, String address, int destPort) {
        try {
        	byte[] outBuf  = msg;
        	this.address = InetAddress.getByName(address);
            DatagramPacket packet 
              = new DatagramPacket(outBuf, outBuf.length, this.address, destPort);
            socket.send(packet);
            System.out.println("Packet sent to "+address+":"+destPort); 
            SysUtils.printhex(outBuf, outBuf.length,true);
            System.out.println(" ");
            return "done";
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error when sending to "+address+":"+destPort+" because "+e.getCause());
			return null;
		}
    }
    
    
}
