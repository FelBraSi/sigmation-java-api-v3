package sigmation.drivers;


import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import sigmation.main.DRS;
import sigmation.mongodb.MongoDriver;
import sigmation.mqtt.pahoMqtt;
import sigmation.utils.SysUtils;

public class DRSPacketProcess {
	
	private final boolean debugThis=true;
	
	private int command =0;
	private byte[] data;
	private String sCommand;
	private DRSFunctions d = new DRSFunctions();
	private pahoMqtt drsMqtt;
	private String objectName;
	private Helpers helpers=new Helpers();
	private String current="";
	private String resourceGroup;  
	private String resourceName;
	private int devicePort=0;
	
	public ArrayList<SubElement> subElementList = new ArrayList<DRSPacketProcess.SubElement>();
	public int[] subElementInts =  new int[4];
	public BigInteger subElementBigInt= BigInteger.valueOf(0);
	
	private final int remoteListBytesSize=15;
	
	public DRSPacketProcess(String objectName,pahoMqtt drsMqtt, int command, byte[] data){
		this.command=command;
		this.data=data;
		this.drsMqtt=drsMqtt;
		this.objectName=objectName; 
	}
	
	public DRSPacketProcess(String objectName,pahoMqtt drsMqtt, int command, byte[] data, BigInteger subElementBigInt){
		this.command=command;
		this.data=data;
		this.drsMqtt=drsMqtt;
		this.objectName=objectName; 
		this.subElementBigInt=subElementBigInt;
	}
	
	public DRSPacketProcess(String objectName,String resourceGroup,  String resourceName, int devicePort,pahoMqtt drsMqtt, int command, byte[] data){
		this.command=command;
		this.data=data;
		this.drsMqtt=drsMqtt;
		this.objectName=objectName; 
		this.resourceGroup=resourceGroup;
		this.resourceName=resourceName;
		this.devicePort=devicePort;
	}
	
	public void init() {
		sCommand = DRSCommandList.map(command); 
		if (sCommand==null) sCommand="noCommand";
			switch(sCommand) {
			
			//Frecuencia
			case "channelSwitch": d.channelSwitch(); break;
			case "rfPowerSwitch": d.rfPowerSwitch(); break;
			case "uplinkATT": d.linkATT(true);break;
			case "downlinkATT": d.linkATT(false);break;
			case "ch1frequency": d.chfrequency(1); break;
			case "ch2frequency": d.chfrequency(2); break;
			case "ch3frequency": d.chfrequency(3); break;
			case "ch4frequency": d.chfrequency(4); break;
			case "ch5frequency": d.chfrequency(5); break;
			case "ch6frequency": d.chfrequency(6); break;
			case "ch7frequency": d.chfrequency(7); break;
			case "ch8frequency": d.chfrequency(8); break;
			case "ch9frequency": d.chfrequency(9); break;
			case "ch10frequency": d.chfrequency(10); break;
			case "ch11frequency": d.chfrequency(11); break;
			case "ch12frequency": d.chfrequency(12); break;
			case "ch13frequency": d.chfrequency(13); break;
			case "ch14frequency": d.chfrequency(14); break;
			case "ch15frequency": d.chfrequency(15); break;
			case "ch16frequency": d.chfrequency(16); break;
			case "choiceOfWorkingMode": d.choiceOfWorkingMode(); break;
			
			//InfoDispositivo
			case "deviceType": d.deviceType(); break;
			case "deviceModel": d.deviceModel(); break;
			case "deviceChannelNumber": d.deviceChannelNumber(); break;
			case "controlUnitSoftwareVersion": d.controlUnitSoftwareVersion(); break;
			case "deviceSerialNumber": d.deviceSerialNumber(); break;
			case "slaveNumber": d.slaveNumber(); break;
			case "ruId": d.ruId(); break;
			
			//ParametrosRed
			case "siteNumber": d.siteNumber(); break;
			case "siteSubDeviceNumber": d.siteSubDeviceNumber(); break;
			case "repeaterIpPort": d.repeaterIpPort(); break;
			case "repeaterIpAddress": d.repeaterIpAddress(); break;
			case "repeaterSubnetMask": d.repeaterSubnetMask(); break;
			case "repeaterDefaultGateway": d.repeaterDefaultGateway(); break;
			case "monitorCenterIPAddress": d.monitorCenterIPAddress(); break;
			case "monitorCenterIPPort": d.monitorCenterIPPort(); break;
			case "psProtocol": d.psProtocol(); break;
			case "communicationStyle": d.communicationStyle(); break;
			case "macAddress": d.macAddress(); break;
			
			//Thresholds
			case "downlinkInputMinThreshold": d.downlinkInputMinThreshold(); break;
			case "downlinkInputMaxThreshold": d.downlinkInputMaxThreshold(); break; 
			case "downlinkOutputMinThreshold": d.downlinkOutputMinThreshold(); break; 
			case "downlinkOutputMaxThreshold": d.downlinkOutputMaxThreshold(); break; 
			case "paTempThreshold": d.paTempThreshold(); break;
			case "downlinkVSWRThreshold": d.downlinkVSWRThreshold(); break;
			case "upstreamNoiseSwitch": d.upstreamNoiseSwitch(); break;
			case "highThresholdUpstreamNoise": d.highThresholdUpstreamNoise(); break;
			case "lowThresholdUpstreamNoise": d.lowThresholdUpstreamNoise(); break;
			case "uplinkNoiseCorrectionValue": d.uplinkNoiseCorrectionValue(); break; 
			case "uplinkNoiseDetectionPar1": d.uplinkNoiseDetectionPar1(); break;
			case "uplinkNoiseDetectionPar2": d.uplinkNoiseDetectionPar2(); break; 
			
			//Alarmas SET
			case "suplyPowerFailAlarmSet": d.suplyPowerFailAlarmSet(); break; 
			case "masterSlaveLinkAlarmSet": d.masterSlaveLinkAlarmSet(); break; 
			case "downlinkOverInputAlarmSet": d.downlinkOverInputAlarmSet(); break; 
			case "downlinkLowInputAlarmSet": d.downlinkLowInputAlarmSet(); break; 
			case "downlinkSWRAlarmSet": d.downlinkSWRAlarmSet(); break; 
			case "paTemphingAlarmSet": d.paTemphingAlarmSet(); break; 
			case "downlinkOverOutputAlarmSet": d.downlinkOverOutputAlarmSet(); break; 
			case "downlinkLowOutputAlarmSet": d.downlinkLowOutputAlarmSet(); break; 
			
			//Alarmas
			case "suplyPowerFailAlarm": alarm(); d.suplyPowerFailAlarm(); break; 
			case "masterSlaveLinkAlarm": alarm(); d.masterSlaveLinkAlarm(); break; 
			case "downlinkOverInputAlarm": alarm(); d.downlinkOverInputAlarm(); break; 
			case "downlinkLowInputAlarm": alarm(); d.downlinkLowInputAlarm(); break; 
			case "downlinkSWRAlarm": alarm(); d.downlinkSWRAlarm(); break; 
			case "paTemphingAlarm": alarm(); d.paTemphingAlarm(); break; 
			case "downlinkOverOutputAlarm": alarm(); d.downlinkOverOutputAlarm(); break; 
			case "downlinkLowOutputAlarm": alarm(); d.downlinkLowOutputAlarm(); break; 
			case "opticalModuleTxRxAlarm": alarm(); d.opticalModuleTxRxAlarm(); break; 
			
			//RealTimePars
			case "downlinkInputPower": d.downlinkInputPower(); break; 
			case "uplinkOutputPower": d.uplinkOutputPower(); break; 
			case "downlinkVSWR": d.downlinkVSWR(); break;  
			case "downlinkOutputPower": d.downlinkOutputPower(); break; 
			case "uplinkInputPower": d.uplinkInputPower(); break; 
			case "paTemp": d.paTemp(); break;   
			
			case "remoteListBytes": d.remoteListBytes(); break;   
			
			default:					    fail(command);
											break;
			} 
		
	} 
	
	private void alarm() {
		DRS.alarm=true;
	}
	
	private void fail(int command) {
		debug("command not recognized : "+command);
	}
	
	 

	
	private void debug(String message) {
		if(debugThis) System.out.println(message);
	}
	
	private void statToMqttCurrent(StatObject thisStatObject, String objectName, String varname, String group, String setPar) {
		statToMqttCurrent(thisStatObject, objectName, varname, group, setPar,0);
	}
	
	private void statToMqttCurrent(StatObject thisStatObject, String objectName, String varname, String group, String setPar, int save) {
		current=d.bytesToData(data, thisStatObject.paramDataType);		
		setPar=current;
		String currentTopic="";
		if(devicePort==0) currentTopic = helpers.statToMqttCurrent(drsMqtt, thisStatObject, objectName, varname, group, setPar);
		else {
			String objectName_ = objectName+"/__resourcegroup/"+resourceGroup+"/__dev/"+resourceName;
			currentTopic = helpers.statToMqttCurrent(drsMqtt, thisStatObject, objectName_, varname, group, setPar); 
		}
		System.out.println(currentTopic+" param type "+thisStatObject.paramDataType+" current val "+current);
		if(save==1) {
			if(SysUtils.getRandomBoolean(0.2))saveRTP(currentTopic,current,thisStatObject.paramUnit);
			//saveRTP(currentTopic,current,thisStatObject.paramUnit);
		} else if(save==2) {
			//if(current.toLowerCase().contentEquals("true") && SysUtils.getRandomBoolean(0.2)) saveAlarmState(currentTopic,current);
			 saveAlarmState(currentTopic,current);
		} 
	}
	
	private void statToMqttCurrent(StatObject thisStatObject, String objectName, String varname, String group, String setPar, int save, double factor) {
		current=d.bytesToData(data, thisStatObject.paramDataType,factor);
		System.out.println(varname+"param type "+thisStatObject.paramDataType+" current val "+current);
		setPar=current;
		String currentTopic="";
		if(devicePort==0) currentTopic = helpers.statToMqttCurrent(drsMqtt, thisStatObject, objectName, varname, group, setPar);
		else {
			String objectName_ = objectName+"/__resourcegroup/"+resourceGroup+"/__dev/"+resourceName;
			currentTopic = helpers.statToMqttCurrent(drsMqtt, thisStatObject, objectName_, varname, group, setPar); 
		}
		if(save==1) {
			//if(SysUtils.getRandomBoolean(0.2)) 
				saveRTP(currentTopic,current,thisStatObject.paramUnit);
		} else if(save==2) {
			//if(current.toLowerCase().contentEquals("true") && SysUtils.getRandomBoolean(0.2)) saveAlarmState(currentTopic,current);
			saveAlarmState(currentTopic,current);
		} 
	}
	
	private void statToMqttVal(StatObject thisStatObject, String objectName, String varname, String group, String setPar) {
		statToMqttVal(thisStatObject, objectName, varname, group, setPar, 0);
	}
	
	private void statToMqttVal(StatObject thisStatObject, String objectName, String varname, String group, String setPar, int save) {
		current=d.bytesToData(data, thisStatObject.paramDataType);
		System.out.println("param type "+thisStatObject.paramDataType);
		setPar=current;
		if(devicePort==0) helpers.statToMqttVal(drsMqtt, thisStatObject, objectName, varname, group, setPar);
		else {			
			//TODO cambiar a VAL
			String objectName_ = objectName+"/__resourcegroup/"+resourceGroup+"/__dev/"+resourceName;
			helpers.statToMqttVal(drsMqtt, thisStatObject, objectName_, varname, group, setPar); 
		}
		
		current=d.bytesToData(data, thisStatObject.paramDataType);
		System.out.println("param type "+thisStatObject.paramDataType);
		setPar=current;
		String currentTopic="";
		if(devicePort==0) currentTopic = helpers.statToMqttVal(drsMqtt, thisStatObject, objectName, varname, group, setPar);
		else {
			String objectName_ = objectName+"/__resourcegroup/"+resourceGroup+"/__dev/"+resourceName;
			currentTopic = helpers.statToMqttVal(drsMqtt, thisStatObject, objectName_, varname, group, setPar); 
		}
		if(save==1) { 
			saveAlarmState(currentTopic,current);
		} 
	}
	
	
	
	private void subStatToMqttCurrent(StatObject thisStatObject, String objectName_, String varname, String group, String setPar) {
		String objectName = objectName_+"/__resourcegroup/"+resourceGroup+"/__dev/"+resourceName;
		helpers.statToMqttCurrent(drsMqtt, thisStatObject, objectName, varname, group, setPar); 
	}
	
	private String getVal(){
		return current;
	}
	
	private void saveRTP(String currentTopic,String thisValue,String paramUnit) {
		String thisTopicId="";
		String[] toGet = {};
 		Object filterVal = currentTopic;
 		MongoDriver thisMongo = new MongoDriver(DRS.mongoServer,DRS.databaseName);
 		List<Document> deployList = thisMongo.getObjects(DRS.collectionName, toGet, "topic",filterVal); 
 		for(Document deployElement: deployList) {
 			thisTopicId= deployElement.getString("_id"); 
 		}
 		System.out.println(currentTopic+" topicId "+thisTopicId+" current val "+thisValue);
 		System.out.println("topic id : "+thisTopicId);
 		thisMongo.insertOneDocument("rtphistory", makeDocDouble(thisTopicId, thisValue, paramUnit,true));
	}
	
	private void saveAlarmState(String currentTopic,String thisValue) {
		String thisTopicId="";
		boolean thisAlarm=false;
		boolean thiBooleanValue = Boolean.parseBoolean(thisValue);
		String[] toGet = {};
 		Object filterVal = currentTopic;
 		MongoDriver thisMongo = new MongoDriver(DRS.mongoServer,DRS.databaseName);
 		List<Document> deployList = thisMongo.getObjects(DRS.collectionName, toGet, "topic",filterVal); 
 		for(Document deployElement: deployList) {
 			thisTopicId= deployElement.getString("_id"); 
 			thisAlarm= (boolean) deployElement.get("message"); 
 		}
 		System.out.println("topic id : "+thisTopicId);
 		if(thisAlarm!=thiBooleanValue)thisMongo.insertOneDocument("alarmhistory", makeDoc(thisTopicId, thisValue, null,false));
	}
	
	private ArrayList<Object[]> makeDoc(String topicId,String value, String unit){
		return makeDoc(topicId, value, unit, true);
	}
	
	private ArrayList<Object[]> makeDoc(String topicId,String value, String unit, boolean intValue){
		ArrayList<Object[]> thisArrayList = new ArrayList<Object[]>();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String[] elements = {"topicId","value","createdAt"};
		Object[] values = {topicId,returnValue(value,intValue),timestamp.getTime()};		
		for(int i=0;i<elements.length;i++) {
			Object[] thisObject = {elements[i],values[i]};
			thisArrayList.add(thisObject);
		}
		return thisArrayList;
	}
	
	private ArrayList<Object[]> makeDocDouble(String topicId,String value, String unit, boolean doubleValue){
		ArrayList<Object[]> thisArrayList = new ArrayList<Object[]>();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String[] elements = {"topicId","value","createdAt"};
		Object[] values = {topicId,returnDoubleValue(value,doubleValue),timestamp.getTime()};		
		for(int i=0;i<elements.length;i++) {
			Object[] thisObject = {elements[i],values[i]};
			thisArrayList.add(thisObject);
		}
		return thisArrayList;
	}
	/*
	private ArrayList<Object[]> makeSubElementDoc(String elementId,String subNumber, String port){
		ArrayList<Object[]> thisArrayList = new ArrayList<Object[]>();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String[] elements = {"elementId","subNumber","port","createdAt"};
		Object[] values = {elementId,returnValue(subNumber,true),port,timestamp.getTime()};		
		for(int i=0;i<elements.length;i++) {
			Object[] thisObject = {elements[i],values[i]};
			thisArrayList.add(thisObject);
		}
		return thisArrayList;
	}
	*/
	private Object returnValue(String val, boolean toInt) {
		if(toInt) {
			return SysUtils.returnInt(val);
		} else return val;		
	}
	
	private Object returnDoubleValue(String val, boolean todouble) {
		if(todouble) {
			return SysUtils.returnDouble(val);
		} else return val;		
	}
	
	class DRSFunctions{
		
		public void chfrequency(int channelNum) {
			
			current = ""+SysUtils.byteToUnsignedInt(data[0]);
			
			debug("Ch "+channelNum);
			
			switch (channelNum) {
				case 1	: 	statToMqttCurrent(DRSStats.Frecuencia.ch1frequency, objectName,"ch1frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 2	:	statToMqttCurrent(DRSStats.Frecuencia.ch2frequency, objectName,"ch2frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 3	:	statToMqttCurrent(DRSStats.Frecuencia.ch3frequency, objectName,"ch3frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 4	:	statToMqttCurrent(DRSStats.Frecuencia.ch4frequency, objectName,"ch4frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 5	:	statToMqttCurrent(DRSStats.Frecuencia.ch5frequency, objectName,"ch5frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 6	:	statToMqttCurrent(DRSStats.Frecuencia.ch6frequency, objectName,"ch6frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 7 	:	statToMqttCurrent(DRSStats.Frecuencia.ch7frequency, objectName,"ch7frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 8	:	statToMqttCurrent(DRSStats.Frecuencia.ch8frequency, objectName,"ch8frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 9	:	statToMqttCurrent(DRSStats.Frecuencia.ch9frequency, objectName,"ch9frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 10	:	statToMqttCurrent(DRSStats.Frecuencia.ch10frequency, objectName,"ch10frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 11	:	statToMqttCurrent(DRSStats.Frecuencia.ch11frequency, objectName,"ch11frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 12	:	statToMqttCurrent(DRSStats.Frecuencia.ch12frequency, objectName,"ch12frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 13	:	statToMqttCurrent(DRSStats.Frecuencia.ch13frequency, objectName,"ch13frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 14	:	statToMqttCurrent(DRSStats.Frecuencia.ch14frequency, objectName,"ch14frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 15	:	statToMqttCurrent(DRSStats.Frecuencia.ch15frequency, objectName,"ch15frequency",DRSStats.Frecuencia.clng,getVal()); break;
				case 16	:	statToMqttCurrent(DRSStats.Frecuencia.ch16frequency, objectName,"ch16frequency",DRSStats.Frecuencia.clng,getVal()); break;
				default	: 	break;
			} 			
		}
		
		public void channelSwitch() {
			byte[] bytearray = {data[1],data[0]};
			current = ""+SysUtils.bytestoInts16(bytearray)[0];
			debug("ch switch "+current);
			statToMqttCurrent(DRSStats.Frecuencia.channelSwitch, objectName,"channelSwitch",DRSStats.Frecuencia.clng,getVal());			
		}
		
		public void rfPowerSwitch() { 
			int thisBooleanInt = SysUtils.byteToUnsignedInt(data[0]);
			if(thisBooleanInt>0) current = "true";
			else current = "false";			
			statToMqttCurrent(DRSStats.Frecuencia.rfPowerSwitch, objectName,"rfPowerSwitch",DRSStats.Frecuencia.clng,getVal());		
		}
		
		public void upstreamNoiseSwitch() { 
			if(devicePort>0) {
				int thisBooleanInt = SysUtils.byteToUnsignedInt(data[0]);
				if(thisBooleanInt>0) current = "true";
				else current = "false";			
				subStatToMqttCurrent(DRSStats.Thresholds.upstreamNoiseSwitch, objectName,"upstreamNoiseSwitch",DRSStats.Thresholds.clng,getVal());	
			}				
		}
		
		public void linkATT(boolean up) {
			if(up) statToMqttCurrent(DRSStats.Frecuencia.uplinkATT, objectName,"uplinkATT",DRSStats.Frecuencia.clng,getVal());
			else statToMqttCurrent(DRSStats.Frecuencia.downlinkATT, objectName,"downlinkATT",DRSStats.Frecuencia.clng,getVal());
		}
		
		public void choiceOfWorkingMode() {
			statToMqttCurrent(DRSStats.Frecuencia.choiceOfWorkingMode, objectName,"choiceOfWorkingMode",DRSStats.Frecuencia.clng,getVal());
		}
		
		//InfoDispositivo
		public void deviceType(){statToMqttCurrent(DRSStats.InfoDispositivo.deviceType, objectName,"deviceType",DRSStats.InfoDispositivo.clng,getVal());}
		public void deviceModel(){statToMqttCurrent(DRSStats.InfoDispositivo.deviceModel, objectName,"deviceModel",DRSStats.InfoDispositivo.clng,getVal());}
		public void deviceChannelNumber(){statToMqttCurrent(DRSStats.InfoDispositivo.deviceChannelNumber, objectName,"deviceChannelNumber",DRSStats.InfoDispositivo.clng,getVal());}
		public void controlUnitSoftwareVersion(){statToMqttCurrent(DRSStats.InfoDispositivo.controlUnitSoftwareVersion, objectName,"controlUnitSoftwareVersion",DRSStats.InfoDispositivo.clng,getVal());}
		public void deviceSerialNumber(){statToMqttCurrent(DRSStats.InfoDispositivo.deviceSerialNumber, objectName,"deviceSerialNumber",DRSStats.InfoDispositivo.clng,getVal());}
		public void slaveNumber(){statToMqttCurrent(DRSStats.InfoDispositivo.slaveNumber, objectName,"slaveNumber",DRSStats.InfoDispositivo.clng,getVal());}
		public void ruId(){statToMqttCurrent(DRSStats.InfoDispositivo.ruId, objectName,"ruId",DRSStats.InfoDispositivo.clng,getVal());}
		
		//ParametrosRed
		public void siteNumber(){statToMqttCurrent(DRSStats.ParametrosRed.siteNumber, objectName,"siteNumber",DRSStats.ParametrosRed.clng,getVal());}
		public void siteSubDeviceNumber(){statToMqttCurrent(DRSStats.ParametrosRed.siteSubDeviceNumber, objectName,"siteSubDeviceNumber",DRSStats.ParametrosRed.clng,getVal());}
		public void repeaterIpPort(){statToMqttCurrent(DRSStats.ParametrosRed.repeaterIpPort, objectName,"repeaterIpPort",DRSStats.ParametrosRed.clng,getVal());}
		public void repeaterIpAddress(){statToMqttCurrent(DRSStats.ParametrosRed.repeaterIpAddress, objectName,"repeaterIpAddress",DRSStats.ParametrosRed.clng,getVal());}
		public void repeaterSubnetMask(){statToMqttCurrent(DRSStats.ParametrosRed.repeaterSubnetMask, objectName,"repeaterSubnetMask",DRSStats.ParametrosRed.clng,getVal());}
		public void repeaterDefaultGateway(){statToMqttCurrent(DRSStats.ParametrosRed.repeaterDefaultGateway, objectName,"repeaterDefaultGateway",DRSStats.ParametrosRed.clng,getVal());}
		public void monitorCenterIPAddress(){statToMqttCurrent(DRSStats.ParametrosRed.monitorCenterIPAddress, objectName,"monitorCenterIPAddress",DRSStats.ParametrosRed.clng,getVal());}
		public void monitorCenterIPPort(){statToMqttCurrent(DRSStats.ParametrosRed.monitorCenterIPPort, objectName,"monitorCenterIPPort",DRSStats.ParametrosRed.clng,getVal());}
		public void psProtocol(){statToMqttCurrent(DRSStats.ParametrosRed.psProtocol, objectName,"psProtocol",DRSStats.ParametrosRed.clng,getVal());}
		public void communicationStyle(){statToMqttCurrent(DRSStats.ParametrosRed.communicationStyle, objectName,"communicationStyle",DRSStats.ParametrosRed.clng,getVal());}
		public void macAddress(){statToMqttCurrent(DRSStats.ParametrosRed.macAddress, objectName,"macAddress",DRSStats.ParametrosRed.clng,getVal());}
		
		//Thresholds
		public void downlinkInputMinThreshold(){statToMqttCurrent(DRSStats.Thresholds.downlinkInputMinThreshold, objectName,"downlinkInputMinThreshold",DRSStats.Thresholds.clng,getVal());}
		public void downlinkInputMaxThreshold(){statToMqttCurrent(DRSStats.Thresholds.downlinkInputMaxThreshold, objectName,"downlinkInputMaxThreshold",DRSStats.Thresholds.clng,getVal());} 
		public void downlinkOutputMinThreshold(){statToMqttCurrent(DRSStats.Thresholds.downlinkOutputMinThreshold, objectName,"downlinkOutputMinThreshold",DRSStats.Thresholds.clng,getVal());} 
		public void downlinkOutputMaxThreshold(){statToMqttCurrent(DRSStats.Thresholds.downlinkOutputMaxThreshold, objectName,"downlinkOutputMaxThreshold",DRSStats.Thresholds.clng,getVal());} 
		public void paTempThreshold(){statToMqttCurrent(DRSStats.Thresholds.paTempThreshold, objectName,"paTempThreshold",DRSStats.Thresholds.clng,getVal());}  
		public void downlinkVSWRThreshold(){statToMqttCurrent(DRSStats.Thresholds.downlinkVSWRThreshold, objectName,"downlinkVSWRThreshold",DRSStats.Thresholds.clng,getVal());}  
		public void highThresholdUpstreamNoise(){statToMqttCurrent(DRSStats.Thresholds.highThresholdUpstreamNoise, objectName,"highThresholdUpstreamNoise",DRSStats.Thresholds.clng,getVal());} 
		public void lowThresholdUpstreamNoise(){statToMqttCurrent(DRSStats.Thresholds.lowThresholdUpstreamNoise, objectName,"lowThresholdUpstreamNoise",DRSStats.Thresholds.clng,getVal());} 
		public void uplinkNoiseCorrectionValue(){statToMqttCurrent(DRSStats.Thresholds.uplinkNoiseCorrectionValue, objectName,"uplinkNoiseCorrectionValue",DRSStats.Thresholds.clng,getVal());} 
		public void uplinkNoiseDetectionPar1(){statToMqttCurrent(DRSStats.Thresholds.uplinkNoiseDetectionPar1, objectName,"uplinkNoiseDetectionPar1",DRSStats.Thresholds.clng,getVal());}  
		public void uplinkNoiseDetectionPar2(){statToMqttCurrent(DRSStats.Thresholds.uplinkNoiseDetectionPar2, objectName,"uplinkNoiseDetectionPar2",DRSStats.Thresholds.clng,getVal());} 
		
		//Alarmas SET
		public void suplyPowerFailAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.suplyPowerFailAlarm, objectName,"suplyPowerFailAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void masterSlaveLinkAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.masterSlaveLinkAlarm, objectName,"masterSlaveLinkAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void downlinkOverInputAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.downlinkOverInputAlarm, objectName,"downlinkOverInputAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void downlinkLowInputAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.downlinkLowInputAlarm, objectName,"downlinkLowInputAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void downlinkSWRAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.downlinkSWRAlarm, objectName,"downlinkSWRAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void paTemphingAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.paTemphingAlarm, objectName,"paTemphingAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void downlinkOverOutputAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.downlinkOverOutputAlarm, objectName,"downlinkOverOutputAlarm",DRSStats.Alarmas.clng,getVal());} 
		public void downlinkLowOutputAlarmSet(){statToMqttCurrent(DRSStats.Alarmas.downlinkLowOutputAlarm, objectName,"downlinkLowOutputAlarm",DRSStats.Alarmas.clng,getVal());} 
		
		//Alarmas
		public void suplyPowerFailAlarm(){statToMqttVal(DRSStats.Alarmas.suplyPowerFailAlarm, objectName,"suplyPowerFailAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void masterSlaveLinkAlarm(){statToMqttVal(DRSStats.Alarmas.masterSlaveLinkAlarm, objectName,"masterSlaveLinkAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void downlinkOverInputAlarm(){statToMqttVal(DRSStats.Alarmas.downlinkOverInputAlarm, objectName,"downlinkOverInputAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void downlinkLowInputAlarm(){statToMqttVal(DRSStats.Alarmas.downlinkLowInputAlarm, objectName,"downlinkLowInputAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void downlinkSWRAlarm(){statToMqttVal(DRSStats.Alarmas.downlinkSWRAlarm, objectName,"downlinkSWRAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void paTemphingAlarm(){statToMqttVal(DRSStats.Alarmas.paTemphingAlarm, objectName,"paTemphingAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void downlinkOverOutputAlarm(){statToMqttVal(DRSStats.Alarmas.downlinkOverOutputAlarm, objectName,"downlinkOverOutputAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void downlinkLowOutputAlarm(){statToMqttVal(DRSStats.Alarmas.downlinkLowOutputAlarm, objectName,"downlinkLowOutputAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		public void opticalModuleTxRxAlarm(){statToMqttVal(DRSStats.Alarmas.opticalModuleTxRxAlarm, objectName,"opticalModuleTxRxAlarm",DRSStats.Alarmas.clng,getVal(),1);} 
		
		//RealTimePars
		public void downlinkInputPower(){statToMqttCurrent(DRSStats.RealTimePars.downlinkInputPower, objectName,"downlinkInputPower",DRSStats.RealTimePars.clng,getVal(),1);} 
		public void uplinkOutputPower(){statToMqttCurrent(DRSStats.RealTimePars.uplinkOutputPower, objectName,"uplinkOutputPower",DRSStats.RealTimePars.clng,getVal(),1);} 
		public void downlinkVSWR(){statToMqttCurrent(DRSStats.RealTimePars.downlinkVSWR, objectName,"downlinkVSWR",DRSStats.RealTimePars.clng,getVal(),1,0.1);}  
		public void downlinkOutputPower(){statToMqttCurrent(DRSStats.RealTimePars.downlinkOutputPower, objectName,"downlinkOutputPower",DRSStats.RealTimePars.clng,getVal(),1);} 
		public void uplinkInputPower(){statToMqttCurrent(DRSStats.RealTimePars.uplinkInputPower, objectName,"uplinkInputPower",DRSStats.RealTimePars.clng,getVal(),1);} 
		public void paTemp(){statToMqttCurrent(DRSStats.RealTimePars.paTemp, objectName,"paTemp",DRSStats.RealTimePars.clng,getVal(),1);}   
		
		/**
		 * Esta funcion recoge la lista de equipos remotos conectados al maestro.
		 * Estos equipos reciben una numeracion dependiendo del orden en el que estan enlistados
		 * El orden de los bytes no es correlativo, y existen bytes de los cuales se desconoce su funcion
		 * debido a falta de casos de uso donde estos varien
		 * 
		 * L: 0x12 
		 * C: A00B 
		 * D:  
		 * 00000000 <--- (00-03)  ???
		 * 01010101 <--- (04-07)  bytes Puertos Opticos en orden 4,3,1,2.
		 * 00000000 <--- (08-11)  ???
		 * 000000   <--- (12-14)  ???
		 * 
		 * */
		public void remoteListBytes() { 
				byte[] subElementBytes = new byte[4];
			if(data.length>=remoteListBytesSize) { 
				subElementInts[2]=SysUtils.byteToUnsignedInt(data[4]);
				subElementBytes[0]=data[5];
				subElementInts[3]=SysUtils.byteToUnsignedInt(data[5]);
				subElementBytes[1]=data[4];
				subElementInts[0]=SysUtils.byteToUnsignedInt(data[6]);
				subElementBytes[3]=data[6];
				subElementInts[1]=SysUtils.byteToUnsignedInt(data[7]);
				subElementBytes[2]=data[7];
				subElementBigInt = new BigInteger(subElementBytes);
				int j=0;
				for (int opticPortQuantity:subElementInts) { 
					for(int i=0 ;  i< opticPortQuantity; i++) {
						int deviceNumber = (j+1) * 10 + (i+1);
						System.out.println(objectName+"> OP"+j+": "+deviceNumber);	
						subElementList.add(new SubElement(objectName, deviceNumber,j+1,i+1,j));
					}
					j++;
				}
			}
		}
		
		public String bytesToData(byte[] datatoFormat, String format ) {
			return bytesToData(datatoFormat, format,1);
		}
		
		public String bytesToData(byte[] datatoFormat, String format, double factor) { 
			
			switch(format) {
				case "uint1": return Double.toString(SysUtils.byteToUnsignedInt(datatoFormat[0])*factor);
				case "uint2": return fromUint2(datatoFormat,factor);
				case "uint4": return fromUint4(datatoFormat,factor);
				case "sint1": return ""+(((int)datatoFormat[0])*factor);
				case "sint2": return fromSint2(datatoFormat,factor);
				case "bit": return toBoolString(SysUtils.byteToUnsignedInt(datatoFormat[0]));
				case "alarm": return toBoolString(SysUtils.byteToUnsignedInt(datatoFormat[0]));
				case "str20": return bytesToString( datatoFormat);
				case "ipaddress": return fromIpAddress(datatoFormat);
				case "macaddress": return fromMacAddress(datatoFormat);
			}			
			return null;
		}
		
		public String toBoolString(int datatoFormat) { 
			if(datatoFormat>0) return "true";
			else return "false";
		}
		
		public String bytesToString(byte[] datatoFormat) {
			try {
				return new String(datatoFormat, "UTF-8");
			} catch (UnsupportedEncodingException e) { 
				e.printStackTrace();
			}
			return null;
		}
		
		public String fromSint2(byte[]  datatoFormat, double factor) { 
			 DRSProtocol xprotocol = new DRSProtocol();
			 return xprotocol.fromSint2(datatoFormat,factor);
		 }
		
		public String fromUint2(byte[]  datatoFormat, double factor) { 
			 DRSProtocol xprotocol = new DRSProtocol();
			 return xprotocol.fromUint2(datatoFormat, factor);
		 }
		
		public String fromUint4(byte[]  datatoFormat, double factor) { 
			 DRSProtocol xprotocol = new DRSProtocol();
			 return xprotocol.fromUint4(datatoFormat, factor);
		 }

		 public String fromMacAddress(byte[]  datatoFormat) { 
			 DRSProtocol xprotocol = new DRSProtocol();
			 String thisString = xprotocol.fromMacAddress(datatoFormat);
			  return thisString;
		 }
		 
		 public String fromIpAddress(byte[]  datatoFormat) { 
			 DRSProtocol xprotocol = new DRSProtocol();
			 return xprotocol.fromIpAddress(datatoFormat);		 
		 }
		 
		 
	}
	
	public class SubElement{
		public String objectName;
		public int deviceNumber;
		public int port;
		public int index;
		public int portIndex;
		
		SubElement(String objectName,int deviceNumber, int port, int index, int portIndex){
			this.objectName = objectName;
			this.deviceNumber=deviceNumber; 
			this.port=port;
			this.index=index;
			this.portIndex=portIndex;
		}
	}
	
	
}
