package sigmation.drivers;

import java.util.ArrayList; 

import sigmation.mqtt.pahoMqtt;
import sigmation.utils.SysUtils;
import sigmation.utils.SysUtils.jsonArray;
import sigmation.utils.SysUtils.jsonObject;

public class Helpers {
	
	public void statToMqtt(pahoMqtt mdbMqtt,StatObject thisStatObject, String objectName, String varname, String groupname, String group, String setPar) {
		statToMqtt( mdbMqtt, thisStatObject,  objectName,  varname,  groupname,  group,  setPar, null, false);
	}
	
	private MqttVarObject getVarObject(pahoMqtt mdbMqtt,String objectName, StatObject thisStatObject, String varname, String group) {
		MqttVarObject thisVarObject; 
		if(group.toLowerCase().contains("realtimeparams")){
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"graph", 
					null, 
					"0", 
					"0", 
					null, 
					thisStatObject.paramUnit, 
					"true");
			System.out.println("control");
		} else if(thisStatObject.paramDataType.toLowerCase().contains("int") && thisStatObject.select){
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"number", 
					thisStatObject.paramOptions, 
					"0", 
					"0", 
					null, 
					thisStatObject.paramUnit, 
					"true"); 
		} 
		else if(thisStatObject.paramDataType.toLowerCase().contains("int")){
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"number", 
					null, 
					"0", 
					"0", 
					null, 
					thisStatObject.paramUnit, 
					"true"); 
		} else if(thisStatObject.paramDataType.toLowerCase().contains("alarm")){
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"alarm", 
					null, 
					"false", 
					"false", 
					"false", 
					thisStatObject.paramUnit, 
					"true");
		} else if(thisStatObject.paramDataType.toLowerCase().contains("bit") && thisStatObject.paramClass.toLowerCase().contains("alarm")){
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"alarm", 
					null, 
					"false", 
					"false", 
					"false", 
					thisStatObject.paramUnit, 
					"true");
		} 
		else if(thisStatObject.paramDataType.toLowerCase().contains("bit")){
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"boolean", 
					null, 
					"false", 
					"false", 
					"false", 
					thisStatObject.paramUnit, 
					"true");
			//System.out.println("control");
		} else { 
			thisVarObject = new MqttVarObject(
					mdbMqtt.getBaseTopic()+objectName, 
					varname, 
					thisStatObject.name, 
					"text", 
					null, 
					"__null", 
					"false", 
					"false", 
					thisStatObject.paramUnit, 
					"true");
		} 	
		return thisVarObject;  
	}
	
	public void statToMqtt(pahoMqtt mdbMqtt,StatObject thisStatObject, String objectName, String varname, String groupname, String group, String setPar, String setCurrent, boolean deploy) {
		MqttVarObject thisVarObject=getVarObject(mdbMqtt,  objectName, thisStatObject, varname, group);
		
		String baseTopic = thisVarObject.basetopic+"/__group/"+group+"/__var/";	
		if(setPar!=null) { thisVarObject.__set=setPar;	thisVarObject.__current=setPar;  }	 
		else { 
			thisVarObject.__set="__null";	thisVarObject.__current="__null";
		}
		if(setCurrent!=null) {thisVarObject.__current="0";}
		mdbMqtt.publish(baseTopic+varname+"/__name", thisVarObject.__name.getBytes()); 
		if(!thisStatObject.readonly) {
			if(deploy && setCurrent==null) mdbMqtt.publish(baseTopic+varname+"/__set", "__null".getBytes());
			else mdbMqtt.publish(baseTopic+varname+"/__set", thisVarObject.__set.getBytes()); 
		}		
		if(thisVarObject.__unit!=null)mdbMqtt.publish(baseTopic+varname+"/__unit", thisVarObject.__unit.getBytes()); 
		else mdbMqtt.publish(baseTopic+varname+"/__unit", " ".getBytes());
		if(thisStatObject.select) mdbMqtt.publish(baseTopic+varname+"/__options", thisVarObject.__options.getBytes());
		mdbMqtt.publish(baseTopic+varname+"/__class", thisVarObject.__class.getBytes()); 		
		 
		if(deploy && setCurrent!=null) {
			mdbMqtt.publish(baseTopic+varname+"/__value", setCurrent.getBytes());  
			mdbMqtt.publish(baseTopic+varname+"/__current", setPar.getBytes()); 
		} else mdbMqtt.publish(baseTopic+varname+"/__current", thisVarObject.__current.getBytes()); 
			
	}
	
	
	
	public String statToMqttCurrent(pahoMqtt mdbMqtt,StatObject thisStatObject, String objectName, String varname, String group, String setCurrent) {
		MqttVarObject thisVarObject=getVarObject(mdbMqtt,  objectName, thisStatObject, varname,group);		
		String baseTopic = thisVarObject.basetopic+"/__group/"+group+"/__var/";	 
		if (setCurrent!=null) { 
			thisVarObject.__current=setCurrent; 
			//System.out.println("setpar "+baseTopic+varname+"/__current "+setCurrent);	 
		} 
		mdbMqtt.publish(baseTopic+varname+"/__current", thisVarObject.__current.getBytes()); 
		if(!thisStatObject.readonly) {
			//mdbMqtt.publish(baseTopic+varname+"/__set", thisVarObject.__current.getBytes()); 
		}	
		return baseTopic+varname+"/__current";
	}
	
	public String statToMqttVal(pahoMqtt mdbMqtt,StatObject thisStatObject, String objectName, String varname, String group, String setCurrent) {
		MqttVarObject thisVarObject=getVarObject(mdbMqtt,  objectName, thisStatObject, varname,group);		
		String baseTopic = thisVarObject.basetopic+"/__group/"+group+"/__var/";	 
		if (setCurrent!=null) { 
			thisVarObject.__value=setCurrent; 
			//System.out.println("setpar "+baseTopic+varname+"/__value "+setCurrent);	 
			}  
			mdbMqtt.publish(baseTopic+varname+"/__value", thisVarObject.__value.getBytes());  
			return baseTopic+varname+"/__value";
	}
	
	public  void resourceGroupObject(pahoMqtt mdbMqtt,String objectName, String rGroupName, String rGroupLabel) {
		String baseTopic = mdbMqtt.getBaseTopic()+objectName;
		String resourceGroupTopic= baseTopic+"/__resourcegroup/"+rGroupName+"/__name";
		mdbMqtt.publish(resourceGroupTopic, rGroupLabel.getBytes()); 
	}
	
	public  void metaObject(pahoMqtt mdbMqtt,String objectName, String labelName, String details){
		String[][] metaObject = {{"__id",objectName},{"__name",labelName},{"__details",details}};
		String message = SysUtils.jsonBuilder(metaObject, "meta");
		String metaTopic = mdbMqtt.getBaseTopic()+objectName+"/__meta/__content";	
		mdbMqtt.publish(metaTopic, message.getBytes()); 
	}
	
	public  void metaObjectComp(pahoMqtt mdbMqtt,String objectName, String labelName, String details,String type,ArrayList<jsonArray> listArgs){
		String[][] metaObject = {{"__id",objectName},{"__name",labelName},{"__details",details}};
		String message = SysUtils.jsonMetaBuilder(metaObject, type, listArgs);
		String metaTopic = mdbMqtt.getBaseTopic()+objectName+"/__meta/__content";	
		mdbMqtt.publish(metaTopic, message.getBytes()); 
	}
	
	public  void basicVarComp(pahoMqtt mdbMqtt,String objectName, String labelName, String message) {
		String varTopic = mdbMqtt.getBaseTopic()+objectName+"/__"+labelName+"/";	
		mdbMqtt.publish(varTopic, message.getBytes());
	}
	
	public  jsonObject[] itemObject(String[][] elements) {
		try { 
			jsonObject[] itemObject= new jsonObject[elements.length];
			int i=0;
			for(String[] element: elements) {
				itemObject[i] = new SysUtils().new jsonObject(element[0], element[1]);
				i++;
			} 
			return itemObject;
		} catch (Exception e) {
			return null;
		}
	}
	 
}
