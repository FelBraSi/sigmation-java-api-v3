package sigmation.drivers;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import sigmation.drivers.DRSPacketProcess.DRSFunctions;
import sigmation.mqtt.pahoMqtt;
import sigmation.utils.CRCXModem;
import sigmation.utils.SysUtils;

public class DRSProtocol {
	/**
	 *  [header][QR][SQ][INF][CMD][CHCK+cola]
	 *     10    1   2    1   3+n      3     
	 *  
	 *  Header= 10 bytes
	 *	QR= 1 byte , comando/respuesta (80/00)
	 *	SQ= 2 bytes, set/query  (0103 / 0102)
	 *	INF=1 byte, information (FF=send, 00=receive editable param., 01=receive non-edit. param.)
	 *	CMD= command data
	 *	CHCK= CRC 16 -XMODEM (byte 1 header -> last byte CMD) 
	 *	7E= cola
	 * 
	 *  Header = [0x7E][X][SN][NUM][Y]
	 *	X= 2bytes, 0301?
	 *	SN= 4bytes, site number on Site management
	 *	NUM=1byte, number of unit (00= Master, 02=Remote 2,...) assign by Site SubDevice number parameter
	 * 	Y= 2bytes, 0100?
 	 *  
	 *  Estructura parámetro único
	 *	[CMD] = [L][code][data]  
	 *	
	 *	L= 1 bytes, value 03+N , N= length of Data
	 *	code= 2 bytes, param code [ver DB]
	 *	Data= N bytes, data (number, string, etc) 
	 *	
	 *	Estructura parámetros encadenados
	 *	[CMD] = [L0][code0][data0]... [LN][codeN][dataN]
	 *	
	 *	N= Quantity of chained values   
	 *
	 *  pre-data bytes = 10 + 1 + 2 + 1 + 3 + 3 = 20 bytes.
	 *  
	 * */
	
	 /**
	  * 
	  * NUM= defines if communication goes for master (00) or remote (XY)
	  * where
	  * X = port number
	  * Y = number of device in current port
	  * 
	  * The format goes as follows as shown in the example
	  * 
	  *    MASTER-----OPTIC PORT 1 ---------- Remote 11
	  *    			|				|
	  *    			|				--------- Remote 12
	  *    			|				|
	  *    			|				--------- Remote 12
	  *    			|
	  *    			|
	  * 			--OPTIC PORT 2 ---------- Remote 21
	  *    			|				|
	  *    			|				--------- Remote 22
	  *    			|
	  *    			|
	  *    	 		--OPTIC PORT 3 ---------- Remote 31
	  *    
	  * Doesn't matter if the devices are connected in daisy chain, the format stays the same   
	  * 
	  * */
	
	 /**Buffers*/
	 byte[] cBuffer; 
	 byte[] preBuffer;
	 int bufferDataSize = 0;
	 int preBufferDataSize = 0;
	 
	 /**Header*/
	 private final	byte 	Head = (byte) 0x7E;
	 private final	byte[] 	Xbytes = {0x01,0x01};
	 private 		byte[] 	SiteNumber= {0,0,0,0};
	 private  		byte 	unitNum ;
	 private final	byte[] 	Ybytes = {0x01,0x00};
	 
	 public   final byte 	QRCommand = (byte) 0x80;
	 public   final byte 	QRReceive = (byte) 0x00;
	 private  		 byte 	QR		  = (byte) 0x00;
	 
	 public   final byte[] SQSet   = {0x01,0x03};
	 public   final byte[] SQQuery = {0x01,0x02};
	 private       	 byte[]	 SQ		 = {0x01,0x03};
	 
	 public   final byte InfSend   	= (byte) 0xFF;
	 public   final byte InfREdit 	= (byte) 0x00;
	 public   final byte InfRNoEdit 	= (byte) 0x01;
	 private  		byte Inf  			= (byte) 0x01;
	 
	 /**Command*/
	 private  		byte 	cmdL ;
	 private  		byte[] 	code = new byte[2];
	 private  		byte[] cData;
	 
	 /**Tail*/
	 byte[] crc = new byte[2];
	 private final byte END = (byte) 0x7E;
	 
	 /**byte address*/
	 private final int headerAddress = 0;
	 private final int XbytesAddress = 1;
	 private final int SiteNumberAddress = 3;
	 private final int unitNumAddress = 7;
	 private final int YbytesAddress = 8;
	 private final int QRAddress = 10;
	 private final int SQAddress = 11;
	 private final int INFAddress = 13;
	 private final int LAddress = 14;
	 private final int codeAddress = 15;
	 private final int dataAddress = 17;
	 
	 private int crcAddress(int cmdLength) {
		 return LAddress+cmdLength; 
	 }
	 
	 
	 
	 public void setQR(int QR) {
		 this.QR= (byte)QR;
	 }
	 
	 /**
	  * 
	  *  .QRCommand for a command request (0x80)
	  *  .QRReceive for a query command (0x00) 
	  * 
	  * */
	 
	 public void setQR(byte QR) {
		 this.QR=  QR;
	 } 
	 
	 /**
	  * 
	  *  .SQSet for a set command ({0x01,0x03})
	  *  .SQQuery for a query command ({0x01,0x02}) 
	  * 
	  * */
	 public void setSQ(byte[] SQ) {
		 this.SQ=  SQ;
	 }
	 
	 public void setSiteNum(byte[] SiteNumber) {
		 if(SiteNumber.length==4) {
			 this.SiteNumber=SiteNumber;  
		 }		 
	 }
	 
	 public void setData(byte[] cData) {
		 this.cData=cData;
		 setL();
	 }
	 
	 public void setUnitNum(int unitNum) {
		 this.unitNum= (byte)unitNum;
	 }
	 
	 public void setUnitNum(byte unitNum) {
		 this.unitNum= unitNum;
	 }
	 
	 public void setCRC() {
		 int i=0;
		 preBuffer = new byte[preBufferDataSize];
		 preBuffer[0] = Xbytes[0];
		 preBuffer[1] = Xbytes[1];
		 preBuffer[2] = SiteNumber[0];
		 preBuffer[3] = SiteNumber[1];
		 preBuffer[4] = SiteNumber[2];
		 preBuffer[5] = SiteNumber[3];
		 preBuffer[6] = unitNum;
		 preBuffer[7] = Ybytes[0];
		 preBuffer[8] = Ybytes[1];
		 preBuffer[9] = QR;
		 preBuffer[10] = SQ[0];
		 preBuffer[11] = SQ[1];
		 preBuffer[12] = Inf;
		 preBuffer[13] = cmdL;
		 preBuffer[14] = code[0];
		 preBuffer[15] = code[1];
		 for (byte var : cData) {
			 preBuffer[16+i] = var;
			 i++;
		 }
		 CRCXModem crcX = new CRCXModem();		 
		 crc = sigmation.utils.SysUtils.intTo2Bytes(crcX.update(preBuffer));
		 crcX.printCRC();
	 }
	 
	 public void setCRCMulti() {
		 int i=0;
		 preBuffer = new byte[13+cData.length];
		 preBuffer[0] = Xbytes[0];
		 preBuffer[1] = Xbytes[1];
		 preBuffer[2] = SiteNumber[0];
		 preBuffer[3] = SiteNumber[1];
		 preBuffer[4] = SiteNumber[2];
		 preBuffer[5] = SiteNumber[3];
		 preBuffer[6] = unitNum;
		 preBuffer[7] = Ybytes[0];
		 preBuffer[8] = Ybytes[1];
		 preBuffer[9] = QR;
		 preBuffer[10] = SQ[0];
		 preBuffer[11] = SQ[1];
		 preBuffer[12] = Inf; 
		 for (byte var : cData) {
			 preBuffer[13+i] = var;
			 i++;
		 }
		 CRCXModem crcX = new CRCXModem();		 
		 crc = sigmation.utils.SysUtils.intTo2Bytes(crcX.update(preBuffer));
		 crcX.printCRC();
	 }
	 
	 public void setPacket() {
		 cBuffer = new byte[bufferDataSize];
		 int i=0;
		 cBuffer[0]=Head;
		 for (byte var : preBuffer) {
			 cBuffer[1+i] = var;
			 i++;
		 }
		 cBuffer[1+i]=crc[1];
		 cBuffer[2+i]=crc[0];
		 cBuffer[3+i]=END;
		 //System.out.println("DRS packet: ");
		 //sigmation.utils.SysUtils.printhex(cBuffer,cBuffer.length);
	 }
	 
	 public byte[] getPacket() {
		 return cBuffer;
	 }
	 
	 public void setup(byte[] SiteNumber, byte unitNum, byte[] SQ, byte[] code, byte[] cData) {
		 this.SiteNumber=SiteNumber;
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQ;
		 this.Inf = InfSend; 
		 setCommandcode(code);
		 setData(cData);
		 setCRC();
		 if(cData!=null) setPacket();
	 }
	 
	 public void setup(byte unitNum, byte[] SQ, byte[] code, byte[] cData) {
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQ;
		 this.Inf = InfSend; 
		 setCommandcode(code);
		 setData(cData);
		 setCRC();	
		 if(cData!=null) setPacket();
	 }
	 
	 public boolean setupSendQuery(byte unitNum, byte[] code, byte[] cData) {
		 if(cData==null) return false;
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQQuery;
		 this.Inf = InfSend; 
		 setCommandcode(code);
		 setData(cData);
		 setCRC();	
		 setPacket();
		 return true;
	 }
	 
	 public boolean setupSendQuery_InverseCode(int sitenumber,byte unitNum, byte[] code, byte[] cData) {
		 if(cData==null) return false;
		 this.SiteNumber= SysUtils.intToByteArrayMSB2LSB(sitenumber, 4);
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQQuery;
		 this.Inf = InfSend; 
		 byte[] code_ = {code[1],code[0]};
		 setCommandcode(code_);
		 setData(cData);
		 setCRC();	
		 setPacket();
		 return true;
	 }
	 
	 public boolean setupSendQueryInverseCode(int sitenumber,byte unitNum, ArrayList<CommandBuffer> commandList) {
		 this.SiteNumber= SysUtils.intToByteArrayMSB2LSB(sitenumber, 4);
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQQuery;
		 this.Inf = InfSend;  
		 cData=commandBufferBytes(commandList);
		 setData(cData);
		 setCRCMulti();	
		 setPacket();
		 return true;
	 }
	 
	 public boolean setupSendSetInverseCode(int sitenumber,byte unitNum, ArrayList<CommandBuffer> commandList) {
		 this.SiteNumber= SysUtils.intToByteArrayMSB2LSB(sitenumber, 4);
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQSet;
		 this.Inf = InfSend;  
		 cData=commandBufferBytes(commandList);
		 setData(cData);
		 setCRCMulti();	
		 setPacket();
		 return true;
	 }
	 
	 private byte[] commandBufferBytes(ArrayList<CommandBuffer> commandList) {
		 byte[] thisData = new byte[byteCount(commandList)] ;
		 int k=0; 
		 for(int i=0;i<commandList.size();i++) {
			 CommandBuffer item = commandList.get(i);
			 int frameSize = SysUtils.byteToUnsignedInt(item.lData[0]);
			 thisData[0+k]= item.lData[0];
			 thisData[1+k]= item.command[1];
			 thisData[2+k]= item.command[0];
			 for(int j=0;j<item.data.length;j++) {
				 thisData[3+k+j]=item.data[j];
			 }
			 k=k+frameSize;
		 }	
		 return thisData;
	 }
	 
	 private int byteCount(ArrayList<CommandBuffer> commandList) {
		 int thisCounter=0;
		 for(int i=0; i<commandList.size();i++) {
			 CommandBuffer item = commandList.get(i);
			 thisCounter= thisCounter+item.data.length+item.command.length+item.lData.length;
		 }		 
		 return thisCounter;
	 }
	 
	 public boolean setupSendSet(int sitenumber,byte unitNum, byte[] code, byte[] cData) {
		 if(cData==null) return false;
		 this.SiteNumber= SysUtils.intToByteArrayMSB2LSB(sitenumber, 4);
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQSet;
		 this.Inf = InfSend; 
		 setCommandcode(code);
		 setData(cData);
		 setCRC();	
		 setPacket();
		 return true;
	 }
	 

	 
	 public boolean setupSendSetInverseCode(int sitenumber,byte unitNum, byte[] code, byte[] cData) {
		 if(cData==null) return false;
		 this.SiteNumber= SysUtils.intToByteArrayMSB2LSB(sitenumber, 4);
		 this.unitNum=unitNum;
		 this.QR = QRCommand;
		 this.SQ = SQSet;
		 this.Inf = InfSend; 
		 byte[] code_ = {code[1],code[0]};
		 setCommandcode(code_);
		 setData(cData);
		 setCRC();	
		 setPacket();
		 return true;
	 }
	 
	 public void setCommandcode(byte[] code) {
		 if(code.length==2) { 
			 this.code=code;
		 }
	 }
	 
	 private void setL() {
		 cmdL=(byte)(3+cData.length);
		 bufferDataSize = 20 + cData.length;
		 preBufferDataSize = bufferDataSize-4;
	 }
	 
	 public class DRSPacketObject{
		    public int command =0;
		    public byte[] data = new byte[2];
		    public String sCommand="noCommand"; 
		    public pahoMqtt drsMqtt;
		    public String objectName;
		    public boolean isResponse=false;
		    public int unitnum=0;
		    public int dataSize=0;
		    public byte[] dataBytes;
		    public ArrayList<byte[]> commandList = new ArrayList<byte[]>();
	 }
	 
	 private boolean checkValidPacket(byte[] packetData) {
		 //System.out.println("packet length "+packetData.length);
		 //SysUtils.printhex(packetData, packetData.length, true);
		 //System.out.println(" ");
		 if(packetData.length<10) return false;
		 if(packetData[headerAddress]!=Head) return false;
		 //BUG EN PROTOCOLO CHINO, NO USAR // if(packetData[packetData.length-1]!=Head) return false;
		 CRCXModem crcX = new CRCXModem();
		 byte[] forCRC = Arrays.copyOfRange(packetData, 1, packetData.length-3); 
		 int crcAddrs= crcAddress(SysUtils.byteToUnsignedInt(packetData[LAddress]));
		 byte[] packetCRC = {packetData[packetData.length-2],packetData[packetData.length-3]};
		 int expectedCRC = crcX.update(forCRC);
		 int[] receivedCRC = SysUtils.bytestoInts16(packetCRC);
		 byte[] expectedCRCb=SysUtils.intTo2Bytes(expectedCRC);
		 if(receivedCRC[0]!=expectedCRC) {
			 /*System.out.print("packet ");
			 SysUtils.printhex(packetCRC, packetCRC.length, true);
			 System.out.print(" vs expected ");
			 SysUtils.printhex( expectedCRCb, expectedCRCb.length, true);
			 System.out.println(" ");
			 System.out.println("CRC: "+receivedCRC[0]+"vs"+expectedCRC+""); */
		 }	
		 int dataSize=packetData.length-INFAddress-4;
		 System.out.println("data size "+dataSize);
		 return true;
	 }
	 
	 private boolean checkInf(byte infdata) {
		 if (infdata!=InfSend) {
			 return true;
		 }
		 else return false;
	 }
	 
	 public DRSPacketObject analyzePacket(byte[] packetData) {
		 byte[] thisCommandBytes;
		 DRSPacketObject thisObject = new DRSPacketObject();
		 if(!checkValidPacket(packetData)) {
			 System.out.println("not valid");
			 return null;
		 } 
		 /*
		 System.out.println("valid");
		 System.out.print("packet bytes: ");
		 SysUtils.printhex(packetData, packetData.length, true);
		 System.out.println(" ");
		 */
		 byte[] commandBytes= {packetData[codeAddress+1],packetData[codeAddress]};
		 
		 int thisCommand = SysUtils.bytestoInts16(commandBytes)[0]; 
		 thisObject.command=thisCommand;
		 thisObject.isResponse=checkInf(packetData[INFAddress]);
		 thisObject.unitnum = SysUtils.byteToUnsignedInt(packetData[unitNumAddress]);
		 try {
			 thisObject.sCommand = DRSCommandList.map(thisCommand);
			 thisObject.dataBytes= Arrays.copyOfRange(packetData, INFAddress+1, packetData.length-3); 
			 thisObject.dataSize=packetData.length-INFAddress-4; 
			 System.out.print("command bytes: ");
			 SysUtils.printhex(thisObject.dataBytes, thisObject.dataBytes.length, true);
			 System.out.println(" ");
			 
			 int i=0;
			 while ( i<thisObject.dataBytes.length) {
				 int thisSize= SysUtils.byteToUnsignedInt(thisObject.dataBytes[i]);
				 if(i+thisSize<=thisObject.dataBytes.length) {
					 thisCommandBytes = Arrays.copyOfRange(thisObject.dataBytes, i, i+thisSize);
					 System.out.print("single command bytes: ");
					 SysUtils.printhex(thisCommandBytes, thisCommandBytes.length, true);
					 System.out.println(" ");
					 thisObject.commandList.add(thisCommandBytes);
				 }				 
				 i=i+thisSize;
			 }
		} catch (Exception e) { 
			e.printStackTrace();
		}		 
		 return thisObject;
	 }
	 
	 public String bytesToData(byte[] datatoFormat, String format) { 
			
			switch(format) {
				case "uint1": return ""+SysUtils.byteToUnsignedInt(datatoFormat[0]);
				case "uint2": return ""+SysUtils.bytestoInts16(datatoFormat)[0];
				case "sint1": return ""+(int)datatoFormat[0];
				case "sint2": return ""+SysUtils.bytestoSignedInts16(datatoFormat);
				case "bit": return toBoolString(SysUtils.byteToUnsignedInt(datatoFormat[0]));
				case "str20": return bytesToString( datatoFormat);
				case "ipaddress": return fromIpAddress(datatoFormat);
				case "macaddress": return fromMacAddress(datatoFormat);
			}			
			return null;
	}
	 
	 public static byte[] getEmptyBytes(String format) {  
		 	//System.out.println("mepty bytes for "+format);
			switch(format) {
				case "uint1": return new byte[1];
				case "uint2": return new byte[2];
				case "uint4": return new byte[4];
				case "sint1": return new byte[1];
				case "sint2": return new byte[2];
				case "bit": return new byte[1];
				case "str20": return new byte[20];
				case "ipaddress": return new byte[4];
				case "macaddress": return new byte[6];
				case "remotelist": return new byte[0];
			}			
			return new byte[1];
	}
	 
	 public byte[] dataToBytes(byte[] datatoFormat, String format) { 
			
			switch(format) {
				case "uint1": return toUint1(datatoFormat);
				case "uint2": return toUint2(datatoFormat);
				case "uint4": return toUint4(datatoFormat);
				case "sint1": return toSint1(datatoFormat);
				case "sint2": return toSint2(datatoFormat);
				case "bit": return toBit(datatoFormat);
				case "alarm": return toBit(datatoFormat);
				case "str20": return toString20(datatoFormat);
				case "ipaddress": return toIpAddress(datatoFormat);
				case "macaddress": return toMacAddress(datatoFormat);
			}			
			return null;
	}
	 
	 public byte[] toUint1(byte[] datatoFormat) {
		 String thisString = bytesToString(datatoFormat);
		 int thisInt = SysUtils.returnInt(thisString);
		 byte[] thisByte = {(byte)thisInt};
		 return thisByte;
	 }
	 
	 public byte[] toUint2(byte[]  datatoFormat) {
		 String thisString = bytesToString(datatoFormat);
		 int thisInt = SysUtils.returnInt(thisString);
		 byte[] thisByte = SysUtils.intTo2Bytes(thisInt);
		 byte[] thisInversedByte={thisByte[1],thisByte[0]};
		 return thisInversedByte;
	 }
	 
	 public byte[] toUint4(byte[]  datatoFormat) {
		 String thisString = bytesToString(datatoFormat);
		 int thisInt = SysUtils.returnInt(thisString); 
		 byte[] nonInvertedByte= ByteBuffer.allocate(4).putInt(thisInt).array();
		 byte[] inversedByte = {nonInvertedByte[3],nonInvertedByte[2],nonInvertedByte[1],nonInvertedByte[0]};
		 return inversedByte;
	 }
	 
	 public byte[] toSint1(byte[]  datatoFormat) {
		 String thisString = bytesToString(datatoFormat);
		 int thisInt = SysUtils.returnInt(thisString);
		 byte[] thisByte = {(byte)thisInt};
		 return thisByte;
	 }
	 
	 public byte[] toBit(byte[]  datatoFormat) {
		 String thisString = bytesToString(datatoFormat);
		 byte thisBool=0;
		 if(thisString.contentEquals("true")) {
			 thisBool=1;
		 } 
		 byte[] thisByte = {thisBool};
		 return thisByte;
	 }
	 
	 public byte[] toSint2(byte[]  datatoFormat) {
		 String thisString = bytesToString(datatoFormat);
		 int thisInt = SysUtils.returnInt(thisString);
		 byte[] thisByte = SysUtils.intTo2Bytes(thisInt);
		 byte[] thisInversedByte={thisByte[1],thisByte[0]};
		 return thisInversedByte;
	 }
	 
	 public byte[] toString20(byte[]  datatoFormat) { 
		 try { 
			 byte[] thisByte = new byte[20];
			 for(int i = 0 ; i < datatoFormat.length && i < thisByte.length; i++) {
				 thisByte[i]=datatoFormat[i];
			 } 
			 return thisByte;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		byte[] bb = {0};
		return bb;
		 
	 }
	 
	 public byte[] toMacAddress(byte[]  datatoFormat) { 
		 try { 
			 byte[] thisByte = new byte[6];
			 String thisString = bytesToString(datatoFormat);
			 String[] ipNumbers;
			 if(thisString.contains("-")) {
				 ipNumbers = thisString.split("-");
			 }
			 else if(thisString.contains(":")) {
				 ipNumbers = thisString.split(":");
			 }  else {
				 return null;
			 }
			 
			 if(ipNumbers.length==6) {
				 for(int i=0; i<4;i++) {
					 thisByte[i]=(byte)SysUtils.returnIntHex(ipNumbers[i]);
				 }
				 return thisByte;
		     } 
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return null;		 
	 }
	 
	 public byte[] toIpAddress(byte[]  datatoFormat) { 
		 try { 
			 byte[] thisByte = new byte[4];
			 String thisString = bytesToString(datatoFormat);
			 String[] ipNumbers = thisString.split("\\.");
			 if(ipNumbers.length==4) {
				 for(int i=0; i<4;i++) {
					 thisByte[i]=(byte)SysUtils.returnInt(ipNumbers[i]);
				 }
				 return thisByte;
		     } 
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return null;		 
	 }
	 
	 public static long byteAsULong(byte b) {
		    return ((long)b) & 0x00000000000000FFL; 
		}

	public static long getUInt32(byte[] bytes) {
		    long value = byteAsULong(bytes[0]) | (byteAsULong(bytes[1]) << 8) | (byteAsULong(bytes[2]) << 16) | (byteAsULong(bytes[3]) << 24);
		    return value;
	} //
		
	
	public String fromUint2(byte[]  datatoFormat, double factor) {
		int thisInteger = ((datatoFormat[1] << 8) & 0x0000ff00) | (datatoFormat[0] & 0x000000ff);
		double thisDouble = thisInteger* factor;
		 String thisString = Double.toString(thisDouble);
		 return thisString;
	 }  
	
	public String fromSint2(byte[]  datatoFormat, double factor) {
		int thisInteger =  ((datatoFormat[1] & 0xff) | (short) (datatoFormat[1] << 8));
		double thisDouble = thisInteger* factor;
		String thisString = Double.toString(thisDouble);
		return thisString;
	 }
	
	 public String fromUint4(byte[]  datatoFormat, double factor) {
		 long thisLong = getUInt32(datatoFormat);
		 double thisDouble = thisLong* factor;
		 String thisString = Double.toString(thisDouble);
		 return thisString;
	 }
	 
	 public String fromMacAddress(byte[]  datatoFormat) { 
		 try { 
			 String thisString = new String();
			 if(datatoFormat.length==6) {
				 thisString=""+SysUtils.byteToStringHex(datatoFormat[0])+
						 	":"+SysUtils.byteToStringHex(datatoFormat[1])+
						 	":"+SysUtils.byteToStringHex(datatoFormat[2])+
						 	":"+SysUtils.byteToStringHex(datatoFormat[3]);
				 return thisString;
			 }
			 return null;	
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return null;		 
	 }
	 
	 public String fromIpAddress(byte[]  datatoFormat) { 
		 try { 
			 String thisString = new String(); 
			 if(datatoFormat.length==4) {
				 thisString=""+SysUtils.byteToUnsignedInt(datatoFormat[0])+
						 	"."+SysUtils.byteToUnsignedInt(datatoFormat[1])+
						 	"."+SysUtils.byteToUnsignedInt(datatoFormat[2])+
						 	"."+SysUtils.byteToUnsignedInt(datatoFormat[3]);
				 return thisString;
			 }
			 return null;
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return null;		 
	 }
	 
	 
	 
	 public String toBoolString(int datatoFormat) { 
			if(datatoFormat>0) return "true";
			else return "false";
		}
		
		public String bytesToString(byte[] datatoFormat) {
			try {
				String thisString = new String(datatoFormat, "UTF-8");
				System.out.println("bytes to string "+thisString);
				return thisString;
			} catch (UnsupportedEncodingException e) { 
				e.printStackTrace();
			}
			return null;
		}
		
     public ArrayList<Object>  bytesToCommandObject(byte[] commandBytes) {
    	 ArrayList<Object> thisObjectList = new ArrayList<Object>();
    	 byte[] thisCommandBytes= {commandBytes[2],commandBytes[1]};
    	 byte[] thisDataBytes =  Arrays.copyOfRange(commandBytes, 3, commandBytes.length);
    	 int thisCommandInt = SysUtils.bytestoInts16(thisCommandBytes)[0];
    	 thisObjectList.add(thisCommandInt);
    	 thisObjectList.add(thisDataBytes);
    	 return thisObjectList;
     }
     
     public class CommandBuffer{
    	 byte[] command;
    	 byte[] data;
    	 byte[] lData;
    	 
    	 public CommandBuffer(byte[] command, byte[] data) {
			this.command=command;
			this.data=data;
			byte[] thisLData= {(byte)(this.command.length+this.data.length+1)};
			lData=thisLData;
		}
     }
	 
}
