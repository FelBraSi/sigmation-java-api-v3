package sigmation.drivers;

import java.util.HashMap;

public class DRSCommandList { 
		
	static HashMap<Integer,String> valueMap = new HashMap<Integer,String>(); 
	static HashMap<Integer,String> valueMap2 = new HashMap<Integer,String>(); 
	static HashMap<String,Integer> valueMapInverse = new HashMap<String,Integer>(); 
	
	public static final String uint1 = "uint1";  //unsigned int, 1 byte
	public static final String uint2 = "uint2";  //unsigned int, 2 bytes
	public static final String uint4 = "uint4";  //unsigned int, 2 bytes
	public static final String sint1 = "sint1";  //signed int, 1 byte
	public static final String sint2 = "sint2";  //signed int, 2 bytes
	public static final String bit = "bit";      //boolean
	public static final String alarm = "alarm";      //boolean
	public static final String str20 = "str20";  //string 20 bytes
	public static final String ipaddress = "ipaddress";  //string 20 bytes
	public static final String macaddress = "macaddress";  //string 20 bytes
	public static final String remotelist = "remotelist";  //string 20 bytes
	
	DRSCommandList (){ 
		init();
	}
	
	public static void init() {
		valueMap.clear();
		valueMapInverse.clear();
		dualmap(Frecuencia.channelSwitch,"channelSwitch",Frecuencia.channelSwitchT);
		dualmap(Frecuencia.rfPowerSwitch,"rfPowerSwitch",Frecuencia.rfPowerSwitchT);
		dualmap(Frecuencia.uplinkATT,"uplinkATT",Frecuencia.uplinkATTT);
		dualmap(Frecuencia.downlinkATT,"downlinkATT",Frecuencia.downlinkATTT);
		dualmap(Frecuencia.ch1frequency,"ch1frequency",Frecuencia.ch1frequencyT);
		dualmap(Frecuencia.ch2frequency,"ch2frequency",Frecuencia.ch2frequencyT);
		dualmap(Frecuencia.ch3frequency,"ch3frequency",Frecuencia.ch3frequencyT);
		dualmap(Frecuencia.ch4frequency,"ch4frequency",Frecuencia.ch4frequencyT);
		dualmap(Frecuencia.ch5frequency,"ch5frequency",Frecuencia.ch5frequencyT);
		dualmap(Frecuencia.ch6frequency,"ch6frequency",Frecuencia.ch6frequencyT);
		dualmap(Frecuencia.ch7frequency,"ch7frequency",Frecuencia.ch7frequencyT);
		dualmap(Frecuencia.ch8frequency,"ch8frequency",Frecuencia.ch8frequencyT);
		dualmap(Frecuencia.ch9frequency,"ch9frequency",Frecuencia.ch9frequencyT);
		dualmap(Frecuencia.ch10frequency,"ch10frequency",Frecuencia.ch10frequencyT);
		dualmap(Frecuencia.ch11frequency,"ch11frequency",Frecuencia.ch11frequencyT);
		dualmap(Frecuencia.ch12frequency,"ch12frequency",Frecuencia.ch12frequencyT);
		dualmap(Frecuencia.ch13frequency,"ch13frequency",Frecuencia.ch13frequencyT);
		dualmap(Frecuencia.ch14frequency,"ch14frequency",Frecuencia.ch14frequencyT);
		dualmap(Frecuencia.ch15frequency,"ch15frequency",Frecuencia.ch15frequencyT);
		dualmap(Frecuencia.ch16frequency,"ch16frequency",Frecuencia.ch16frequencyT);
		dualmap(Frecuencia.choiceOfWorkingMode,"choiceOfWorkingMode",Frecuencia.choiceOfWorkingModeT);
		
		dualmap(InfoDispositivo.deviceType,"deviceType",InfoDispositivo.deviceTypeT);
		dualmap(InfoDispositivo.deviceModel,"deviceModel",InfoDispositivo.deviceModelT);
		dualmap(InfoDispositivo.deviceChannelNumber,"deviceChannelNumber",InfoDispositivo.deviceChannelNumberT);
		dualmap(InfoDispositivo.controlUnitSoftwareVersion,"controlUnitSoftwareVersion",InfoDispositivo.controlUnitSoftwareVersionT);
		dualmap(InfoDispositivo.deviceSerialNumber,"deviceSerialNumber",InfoDispositivo.deviceSerialNumberT);
		dualmap(InfoDispositivo.slaveNumber,"slaveNumber",InfoDispositivo.slaveNumberT); 
		dualmap(InfoDispositivo.ruId,"ruId",InfoDispositivo.ruIdT); 
		
		dualmap(ParametrosRed.siteNumber,"siteNumber",ParametrosRed.siteNumberT);
		dualmap(ParametrosRed.siteSubDeviceNumber,"siteSubDeviceNumber",ParametrosRed.siteSubDeviceNumberT);
		dualmap(ParametrosRed.repeaterIpPort,"repeaterIpPort",ParametrosRed.repeaterIpPortT);
		dualmap(ParametrosRed.repeaterIpAddress,"repeaterIpAddress",ParametrosRed.repeaterIpAddressT);
		dualmap(ParametrosRed.repeaterSubnetMask,"repeaterSubnetMask",ParametrosRed.repeaterSubnetMaskT);
		dualmap(ParametrosRed.repeaterDefaultGateway,"repeaterDefaultGateway",ParametrosRed.repeaterDefaultGatewayT);
		dualmap(ParametrosRed.monitorCenterIPAddress,"monitorCenterIPAddress",ParametrosRed.monitorCenterIPAddressT);
		dualmap(ParametrosRed.monitorCenterIPPort,"monitorCenterIPPort",ParametrosRed.monitorCenterIPPortT);
		dualmap(ParametrosRed.psProtocol,"psProtocol",ParametrosRed.psProtocolT);
		dualmap(ParametrosRed.communicationStyle,"communicationStyle",ParametrosRed.communicationStyleT);
		dualmap(ParametrosRed.macAddress,"macAddress",ParametrosRed.macAddressT); 
		
		dualmap(Thresholds.alarmDelay,"alarmDelay",Thresholds.alarmDelayT);
		dualmap(Thresholds.downlinkInputMinThreshold,"downlinkInputMinThreshold",Thresholds.downlinkInputMinThresholdT);
		dualmap(Thresholds.downlinkInputMaxThreshold,"downlinkInputMaxThreshold",Thresholds.downlinkInputMaxThresholdT);  
		
		dualmap(Thresholds.downlinkOutputMinThreshold,"downlinkOutputMinThreshold",Thresholds.downlinkOutputMinThresholdT);  
		dualmap(Thresholds.downlinkOutputMaxThreshold,"downlinkOutputMaxThreshold",Thresholds.downlinkOutputMaxThresholdT);  
		dualmap(Thresholds.paTempThreshold,"paTempThreshold",Thresholds.paTempThresholdT);  
		dualmap(Thresholds.downlinkVSWRThreshold,"downlinkVSWRThreshold",Thresholds.downlinkVSWRThresholdT);  
		dualmap(Thresholds.upstreamNoiseSwitch,"upstreamNoiseSwitch",Thresholds.upstreamNoiseSwitchT);  
		dualmap(Thresholds.highThresholdUpstreamNoise,"highThresholdUpstreamNoise",Thresholds.highThresholdUpstreamNoiseT);  
		dualmap(Thresholds.lowThresholdUpstreamNoise,"lowThresholdUpstreamNoise",Thresholds.lowThresholdUpstreamNoiseT);  
		dualmap(Thresholds.uplinkNoiseCorrectionValue,"uplinkNoiseCorrectionValue",Thresholds.uplinkNoiseCorrectionValueT);  
		dualmap(Thresholds.uplinkNoiseDetectionPar1,"uplinkNoiseDetectionPar1",Thresholds.uplinkNoiseDetectionPar1T);  
		dualmap(Thresholds.uplinkNoiseDetectionPar2,"uplinkNoiseDetectionPar2",Thresholds.uplinkNoiseDetectionPar2T);  
		
		dualmap(Alarmas.suplyPowerFailAlarm[0],"suplyPowerFailAlarmSet",Alarmas.suplyPowerFailAlarmT[0]); 
		dualmap(Alarmas.masterSlaveLinkAlarm[0],"masterSlaveLinkAlarmSet",Alarmas.masterSlaveLinkAlarmT[0]); 
		dualmap(Alarmas.downlinkOverInputAlarm[0],"downlinkOverInputAlarmSet",Alarmas.downlinkOverInputAlarmT[0]); 
		dualmap(Alarmas.downlinkLowInputAlarm[0],"downlinkLowInputAlarmSet",Alarmas.downlinkLowInputAlarmT[0]); 
		dualmap(Alarmas.downlinkSWRAlarm[0],"downlinkSWRAlarmSet",Alarmas.downlinkSWRAlarmT[0]); 
		dualmap(Alarmas.paTemphingAlarm[0],"paTemphingAlarmSet",Alarmas.paTemphingAlarmT[0]); 
		dualmap(Alarmas.downlinkOverOutputAlarm[0],"downlinkOverOutputAlarmSet",Alarmas.downlinkOverOutputAlarmT[0]); 
		dualmap(Alarmas.downlinkLowOutputAlarm[0],"downlinkLowOutputAlarmSet",Alarmas.downlinkLowOutputAlarmT[0]); 
		dualmap(Alarmas.opticalModuleTxRxAlarm[0],"opticalModuleTxRxAlarmSet",Alarmas.opticalModuleTxRxAlarmT[0]); 
				
		dualmap(Alarmas.suplyPowerFailAlarm[1],"suplyPowerFailAlarm",Alarmas.suplyPowerFailAlarmT[1]); 
		dualmap(Alarmas.masterSlaveLinkAlarm[1],"masterSlaveLinkAlarm",Alarmas.masterSlaveLinkAlarmT[1]); 
		dualmap(Alarmas.downlinkOverInputAlarm[1],"downlinkOverInputAlarm",Alarmas.downlinkOverInputAlarmT[1]); 
		dualmap(Alarmas.downlinkLowInputAlarm[1],"downlinkLowInputAlarm",Alarmas.downlinkLowInputAlarmT[1]); 
		dualmap(Alarmas.downlinkSWRAlarm[1],"downlinkSWRAlarm",Alarmas.downlinkSWRAlarmT[1]); 
		dualmap(Alarmas.paTemphingAlarm[1],"paTemphingAlarm",Alarmas.paTemphingAlarmT[1]); 
		dualmap(Alarmas.downlinkOverOutputAlarm[1],"downlinkOverOutputAlarm",Alarmas.downlinkOverOutputAlarmT[1]); 
		dualmap(Alarmas.downlinkLowOutputAlarm[1],"downlinkLowOutputAlarm",Alarmas.downlinkLowOutputAlarmT[1]); 
		dualmap(Alarmas.opticalModuleTxRxAlarm[1],"opticalModuleTxRxAlarm",Alarmas.opticalModuleTxRxAlarmT[1]);  
		
		dualmap(RealTimePars.downlinkInputPower,"downlinkInputPower",RealTimePars.downlinkInputPowerT); 
		dualmap(RealTimePars.uplinkOutputPower,"uplinkOutputPower",RealTimePars.uplinkOutputPowerT); 
		
		dualmap(RealTimePars.downlinkVSWR,"downlinkVSWR",RealTimePars.downlinkVSWRT); 
		dualmap(RealTimePars.downlinkOutputPower,"downlinkOutputPower",RealTimePars.downlinkOutputPowerT); 
		dualmap(RealTimePars.uplinkInputPower,"uplinkInputPower",RealTimePars.uplinkInputPowerT); 
		dualmap(RealTimePars.paTemp,"paTemp",RealTimePars.paTempT); 
		
		dualmap(Discovery.remoteListBytes,"remoteListBytes",Discovery.remoteListBytesT); 
	}
	
	private static void dualmap(int keyObject, String valObject, String valType) {
		valueMap.put(keyObject,valObject);
		valueMap2.put(keyObject,valType);
		valueMapInverse.put(valObject,keyObject);
	}
	
	public static String map(int thisValue) {
		init();
		return valueMap.get(thisValue);
	}
	
	public static String mapType(int thisValue) {
		init();
		return valueMap2.get(thisValue);
	}
	
	public static String mapTypeFromString(String thisValue) {
		init();
		int val = valueMapInverse.get(thisValue);
		return valueMap2.get(val);
	}
	
	public static int mapKey(String thisValue) {
		init(); 
			//System.out.println(" key "+thisValue);
			int val = valueMapInverse.get(thisValue);
			return val;  
	}

	public static class Frecuencia { 
		public static final int channelSwitch 	= 0x0a16;
		public static final int rfPowerSwitch 	= 0x0401;
		public static final int uplinkATT 	  	= 0x0440;
		public static final int downlinkATT 		= 0x0440;
		public static final int ch1frequency 	= 0x0410;
		public static final int ch2frequency 	= 0x0411;
		public static final int ch3frequency 	= 0x0412;
		public static final int ch4frequency 	= 0x0413;
		public static final int ch5frequency 	= 0x0414;
		public static final int ch6frequency 	= 0x0415;
		public static final int ch7frequency 	= 0x0416;
		public static final int ch8frequency 	= 0x0417;
		public static final int ch9frequency 	= 0x0418;
		public static final int ch10frequency 	= 0x0419;
		public static final int ch11frequency 	= 0x041a;
		public static final int ch12frequency 	= 0x041b;
		public static final int ch13frequency 	= 0x041c;
		public static final int ch14frequency 	= 0x041d;
		public static final int ch15frequency 	= 0x041e;
		public static final int ch16frequency 	= 0x041f;
		public static final int choiceOfWorkingMode = 0x0bef ; 
		
		public static final String channelSwitchT 	= uint2;
		public static final String rfPowerSwitchT 	= bit;
		public static final String uplinkATTT 	  	= uint1;
		public static final String downlinkATTT 		= uint1;
		public static final String ch1frequencyT 	= uint2;
		public static final String ch2frequencyT 	= uint2;
		public static final String ch3frequencyT 	= uint2;
		public static final String ch4frequencyT 	= uint2;
		public static final String ch5frequencyT 	= uint2;
		public static final String ch6frequencyT 	= uint2;
		public static final String ch7frequencyT 	= uint2;
		public static final String ch8frequencyT 	= uint2;
		public static final String ch9frequencyT 	= uint2;
		public static final String ch10frequencyT 	= uint2;
		public static final String ch11frequencyT 	= uint2;
		public static final String ch12frequencyT 	= uint2;
		public static final String ch13frequencyT 	= uint2;
		public static final String ch14frequencyT 	= uint2;
		public static final String ch15frequencyT 	= uint2;
		public static final String ch16frequencyT 	= uint2;
		public static final String choiceOfWorkingModeT = uint1 ; 
	}
	
	public static class InfoDispositivo {  
		public static final int deviceType = 0x0003;
		public static final int deviceModel = 0x0004 ;
		public static final int deviceChannelNumber = 0x0006 ;
		public static final int controlUnitSoftwareVersion = 0x000a ;
		public static final int deviceSerialNumber = 0x0005 ;
		public static final int slaveNumber = 0x0ab6 ;
		public static final int ruId = 0x0B21;
		
		public static final String deviceTypeT = uint1;
		public static final String deviceModelT = str20 ;
		public static final String deviceChannelNumberT = uint1 ;
		public static final String controlUnitSoftwareVersionT = str20 ;
		public static final String deviceSerialNumberT = str20 ;
		public static final String slaveNumberT = uint1 ;
		public static final String ruIdT = uint1;
	}
	
	public static class ParametrosRed { 
		public static final int siteNumber = 0x0101 ;
		public static final int siteSubDeviceNumber = 0x0102 ;
		public static final int repeaterIpPort = 0x0139 ;
		public static final int repeaterIpAddress = 0x0151 ;
		public static final int repeaterSubnetMask = 0x0152 ;
		public static final int repeaterDefaultGateway = 0x0153 ;
		public static final int monitorCenterIPAddress = 0x0130 ;
		public static final int monitorCenterIPPort = 0x0131 ; 
		public static final int psProtocol = 0x0138 ;  
		public static final int communicationStyle = 0x0142 ;
		public static final int macAddress = 0x0b4c ; 
		
		public static final String siteNumberT = uint4 ;
		public static final String siteSubDeviceNumberT = uint1 ;
		public static final String repeaterIpPortT = uint2 ;
		public static final String repeaterIpAddressT = ipaddress ;
		public static final String repeaterSubnetMaskT = ipaddress ;
		public static final String repeaterDefaultGatewayT = ipaddress ;
		public static final String monitorCenterIPAddressT = ipaddress ;
		public static final String monitorCenterIPPortT = uint2 ; 
		public static final String psProtocolT = uint1 ;  
		public static final String communicationStyleT = uint1 ;
		public static final String macAddressT = macaddress ; 
	}
	
	
	public static class Thresholds { 
		public static final int alarmDelay = 0x0a27 ;
		public static final int downlinkInputMinThreshold = 0x0453 ; 
		public static final int downlinkInputMaxThreshold = 0x0454 ; 
		
		public static final int downlinkOutputMinThreshold = 0x0455; 
		public static final int downlinkOutputMaxThreshold = 0x0456; 
		public static final int paTempThreshold = 			 0x0451; 
		public static final int downlinkVSWRThreshold = 		 0x0450; 
		public static final int upstreamNoiseSwitch = 		 0x0BE0; 
		public static final int highThresholdUpstreamNoise = 0x0BE1; 
		public static final int lowThresholdUpstreamNoise =  0x0BE2; 
		public static final int uplinkNoiseCorrectionValue = 0x0BE3; 
		public static final int uplinkNoiseDetectionPar1 = 	 0x0BE4; 
		public static final int uplinkNoiseDetectionPar2 = 	 0x0BE5; 
		
		public static final String alarmDelayT = uint1 ;
		public static final String downlinkInputMinThresholdT = sint1 ; 
		public static final String downlinkInputMaxThresholdT = sint1 ; 
		
		public static final String downlinkOutputMinThresholdT = sint1; 
		public static final String downlinkOutputMaxThresholdT = sint1; 
		public static final String paTempThresholdT = 			 sint1; 
		public static final String downlinkVSWRThresholdT = 		 uint1; 
		public static final String upstreamNoiseSwitchT = 		 bit; 
		public static final String highThresholdUpstreamNoiseT = sint1; 
		public static final String lowThresholdUpstreamNoiseT =  sint1; 
		public static final String uplinkNoiseCorrectionValueT = sint1; 
		public static final String uplinkNoiseDetectionPar1T = 	 uint1; 
		public static final String uplinkNoiseDetectionPar2T = 	 uint1; 
	}
	
	public static class Alarmas { 
		public static final int[] suplyPowerFailAlarm = {0x0201,0x0301} ;
		public static final int[] masterSlaveLinkAlarm = {0x020f,0x030f} ;
		//master only
		public static final int[] downlinkOverInputAlarm = {0x0210,0x0310} ;
		public static final int[] downlinkLowInputAlarm = {0x0211,0x0311} ;
		//remote only 
		public static final int[] downlinkSWRAlarm = {0x0214,0x0314} ;
		public static final int[] paTemphingAlarm = {0x0206,0x0306} ;
		public static final int[] downlinkOverOutputAlarm = {0x0212,0x0312} ;
		public static final int[] downlinkLowOutputAlarm = {0x0213,0x0313} ;
		
		//remote, no seteable 
		public static final int[] opticalModuleTxRxAlarm = {0x020e,0x030e} ;
		
		
		
		public static final String[] suplyPowerFailAlarmT = {alarm,alarm} ;
		public static final String[] masterSlaveLinkAlarmT = {alarm,alarm} ;
		//master only
		public static final String[] downlinkOverInputAlarmT = {alarm,alarm} ;
		public static final String[] downlinkLowInputAlarmT = {alarm,alarm} ;
		//remote only 
		public static final String[] downlinkSWRAlarmT = {alarm,alarm} ;
		public static final String[] paTemphingAlarmT = {alarm,alarm} ;
		public static final String[] downlinkOverOutputAlarmT = {alarm,alarm} ;
		public static final String[] downlinkLowOutputAlarmT = {alarm,alarm} ;
		//remote, no seteable 
		public static final String[] opticalModuleTxRxAlarmT = {bit,bit} ;
	}
	
	public static class RealTimePars { 
		public static final int downlinkInputPower = 0x0502 ; 
		public static final int uplinkOutputPower = 0x050d ; 
		
		public static final int downlinkVSWR = 0x0506; 
		public static final int downlinkOutputPower = 0x0503; 
		public static final int uplinkInputPower = 0x0525;  
		public static final int paTemp = 0x0501;  
		
		
		public static final String downlinkInputPowerT = sint1 ; 
		public static final String uplinkOutputPowerT = sint1 ; 
		
		public static final String downlinkVSWRT = uint1; 
		public static final String downlinkOutputPowerT = sint1; 
		public static final String uplinkInputPowerT = sint1;  
		public static final String paTempT = sint1;  
	}
	
	public static class Discovery { 
		public static final int remoteListBytes = 0x0BA0 ;  		
		
		public static final String remoteListBytesT = remotelist ;  
	}
	
	public static class MainObjectSubList{
		public static final String[] allstats1 = {
				"remoteListBytes" 				
				};
	}
	
	public static class MainObjectVarList{
		public static final String[] allstats1 = {
				"downlinkLowInputAlarmSet",
				"downlinkOverInputAlarmSet",
				"masterSlaveLinkAlarmSet",
				"suplyPowerFailAlarmSet",
				
				"ch1frequency",
				"ch2frequency",
				"ch3frequency",
				"ch4frequency",
				"ch5frequency",
				"ch6frequency",
				"ch7frequency",
				"ch8frequency",
				"ch9frequency",
				"ch10frequency",
				"ch11frequency",
				"ch12frequency",
				"ch13frequency",
				"ch14frequency",
				"ch15frequency",
				"ch16frequency",
				"channelSwitch",
				"choiceOfWorkingMode",
				"downlinkATT",
				"rfPowerSwitch",
				"uplinkATT",
				
				"controlUnitSoftwareVersion",
				"deviceChannelNumber",
				"deviceModel",
				"deviceSerialNumber",
				"deviceType",
				"slaveNumber",

				"communicationStyle",
				"macAddress",
				"monitorCenterIPAddress" 					
				};
		public static final 	String[] allstats2 = { 
			"monitorCenterIPPort",
			"psProtocol",
			"repeaterDefaultGateway",
			"repeaterIpAddress",
			"repeaterIpPort",
			"repeaterSubnetMask",
			"siteSubDeviceNumber",
			"siteNumber",
			 "downlinkInputPower",
			"uplinkOutputPower",
			"macAddress",
			"monitorCenterIPAddress", 
			"alarmDelay",
			"downlinkInputMaxThreshold",
			"downlinkInputMinThreshold"	,
			"suplyPowerFailAlarm",
			"downlinkOverInputAlarm",
			"masterSlaveLinkAlarm",
			"downlinkLowInputAlarm"
			};
		
		public static final String[] allRTP= {
				"downlinkInputPower",
				"uplinkOutputPower"
				};
		public static final String[] allAlarms= {
				"suplyPowerFailAlarm",
				"downlinkOverInputAlarm",
				"masterSlaveLinkAlarm",
				"downlinkLowInputAlarm"
				};
	}
	
	
	public static class SubObjectVarList{
		public static final String[] allstats1 = {
				"downlinkLowOutputAlarmSet",
				"downlinkOverOutputAlarmSet",
				"masterSlaveLinkAlarmSet",
				"suplyPowerFailAlarmSet",
				"paTemphingAlarm",
				"opticalModuleTxRxAlarm",
				
				"ch1frequency",
				"ch2frequency",
				"ch3frequency",
				"ch4frequency",
				"ch5frequency",
				"ch6frequency",
				"ch7frequency",
				"ch8frequency",
				"ch9frequency",
				"ch10frequency",
				"ch11frequency",
				"ch12frequency",
				"ch13frequency",
				"ch14frequency",
				"ch15frequency",
				"ch16frequency",
				"channelSwitch",
				"choiceOfWorkingMode",
				"downlinkATT",
				"rfPowerSwitch",
				"uplinkATT",
				
				"controlUnitSoftwareVersion",
				"deviceChannelNumber",
				"deviceModel",
				"deviceSerialNumber",
				"deviceType",
				"ruId",	
				"macAddress"	,
				"suplyPowerFailAlarm",
				"paTemphingAlarm",
				"opticalModuleTxRxAlarm"				
				};
		public 	static final String[] allstats2 = { 
				"communicationStyle",
				"monitorCenterIPAddress" ,
				"monitorCenterIPPort",
				"psProtocol",
				"repeaterDefaultGateway",
				"repeaterIpAddress",
				"repeaterIpPort",
				"repeaterSubnetMask",
				"siteSubDeviceNumber",
				"siteNumber",
				
				"downlinkVSWR",
				"downlinkOutputPower",   
				"uplinkInputPower",
				"paTemp",
				
				"alarmDelay",
				"downlinkOutputMinThreshold",
				"downlinkOutputMaxThreshold",
				"paTempThreshold",
				"downlinkVSWRThreshold",
				"upstreamNoiseSwitch",
				"highThresholdUpstreamNoise",
				"lowThresholdUpstreamNoise",
				"uplinkNoiseCorrectionValue",
				"uplinkNoiseDetectionPar1",
				"uplinkNoiseDetectionPar2",
				"downlinkLowOutputAlarm"
				,"downlinkOverOutputAlarm",
				"masterSlaveLinkAlarm"
			};
		
		public static final String[] allAlarms= {
				"downlinkLowOutputAlarm"
				,"downlinkOverOutputAlarm",
				"masterSlaveLinkAlarm",
				"suplyPowerFailAlarm",
				"paTemphingAlarm",
				"opticalModuleTxRxAlarm"
		};
		
		public static final String[] allRTP= {
				"downlinkVSWR" ,
				"downlinkOutputPower",
				"uplinkInputPower",
				"paTemp"
		};
		
		
	}
}
