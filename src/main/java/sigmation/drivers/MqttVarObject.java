package sigmation.drivers;

import java.util.ArrayList;
import java.util.List;

public class MqttVarObject {
		public String basetopic;
		public String varname;
		public String __name ;
		public String __class;
		public String __limited;
		public String __options;
		public String __current="0";
		public String __set;
		public String __value;
		public String __unit=" ";
		public String __active;
		
		public List<String[]> getMqttVarObjectPars() {
			List<String[]> thisList = new ArrayList<String[]>();
			if(__name!=null) thisList.add(getParams("__name",__name));
			if(__class!=null) thisList.add(getParams("__class",__class));
			if(__limited!=null) thisList.add(getParams("__limited",__limited));
			if(__options!=null) thisList.add(getParams("__options",__options)); 
			if(__current!=null) thisList.add(getParams("__current",__current));
			if(__set!=null) thisList.add(getParams("__set",__set));
			if(__value!=null) thisList.add(getParams("__value",__value));
			if(__unit!=null) thisList.add(getParams("__unit",__unit));
			if(__active!=null) thisList.add(getParams("__active",__active));
			return thisList;
		}
		
		public String[] getParams(String paramName, String paramValue) {
			String[] thisObject = new String[2];
			thisObject[0]= basetopic+paramName;
			thisObject[1]= paramValue;
			return thisObject;
		}
		
		public MqttVarObject(String basetopic,String varname, String __class) {
			this.basetopic=basetopic;
			this.varname=varname;			
		} 
		
		public MqttVarObject(String basetopic,String varname,String __name,String __class,String __current, String __unit,String __active) {
			this.basetopic=basetopic;
			this.varname=varname;	
			this.__name=__name;
			this.__class=__class;
			this.__limited="false";
			this.__current=__current; 
			this.__unit=__unit;
			this.__active=__active;
		}
		
		public MqttVarObject(String basetopic,String varname,String __name,String __class,String __current,boolean __set, String __unit,String __active) {
			this.basetopic=basetopic;
			this.varname=varname;	
			this.__name=__name;
			this.__class=__class;
			this.__limited="false";
			this.__current=__current;
			if (__set) this.__set="__first_set";
			this.__unit=__unit;
			this.__active=__active;
		}
		
		public MqttVarObject(String basetopic,String varname,String __name,String __class,String __current,boolean __set,String __value,String __unit,String __active) {
			this.basetopic=basetopic;
			this.varname=varname;	
			this.__name=__name;
			this.__class=__class;
			this.__limited="false";
			this.__current=__current;
			if (__set) this.__set="__first_set";
			this.__value=__value;
			this.__unit=__unit;
			this.__active=__active;
		}
		
		public MqttVarObject(String basetopic,String varname,String __name,String __class,String __options,String __current,String __set,String __value,String __unit,String __active) {
			this.basetopic=basetopic;
			this.varname=varname;	
			this.__name=__name;
			this.__class=__class;
			this.__limited="true";
			this.__options= __options;
			this.__current=__current;
			this.__set=__set;
			this.__value=__value;
			this.__unit=__unit;
			this.__active=__active;
		}
		
}
