package sigmation.drivers;

public class StatObject{
	public int paramCode;
	public int statusCode;
	public boolean select = false;
	public String name = "nonset";
	public boolean grouped=false;
	public String paramClass = "";
	public String paramDataType = "text";
	public String paramUnit=" ";
	public String paramOptions = null;
	public boolean readonly=false;
	
	public StatObject(
			int paramCode,
			boolean select,
			String name,
			boolean grouped,
			String paramClass,
			String paramDataType,
			String paramUnit
			) {
		
		this.paramCode=paramCode;
		this.select=select;
		this.name=name;
		this.grouped=grouped;
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
	}
	
	public StatObject(
			int paramCode, 
			String name,
			boolean grouped,
			String paramClass,
			String paramDataType,
			String paramUnit
			) {
		
		this.paramCode=paramCode; 
		this.name=name;
		this.grouped=grouped;
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
	}
	
	public StatObject(
			int paramCode, 
			String name, 
			String paramClass,
			String paramDataType,
			String paramUnit,
			boolean select,
			String paramOptions
			) {
		
		this.paramCode=paramCode; 
		this.name=name; 
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
		this.select=select;
		this.paramOptions=paramOptions;
	}
	
	public StatObject(
			int paramCode, 
			String name, 
			String paramClass,
			String paramDataType,
			String paramUnit,
			boolean select,
			String paramOptions,
			boolean readonly
			) {
		
		this.paramCode=paramCode; 
		this.name=name; 
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
		this.select=select;
		this.paramOptions=paramOptions;
		this.readonly=readonly;
	}
	
	public StatObject(
			int paramCode, 
			String name, 
			String paramClass,
			String paramDataType,
			String paramUnit,
			boolean readonly
			) {
		
		this.paramCode=paramCode; 
		this.name=name;
		this.grouped=true;
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
		this.readonly=readonly;
	}
	
	public StatObject(
			int paramCode, 
			String name, 
			String paramClass,
			String paramDataType,
			String paramUnit 
			) {
		
		this.paramCode=paramCode; 
		this.name=name;
		this.grouped=true;
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
		this.readonly=false;
	}
	
	public StatObject(
			int[] paramAndStatusCode,
			String name, 
			String paramClass,
			String paramDataType,
			String paramUnit
			) {
		
		this.paramCode=paramAndStatusCode[0]; 
		this.statusCode=paramAndStatusCode[1];
		this.name=name;
		this.grouped=true;
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
	}	
	
	public StatObject(
			int[] paramAndStatusCode,
			String name, 
			String paramClass,
			String paramDataType,
			String paramUnit,
			boolean readonly
			) {
		
		this.paramCode=paramAndStatusCode[0]; 
		this.statusCode=paramAndStatusCode[1];
		this.name=name;
		this.grouped=true;
		this.paramClass=paramClass;
		this.paramDataType=paramDataType;
		this.paramUnit=paramUnit;
		this.readonly=false;
	}
	
}
