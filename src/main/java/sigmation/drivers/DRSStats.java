package sigmation.drivers;

import sigmation.utils.SysUtils;

public class DRSStats {
	/**
	 * Tipos de variables
	 * 
	 * */
	private static final String uint1 = "uint1";  //unsigned int, 1 byte
	private static final String uint2 = "uint2";  //unsigned int, 2 bytes
	private static final String uint4 = "uint4";  //unsigned int, 2 bytes
	private static final String sint1 = "sint1";  //signed int, 1 byte
	private static final String sint2 = "sint2";  //signed int, 2 bytes
	private static final String bit = "bit";      //boolean
	private static final String str20 = "str20";  //string 20 bytes
	private static final String ipaddress = "ipaddress";  //string 20 bytes
	private static final String macaddress = "macaddress";  //string 20 bytes
	
	/**
	 * 
	 * Unidades
	 * 
	 * */	
	private static final String hex = "Hex";  //string 20 bytes
	private static final String db = "[dB]";  //string 20 bytes
	private static final String dbm = "[dBm]";  //string 20 bytes
	private static final String mhz = "[Mhz]";  //string 20 bytes
	private static final String sec = "[s]";  //string 20 bytes
	private static final String celcius = "[°C]";  //string 20 bytes
	
	public static class Frecuencia {
		DRSCommandList.Frecuencia d = new DRSCommandList.Frecuencia();
		public static final String cln = "Frequency"; 
		public static final String clng = "Frequency"; 
		private static final String options = generateChannels();
		public static final StatObject channelSwitch = new StatObject(DRSCommandList.Frecuencia.channelSwitch, "Channel Switch", cln, DRSCommandList.Frecuencia.channelSwitchT, hex,true,generateChannelChoice());
		public static final StatObject rfPowerSwitch = new StatObject(DRSCommandList.Frecuencia.rfPowerSwitch, "RF Switch", cln, DRSCommandList.Frecuencia.rfPowerSwitchT, null);
		public static final StatObject uplinkATT = new StatObject(DRSCommandList.Frecuencia.uplinkATT,"Uplink ATT", cln, DRSCommandList.Frecuencia.uplinkATTT, db);
		public static final StatObject downlinkATT = new StatObject(DRSCommandList.Frecuencia.downlinkATT,"Downlink ATT", cln, DRSCommandList.Frecuencia.downlinkATTT, db);
		public static final StatObject ch1frequency = new StatObject(DRSCommandList.Frecuencia.ch1frequency,"Channel Freq. 1", cln, DRSCommandList.Frecuencia.ch1frequencyT, mhz,true,options);
		public static final StatObject ch2frequency = new StatObject(DRSCommandList.Frecuencia.ch2frequency,"Channel Freq. 2", cln, DRSCommandList.Frecuencia.ch2frequencyT, mhz,true,options);
		public static final StatObject ch3frequency = new StatObject(DRSCommandList.Frecuencia.ch3frequency,"Channel Freq. 3", cln, DRSCommandList.Frecuencia.ch3frequencyT, mhz,true,options);
		public static final StatObject ch4frequency = new StatObject(DRSCommandList.Frecuencia.ch4frequency,"Channel Freq. 4", cln, DRSCommandList.Frecuencia.ch4frequencyT, mhz,true,options);
		public static final StatObject ch5frequency = new StatObject(DRSCommandList.Frecuencia.ch5frequency,"Channel Freq. 5", cln, DRSCommandList.Frecuencia.ch5frequencyT, mhz,true,options);
		public static final StatObject ch6frequency = new StatObject(DRSCommandList.Frecuencia.ch6frequency,"Channel Freq. 6", cln, DRSCommandList.Frecuencia.ch6frequencyT, mhz,true,options);
		public static final StatObject ch7frequency = new StatObject(DRSCommandList.Frecuencia.ch7frequency,"Channel Freq. 7", cln, DRSCommandList.Frecuencia.ch7frequencyT, mhz,true,options);
		public static final StatObject ch8frequency = new StatObject(DRSCommandList.Frecuencia.ch8frequency,"Channel Freq. 8", cln, DRSCommandList.Frecuencia.ch8frequencyT, mhz,true,options);
		public static final StatObject ch9frequency = new StatObject(DRSCommandList.Frecuencia.ch9frequency,"Channel Freq. 9", cln, DRSCommandList.Frecuencia.ch9frequencyT, mhz,true,options);
		public static final StatObject ch10frequency = new StatObject(DRSCommandList.Frecuencia.ch10frequency,"Channel Freq. 10", cln, DRSCommandList.Frecuencia.ch10frequencyT, mhz,true,options);
		public static final StatObject ch11frequency = new StatObject(DRSCommandList.Frecuencia.ch11frequency,"Channel Freq. 11", cln, DRSCommandList.Frecuencia.ch11frequencyT, mhz,true,options);
		public static final StatObject ch12frequency = new StatObject(DRSCommandList.Frecuencia.ch12frequency,"Channel Freq. 12", cln, DRSCommandList.Frecuencia.ch12frequencyT, mhz,true,options);
		public static final StatObject ch13frequency = new StatObject(DRSCommandList.Frecuencia.ch13frequency,"Channel Freq. 13", cln, DRSCommandList.Frecuencia.ch13frequencyT, mhz,true,options);
		public static final StatObject ch14frequency = new StatObject(DRSCommandList.Frecuencia.ch14frequency,"Channel Freq. 14", cln, DRSCommandList.Frecuencia.ch14frequencyT, mhz,true,options);
		public static final StatObject ch15frequency = new StatObject(DRSCommandList.Frecuencia.ch15frequency,"Channel Freq. 15", cln, DRSCommandList.Frecuencia.ch15frequencyT, mhz,true,options);
		public static final StatObject ch16frequency = new StatObject(DRSCommandList.Frecuencia.ch16frequency,"Channel Freq. 16", cln, DRSCommandList.Frecuencia.ch16frequencyT, mhz,true,options);
		public static final StatObject choiceOfWorkingMode = new StatObject(DRSCommandList.Frecuencia.choiceOfWorkingMode,"Working Mode", cln, DRSCommandList.Frecuencia.choiceOfWorkingModeT, null,true,choiceOfWorkingMode()); 
	}
	
	public static class InfoDispositivo {
		public static final String cln = "Device Info"; 
		public static final String clng = "DeviceInfo";  
		public static final StatObject deviceType = new StatObject(DRSCommandList.InfoDispositivo.deviceType, "Device Type", cln, DRSCommandList.InfoDispositivo.deviceTypeT, null,true,deviceType());
		public static final StatObject deviceModel = new StatObject(DRSCommandList.InfoDispositivo.deviceModel, "Device Model",cln, DRSCommandList.InfoDispositivo.deviceModelT, null,true);
		public static final StatObject deviceChannelNumber = new StatObject(DRSCommandList.InfoDispositivo.deviceChannelNumber, "Device Channel",cln, DRSCommandList.InfoDispositivo.deviceChannelNumberT, null);
		public static final StatObject controlUnitSoftwareVersion = new StatObject(DRSCommandList.InfoDispositivo.controlUnitSoftwareVersion, "Software Ver.",cln, DRSCommandList.InfoDispositivo.controlUnitSoftwareVersionT, null,true);
		public static final StatObject deviceSerialNumber = new StatObject(DRSCommandList.InfoDispositivo.deviceSerialNumber, "Serial N.",cln, DRSCommandList.InfoDispositivo.deviceSerialNumberT, null,true);
		//maestro
		public static final StatObject slaveNumber = new StatObject(DRSCommandList.InfoDispositivo.slaveNumber, "Slaves N.",cln, DRSCommandList.InfoDispositivo.slaveNumberT, null,true);
		//remoto
		public static final StatObject ruId = new StatObject(DRSCommandList.InfoDispositivo.ruId, "Remote Unit ID",cln, DRSCommandList.InfoDispositivo.ruIdT, null,true);
		
	}
	
	public static class ParametrosRed {
		public static final String cln = "Network Params."; 
		public static final String clng = "NetworkParam"; 
		public static final StatObject siteNumber = new StatObject(DRSCommandList.ParametrosRed.siteNumber, "Site Number",cln, DRSCommandList.ParametrosRed.siteNumberT, null,true);
		public static final StatObject siteSubDeviceNumber = new StatObject(DRSCommandList.ParametrosRed.siteSubDeviceNumber, "Sub Device N.",cln, DRSCommandList.ParametrosRed.siteSubDeviceNumberT, null,true);
		public static final StatObject repeaterIpPort = new StatObject(DRSCommandList.ParametrosRed.repeaterIpPort, "Repeater IP Port",cln, DRSCommandList.ParametrosRed.repeaterIpPortT, null,true);
		public static final StatObject repeaterIpAddress = new StatObject(DRSCommandList.ParametrosRed.repeaterIpAddress, "Repeater IP Address",cln, DRSCommandList.ParametrosRed.repeaterIpAddressT, null,true);
		public static final StatObject repeaterSubnetMask = new StatObject(DRSCommandList.ParametrosRed.repeaterSubnetMask, "Repeater Subnet Mask",cln, DRSCommandList.ParametrosRed.repeaterSubnetMaskT, null,true);
		public static final StatObject repeaterDefaultGateway = new StatObject(DRSCommandList.ParametrosRed.repeaterDefaultGateway, "Repeater Gateway IP",cln, DRSCommandList.ParametrosRed.repeaterDefaultGatewayT, null,true);
		public static final StatObject monitorCenterIPAddress = new StatObject(DRSCommandList.ParametrosRed.monitorCenterIPAddress, "Monitoring Center IP",cln, DRSCommandList.ParametrosRed.monitorCenterIPAddressT, null,true);
		public static final StatObject monitorCenterIPPort = new StatObject(DRSCommandList.ParametrosRed.monitorCenterIPPort, "Monitoring Center Port",cln, DRSCommandList.ParametrosRed.monitorCenterIPPortT, null,true); 
		public static final StatObject psProtocol = new StatObject(DRSCommandList.ParametrosRed.psProtocol, "Protocol",cln, DRSCommandList.ParametrosRed.psProtocolT, null,true,psProtocol(),true);  
		public static final StatObject communicationStyle = new StatObject(DRSCommandList.ParametrosRed.communicationStyle, "Interface Com.",cln, DRSCommandList.ParametrosRed.communicationStyleT, null,true,communicationStyle(),true);
		public static final StatObject macAddress = new StatObject(DRSCommandList.ParametrosRed.macAddress, "Mac Address",cln, DRSCommandList.ParametrosRed.macAddressT, null,true); 
	}
	
	
	public static class Thresholds {
		public static final String cln = "Thresholds"; 
		public static final String clng = "Thresholds";  
		public static final StatObject alarmDelay = new StatObject(0x0a27, "Alarm Delay",cln, uint1, sec,true,alarmDelay());
		//maestro
		public static final StatObject downlinkInputMinThreshold = new StatObject(DRSCommandList.Thresholds.downlinkInputMinThreshold, "Downlink Input Min.",cln, DRSCommandList.Thresholds.downlinkInputMinThresholdT, dbm); 
		public static final StatObject downlinkInputMaxThreshold = new StatObject(DRSCommandList.Thresholds.downlinkInputMaxThreshold, "Downlink Input Max.",cln, DRSCommandList.Thresholds.downlinkInputMaxThresholdT, dbm); 
		//remotos
		public static final StatObject downlinkOutputMinThreshold = new StatObject(DRSCommandList.Thresholds.downlinkOutputMinThreshold, "Downlink Output Min.",cln, DRSCommandList.Thresholds.downlinkOutputMinThresholdT, dbm); 
		public static final StatObject downlinkOutputMaxThreshold = new StatObject(DRSCommandList.Thresholds.downlinkOutputMaxThreshold, "Downlink Output Max.",cln, DRSCommandList.Thresholds.downlinkOutputMaxThresholdT, dbm); 
		public static final StatObject paTempThreshold = 			 new StatObject(DRSCommandList.Thresholds.paTempThreshold, "Pa Themping Threshold",cln, DRSCommandList.Thresholds.paTempThresholdT, celcius); 
		public static final StatObject downlinkVSWRThreshold = 		 new StatObject(DRSCommandList.Thresholds.downlinkVSWRThreshold, "VSWR Downlink Threshold",cln, DRSCommandList.Thresholds.downlinkVSWRThresholdT, null); 
		public static final StatObject upstreamNoiseSwitch = 		 new StatObject(DRSCommandList.Thresholds.upstreamNoiseSwitch, "Upstream Noise Switch",cln, DRSCommandList.Thresholds.upstreamNoiseSwitchT, null); 
		public static final StatObject highThresholdUpstreamNoise = new StatObject(DRSCommandList.Thresholds.highThresholdUpstreamNoise, "High Threshold Upstream Noise",cln, DRSCommandList.Thresholds.highThresholdUpstreamNoiseT, dbm); 
		public static final StatObject lowThresholdUpstreamNoise =  new StatObject(DRSCommandList.Thresholds.lowThresholdUpstreamNoise, "Low Threshold Upstream Noise",cln, DRSCommandList.Thresholds.lowThresholdUpstreamNoiseT, dbm); 
		public static final StatObject uplinkNoiseCorrectionValue = new StatObject(DRSCommandList.Thresholds.uplinkNoiseCorrectionValue, "Uplink Noise Correction Value",cln, DRSCommandList.Thresholds.uplinkNoiseCorrectionValueT, dbm); 
		public static final StatObject uplinkNoiseDetectionPar1 = 	 new StatObject(DRSCommandList.Thresholds.uplinkNoiseDetectionPar1, "Uplink Noise Detection Par 1",cln, DRSCommandList.Thresholds.uplinkNoiseDetectionPar1T, dbm); 
		public static final StatObject uplinkNoiseDetectionPar2 = 	 new StatObject(DRSCommandList.Thresholds.uplinkNoiseDetectionPar2, "Uplink Noise Detection Par 2",cln, DRSCommandList.Thresholds.uplinkNoiseDetectionPar2T, dbm); 
	}
	
	public static class Alarmas { 
		public static final String cln = "Alarms";  
		public static final String clng = "Alarms"; 
		public static final StatObject suplyPowerFailAlarm = new StatObject(DRSCommandList.Alarmas.suplyPowerFailAlarm, "Suply Power Fail Alarm",cln, DRSCommandList.Alarmas.suplyPowerFailAlarmT[0], null);
		public static final StatObject masterSlaveLinkAlarm = new StatObject(DRSCommandList.Alarmas.masterSlaveLinkAlarm, "Master Slave Link Alarm",cln, DRSCommandList.Alarmas.masterSlaveLinkAlarmT[0], null);
		public static final StatObject downlinkOverInputAlarm = new StatObject(DRSCommandList.Alarmas.downlinkOverInputAlarm, "Downlink Over Input Alarm",cln, DRSCommandList.Alarmas.downlinkOverInputAlarmT[0], null);
		public static final StatObject downlinkLowInputAlarm = new StatObject(DRSCommandList.Alarmas.downlinkLowInputAlarm, "Downlink Low Input Alarm",cln, DRSCommandList.Alarmas.downlinkLowInputAlarmT[0], null);	
		//remoto
		public static final StatObject downlinkSWRAlarm = new StatObject(DRSCommandList.Alarmas.downlinkSWRAlarm, "SWR Downlink",cln, DRSCommandList.Alarmas.downlinkSWRAlarmT[0], null);
		public static final StatObject paTemphingAlarm = new StatObject(DRSCommandList.Alarmas.paTemphingAlarm, "Pa Temphing Alarm",cln, DRSCommandList.Alarmas.paTemphingAlarmT[0], null);
		public static final StatObject downlinkOverOutputAlarm = new StatObject(DRSCommandList.Alarmas.downlinkOverOutputAlarm, "Downlink Over Output Alarm",cln, DRSCommandList.Alarmas.downlinkOverOutputAlarmT[0], null);
		public static final StatObject downlinkLowOutputAlarm = new StatObject(DRSCommandList.Alarmas.downlinkLowOutputAlarm, "Downlink Low Output Alarm",cln, DRSCommandList.Alarmas.downlinkLowOutputAlarmT[0], null);
		public static final StatObject opticalModuleTxRxAlarm = new StatObject(DRSCommandList.Alarmas.opticalModuleTxRxAlarm, "Optical Module TxRx Alarm",cln, DRSCommandList.Alarmas.opticalModuleTxRxAlarmT[0], null,true);
	}
	
	public static class RealTimePars {
		public static final String cln = "Real Time Params.";   
		public static final String clng = "RealTimeParams"; 
		public static final StatObject downlinkInputPower = new StatObject(DRSCommandList.RealTimePars.downlinkInputPower, "Downlink Input Power",cln, sint1, dbm,true); 
		public static final StatObject uplinkOutputPower = new StatObject(DRSCommandList.RealTimePars.uplinkOutputPower, "Uplink Output Power",cln, sint1, dbm,true);  
		//Remotos
		public static final StatObject downlinkVSWR = new StatObject(DRSCommandList.RealTimePars.downlinkVSWR, "VSWR Downlink",cln, DRSCommandList.RealTimePars.downlinkVSWRT, null,true); 
		public static final StatObject downlinkOutputPower = new StatObject(DRSCommandList.RealTimePars.downlinkOutputPower, "Downlink Output Power",cln, DRSCommandList.RealTimePars.downlinkOutputPowerT, dbm,true); 
		public static final StatObject uplinkInputPower = new StatObject(DRSCommandList.RealTimePars.uplinkInputPower, "Uplink Input Power",cln, DRSCommandList.RealTimePars.uplinkInputPowerT, dbm,true);  
		public static final StatObject paTemp = new StatObject(DRSCommandList.RealTimePars.paTemp, "Pa Temphing",cln, DRSCommandList.RealTimePars.paTempT, celcius,true);  
	}
	
	private static String generateChannels() {
		String bufferString="{ \"type\":\"select\",\"elements\": [";
		for(int i=0; i < 241; i++) {
			double freq1= 417 + (i* 0.0125);
			double freq2= 427 + (i* 0.0125);
			if(i>0) bufferString=bufferString+",";
			bufferString= bufferString + "{\"label\":\"CH"+i+" "+freq1+" "+freq2+"\",\"value\":\""+i+"\"}";
		}
		bufferString= bufferString + "]}";
		System.out.println("bufferstring: "+bufferString);
		return bufferString;
	}
	
	private static String generateChannelChoice() {
		String bufferString="{ \"type\":\"multi\",\"elements\": [";
		for(int i=0; i < 16; i++) { 
			double thisValue= Math.pow(2, i);
			if(i>0) bufferString=bufferString+",";
			bufferString= bufferString + "{\"label\":\"CH "+(i+1)+"\",\"value\":\""+thisValue+"\"}";
		}
		bufferString= bufferString + "]}";
		System.out.println("bufferstring: "+bufferString);
		return bufferString;
	}
	
	private static String choiceOfWorkingMode() {
		String bufferString="{ \"type\":\"select\",\"elements\": [ {\"label\":\"WideBand \",\"value\":\"2\"}"+",{\"label\":\"Channel \",\"value\":\"3\"} ]}";
		System.out.println("bufferstring: "+bufferString);
		return bufferString;
	}
	
	private static String psProtocol() {
		String bufferString="{ \"type\":\"select\",\"elements\": [ {\"label\":\"IP+UDP\",\"value\":\"1\"}"+",{\"label\":\"IP+TCP\",\"value\":\"2\"} ]}";
		System.out.println("bufferstring: "+bufferString);
		return bufferString;
	}
	
	private static String deviceType() {
		String[][] args = {
							{"Band Selective","1"},
							{"Channel Selective","2"},
							{"F.O. Wired Coupling Local Unit","3"},
							{"F.O. Wireless Coupling Local Unit","4"},
							{"F.O. Broadband remote Unit","5"},
							{"F.O. Freq. Select. remote Unit","6"}
						  };
		String type= "select";
		return SysUtils.jsonOptionBuilder(args, type);
	}
	
	private static String communicationStyle() {
		String[][] args = {
							{"Local RS232","0"},
							{"Remote SMS","1"},
							{"GPRS","2"},
							{"Ethernet","3"}, 
						  };
		String type= "select";
		return SysUtils.jsonOptionBuilder(args, type);
	}
	
	private static String alarmDelay() {
		String[][] args = {
							{"Delay 180 s","180"},
							{"Delay 60 s","60"},
							{"Delay 20 s","20"}
						  };
		String type= "select";
		return SysUtils.jsonOptionBuilder(args, type);
	}
	
	
}
