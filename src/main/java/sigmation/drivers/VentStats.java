package sigmation.drivers;
 

public class VentStats {
	/**
	 * Tipos de variables
	 * 
	 * */
	private static final String uint1 = "uint1";  //unsigned int, 1 byte
	private static final String uint2 = "uint2";  //unsigned int, 2 bytes
	private static final String sint1 = "sint1";  //signed int, 1 byte
	private static final String sint2 = "sint2";  //signed int, 2 bytes
	private static final String bit = "bit";      //boolean
	private static final String str20 = "str20";  //string 20 bytes
	
	/**
	 * 
	 * Unidades
	 * 
	 * */	
	private static final String hex = "Hex";  //string 20 bytes
	private static final String db = "[dB]";  //string 20 bytes
	private static final String dbm = "[dBm]";  //string 20 bytes
	private static final String mhz = "[Mhz]";  //string 20 bytes
	private static final String sec = "[s]";  //string 20 bytes 
	
	public static class ParametrosRed {
		public static final String cln = "Param. de Red"; 
		public static final String clng = "ParametrosRed";  
		public static final StatObject deviceModbusAddress = new StatObject(0x0003, "Direccion Modbus", cln, uint1, null);
		public static final StatObject deviceModemAddress = new StatObject(0x0004, "Direccion Modem LeakyFeeder",cln, str20, null);
		public static final StatObject deviceIPAddress = new StatObject(0x0130, "IP (solo conx. Ethernet)",cln, str20, null);
	} 
	
	public static class Controles {
		public static final String cln = "Controles"; 
		public static final String clng = "controles";  
		public static final StatObject relayVentilador = new StatObject(0x0a27, "Activar Ventilador", cln, bit, null); 
	}   
	
}
