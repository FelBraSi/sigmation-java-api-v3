package sigmation.main;


/*
 * NOTA
 * 
 * una vez ingresado el numero serial correspondiente
 * descomentar lineas en Syscheck
 * 
 * 
 */

import sigmation.io.SysCom;
import sigmation.io.SystemIO;
import sigmation.io.WMXModCom;
import sigmation.utils.Definitions;
import sigmation.utils.ReadConfig;
import sigmation.utils.RunShellCommandFromJava;
import sigmation.utils.SysUtils; 
import sigmation.modbus.ModbusApp;
import sigmation.modbus.ModbusAppV2;
import sigmation.remoteServer.remoteTagServer;
import sigmation.utils.PromptColors;;


public class Sigmation {
	
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 		PARAMETERS, DONT CHANGE THIS UNLESS YOU KNOW WHAT YOU ARE DOING		//
	 *      -FBS-																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 */
	
	// Sigmation server systems
	public final static int ApiSystem_tag  		= Definitions.ApiSystem_tag;     	// sistema tag
	public final static int ApiSystem_msg  		= Definitions.ApiSystem_msg;		// sistema mensajes
	public final static int ApiSystem_vent 		= Definitions.ApiSystem_vent;		// ventilacion
	public final static int ApiSystem_drfp 		= Definitions.ApiSystem_drfp;		// diagnostico fuentes
	public final static int ApiSystem_dram 		= Definitions.ApiSystem_dram;		// diagnostico amp.
	public final static int ApiSystem_fsys 		= Definitions.ApiSystem_fsys;		// firewatcher
	public final static int ApiSystem_mb_dolf  	= Definitions.ApiSystem_mb_dolf;	// dolf
	public final static int ApiSystem_drs		= Definitions.ApiSystem_drs;		// DRS
	public final static int ApiSystem_mb_dolfv2  	= Definitions.ApiSystem_mb_dolfv2;	// dolf
	// Other systems, start from 100+
	public final static int ApiSystem_r_limit  	= Definitions.ApiSystem_r_limit;  //desde aca comienzan sistemas remotos
	public final static int ApiSystem_r_tgsrv  	= Definitions.ApiSystem_r_tgsrv;	//Remote Tag Server
	
	// Sigmation server versions (hardware)
	public final static int sversion_stel_1  	= Definitions.sversion_stel_1;   	// Sigma telecom v1
	public final static int sversion_stel_2  	= Definitions.sversion_stel_2;	// Sigma telecom v2
	public final static int sversion_stel_3_wmx	= Definitions.sversion_stel_3_wmx;// Sigma telecom v3 + wmx, using Raveon modems
	public final static int sversion_ssa_2 		= Definitions.sversion_ssa_2;		// Sigma sudafrica v2 
	//Other systems, start from 100+
	public final static int sversion_r_tag_1   	= Definitions.sversion_r_tag_1;	//Remote Tag Server	
	
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			HW CONFIG										//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
	/*  // uncomment for RD South Africa Server 2.0
	public static int ApiSystem 	= ApiSystem_dram;   // check above: sigmation server systems
	public static int serverVersion	= sversion_ssa_2;    // check above: sigmation server versions (hardware)
	*/
	
	/*  //uncomment for firewatcher
	public static int ApiSystem = ApiSystem_fsys;      // check above: sigmation server systems
	public static int serverVersion= sversion_stel_3_wmx;    // check above: sigmation server versions (hardware)
	*/
	
	/*  //uncomment for DOLF
	public static int ApiSystem = ApiSystem_mb_dolf;         // check above: sigmation server systems
	public static int serverVersion= sversion_stel_3_wmx;    // check above: sigmation server versions (hardware)
	*/
	
	// uncomment for remote tag server
	public static int ApiSystem 	= ApiSystem_r_tgsrv;   // check above: sigmation server systems
	public static int serverVersion	= sversion_stel_3_wmx;    // check above: sigmation server versions (hardware)
	
	
	/*// uncomment for tag server
	public static int ApiSystem 	= ApiSystem_tag;   // check above: sigmation server systems
	public static int serverVersion	= sversion_stel_3_wmx;    // check above: sigmation server versions (hardware)
	*/
	
	
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			MISC. CONFIG									//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */
	
	//databases default values
	public static String database = "ampdb";
    public static String user = "root";
    public static String pass = "orca";
    public static String remoteuser = "workbench";
    public static String remotepass = "sigmati0n";
    
    
	public static boolean noPiMode 		= false;          	// set true for local testings on non rpi machines, for production set to FALSE!!!!
    public static boolean piRFlag		= false;			// reset flag, leave it untouched, setting it to true will constantly reset the device
    public static boolean WMX			= true;   			// use WMX protocol, set to true if sversion_stel_3_wmx
    public static boolean rs485Ctrl		= false; 			// use RS 485 output control
    public static boolean midnightReset	= false; 			// Reset @ hour:minute
    public static boolean hardReset		= false; 			// if false, service restarts, if true, CPU reboots
    public static int[] hour			= {0,0};			// default 00:00 reset time
    public static boolean resetSignal	= false;
    public static boolean startSignal	= false; 
	
	final static String SerialBound = "000000004532e021"; 
	public static RunShellCommandFromJava runner;
	
	static Thread hearBeat;
	/*
	private static void createSystemIniDB(){
		MySQL cr = new MySQL();
		cr.database="config";		
		String sentence = Definitions.MySQLSentences.createSystemDefinitions;
		cr.connect();  
		cr.rawUpdateMySQLCommand(sentence);
		sentence = Definitions.MySQLSentences.insertSystemDefinitions;
		cr.rawUpdateMySQLCommand(sentence);
		sentence = Definitions.MySQLSentences.createSystemIniDatabase;
		cr.rawUpdateMySQLCommand(sentence);
		sentence = Definitions.MySQLSentences.insertSystemIni;
		cr.rawUpdateMySQLCommand(sentence); 
		cr.close();
	}

	private static void loadSystemIniDB(){
		
	}
	*/
	public static void initPi(){
		SystemIO.initGpio(serverVersion, noPiMode, piRFlag, rs485Ctrl);		
	}
 	
	public static void sysInfo(){		
		SystemIO.sysInfo();		
	}


	public static void piResetFlag(){
		SysUtils.piRFlag=true;	
		System.out.println("<<<<  awaiting reset >>>>");
	}	 
	
	public static void modbus(){
		ModbusApp remotemodbus = new ModbusApp(rs485Ctrl, WMX); 
		remotemodbus.loop();
	}
	
	public static void dolf(){
		ModbusAppV2 remotemodbus = new ModbusAppV2(rs485Ctrl, WMX); 
		remotemodbus.loop();
	}
	
	public static void drs(){
		DRS.init(); 
		DRS.loop();
	}

	public static void startResetButton(){
		SystemIO.startResetButton();		
	}
	
	public static void fsys(){
		FireAlarmSystemB.init(WMX);
		FireAlarmSystemB.loop();
	}
	
	
	public static void tnt(){
		TNT.init(rs485Ctrl, WMX); 
	}
	
	public static void rd(){
		System.out.println("[Starting MB Protocol] ");
		RDV2.loop();
	}
	
	public static void msg(){
		SysUtils.indev("MSG");
	}
	
	public static void vent(){
		SysUtils.indev("VENT");
	}
	
	public static void psm(){
		SysUtils.indev("PSM");
	}
		
	public static class remoteSystems{
		/**
		 *   Remote systems that are not intented to work inside a Sigmation server
		 *   Sigmation API is still used to keep all the standars, but some GPIO usages change
		 *   ----ie: the LCD and Reset button are not used
		 *   While other functions and GPIO usages remain the same
		 *   ----ie: Modem 
		 * */
		
		public static void remoteTagServer(){ 
				remoteTagServer.init(WMX);
				remoteTagServer.loop(); 
		}
	}
	
	public static void ApiSystemError(){
		System.out.println("Wrong System Number "+ApiSystem);
		System.exit(-1);
	}
	
	public static void finishThread(){
		resetSignal=true;
	}
	
	public static void systemInit(){
		SysUtils.printDate();
		ReadConfig.getProps();
        if(!noPiMode){
        	initPi();
        	if(ApiSystem<ApiSystem_r_limit) sysInfo(); 
        	if(ApiSystem<ApiSystem_r_limit) startResetButton(); 
        } else {
        	SysUtils.noPiModeWarning();
        }         					
    	if(Sigmation.midnightReset) SysUtils.systemScheduler.init((long)hour[0], (long)hour[1], hardReset);
    	//SystemScheduler.init();
	}
	
    public static void main(String args[]) throws InterruptedException {
    	systemInit();     
    	
    	  resetSignal=false;
    	  try{  		  	
    	       		  
              switch(ApiSystem){ 
              	case	ApiSystem_tag		:	tnt();
              									break;
              	case	ApiSystem_msg		:	msg();
												break;
              	case	ApiSystem_vent		:	vent();
              									break;
              	case	ApiSystem_drfp		:	psm();
  												break;
              	case	ApiSystem_dram		:	rd();
  												break;
              	case	ApiSystem_fsys		:	fsys();
  												break;
              	case	ApiSystem_mb_dolf	:	modbus();
  												break;
              	case	ApiSystem_drs		:	drs();
												break;
              	case	ApiSystem_mb_dolfv2:	dolf();
              									break;								
              	case	ApiSystem_r_tgsrv	:	remoteSystems.remoteTagServer();
												break;
												
              	case	0x55:	SysCom.serialIni();  //COM para sigmation v1
              					break;
              	case	0xAA:	WMXModCom.testCom();
								break;				
              	default		:	ApiSystemError();
  								break;
              
              } 
              
      	}
    	  
      	catch(Exception ex) {
              System.out.println(" ==>> PROGRAM ERROR : " + ex.getMessage());
              ex.printStackTrace(); 
              SystemIO.currentTask=">ER 0x002       ";
              if(ApiSystem<ApiSystem_r_limit) sysInfo();
			  SysUtils.piRFlag=true;	
              SysUtils.piReset();
              return; 
           }
          	

    }
}



