package sigmation.main;
 
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.bson.Document;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttTopic;

import sigmation.drivers.DRSCommandList;
import sigmation.drivers.DRSPacketProcess;
import sigmation.drivers.DRSPacketProcess.SubElement;
import sigmation.drivers.DRSProtocol;
import sigmation.drivers.DRSProtocol.DRSPacketObject;
import sigmation.drivers.DRSStats;
import sigmation.drivers.Helpers; 
import sigmation.drivers.StatObject; 
import sigmation.io.UDPHandler;
import sigmation.io.UDPHandler.UDPReceive;
import sigmation.mongodb.MongoDriver;
import sigmation.mqtt.pahoMqtt;
import sigmation.mqtt.pahoMqtt.*;
import sigmation.utils.ReadConfig;
import sigmation.utils.SigmationVarTopic;
import sigmation.utils.SysUtils;
import sigmation.utils.SysUtils.jsonArray;
import sigmation.utils.SysUtils.jsonObject;

/**
 * 
 * Sistema DRS basado en cliente Meteor + Mongodb + MQTT
 * 
 * 
 * */

public class DRS {
	
	/*==========================================
	 * 
	 * MONGODB CONFIG
	 * 
	 ==========================================*/
	
	public final static String mongoServer="mongodb://127.0.0.1:3001/meteor";
	public final static String databaseName= "meteor"; 
	public final static String collectionName= "mqttcollection";  

	/*==========================================
	 * 
	 * MONGODB CONFIG
	 * 
	 ==========================================*/
	
	private static int defaultUdpServerPort=4445; 

	/*==========================================
	 * 
	 * MQTT CONFIG
	 * 
	 ==========================================*/
	
	private static String SmgSys = "____DRS";
	private static String res = "__dev";
	private static String param= "__set"; 
	private static boolean inDeploy=true;
	private static boolean checkports=true;
	
	/*==========================================
	 * 
	 * SUBSYS VARS
	 * 
	 ==========================================*/
	
	private static MongoDriver drsMongoDB;
	private static pahoMqtt drsMqtt;
	private static UDPHandler udphandler = new UDPHandler(defaultUdpServerPort); 
	
	public static boolean alarm=false;
	
	private static boolean debugThis=false;
	private static boolean loopbackTest=false;
	private static double alarmProbability=0.5; //loopback test only
	
	/*==========================================
	 * 
	 * TIMER VARS SECONDS
	 * 
	 ==========================================*/
	
	private static long checkPollingCollectionTimerSeconds= 11;  
	private static long checkRTPTimerSeconds= 51;//31*6;  
	private static long checkAlarmsTimerSeconds= 41;
	private static long checkRTPByAlarmTimerSeconds= 7;  
	private static long queryCollectionTimerSeconds= 127;  

	/*==========================================
	 * 
	 * TIMER VARS 
	 * 
	 ==========================================*/
	
	private static final long checkPollingCollectionTimer=checkPollingCollectionTimerSeconds*1000;
	private static final long checkRTPTimer=checkRTPTimerSeconds*1000;
	private static final long checkAlarmsTimer=checkAlarmsTimerSeconds*1000;
	private static final long checkRTPByAlarm=checkRTPByAlarmTimerSeconds*1000;
	private static final long queryCollectionTimer=queryCollectionTimerSeconds*1000; 
	
	
	/*==========================================
	 * 
	 * TIMERS
	 * 
	 ==========================================*/
		
	public static void checkPollingCollectionStart(){
		Timer timer = new Timer();	 
		timer.schedule(new DeployElement(), 0, checkPollingCollectionTimer);  
	}
	
	public static void checkRTPStart(){
		Timer timer = new Timer();	 
		timer.schedule(new QueryRTPElement(), 0, checkRTPTimer);  
	}
	
	public static void checkRTPByAlarmStart(){
		Timer timer = new Timer();	 
		timer.schedule(new QueryRTPByAlarmElement(), 0, checkRTPByAlarm);  
	}
	
	public static void checkAlarmsStart(){
		Timer timer = new Timer();	 
		timer.schedule(new queryAlarms(), 0, checkAlarmsTimer);  
	}
	
	public static void queryCollectionStart(){
		Timer timer = new Timer();	 
		timer.schedule(new queryStats(), 0, queryCollectionTimer);  
	}
	
	

	/*==========================================
	 * 
	 * TIMERS CLASSES
	 * 
	 ==========================================*/
	
	private static class DeployElement extends TimerTask {		
		public void run() { 
			//if(debugThis) System.out.println("[Deployment check]");
			deployElement("anything"); 
		}
	}
	
	private static class QueryRTPElement extends TimerTask {		
		public void run() { 
			//if(debugThis) System.out.println("[Deployment check]"); 
			if(!inDeploy) realTimeStatsPolling();
		}
	}
	
	private static class QueryRTPByAlarmElement extends TimerTask {		
		public void run() { 
			//if(debugThis) System.out.println("[Deployment check]"); 
			if(alarm && !inDeploy ) {
				alarm=false;
				realTimeStatsPolling();
			}			
		}
	}
	
	private static class queryAlarms extends TimerTask {		
		public void run() { 
			//if(debugThis) System.out.println("[Deployment check]"); 
			if(!inDeploy ) {
				realTimeStatsPolling(true);
			}			
		}
	}
	
	private static class queryStats extends TimerTask {		
		public void run() { 
			if(!inDeploy) queryElement();		
		}
	}
	 
	
	/*==========================================
	 * 
	 * METHODS
	 * 
	 ==========================================*/
	
	private static void getProperties(){
		String prefix="drs";
		String SloopbackTest="false";
		//ReadConfig.properties.printProps();
		String thisMode = ReadConfig.properties.getString(prefix+"Mode", "deploy");
		defaultUdpServerPort = ReadConfig.properties.getInt(prefix+"UdpPort", defaultUdpServerPort);
		System.out.println("================== DRS SPECIAL CONF =====================");
		
		if(thisMode.toLowerCase().contentEquals("development")) {
			debugThis=true;			
			SloopbackTest=ReadConfig.properties.getString(prefix+"Loopback", SloopbackTest);			
			if(SloopbackTest.toLowerCase().contentEquals("true")) loopbackTest=true;
			else loopbackTest=false;  			
			alarmProbability= ReadConfig.properties.getDouble(prefix+"AlarmProbability", alarmProbability);
			System.out.println("Loopback test "+SloopbackTest);
			System.out.println("alarm probability "+alarmProbability*100+"%");
			System.out.println("Debuging"); 
			
		} else if(thisMode.toLowerCase().contentEquals("debug")){
			debugThis=true; 
			System.out.println("Debuging");  
		}else {
			debugThis=false; 
			System.out.println("On deploy mode");  
			
		}
		System.out.println("drsUdpPort "+defaultUdpServerPort); 
		System.out.println("=========================================================");
	}
	
	public static void init() {
		System.out.println("init");
		getProperties();
		DRSCommandList.init();
		sleep(1000);
		//iniciar link a MongoDB
		startMongo();
		//iniciar subscripcion MQTT
		startMQTT();
		//cargar opciones (se cargan desde base de datos solo una vez por ejecucion, el resto del tiempo se realiza por MQTT)
		startUDPHandler();
		//Packet UDP de prueba		
		//testPacket();
		//iniciar hilo de carga de lista polling
		checkPollingCollectionStart();
		//iniciar polling de variables RTP
		checkRTPStart();
		//iniciar polling de alarmas
		//checkAlarmsStart();
		//iniciar polling de variables RTP ante alarmas
		//checkRTPByAlarmStart();
		//iniciar hilo de query de todos los stats de cada equipo
		//queryCollectionStart(); 
	}
	
	public static void loop() {
		//System.out.println("loop"); 	
	}
	
	private static void testPacket() {
		DRSProtocol xprotocol = new DRSProtocol(); 
		byte[] code = {0x31,0x01};
		byte[] cData = {0x28,0x23};
		xprotocol.setupSendQuery((byte)0, code, cData);
		xprotocol.setupSendSet(0,(byte)0, code, cData); 
		sendCommand(xprotocol,"127.0.0.1",defaultUdpServerPort);
	}
	
	private static void startUDPHandler() {
		UDPReceive receiver = udphandler.new UDPReceive() {
			@Override
			public void UDPMessageReceived(byte[] received, String currentAddress, int port ) {
				try {
					int siteNum=0;
					System.out.println("RECEIVED DRS packet from "+currentAddress+": ");
		   		    sigmation.utils.SysUtils.printhex(received,received.length,true);
		   		    System.out.println(" ");
		   		    // EXAMINAR PAQUETE Y HACER WHATEVER
		   		    DRSProtocol xprotocol = new DRSProtocol(); 
		   		    DRSPacketObject thisUdpObject = xprotocol.analyzePacket(received);
			   		String[] toGet = {};
			 		Object filterVal = currentAddress;
			 		List<Document> deployList = drsMongoDB.getObjects("pollingdevices", toGet, "ip",filterVal);
			 		String objectName= "null";
			 		int devicePort=0;
			 		BigInteger thisBigInt = BigInteger.valueOf(0);
			 		for(Document deployElement: deployList) {
			 			objectName= deployElement.getString("_id"); 
			 			siteNum=(int)deployElement.get("sitenum");
			 			thisBigInt=new BigInteger(deployElement.getString("subdevices"));
			 		} 
			 		if(thisUdpObject.unitnum>0) {
			 			if(thisUdpObject.unitnum>0x40) {
			 				devicePort=4;
			 			} else if(thisUdpObject.unitnum>0x30) {
			 				devicePort=3;	
				 		} else if(thisUdpObject.unitnum>0x20) {
				 			devicePort=2;	 				
			 		    } else if(thisUdpObject.unitnum>0x10) {
			 		    	devicePort=1;	
						}
			 		} 
			 		Helpers helpers = new Helpers();
					helpers.basicVarComp(drsMqtt, objectName, "online", "true");
			 		boolean isResponseOrLoopback =  (thisUdpObject.isResponse || loopbackTest);
			 		
		   		    if(!objectName.contentEquals("null") && isResponseOrLoopback) {
		   		    	for(int i=0;i<thisUdpObject.commandList.size();i++) {
		   		    		ArrayList<Object> udpCommandObjects = xprotocol.bytesToCommandObject(thisUdpObject.commandList.get(i));
		   		    		int thisCommand = (int) udpCommandObjects.get(0);
		   		    		byte[] thisDataBytes = (byte[]) udpCommandObjects.get(1);
		   		    		if(devicePort==0) {
			   		    		DRSPacketProcess processor = new DRSPacketProcess(objectName,drsMqtt, thisCommand, thisDataBytes,thisBigInt );
					   		    processor.init();
					   		    
					   		    	if(thisBigInt!=processor.subElementBigInt) {
					   		    		System.out.println("Remote changes detected : "+thisBigInt+" > "+processor.subElementBigInt);
					   		    		int[] subsToDissable = SysUtils.bigIntToIntArrayIndexOrder(thisBigInt, 4); 
					   		    		int[] subsToPoll =SysUtils.bigIntToIntArrayIndexOrder(processor.subElementBigInt, 4); 
										int j=0;
										for (int opticPortQuantity:subsToDissable) { 
											int localToDissable = subsToPoll[j];
											for(int k=0 ;  k< opticPortQuantity; k++) {
												int portNumber = (j+1) ;
												int indexNumber =(k+1);
												int deviceNumber = portNumber * 10 + indexNumber;  
												if(indexNumber >localToDissable) {
													String objectName__ = objectName+"/__resourcegroup/"+"optic"+portNumber+"/__dev/"+deviceNumber;
													helpers.basicVarComp(drsMqtt, objectName__, "enable", "false"); 
												}													
											}
											j++;
										}
					   		    		drsMongoDB.updateOneDocument(objectName, "pollingdevices", "subdevices", ""+processor.subElementBigInt,"bigint");
					   		    		if(processor.subElementList.size()>0) {
							   		    	for(int m=0;m<processor.subElementList.size();m++) {
							   		    		SubElement subElem = processor.subElementList.get(m);
							   		    		if (subElem.index>subsToDissable[subElem.portIndex]) startDrsSubObject(siteNum,SysUtils.intToHexFormatOnly(subElem.deviceNumber),true, subElem.objectName, "optic"+subElem.port, ""+subElem.deviceNumber, currentAddress, ""+port);
							   		    	}
					   		    		}
					   		    	}					   		    	
					   		    
			   		    	}else {
			   		    		String resourceGroup="optic"+devicePort;
			   		    		String resourceName=""+SysUtils.byteToStringHex((byte)thisUdpObject.unitnum);
			   		    		DRSPacketProcess processor = new DRSPacketProcess(objectName,resourceGroup,resourceName,devicePort,drsMqtt, thisCommand, thisDataBytes);
					   		    processor.init();
			   		    	}
		   		    	}
		   		    	
		   		    } else if(!thisUdpObject.isResponse && !inDeploy){
		   		    	System.out.println("Object from "+currentAddress+" is not a response package");
		   		    } else if(inDeploy){
		   		    	System.out.println("System in deploy");
		   		    } else {
		   		    	System.out.println("Object from "+currentAddress+" doesn't exist on polling list");
		   		    }
				} catch (Exception e) {
					//e.printStackTrace();
					return;
				}
			}
		};
		udphandler.addListener(receiver);
		udphandler.start();		
	}
	
	private static void startMongo() {
		drsMongoDB = new MongoDriver(mongoServer,databaseName); 
	}
	
	private static void startMQTT() {
		drsMqtt = new pahoMqtt(SmgSys, res, param);
		Responder thisresponder = drsMqtt.new Responder() {
			@Override
			public void mqttMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token) {
				MqttTopic mqttTopic = publisher.getTopic(stopic.getCurrentTopic()); 
				   if(!stopic.getCurrentValString().contentEquals("__first_set") && !stopic.getCurrentValString().contentEquals("__null")) {
					   //token = mqttTopic.publish(stopic.getCurrentVal(),0,false);
					   if(topicProcessing(stopic)) {
							//avisar success
						} else {
							//TODO avisar falla
							System.out.println("Falla de protocolo o formato de mensaje");
						}
				   }			
			}
		};
		Responder thisDeploy = drsMqtt.new Responder() {
			@Override
			public void mqttSetMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token, String resource) {
				MqttTopic mqttTopic = publisher.getTopic(stopic.getCurrentTopic()); 
				if(!stopic.getCurrentValString().contentEquals("true")) {
					   if(topicProcessing(stopic)) {
							//token = mqttTopic.publish(stopic.getCurrentVal(),0,false);
						   System.out.println("Deploy Command Received");
						} else {
							System.out.println("Device has been deployed");
						}
				   }
			}
		};
		drsMqtt.addListener(thisresponder); 
		drsMqtt.addListener(thisDeploy);
		drsMqtt.start();
	}
	
	private static void deployElement(String resource) {
		try {
			String[] toGet = {};
			Object filterVal = true;
			List<Document> deployList = drsMongoDB.getObjects("pollingdevices", toGet, "deploy",filterVal);
			for(Document deployElement: deployList) {
				inDeploy=true;
				String thisId= deployElement.getString("_id");
				String devicename= deployElement.getString("devicename");
				String ip= deployElement.getString("ip");
				int port= (int) deployElement.get("port"); 
				Boolean deploy= (Boolean) deployElement.get("deploy"); 
				Boolean dontShowThis= (Boolean) deployElement.get("dontShowThis");
				int siteNum = (int) deployElement.get("sitenum"); 
				System.out.println("Deploy "+thisId+" - "+devicename+" - "+ip+" - "+port+" - "+deploy+" - "+dontShowThis);
				if(deploy) {
					if(!dontShowThis) startDrsObject(siteNum,true,thisId,ip,""+port);
					drsMongoDB.updateOneDocument(thisId, "pollingdevices", "deploy", "false","boolean");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) { 
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Data Fetch from database failed, database probably not ready");
		}
		//System.out.println("Deploy finished");
		inDeploy=false;
	}
	
	private static void queryElement() {
		try {
			String[] toGet = {};
			Object filterVal = false;
			List<Document> deployList = drsMongoDB.getObjects("pollingdevices", toGet, "dontShowThis",filterVal);
			for(Document deployElement: deployList) {
				String thisId= deployElement.getString("_id");  
				String devicename= deployElement.getString("devicename");  
				String ip= deployElement.getString("ip");  
				int port= (int) deployElement.get("port"); 
				int siteNum= (int) deployElement.get("sitenum"); 
				int pollingState = (int) deployElement.get("pollingstate");  
				Boolean deploy= (Boolean) deployElement.get("deploy");  
				Boolean dontShowThis= (Boolean) deployElement.get("dontShowThis");  
				System.out.println("Deploy "+thisId+" - "+devicename+" - "+ip+" - "+port+" - "+deploy+" - "+dontShowThis);  
				if(!dontShowThis) { 
					pollingState++;
					if(pollingState>8) { 
						Helpers helpers = new Helpers();
						helpers.basicVarComp(drsMqtt, thisId, "online", "false");
					}
					drsMongoDB.updateOneDocument(thisId, "pollingdevices", "pollingstate", ""+pollingState,"int");
					realTimeStatsPolling(siteNum,(byte)0,DRSCommandList.MainObjectVarList.allstats1, thisId, ip, port);
					sleep(3000);
					realTimeStatsPolling(siteNum,(byte)0,DRSCommandList.MainObjectVarList.allstats2, thisId, ip, port);
					sleep(3000);
					realTimeStatsPolling(siteNum,(byte)0,DRSCommandList.MainObjectSubList.allstats1, thisId, ip, port);
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Data Fetch from database failed, database probably not ready");
		}
		
	}
	
	private static boolean topicProcessing(SigmationVarTopic stopic) {  
		Object[][] stats = getStatsFromTopic(stopic.getVarTopicId());
		DRSProtocol protocol = new DRSProtocol();
		byte unitnum= (byte)0;
		if(stopic.isSubElement()) {
			int thisInt = SysUtils.returnIntHex(stopic.getResourceName());
			unitnum = (byte)thisInt;
		} 
		String toMap = stopic.getVarName(); 
		String mapped = DRSCommandList.mapTypeFromString(toMap); 
		if(mapped.toLowerCase().contentEquals(DRSCommandList.alarm)) toMap=toMap+"Set"; 
		boolean packetReady= protocol.setupSendSetInverseCode((int) stats[7][1],unitnum, SysUtils.intTo2Bytes(DRSCommandList.mapKey(toMap)), protocol.dataToBytes(stopic.getCurrentVal(),DRSCommandList.mapTypeFromString(toMap) ));
		if(packetReady) {
			sendCommand(protocol,(String) stats[2][1],(int)stats[3][1]);
			sleep(1000);
			sendCommand(protocol,(String) stats[2][1],(int)stats[3][1]);
		}
			
		return packetReady;
	}
	
	private static void sendCommand(DRSProtocol protocol, String thisIP, String thisPort) {
		sendCommand( protocol,  thisIP,  SysUtils.returnInt(thisPort))	;
	}
	
	private static void sendCommand(DRSProtocol protocol, String thisIP, int thisPort) {
		udphandler.sendCommand(protocol.getPacket(), thisIP, thisPort);	
		sleep(10);
		//udphandler.sendCommand(protocol.getPacket(), thisIP, thisPort);	
	}
	
	private static Object[][] getStatsFromTopic(String topicId) { 
		System.out.println("Id "+topicId);
		Object[][] stats = new Object[8][2];
		List<Document> objectList = drsMongoDB.getObjects("pollingdevices", null, "_id", topicId);
		for(Document objectElement: objectList) {
			String thisId= objectElement.getString("_id");
			String subDevices = objectElement.getString("subdevices");
			int siteNum =  (int) objectElement.get("sitenum"); 
			String devicename= objectElement.getString("devicename");
			String ip= objectElement.getString("ip");
			int port= (int) objectElement.get("port"); 
			Boolean deploy= (Boolean) objectElement.get("deploy"); 
			Boolean dontShowThis= (Boolean) objectElement.get("dontShowThis");
			System.out.println("Set this "+thisId+" - "+devicename+" - "+ip+" - "+port+" - "+deploy+" - "+dontShowThis); 
			stats[0][0]= "string"; stats[0][1]=thisId;
			stats[1][0]= "string"; stats[1][1]=devicename;
			stats[2][0]= "string"; stats[2][1]=ip;
			stats[3][0]= "int";    stats[3][1]=port;
			stats[4][0]= "bool";   stats[4][1]=deploy;
			stats[5][0]= "bool";   stats[5][1]=dontShowThis;
			stats[6][0]= "string"; stats[6][1]=subDevices;
			stats[7][0]= "int";    stats[7][1]=siteNum;
		}
		return stats;
	}
	/*
	static class DrsObjectStats {
		static String ipAddress="127.0.0.1";
		static String port = "5000";
	}*/
	
	private static void statToMqtt(boolean doMqtt, StatObject thisStatObject, String objectName, String varname, String groupname, String group, String ipAddress, String port) {
		Helpers helpers = new Helpers();
		helpers.statToMqtt(drsMqtt,thisStatObject, objectName, varname, groupname, group, null); 
		DRSProtocol protocol = new DRSProtocol();
		String toMap = varname;
		System.out.println(groupname+" - "+varname);
		byte[] data = {0};
		//protocol.setupSendQueryInverseCode((byte)0, SysUtils.intTo2Bytes(DRSCommandList.mapKey(toMap)),data); 
		//sendCommand(protocol, ipAddress,port);
	}
	

	private static void statToMqtt(boolean doMqtt,StatObject thisStatObject, String objectName, String varname, String groupname, String group, String setPar, String ipAddress, String port) {
		statToMqtt(doMqtt, thisStatObject,  objectName,  varname,  groupname,  group,  setPar,  null, ipAddress,  port);
	}
	
	private static void statToMqtt(boolean doMqtt,StatObject thisStatObject, String objectName, String varname, String groupname, String group, String setPar, String setVal, String ipAddress, String port) {
		Helpers helpers = new Helpers();
		if(doMqtt) helpers.statToMqtt(drsMqtt,thisStatObject, objectName,varname,groupname,group,setPar,setVal,true); 
		System.out.println(groupname+" - "+varname); 
	}
	
	private static void sleep(int miliseconds) {
		try {
			Thread.sleep(miliseconds);
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("sleep failed");
		}
	}
	
	
	private static void statQuery(int siteNum, String objectName,String thisIp,int thisPort, String varname) { 
		DRSProtocol protocol = new DRSProtocol(); 
		String toMap = varname;
		//System.out.println("Stat query - "+varname);
		//Random rd = new Random();
	    //byte[] arr = new byte[2];
	    //if(loopbackTest) rd.nextBytes(arr);
		//byte[] data = {arr[0]};
		//protocol.setupSendQueryInverseCode((byte)0, SysUtils.intTo2Bytes(DRSCommandList.mapKey(toMap)),data); 
		//sendCommand(protocol,thisIp,thisPort); 
	}
	
	private static void statQuery(int siteNum, int unitnum, String objectName,String thisIp,int thisPort, String[] varname) { 
		DRSProtocol protocol = new DRSProtocol();
		ArrayList<DRSProtocol.CommandBuffer> commandBufferList = new ArrayList<DRSProtocol.CommandBuffer>();
		for(int i=0;i<varname.length;i++) {
			String toMap = varname[i];
			int intCommand= DRSCommandList.mapKey(toMap);
			String type = DRSCommandList.mapType(intCommand);
			//System.out.println("Stat query - "+varname);
			/*Random rd = new Random();
		    byte[] arr = new byte[2];
		    if(loopbackTest) rd.nextBytes(arr);*/
			byte[] data = DRSProtocol.getEmptyBytes(type);
			//mapType
			DRSProtocol.CommandBuffer thisBuffer = protocol.new CommandBuffer(SysUtils.intTo2Bytes(intCommand), data);
			commandBufferList.add(thisBuffer);
		}		
		protocol.setupSendQueryInverseCode(siteNum,(byte)unitnum, commandBufferList); 
		sendCommand(protocol,thisIp,thisPort);
		//
	}
	
	private static void alarmQuery(String objectName,String thisIp,int thisPort,  String varname) { 
		DRSProtocol protocol = new DRSProtocol();
		String toMap = varname;
		System.out.println("Stat query - "+varname); 
	    byte thisAlarm = 0;
	    if(SysUtils.getRandomBoolean(true)) {
	    	thisAlarm = 1;
	    }
		byte[] data = {thisAlarm};
		//protocol.setupSendQueryInverseCode((byte)0, SysUtils.intTo2Bytes(DRSCommandList.mapKey(toMap)),data); 
		//sendCommand(protocol,thisIp,thisPort);
	}
	
	private static void realTimeStatsPolling() {
		realTimeStatsPolling(false);
	}
	
	private static int[] decodeSubDevices(String subDevices) {
		BigInteger subsInt = new BigInteger(subDevices);
		return SysUtils.bigIntToIntArrayIndexOrder(subsInt, 4);
	}
	
	private static void realTimeStatsPolling(boolean alarms) { 
		try {
			String[] toGet = {};
			Object filterVal = false;
			List<Document> deployList = drsMongoDB.getObjects("pollingdevices", toGet, "dontShowThis",filterVal);
			for(Document deployElement: deployList) {
				String thisId= deployElement.getString("_id");
				String devicename= deployElement.getString("devicename");
				String ip= deployElement.getString("ip");
				int port= (int) deployElement.get("port"); 
				int siteNum= (int) deployElement.get("sitenum"); 
				int pollingState = (int) deployElement.get("pollingstate"); 
				String subDevices= deployElement.getString("subdevices"); 
				Boolean query= (Boolean) deployElement.get("deploy"); 
				Boolean dontShowThis= (Boolean) deployElement.get("dontShowThis");
				System.out.println("query "+thisId+" - "+devicename+" - "+ip+" - "+port+" - "+query+" - "+dontShowThis);
				if(!query) {  
					pollingState++;
					if(pollingState>8) { 
						Helpers helpers = new Helpers();
						helpers.basicVarComp(drsMqtt, thisId, "online", "false");
					}
					drsMongoDB.updateOneDocument(thisId, "pollingdevices", "pollingstate", ""+pollingState,"int");
					if(alarms) {
						statQuery(siteNum,(byte)0,thisId, ip, port,((alarms) ? DRSCommandList.MainObjectVarList.allAlarms : DRSCommandList.MainObjectVarList.allRTP));
						sleep(3000);
						if(checkports) {
							realTimeStatsPolling(siteNum,DRSCommandList.MainObjectSubList.allstats1, thisId, ip, port);
							sleep(3000);
						}						
					}else {
						realTimeStatsPolling(siteNum,(byte)0,DRSCommandList.MainObjectVarList.allstats1, thisId, ip, port);
						sleep(3000);
						realTimeStatsPolling(siteNum,(byte)0,DRSCommandList.MainObjectVarList.allstats2, thisId, ip, port);
						sleep(3000);
						if(checkports) {
							realTimeStatsPolling(siteNum,DRSCommandList.MainObjectSubList.allstats1, thisId, ip, port);
							sleep(3000);
						}
					}
					int[] subsToPoll = decodeSubDevices(subDevices);
					int j=0;
					for (int opticPortQuantity:subsToPoll) { 
						for(int i=0 ;  i< opticPortQuantity; i++) {
							int deviceNumber = (j+1) * 0x10 + (i+1);  
							if(alarms) statQuery(siteNum,deviceNumber,thisId, ip, port,((alarms) ? DRSCommandList.SubObjectVarList.allAlarms : DRSCommandList.SubObjectVarList.allRTP));
							else {
								realTimeStatsPolling(siteNum,(byte)deviceNumber,DRSCommandList.SubObjectVarList.allstats1, thisId, ip, port);
								sleep(3000);
								realTimeStatsPolling(siteNum,(byte)deviceNumber,DRSCommandList.SubObjectVarList.allstats2, thisId, ip, port);
								
							}
							sleep(3000);
						}
						j++;
					}
					
					if(loopbackTest && SysUtils.getRandomBoolean(alarmProbability)) {
						alarmQuery(thisId, ip, port, "suplyPowerFailAlarm");
						alarmQuery(thisId, ip, port, "downlinkInputPower");
				    }
				}			
			}

			if(alarms) checkports=!checkports;
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("RTP query failed");
			if(alarms) checkports=!checkports;
		}		
	}
	
	private static void realTimeStatsPolling(int siteNum,String[] myVars,String thisId, String ip, int port ) { 
		try {   
				System.out.println("query - "+ip+" - "+port+" - " ); 
					statQuery(siteNum,0,thisId, ip, port,myVars);
					Thread.sleep(50);  
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private static void realTimeStatsPolling(int siteNum,byte unitnum, String[] myVars,String thisId, String ip, int port ) { 
		try {   
				System.out.println("query - "+ip+" - "+port+" - " ); 
					statQuery(siteNum,unitnum,thisId, ip, port,myVars);
					Thread.sleep(50);  
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	 
	
	private static void startDrsObject(int siteNum, boolean doMqtt,String objectName, String ipAddress, String port) {
		//DrsObjectStats.ipAddress=ipAddress;
		//DrsObjectStats.port=port;
		drsMongoDB.updateOneDocument(objectName, "pollingdevices", "subdevices", ""+0,"bigint");
		Helpers helpers = new Helpers();
		statToMqtt(doMqtt,DRSStats.Alarmas.downlinkLowInputAlarm, objectName,"downlinkLowInputAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.downlinkOverInputAlarm, objectName,"downlinkOverInputAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.masterSlaveLinkAlarm, objectName,"masterSlaveLinkAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.suplyPowerFailAlarm, objectName,"suplyPowerFailAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch1frequency, objectName,"ch1frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch2frequency, objectName,"ch2frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch3frequency, objectName,"ch3frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch4frequency, objectName,"ch4frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch5frequency, objectName,"ch5frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch6frequency, objectName,"ch6frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch7frequency, objectName,"ch7frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch8frequency, objectName,"ch8frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch9frequency, objectName,"ch9frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch10frequency, objectName,"ch10frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch11frequency, objectName,"ch11frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch12frequency, objectName,"ch12frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch13frequency, objectName,"ch13frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch14frequency, objectName,"ch14frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch15frequency, objectName,"ch15frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch16frequency, objectName,"ch16frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.channelSwitch, objectName,"channelSwitch",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.choiceOfWorkingMode, objectName,"choiceOfWorkingMode",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.downlinkATT, objectName,"downlinkATT",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.rfPowerSwitch, objectName,"rfPowerSwitch",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.uplinkATT, objectName,"uplinkATT",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.controlUnitSoftwareVersion, objectName,"controlUnitSoftwareVersion",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceChannelNumber, objectName,"deviceChannelNumber",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceModel, objectName,"deviceModel",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceSerialNumber, objectName,"deviceSerialNumber",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceType, objectName,"deviceType",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.slaveNumber, objectName,"slaveNumber",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);

		statToMqtt(doMqtt,DRSStats.ParametrosRed.communicationStyle, objectName,"communicationStyle",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,"3",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.macAddress, objectName,"macAddress",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.monitorCenterIPAddress, objectName,"monitorCenterIPAddress",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.monitorCenterIPPort, objectName,"monitorCenterIPPort",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.psProtocol, objectName,"psProtocol",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterDefaultGateway, objectName,"repeaterDefaultGateway",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterIpAddress, objectName,"repeaterIpAddress",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng, ipAddress,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterIpPort, objectName,"repeaterIpPort",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng, port,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterSubnetMask, objectName,"repeaterSubnetMask",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.siteSubDeviceNumber, objectName,"siteSubDeviceNumber",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.siteNumber, objectName,"siteNumber",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.RealTimePars.downlinkInputPower, objectName,"downlinkInputPower",DRSStats.RealTimePars.cln,DRSStats.RealTimePars.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.RealTimePars.uplinkOutputPower, objectName,"uplinkOutputPower",DRSStats.RealTimePars.cln,DRSStats.RealTimePars.clng,"0",ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.Thresholds.alarmDelay, objectName,"alarmDelay",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"20",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Thresholds.downlinkInputMaxThreshold, objectName,"downlinkInputMaxThreshold",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Thresholds.downlinkInputMinThreshold, objectName,"downlinkInputMinThreshold",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		
		helpers.resourceGroupObject( drsMqtt, objectName,  "optic1",  "Optic Port 1") ;
		helpers.resourceGroupObject( drsMqtt, objectName,  "optic2",  "Optic Port 2") ;
		helpers.resourceGroupObject( drsMqtt, objectName,  "optic3",  "Optic Port 3") ;
		helpers.resourceGroupObject( drsMqtt, objectName,  "optic4",  "Optic Port 4") ;
		
		helpers.metaObjectComp(drsMqtt, objectName, objectName, "---","networkDevice", getMetaParams());
		helpers.basicVarComp(drsMqtt, objectName, "enable", "true");
		helpers.basicVarComp(drsMqtt, objectName, "online", "true");
		
		realTimeStatsPolling(siteNum,DRSCommandList.MainObjectVarList.allstats1, objectName, ipAddress, SysUtils.returnInt(port));
		sleep(1000);
		realTimeStatsPolling(siteNum,DRSCommandList.MainObjectVarList.allstats2, objectName, ipAddress, SysUtils.returnInt(port));
		sleep(2000);
		realTimeStatsPolling(siteNum,DRSCommandList.MainObjectSubList.allstats1, objectName, ipAddress, SysUtils.returnInt(port));
	} 
	
	private static void startDrsSubObject(int siteNum, byte unitnum, boolean doMqtt,String objectName_,String resourceGroup, String resourceName, String ipAddress, String port) {

		Helpers helpers = new Helpers();
		String objectName = objectName_+"/__resourcegroup/"+resourceGroup+"/__dev/"+resourceName;
		statToMqtt(doMqtt,DRSStats.Alarmas.downlinkLowOutputAlarm, objectName,"downlinkLowOutputAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.downlinkOverOutputAlarm, objectName,"downlinkOverOutputAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.masterSlaveLinkAlarm, objectName,"masterSlaveLinkAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.suplyPowerFailAlarm, objectName,"suplyPowerFailAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.paTemphingAlarm, objectName,"paTemphingAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Alarmas.opticalModuleTxRxAlarm, objectName,"opticalModuleTxRxAlarm",DRSStats.Alarmas.cln,DRSStats.Alarmas.clng,"true","false",ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch1frequency, objectName,"ch1frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch2frequency, objectName,"ch2frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch3frequency, objectName,"ch3frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch4frequency, objectName,"ch4frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch5frequency, objectName,"ch5frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch6frequency, objectName,"ch6frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch7frequency, objectName,"ch7frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch8frequency, objectName,"ch8frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch9frequency, objectName,"ch9frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch10frequency, objectName,"ch10frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch11frequency, objectName,"ch11frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch12frequency, objectName,"ch12frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch13frequency, objectName,"ch13frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch14frequency, objectName,"ch14frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch15frequency, objectName,"ch15frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.ch16frequency, objectName,"ch16frequency",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.channelSwitch, objectName,"channelSwitch",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.choiceOfWorkingMode, objectName,"choiceOfWorkingMode",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.downlinkATT, objectName,"downlinkATT",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.rfPowerSwitch, objectName,"rfPowerSwitch",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Frecuencia.uplinkATT, objectName,"uplinkATT",DRSStats.Frecuencia.cln,DRSStats.Frecuencia.clng,"0",ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.controlUnitSoftwareVersion, objectName,"controlUnitSoftwareVersion",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceChannelNumber, objectName,"deviceChannelNumber",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceModel, objectName,"deviceModel",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceSerialNumber, objectName,"deviceSerialNumber",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.deviceType, objectName,"deviceType",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.InfoDispositivo.ruId, objectName,"ruId",DRSStats.InfoDispositivo.cln,DRSStats.InfoDispositivo.clng,resourceName,ipAddress,port);
		
		statToMqtt(doMqtt,DRSStats.ParametrosRed.communicationStyle, objectName,"communicationStyle",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.macAddress, objectName,"macAddress",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.monitorCenterIPAddress, objectName,"monitorCenterIPAddress",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.monitorCenterIPPort, objectName,"monitorCenterIPPort",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.psProtocol, objectName,"psProtocol",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterDefaultGateway, objectName,"repeaterDefaultGateway",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterIpAddress, objectName,"repeaterIpAddress",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng, ipAddress,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterIpPort, objectName,"repeaterIpPort",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng, port,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.repeaterSubnetMask, objectName,"repeaterSubnetMask",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,"255.255.255.0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.siteSubDeviceNumber, objectName,"siteSubDeviceNumber",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		statToMqtt(doMqtt,DRSStats.ParametrosRed.siteNumber, objectName,"siteNumber",DRSStats.ParametrosRed.cln,DRSStats.ParametrosRed.clng,ipAddress,port);
		 
		statToMqtt(doMqtt,DRSStats.RealTimePars.downlinkVSWR, objectName,"downlinkVSWR",DRSStats.RealTimePars.cln,DRSStats.RealTimePars.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.RealTimePars.downlinkOutputPower, objectName,"downlinkOutputPower",DRSStats.RealTimePars.cln,DRSStats.RealTimePars.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.RealTimePars.uplinkInputPower, objectName,"uplinkInputPower",DRSStats.RealTimePars.cln,DRSStats.RealTimePars.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.RealTimePars.paTemp, objectName,"paTemp",DRSStats.RealTimePars.cln,DRSStats.RealTimePars.clng,"0",ipAddress,port); 
		
		statToMqtt(doMqtt,DRSStats.Thresholds.alarmDelay, objectName,"alarmDelay",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"20",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Thresholds.downlinkOutputMinThreshold, objectName,"downlinkOutputMinThreshold",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port);
		statToMqtt(doMqtt,DRSStats.Thresholds.downlinkOutputMaxThreshold, objectName,"downlinkOutputMaxThreshold",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.paTempThreshold, objectName,"paTempThreshold",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.downlinkVSWRThreshold, objectName,"downlinkVSWRThreshold",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.upstreamNoiseSwitch, objectName,"upstreamNoiseSwitch",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"false",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.highThresholdUpstreamNoise, objectName,"highThresholdUpstreamNoise",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.lowThresholdUpstreamNoise, objectName,"lowThresholdUpstreamNoise",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.uplinkNoiseCorrectionValue, objectName,"uplinkNoiseCorrectionValue",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.uplinkNoiseDetectionPar1, objectName,"uplinkNoiseDetectionPar1",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		statToMqtt(doMqtt,DRSStats.Thresholds.uplinkNoiseDetectionPar2, objectName,"uplinkNoiseDetectionPar2",DRSStats.Thresholds.cln,DRSStats.Thresholds.clng,"0",ipAddress,port); 
		helpers.metaObjectComp(drsMqtt, objectName, resourceName, " ","networkDevice", getSubMetaParams());
		helpers.basicVarComp(drsMqtt, objectName, "enable", "true");  
		helpers.basicVarComp(drsMqtt, objectName, "online", "true");
		
		realTimeStatsPolling( siteNum,  unitnum,DRSCommandList.SubObjectVarList.allstats1, objectName, ipAddress, SysUtils.returnInt(port));
		sleep(1000);
		realTimeStatsPolling( siteNum,  unitnum,DRSCommandList.SubObjectVarList.allstats2, objectName, ipAddress, SysUtils.returnInt(port));
		sleep(2000);	
	} 
	
	private static ArrayList<jsonObject[]> getSecondaryInfo() {
		Helpers helpers = new Helpers();
		String[][] siteNumber = {{"groupname",DRSStats.ParametrosRed.clng},{"varname","siteNumber"}};
		String[][] repeaterIpAddress = {{"groupname",DRSStats.ParametrosRed.clng},{"varname","repeaterIpAddress"}};
		ArrayList<jsonObject[]> items = new ArrayList<SysUtils.jsonObject[]>();	
		items.add(helpers.itemObject(siteNumber));
		items.add(helpers.itemObject(repeaterIpAddress));
		return items;
	}
	
	private static ArrayList<jsonObject[]> getSubSecondaryInfo() {
		Helpers helpers = new Helpers();
		String[][] siteNumber = {{"groupname",DRSStats.RealTimePars.clng},{"varname","uplinkInputPower"}};
		String[][] repeaterIpAddress = {{"groupname",DRSStats.RealTimePars.clng},{"varname","downlinkOutputPower"}};
		String[][] repeaterSerialNumber = {{"groupname",DRSStats.InfoDispositivo.clng},{"varname","deviceSerialNumber"}};
		ArrayList<jsonObject[]> items = new ArrayList<SysUtils.jsonObject[]>();	
		items.add(helpers.itemObject(siteNumber));
		items.add(helpers.itemObject(repeaterIpAddress));
		items.add(helpers.itemObject(repeaterSerialNumber));
		return items;
	}
	
	private static ArrayList<jsonObject[]> getcontrols() {
		ArrayList<jsonObject[]> items = new ArrayList<SysUtils.jsonObject[]>();
		jsonObject groupname = new SysUtils().new jsonObject("groupname", "groupnamehere");
		jsonObject varname = new SysUtils().new jsonObject("varname", "varnamehere");		
		jsonObject[] itemObject = {groupname,varname};
		items.add(itemObject);
		return items;
	}
	
	private static ArrayList<jsonArray> getSubMetaParams(){
		ArrayList<jsonArray> metaParams = new ArrayList<SysUtils.jsonArray>();
		jsonArray metaParamsInfo = new SysUtils().new jsonArray("secondaryInfo", getSubSecondaryInfo());
		metaParams.add(metaParamsInfo);
		//jsonArray metaParamsControls = new SysUtils().new jsonArray("secondaryInfo", getcontrols()); //no hay controles en este modulo
		//metaParams.add(metaParamsControls); //no hay controles en este modulo
		return metaParams;
	}
	
	private static ArrayList<jsonArray> getMetaParams(){
		ArrayList<jsonArray> metaParams = new ArrayList<SysUtils.jsonArray>();
		jsonArray metaParamsInfo = new SysUtils().new jsonArray("secondaryInfo", getSecondaryInfo());
		metaParams.add(metaParamsInfo);
		//jsonArray metaParamsControls = new SysUtils().new jsonArray("secondaryInfo", getcontrols()); //no hay controles en este modulo
		//metaParams.add(metaParamsControls); //no hay controles en este modulo
		return metaParams;
	}
}
