package sigmation.main;

import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import sigmation.io.ComLayer;
import sigmation.io.SysCom; 
import sigmation.io.WMXModCom;
import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;

public class FireAlarmSystemB {
	
	static final byte fwappid = (byte)1;
	public static final String thisDeviceTable="alarm_insta";
	public static final String thisDeviceIndex="series";
	static final boolean doBroadcast=true;
	
	private static ArrayList<String[]> AmpList ;
	private static int pingCounter=0;
	
	private static ArrayList<String[]> executions;
	private static int executionCounter=0;
	private static boolean alternateExecutions=false;
	
	public static boolean WMX=false;
	

	
	
	static boolean fsysdebug=true;
	
	//fw data packet info
	static byte fwaddress;
	static byte fwzone;
	static byte fwexecutiontask;
	static byte[] fwdata;
	static byte[] fwdatabuffer;
	static byte[] fwExeidBytes=new byte[4];
	static int fwExeId=0;
	
	static final byte zoneNoAlarm= (byte)0xB0; 
	static final byte zoneAlarm= (byte)0xC1; 
	
	

	
	
	static final byte MasterAddress= (byte)0xff;
	
	private static boolean packetAnalizer(){
		byte[] localdata = getPacketData();
		if(localdata==null){
			resetProtocol();
			return false;
		}
		if(localdata[0]!=(byte)0) {
			resetProtocol();
			return false;
		}
		fwExeId=0;
		fwdata=localdata;
		fwaddress= getCurrentAddress();
		if(SysUtils.byteToUnsignedInt(fwaddress)>255 || SysUtils.byteToUnsignedInt(fwaddress)<1) return false;
		fwzone = getCurrentZone();
		fwdatabuffer= getDataBuffer();
		resetProtocol();
		fwexecutiontask=fwdatabuffer[0];
		if(fwdatabuffer.length>fwExeidBytes.length && isServerExecution()){
			int localLength=fwdatabuffer.length;
			int localExeLength=fwExeidBytes.length;
			int localstartpoint=localLength-localExeLength;
			for(int i=localstartpoint; i<localLength;i++){
				if(i-localstartpoint<localExeLength) fwExeidBytes[i-localstartpoint]=fwdatabuffer[i];
			}
			fwExeId=SysUtils.bytestoInts32(fwExeidBytes)[0];
		}
		
		return true;
	}
	
	private static boolean isServerExecution(){
		if(fwexecutiontask==(byte)0x00 || fwexecutiontask==(byte)0x01){
			return true;
		}
		return false;
	}
	
	private static boolean isRemoteExecution(){
		if(fwexecutiontask==(byte)0xA7){
			return true;
		}
		return false;
	}
	
	private static boolean isAlarmActivation(){
		if(fwexecutiontask==(byte)0xA7 || fwexecutiontask==(byte)0x01){
			return true;
		}
		return false;
	}
	
	private static void alarmStateSet(byte series,byte zone,int statex, int teststate,byte localstate){
		String[] args1= {"series","alarm_state","alarm_s_test","online","zone","state"};
		String[] dat= new String[6];
		
		dat[0]= "'"+series+"'";
		dat[1]= ""+statex;
		dat[2]= ""+teststate;
		dat[3]= "1";
		dat[4]= ""+zone;
		dat[5]= ""+localstate;
		
		String condition = " series= "+dat[0]+" ";
		String table="alarm_insta";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.addUpdate(table, args1,condition, dat);			
		
	}

	private static void alarmZoneStateSet(byte zone,int statex, int teststate,byte localstate){
		String[] args1= {"alarm_state","alarm_s_test","online","zone","state"};
		String[] dat= new String[6];
		
		dat[0]= ""+statex;
		dat[1]= ""+teststate;
		dat[2]= "1";
		dat[3]= ""+zone;
		dat[4]= ""+localstate;
		
		String condition = " zone= "+dat[3]+" ";
		
		String table="alarm_insta";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.update(table, args1,condition, dat);
		
	}
	
	private static boolean getifzoneexist(byte zone){
		String argument= "series";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="alarm_insta";
		int zoneint = zone & 0xff;
		if (cn.select(table, argument, " zone="+zoneint).size()>0) return true;
		else return false;
	}
	
	public static void alarmSetOffline(byte series){
		alarmSetOffline(SysUtils.byteToUnsignedInt(series));	
	}
	
	public static void alarmSetOffline(int series){
		String[] args1= {"series","online"};
		String[] data= new String[2];
		
		data[0]= ""+series;
		data[1]= ""+0;
		
		String condition = " series= '"+data[0]+"' ";
		
		String table="alarm_insta";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.ifExistsUpdate(table, args1,condition, data);			
	}
	
	public static void alarmActivation(byte series,byte zone){
		alarmZoneStateSet(zone,1,0,(byte)0);	
		alarmStateSet(series,zone,1,0,(byte)0); 
	}

	public static void alarmDeactivation(byte series,byte zone){
		alarmStateSet(series,zone,0,0,(byte)0);
		alarmZoneStateSet(zone,0,0,(byte)0); 
	}
	
	public static void getState(byte series,byte zone,byte[] data){
		alarmStateSet(series,zone,0,0,data[1]);
	}
	
	public static void alarmTest(byte series,byte zone){
		alarmStateSet(series,zone,0,1,(byte)0);
	}
	
	public static void refreshState(byte series,byte zone,byte[] data){
		alarmStateSet(series,zone,0,0,data[1]);
	}

	
	private static boolean getEOF(){
		if(WMX){   
			return WMXModCom.eof_received;
		}
		else{ 
			return SysCom.eof_received;
		}
	}
	
	
	private static void exeidDone(int exeid){
		exeidDone(""+exeid);
	}
	
	private static void exeidDone(String exeid){	
		int exeid_num = DatatypeConverter.parseInt(exeid);
		String[] args1= {"state"};
		String[] data= new String[1];
		
		data[0]= "1";
		
		String condition = " exeid= "+exeid_num+" ";
		
		String table="execution";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.addUpdate(table, args1,condition, data);	
	}

	
	private static void serialsend(byte localdest,byte[] localdata, boolean datalength ){
			serialSendData(fwappid, localdest, MasterAddress, localdata.length, localdata); 		
	}
	
	private static void serialSendData(int appid, int dest , int orig, int largodata, byte[] localdata){
		serialSendData(appid,dest ,orig, largodata, localdata, false);
	} 
	
	private static void pingStart(){	 
		ComLayer.pingStart();
	}
	
	private static void deviceInitTime(int dest){
		ComLayer.deviceInitTime(dest); 
	}
	
	private static void pingCheck(){	
		ComLayer.pingCheck();
	}
	
	private static void sendingCheck(){	
		ComLayer.sendingCheck();
	}
	
	private static void checkOffline(){	
		ComLayer.checkOffline();
	}
	
	private static void serialSendData(int appid, int dest , int orig, int largodata, byte[] localdata, boolean broadcast){
		ComLayer.serialSendData(appid, dest, orig, largodata, localdata, broadcast);		
	}
	
	private static byte[] getPacketData(){
		return ComLayer.getPacketData();
	}
	//
	private static byte[] getDataBuffer(){
		return ComLayer.getDataBuffer();
	}
	
	private static byte getCurrentAddress(){
		return ComLayer.getCurrentAddress();
	}
	
	private static byte getCurrentZone(){
		return ComLayer.getCurrentZone();
	}
	
	private static int returnInt(String evaluateMe){
		return SysUtils.returnInt(evaluateMe);
	}
	
	private static void userExecuter(String exeid,String app_id, String task,String value, String statex, String value2){	
		int exe_id = returnInt(exeid);
		int app_idnum = returnInt(app_id);
		int tasknum = returnInt(task);
		int valuenum = returnInt(value);
		int statenum = returnInt(statex);
		int valuenum2 = returnInt(value2);
		byte[] localdata = new byte[] {
	    		(byte)tasknum,
	    		(byte)statenum,
	    		(byte)valuenum2,
	            (byte)(exe_id >>> 24),
	            (byte)(exe_id >>> 16),
	            (byte)(exe_id >>> 8),
	            (byte) exe_id};
		byte localdest = (byte)valuenum;
		switch(app_idnum){
			case 1:	serialsend(localdest,localdata, true );
					break; 		
		}	
	}
	
	private static void getExecutionList(){
		String argument= " exeid,appid,task,value,state ,value_ex1";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="execution";
		executions = cn.select(table, argument, "state=0");
	}
	
	private static boolean executeTurn(){
		alternateExecutions=!alternateExecutions;
		return alternateExecutions;
	}
	
	private static boolean getuserExecutions(){	
		if(!executeTurn()) return false;
		if(executions==null || executions.isEmpty()  || executions.size()==0 ){
			getExecutionList();
			executionCounter=0;
			return false;
		} else if(executionCounter>executions.size()-1){
			getExecutionList();
			executionCounter=0;
			return false;
		}
		
		String[] userexe=executions.get(executionCounter);
		if(fsysdebug) System.out.println("<<<Starting user execution n "+userexe[0]+">>>");
		userExecuter(userexe[0],userexe[1],userexe[2],userexe[3],userexe[4],userexe[5]);
		if(fsysdebug) System.out.println("<<< User executions complete>>>");
		SysUtils.currentTask="User Exe End    ";
		if (executions.size()>0) SysUtils.sysInfo();
		executionCounter++;
		return true;
	}
	
	private static boolean loadDeviceList(){
		if(AmpList==null || AmpList.isEmpty()){
			if(ComLayer.pingNextEnabled){
				getDeviceList();
				pingCounter=0;
			}
			return true;
		} else if(pingCounter>AmpList.size()-1){
			if(ComLayer.pingNextEnabled){
				getDeviceList();
				pingCounter=0;
			}
			return true;
		}
		return false;
	}
	
	private static void getPingExecutions(){	
		pingCheck();											//revisa el tiempo entre ejecuciones de ping, una ronda de pings cada 20 segundos maximo
		if(loadDeviceList()) return;							//si cumple condiciones para volver a cargar lista, o ya cumplio ronda de pings, entonces sale de la funcion
		if(pingCounter==0) pingStart(); 
		String[] statsx=AmpList.get(pingCounter); 				//obtiene datos de ping
		if(fsysdebug) System.out.println("<<<ping execution "+statsx[0]+" >>>");
		userExecuter("0","1","3",statsx[0],statsx[1],"0");					//ping 
		if(fsysdebug) System.out.println("<<<ping execution complete>>>");
		pingCounter++;
		if(pingCounter>AmpList.size()) SysUtils.currentTask="Ping Exe End    ";
		SysUtils.sysInfo();
	}
	
	private static boolean packetExecuter(){
		if(fsysdebug) System.out.println("[executing packet]");
		boolean eof=packetAnalizer();
		if(eof){
			ComLayer.deviceKeepAlive[SysUtils.byteToUnsignedInt(fwaddress)]=false;
			fireSystem(fwdatabuffer,fwaddress,fwzone);
			if(isServerExecution()|| isRemoteExecution()){
				if(isServerExecution()){
					if(fsysdebug) System.out.println("Is server execution "+fwExeId);
					if(fwExeId!=0)exeidDone(fwExeId);
				}
				if(getifzoneexist(fwzone)){
					byte thiscommand= zoneNoAlarm; 
					if(isAlarmActivation()) thiscommand= zoneAlarm; 
					if(fsysdebug) System.out.println("broadcast");
					zoneExecuter(fwappid, SysUtils.byteToUnsignedInt(thiscommand), fwzone);
				}
			}       		 		
		}
		return true;
	}
	
	private static void zoneExecuter(int app_id, int task,int value){	
		byte[] localdata = {(byte)task,(byte)value};  
		byte localdest = (byte)0;
		switch(app_id){  
			case 1:	 try {
				       for(int i=0;i<1;i++){
				    	   serialSendData(fwappid, localdest, MasterAddress, localdata.length, localdata, doBroadcast); 							
							}   
						}catch (Exception e) {
							//e.printStackTrace();
						}
					break; 		
		}	
	}
	
	public static void getDeviceList(){

		String argument= "series,zone";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="alarm_insta";
		AmpList = cn.select(table, argument, " 1 ORDER BY series ");
		if (!AmpList.isEmpty()){
			ComLayer.pingTime= (ComLayer.packetDelay * AmpList.size() * 2);
			ComLayer.offlineTime= 3*ComLayer.pingTime;
			ComLayer.deviceActive=new boolean[255];
			for(int i=0;i< AmpList.size();i++){
				int series = SysUtils.returnInt(AmpList.get(i)[0]);  
				if(series>1)ComLayer.deviceActive[series]=true;
			}
		} else {
			ComLayer.offlineTime=120000;
			ComLayer.pingTime= 20000;
		}
	}
	
	public static void resetProtocol(){
		ComLayer.resetProtocol();
	}
	
	public static void init(boolean wmxb){
		WMX = wmxb;
		ComLayer.init(WMX,thisDeviceTable,thisDeviceIndex,fwappid,MasterAddress);
		loadDeviceList();
	}
	

	
	private static void standby(){
		SysUtils.currentTask="FSYS In Standby  ";
	    SysUtils.sysInfo();
	}

	public static void loop(){
		ComLayer.demo();
		while(!Sigmation.resetSignal){
			try {
				if(getEOF())packetExecuter();  			// si EOF, analiza paquete
				if(!ComLayer.sendingPacket){   			// si no esta enviando algo
					if(!getuserExecutions()) {  		// ejecuta tareas de usuario
							getPingExecutions();  		// si no hay tareas de usuario, realiza ping
					}  
				}
				sendingCheck(); 						// revisa tiempo de envio
				checkOffline(); 						// si pasa mas de 60 segundos para un MLA sin responder, se declara offline
				standby();
			} catch (Exception e) {
				if(fsysdebug)e.printStackTrace();
			}
		}	
	}
	
	
	
	public static void fireSystem(byte[] datax, byte series, byte zone){
		if(fsysdebug) System.out.println("[FW:command]");
		if(fsysdebug) System.out.println(SysUtils.byteToUnsignedInt(datax[0]));
		deviceInitTime(SysUtils.byteToUnsignedInt(series));
		switch(datax[0]){
		    
			// respuestas de ejecucion
			case (byte)0x01: 	alarmActivation(series,zone);
								if(fsysdebug) System.out.println("activated by app "+SysUtils.byteToUnsignedInt(series));
								break;
			case (byte)0x00: 	alarmDeactivation(series,zone);
								break;
			case (byte)0x03: 	//error state
								break;			
			// ejecuciones exclusivas desde MLA			
			case (byte)0xA6: 	alarmTest(series,zone);
								break;
			case (byte)0xA7: 	alarmActivation(series,zone);
								if(fsysdebug) System.out.println("activated by button "+SysUtils.byteToUnsignedInt(series));
								break;
			case (byte)0xA8: 	alarmDeactivation(series,zone);
								break;			
			default		   : 	//refreshState(series,zone,datax);
								break;				
		
		}

		
	}
	
}
