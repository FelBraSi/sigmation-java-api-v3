package sigmation.main;

import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import sigmation.io.SysCom; 
import sigmation.io.WMXModCom;
import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;

@Deprecated
public class FireAlarmSystem {
	
	private static ArrayList<String[]> AmpList ;
	
	static java.util.Date date1= new java.util.Date();
	static java.util.Date date2= new java.util.Date();
	static java.util.Date wakedate1= new java.util.Date();
	static java.util.Date wakedate2= new java.util.Date();
	static long difference = date2.getTime() - date1.getTime();
	static long difference2 = wakedate2.getTime() - wakedate1.getTime();
	public static int fsys_alldevicecheck_shift=1;
	public static boolean WMX=false;
	public static final int timeout = 1000;
	public static final int pingTime= 20000;
	
	private static void alarmStateSet(byte series,byte zone,int statex, int teststate,byte localstate){
		String[] args1= {"series","alarm_state","alarm_s_test","online","zone","state"};
		String[] dat= new String[6];
		
		dat[0]= "'"+series+"'";
		dat[1]= ""+statex;
		dat[2]= ""+teststate;
		dat[3]= "1";
		dat[4]= ""+zone;
		dat[5]= ""+localstate;
		
		String condition = " series= "+dat[0]+" ";
		String table="alarm_insta";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.addUpdate(table, args1,condition, dat);			
		
	}

	private static void alarmZoneStateSet(byte zone,int statex, int teststate,byte localstate){
		String[] args1= {"alarm_state","alarm_s_test","online","zone","state"};
		String[] dat= new String[6];
		
		dat[0]= ""+statex;
		dat[1]= ""+teststate;
		dat[2]= "1";
		dat[3]= ""+zone;
		dat[4]= ""+localstate;
		
		String condition = " zone= "+dat[3]+" ";
		
		String table="alarm_insta";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.update(table, args1,condition, dat);
		
	}
	
	private static boolean getifzoneexist(byte zone){
		String argument= "series";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="alarm_insta";
		int zoneint = zone & 0xff;
		if (cn.select(table, argument, " zone="+zoneint).size()>0) return true;
		else return false;
	}
	
	public static void alarmSetOffline(byte series){
		String[] args1= {"series","online"};
		String[] data= new String[2];
		
		data[0]= ""+series;
		data[1]= ""+0;

		
		String condition = " series= '"+data[0]+"' ";
		
		String table="alarm_insta";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.ifExistsUpdate(table, args1,condition, data);			
		
	}
	
	
	
	public static void alarmActivation(byte series,byte zone){
		alarmZoneStateSet(zone,1,0,(byte)0);	
		alarmStateSet(series,zone,1,0,(byte)0); 
	}

	public static void alarmDeactivation(byte series,byte zone){
		alarmStateSet(series,zone,0,0,(byte)0);
		alarmZoneStateSet(zone,0,0,(byte)0); 
	}
	
	public static void getState(byte series,byte zone,byte[] data){
		alarmStateSet(series,zone,0,0,data[1]);
	}
	
	public static void alarmTest(byte series,byte zone){
		alarmStateSet(series,zone,0,1,(byte)0);
	}
	
	public static void refreshState(byte series,byte zone,byte[] data){
		alarmStateSet(series,zone,0,0,data[1]);
	}

	
	private static boolean getEOF(){
		if(WMX){  
			return WMXModCom.eof_received;
		}
		else{
			return SysCom.eof_received;
		}
	}
	
	public static void init(boolean wmxb){
				WMX = wmxb;
	          	date1= new java.util.Date();
				date2= new java.util.Date();
				wakedate1= new java.util.Date();
				wakedate2= new java.util.Date();
				difference = date2.getTime() - date1.getTime();
				difference2 = wakedate2.getTime() - wakedate1.getTime();
				if(WMX){
					WMXModCom.modbusIni();
				} else {
					SysCom.serialIni();
				}
	}
	
	private static void exeidDone(String exeid){	
		int exeid_num = DatatypeConverter.parseInt(exeid);
		String[] args1= {"state"};
		String[] data= new String[1];
		
		data[0]= "1";
		
		String condition = " exeid= "+exeid_num+" ";
		
		String table="execution";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.addUpdate(table, args1,condition, data);	
	}
	
	private static void userExecuter(String app_id, String task,String value){	
		int app_idnum = DatatypeConverter.parseInt(app_id);
		int tasknum = DatatypeConverter.parseInt(task);
		int valuenum = DatatypeConverter.parseInt(value);
		byte[] localdata = {(byte)tasknum};
		byte localdest = (byte)valuenum;
		switch(app_idnum){
			case 1:	serialsend(localdest,localdata); 
					break; 	
		}	
	}
	
	private static void serialsend(byte localdest,byte[] localdata ){
			serialSendData(1, localdest, 0xFF, 1, localdata); 		
	}
	
	private static void serialsend(byte localdest,byte[] localdata, boolean datalength ){
			serialSendData(1, localdest, 0xFF, localdata.length, localdata); 		
	}
	
	private static void serialSendData(int appid, int dest , int orig, int largodata, byte[] localdata){
		serialSendData(appid,dest ,orig, largodata, localdata, false);
	}
	
	private static void serialSendData(int appid, int dest , int orig, int largodata, byte[] localdata, boolean broadcast){
		if(WMX){
			byte bytedest=0;
			WMXModCom.resetModem();
			if(broadcast) bytedest= (byte)0x00;
			else bytedest =(byte)dest;
			SysCom.output_buffer(appid, bytedest, orig, largodata, localdata);
			if(broadcast) WMXModCom.setBroadcast();
			else WMXModCom.setDestinationInt(dest);
			WMXModCom.modFunction.sendMBD(SysCom.out_buffer);
		}else{
			SysCom.output_buffer(appid, dest, orig, largodata, localdata);
			SysCom.serialSend();	
		}			
	}
	
	private static byte[] getPacketData(){
		if(WMX){
			return SysCom.packetCheck(WMXModCom.getStoredData());
		}
		else{
			return SysCom.packetCheck();
		} 
	}
	//
	private static byte[] getDataBuffer(){
		if(WMX){
			return SysCom.databuffer;
		}
		else{
			return SysCom.databuffer;
		} 
	}
	
	private static byte getCurrentAddress(){
		if(WMX){ 
			return (byte)Integer.parseInt(WMXModCom.source_address);
		}
		else{
			return SysCom.currentAddress;
		} 
	}
	
	private static byte getCurrentZone(){
		if(WMX){
			return SysCom.currentZone;
		}
		else{
			return SysCom.currentZone;
		} 
	}
	 
	
	private static int returnInt(String evaluateMe){
		int eval =0;
		if(evaluateMe==null){
			return 0;
		} else{
			try {
				eval= DatatypeConverter.parseInt(evaluateMe);
			} catch (Exception e) {
				return 0;
			}
		}	
		return eval;
	}
	
	private static void userExecuter(String app_id, String task,String value, String statex, String value2){	
		int app_idnum = returnInt(app_id);
		int tasknum = returnInt(task);
		int valuenum = returnInt(value);
		int statenum = returnInt(statex);
		int valuenum2 = returnInt(value2);
		byte[] localdata = {(byte)tasknum,(byte)statenum,(byte)valuenum2};
		byte localdest = (byte)valuenum;
		switch(app_idnum){
			case 1:	serialsend(localdest,localdata, true );
					break; 		
		}	
	}
	
	private static void getuserExecutions(){	
		String argument= " exeid,appid,task,value,state ,value_ex1";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="execution";
		ArrayList<String[]> executions = cn.select(table, argument, "state=0");
		if(executions==null | executions.isEmpty()){return;}
		System.out.println("<<<Starting user executions>>>");
		SysUtils.currentTask="User Exe Start";
	    if (executions.size()>0) SysUtils.sysInfo();
		for(int i=0; i<executions.size();i++){	
			
			String[] userexe= executions.get(i);
			int j=0;
			for (j=0;j<1;j++){
				java.util.Date date1= new java.util.Date();
				java.util.Date date2= new java.util.Date();
				long difference = date2.getTime() - date1.getTime();
				userExecuter(userexe[1],userexe[2],userexe[3],userexe[4],userexe[5]);
				while(!getEOF() && difference<timeout){
					date2= new java.util.Date();
					difference = date2.getTime() - date1.getTime();
				}
				boolean eof = getEOF();
				if(eof){
					byte localaddress = getCurrentAddress();
					byte localzone = getCurrentZone();
					byte[] localdata = getPacketData();
					byte[] localdatabuffer = getDataBuffer();
					if(localdata[0]!=(byte)0) eof=false;
					if(eof){						
						 System.out.println("<answer received>");
						 fireSystem(localdatabuffer,localaddress,localzone);
						 if(localdatabuffer[0]==(byte)0x01 || localdatabuffer[0]==(byte)0x00 || localdatabuffer[0]==(byte)0xA7 ){
								if(localdatabuffer[0]==0xA7) localdatabuffer[0]=(byte)0x01;
								byte ccommand= localdatabuffer[0];
								resetProtocol(); 
								if(getifzoneexist(localzone))zoneExecuter("1", ""+(int)ccommand, ""+localzone);
								else {
										System.out.println("\n attempt to write an unexistant alarm "+localzone);
										try {
											Thread.sleep(10000);
										} catch (Exception e) {
											// TODO: handle exception
										}
								}
						}
						System.out.println("< "+userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" completed>");
			            SysUtils.currentTask=userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" SC     ";
			            SysUtils.sysInfo();
						if(localdatabuffer[0]!=(byte)0x03)exeidDone(userexe[0]);
						int valuenum = DatatypeConverter.parseInt(userexe[3]);
						if(localaddress==(byte)valuenum) {
							                       j=3; 
							                       System.out.println("< "+userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" completed>");
							                       SysUtils.currentTask=userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" SC     ";
							                       SysUtils.sysInfo();
							                       if(localdatabuffer[0]!=(byte)0x03) exeidDone(userexe[0]);
												   }  // si respuesta es del dispositivo esperado, continua con el siguiente
						else if(j ==2){
							System.out.println(" CA: "+getCurrentAddress()+" - DA: "+valuenum);
							alarmSetOffline((byte)i); //si van 3 intentos sin respuesta del dispositivo, registra offline
							System.out.println("< "+userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" failed> ");
							SysUtils.currentTask=userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" FL     ";
							SysUtils.sysInfo();
						}
					}
					else if(j==2){
						alarmSetOffline((byte)i); //si van 3 intentos sin respuesta del dispositivo, registra offline
						System.out.println("< "+userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" failed> ");
						SysUtils.currentTask=userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" FL     ";
						SysUtils.sysInfo();
					}
				} else if(j==2){
					alarmSetOffline((byte)i); //si van 3 intentos sin respuesta del dispositivo, registra offline
					System.out.println("< "+userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" failed> ");
					SysUtils.currentTask=userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" FL     ";
					SysUtils.sysInfo();
				}
				
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) { 
					e.printStackTrace();
					SysUtils.currentTask=">ER 0x008       ";
					  SysUtils.sysInfo();
				}
			}
			
			if(j==1){
				alarmSetOffline((byte)i); //si van 3 intentos sin respuesta del dispositivo, registra offline
				System.out.println("< "+userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" failed> ");
				SysUtils.currentTask=userexe[0]+" "+userexe[1]+" "+userexe[2]+" "+userexe[3]+" FL     ";
				SysUtils.sysInfo();
			}
			
		}
		System.out.println("<<< User executions complete>>>");
		SysUtils.currentTask="User Exe End    ";
		if (executions.size()>0) SysUtils.sysInfo();
	}
	
	private static void packetExecuter(){
		boolean eof=true;
		byte localaddress = getCurrentAddress();
		byte localzone = getCurrentZone();
		byte[] localdata = getPacketData();
		byte[] localdatabuffer = getDataBuffer();
		if(localdata[0]!=(byte)0) eof=false;
		if(eof){
			switch(localdata[1]){
			case 1:	fireSystem(localdatabuffer,localaddress,localzone);
					if(localdatabuffer[0]==(byte)0x01 || localdatabuffer[0]==(byte)0x00 || localdatabuffer[0]==(byte)0xA7 ){
						if(localdatabuffer[0]==(byte)0xA7) localdatabuffer[0]=(byte)0x01;
						byte thizone=localzone;
						byte thiscommand= localdatabuffer[0]; 
						zoneExecuter("1", ""+(int)thiscommand, ""+thizone);
					}    
					resetProtocol();     
					break; 		
			}  
		}		
	}
	
	private static void zoneExecuter(String app_id, String task,String value){	
		int app_idnum = DatatypeConverter.parseInt(app_id);
		int tasknum = DatatypeConverter.parseInt(task);
		int valuenum = DatatypeConverter.parseInt(value);
		byte[] localdata = {(byte)tasknum,(byte)valuenum};  
		byte localdest = (byte)0;
		switch(app_idnum){  
			case 1:	 try {
				       for(int i=0;i<1;i++){
				    	   serialSendData(1, localdest, 0xFF, localdata.length, localdata, true); 
								Thread.sleep(100);								
							}   
						}catch (Exception e) {
							e.printStackTrace();
						}
					break; 		
		}	
	}
	
	public static void getDeviceList(){

		String argument= "series";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="alarm_insta";
		AmpList = cn.select(table, argument, " 1 ORDER BY series ");
	}
	
	private static void fsys_pingAllDevices(){	
		/**
		 * En este modo de ejecucion
		 * Entra en modo de pregunta>respuesta
		 * intenta 3 veces por dispositivo
		 * espera cada ves por 300 ms
		 * checkea en cada recepcion si
		 * paquete es del dispositivo
		 * esperado
		 * 
		 * */
		getDeviceList();
		for(int k=0; k<AmpList.size();k++){
			getuserExecutions();
			int j=0;
			for (j=0;j<1;j++){
				String[] statsx=AmpList.get(k);
				java.util.Date date1= new java.util.Date();
				java.util.Date date2= new java.util.Date();
				long difference = date2.getTime() - date1.getTime();
				userExecuter(""+1,""+3,statsx[0]);
				while(!getEOF() && difference<timeout){
					date2= new java.util.Date();
					difference = date2.getTime() - date1.getTime();
				}
				boolean eof = getEOF();
				if(eof){
					byte localaddress = getCurrentAddress();
					byte localzone = getCurrentZone();
					byte[] localdata = getPacketData();
					byte[] localdatabuffer = getDataBuffer();
					if(localdata[0]!=(byte)0) eof=false;
					if(eof){ 
						switch(localdata[1]){
						case 1:	fireSystem(localdatabuffer,localaddress,localzone); 
									if(localdatabuffer[0]==(byte)0x01 || localdatabuffer[0]==(byte)0x00 || localdatabuffer[0]==(byte)0xA7 ){
									    if(localdatabuffer[0]==0xA7) localdatabuffer[0]=(byte)0x01;	
										byte ccommand= localdatabuffer[0];
										zoneExecuter("1", ""+(int)ccommand, ""+localzone);
									}			
						}	
						if(localaddress==(byte)(Integer.parseInt(statsx[0]))) {j=3; 
												   System.out.println("<Device "+Integer.parseInt(statsx[0])+" is Online>");
												   SysUtils.currentTask="<"+Integer.parseInt(statsx[0])+" Online>  ";
												   SysUtils.sysInfo(); 
						}  // si respuesta es del dispositivo esperado, continua con el siguiente
												   
						else if(j==2){
							alarmSetOffline((byte)Integer.parseInt(statsx[0])); //si van 3 intentos sin respuesta del dispositivo, registra offline
							System.out.println("<Device "+Integer.parseInt(statsx[0])+" is Offline>");
							SysUtils.currentTask="<"+Integer.parseInt(statsx[0])+" Offline>    ";
							SysUtils.sysInfo();
						}
					}
					else if(j==2){
						alarmSetOffline((byte)Integer.parseInt(statsx[0])); //si van 3 intentos sin respuesta del dispositivo, registra offline
						System.out.println("<Device "+Integer.parseInt(statsx[0])+" is Offline>");
						SysUtils.currentTask="< "+Integer.parseInt(statsx[0])+" Offline>    ";
						SysUtils.sysInfo();
					}
				}
				
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) { 
					e.printStackTrace();
					SysUtils.currentTask=">ER 0x009       ";
					SysUtils.sysInfo();
				}
			} 
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) { 
				e.printStackTrace();
				SysUtils.currentTask=">ER 0x00A       ";
				  SysUtils.sysInfo();
			}

		}
	}
	
	public static void resetProtocol(){
		if(WMX){
			WMXModCom.resetProtocol();
			SysCom.resetProtocol();
		}else{
			SysCom.resetProtocol();
		}
	}
	
	public static void loop(){
		getDeviceList();
		date2= new java.util.Date(); 
	    difference = date2.getTime() - date1.getTime(); 
	    boolean eof = getEOF();
  	    if(eof){ 
  	    	packetExecuter();
  	    	resetProtocol();
  	    } else {
  	    	getuserExecutions();
  	    	SysUtils.currentTask="FSYS In Standby  ";
			    SysUtils.sysInfo();
  	    }  
  	    	date1= new java.util.Date();
  	    	date2= new java.util.Date();
  	    	System.out.println("<<<<  Starting All Devices Checking >>>>");
  	    	SysUtils.currentTask="All Devices Chck";
  	    	SysUtils.sysInfo();
  	    	fsys_pingAllDevices(); 
  	    	System.out.println("<<<<  All Devices Checking Finished >>>>");
  	    	SysUtils.currentTask="Devc. Chck ps   ";
  	    	SysUtils.sysInfo(); 
        while(!Sigmation.resetSignal) {
            try { 
            	date2= new java.util.Date(); 
        	    difference = date2.getTime() - date1.getTime(); 
        	    eof = getEOF();
          	    if(eof){ 
          	    	packetExecuter();
          	    	resetProtocol();
          	    } else {
          	    	getuserExecutions();
          	    	SysUtils.currentTask="FSYS In Standby  ";
					    SysUtils.sysInfo();
          	    } 
          	  if(difference>pingTime){
          	    	date1= new java.util.Date();
          	    	date2= new java.util.Date();
          	    	System.out.println("<<<<  Starting All Devices Checking >>>>");
          	    	SysUtils.currentTask="All Devices Chck";
          	    	SysUtils.sysInfo();
          	    	fsys_pingAllDevices(); 
          	    	System.out.println("<<<<  All Devices Checking Finished >>>>");
          	    	SysUtils.currentTask="Devc. Chck ps   ";
          	    	SysUtils.sysInfo();
          	  }
          	Thread.sleep(100);
            } 
            catch(Exception ex){
                ex.printStackTrace();
                SysUtils.currentTask=">ER 0x004       ";
  			    SysUtils.sysInfo();
            }
            
        }
        
    } 
	
	
	public static void fireSystem(byte[] datax, byte series, byte zone){
		System.out.println("[FW:command]");
		System.out.println((int)datax[0]);
		switch(datax[0]){
		    
			// respuestas de ejecucion
			case (byte)0x01: 	alarmActivation(series,zone);
								break;
			case (byte)0x00: 	alarmDeactivation(series,zone);
								break;
			case (byte)0x03: 	//error state
								break;			
			// ejecuciones exclusivas desde MLA			
			case (byte)0xA6: 	alarmTest(series,zone);
								break;
			case (byte)0xA7: 	alarmActivation(series,zone);
								break;
			case (byte)0xA8: 	alarmDeactivation(series,zone);
								break;			
			default		   : 	//refreshState(series,zone,datax);
								break;				
		
		}

		
	}
	
}
