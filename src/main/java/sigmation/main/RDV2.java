package sigmation.main;
 
import java.util.ArrayList; 
import javax.xml.bind.DatatypeConverter;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.StopBits;

import sigmation.io.SystemIO;
import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;
import sigmation.modbus.ModCom;



public class RDV2 {
	
	public static ArrayList<String[]> DeviceList;
	public static ArrayList<String[]> module_config;	
	public static int discovery_max = 0;
	public static int discovery_min = 0;
	public static int[][] pollingList;
	public static int[][] discoveryList;
	public static int[][][] maxminList = new int[25][15][2];
	public static int pollingCounter=0;
	public static boolean pollingFlag=false;
	public static String status="Sigmation";
	public static int p_counter=0;
	final static int p_counter_max=3;
	public static int d_counter=0;
	final static int d_counter_max=250;
	public static boolean discovery=false;
	public static boolean modbuserror=true;
	
	
	public static void MBTest(){
		int[] mydata  = {0x0a0b,0x0c0d,0x0102};
		int[] mydata2 = {0x0a0b,0x0c0d,0x0102, 0x0202};
		ModCom.modFunction.f1((byte)0x11, 0x0013, 0x0025);
		ModCom.modFunction.f2((byte)0x12, 0x00A3, 0x000A);
		ModCom.modFunction.f3((byte)0x13, 0x00A3, 0x000A);
		ModCom.modFunction.f4((byte)0x14, 0x00A3, 0x000A);
		ModCom.modFunction.f5((byte)0x15, 0x00A3, true);
		ModCom.modFunction.f6((byte)0x16, 0x00A3, 0x000A);
		ModCom.modFunction.f15((byte)0x15, 0x00A3, 0x000A, mydata );
		ModCom.modFunction.f16((byte)0x16, 0x00A3, 0x000A, mydata2);
		ModCom.modFunction.f3((byte)0x11, 0x0013, 0x0025);
		
	}
	
	public static int[] meassureCodes(){
		int[] codes = {0,0};
		
		int type= ModCom.ResponseData[14];  
		System.out.println("\n\n-------- CODES -------\n");
		for (int i=1; i<14; i++){
			int[] minmax = maxminList[type][i];
			
			if (type!=9){
				if(i==1 || i==2 || i==4){
					codes=doublerange(minmax[0],minmax[1],i,codes);
				}else {
					codes=simplerange(minmax[0],minmax[1],i,codes);
				}
			} else {
				if(i==3 || i==5){
					codes=doublerange(minmax[0],minmax[1],i,codes);
				}else {
					codes=simplerange(minmax[0],minmax[1],i,codes);
				}
				
			}
			
		}
			
		System.out.println("\n\n-------- END CODES -------\n");
		return codes;
	}
	
	public static int[] doublerange(int minvalue, int maxvalue, int valueindex, int[] lastCode){
		int[] localcode = lastCode;
		int localvalue = ModCom.ResponseData[valueindex];
		if (localvalue>=127){  //positive range
			if(localvalue>maxvalue) {localcode[1] = localcode[1]+ ((int) Math.pow(2, valueindex)); System.out.println("\n\n doublerange positive "+localvalue+" > "+maxvalue+" -----\n");}
			if(localvalue<minvalue) {localcode[0] = localcode[0]+ ((int) Math.pow(2, valueindex)); System.out.println("\n\n doublerange positive "+localvalue+" < "+minvalue+" -----\n");}
		} else {  //negative range
			if (maxvalue>=127) maxvalue = 127-maxvalue;
			if (minvalue<0) minvalue = 0 - minvalue;
			if(localvalue>minvalue) {localcode[0] = localcode[0]+ ((int) Math.pow(2, valueindex)); System.out.println("\n\n doublerange negative "+localvalue+" > "+minvalue+" -----\n");}
			if(localvalue<maxvalue) {localcode[1] = localcode[1]+ ((int) Math.pow(2, valueindex)); System.out.println("\n\n doublerange negative "+localvalue+" < "+maxvalue+" -----\n");}
		}
		return localcode;
	}
	
	public static int[] simplerange (int minvalue, int maxvalue, int valueindex, int[] lastCode){
		int[] localcode = lastCode;
		int localvalue = ModCom.ResponseData[valueindex];
		if(localvalue>maxvalue) {localcode[1] = localcode[1]+((int) Math.pow(2, valueindex)); System.out.println("\n\n simplerange "+localvalue+" > "+maxvalue+" -----\n");}
		if(localvalue<minvalue) {localcode[0] = localcode[0]+((int) Math.pow(2, valueindex)); System.out.println("\n\n simplerange "+localvalue+" < "+minvalue+" -----\n");}
		return localcode;
	}
	
	public static void setup(){		
		String[] sDiscovery_max=module_config.get(SysUtils.returnArrayIndex(module_config,"discovery_max"));
		discovery_max=(int)DatatypeConverter.parseFloat(sDiscovery_max[1]);	
		String[] sDiscovery_min=module_config.get(SysUtils.returnArrayIndex(module_config,"discovery_min"));
		discovery_min=(int)DatatypeConverter.parseFloat(sDiscovery_min[1]);
		System.out.println("\nDiscovery min - max : "+discovery_min+" - "+discovery_max); 		
	}
	
	public static void packetdebug(){
		
		System.out.println("\n\n---------------\n");
		System.out.println("packet received");
		System.out.print("function:");
		SysUtils.printbyte2hex(ModCom.iFunction);
		System.out.print("\naddress:");
		SysUtils.printbyte2hex(ModCom.iAddress);
		System.out.print("\nDatasize:");
		SysUtils.printbyte2hex((byte)ModCom.iDataSize);
		System.out.println("\n---------------\n");
	}

	private static void checkError(){
		System.out.println("\nModbus Error Check command - error");
		SysUtils.printbyte2hex((byte)(ModCom.iFunction - (byte)0x80));
		SysUtils.printbyte2hex(ModCom.iError);
		ModCom.resetProtocol();
		modbuserror=true;
	}
	
	private static int generateTables(int mbaddress, int mbid){
		
		String[] args1= {"modbus_address","id"};
		int offset = 0;
		MySQL cn = new MySQL();
		cn.database="ampdb";
		String table="device_insta";	
		cn.connect();
		
		if(cn.exists(table, "id="+mbid)){
			return 0;
		} else {
			while ((offset+mbaddress)<250){
				if(cn.exists(table, "modbus_address="+(offset+mbaddress))){
					if(cn.exists(table, "mapid = 0 AND modbus_address="+(offset+mbaddress))){
						String[] data= {""+(offset+mbaddress),""+mbid};
						cn.update(table, args1, " mapid = 0 AND modbus_address="+(offset+mbaddress), data);
						return offset+1;
					} else {
						offset++;
					}
				} else {
					String[] data= {""+(offset+mbaddress),""+mbid};
					cn.insert(table, args1, data);
					return offset+1;
				}
			}
			
		}	
		
		return offset;
	}
	
	public static void getConfig(){
		MySQL cn = new MySQL();
		cn.database="config";
		cn.connect();
		String table="module_config";
		String argument= "param,value";
		module_config = cn.select(table, argument, " module='rd' ");
		setup();
	}
	
	public static void getPollingList(){
		pollingList = new int[DeviceList.size()][3];  // modbusAddress, ID, type
		for(int i=0; i<DeviceList.size();i++){
			String[] address= DeviceList.get(i);
			pollingList[i][0] = Integer.parseInt(address[0]);
			pollingList[i][1] = Integer.parseInt(address[1]);
			pollingList[i][2] = Integer.parseInt(address[2]); 
		}
	}
	
	public static void getDeviceList(){

		String argument= "modbus_address, ID, type";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="device_insta";
		DeviceList = cn.select(table,argument," preserve = 1 OR mapid>0 ");
		if(DeviceList==null | DeviceList.isEmpty()){
			//do something
			sysInfo("No act.dev.found");
			System.out.println("\nNo active devices found");
			p_counter=4;
		} else {
			//do something
			//System.out.println("\nfound something!");
			getPollingList();
			
		}
	}	
	
	public static void setMinMax(ArrayList<String[]> maxminListx){
		for(int i=0; i<maxminListx.size();i++){
			String[] localvar= maxminListx.get(i);
			int type =  Integer.parseInt(localvar[15]);
			String sValue = localvar[16];
			int tValue = 0;
			if (sValue.contentEquals("min")) tValue = 0;
			else tValue = 1;
			for(int j=0;j<15;j++){
				if(localvar[j]!=null) maxminList[type][j][tValue] = Integer.parseInt(localvar[j]);
				else if (tValue==0) maxminList[type][j][tValue] = -99999999;
				else if (tValue==1) maxminList[type][j][tValue] = 99999999; 
				//if(localvar[j]!=null) System.out.print("\n> "+type+" -  "+j+" - "+sValue+"  :"+localvar[j]+" -> "+ maxminList[type][j][tValue]);
			}
			System.out.print("\n");
		}
		//System.out.println("> maxmin: "+Arrays.deepToString(maxminList));
	}
	
	public static void getMinMax(){
		String table="device_type_stats";
		String tn = table;
		String argument= tn+".0,"+tn+".1,"+tn+".2,"+tn+".3,"+tn+".4,"+tn+".5,"+tn+".6,"+tn+".7,"+tn+".8,"+tn+".9,"+tn+".10,"+tn+".11,"+tn+".12,"+tn+".13,"+tn+".14, type, value ";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect(); 
		ArrayList<String[]> maxminList = cn.select(table, argument, " 1 ");
		if(maxminList==null | maxminList.isEmpty()){
			//do something
			System.out.println("\nNo active devices found");
		} else {
			//do something
			//System.out.println("\nfound something!");
			setMinMax(maxminList);
			
		}
	}	
	
	public static void getDiscoveryList(){
		discoveryList = new int[DeviceList.size()][4];  // modbusAddress, ID, type
		for(int i=0; i<DeviceList.size();i++){
			String[] address= DeviceList.get(i);
			discoveryList[i][0] = Integer.parseInt(address[0]);
			discoveryList[i][1] = Integer.parseInt(address[1]);
			discoveryList[i][2] = Integer.parseInt(address[2]); 
			discoveryList[i][3] = Integer.parseInt(address[3]); 
		}
	}
	
	public static void getAllDeviceList(){

		String argument= "modbus_address, ID, type, preserve";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="device_insta";
		DeviceList = cn.select(table, argument, " 1 ");
		if(DeviceList==null | DeviceList.isEmpty()){
			//do something
			sysInfo("No dev. found");
			System.out.println("\nNo devices found");
		} else {
			//do something
			//System.out.println("\nfound something!");
			getDiscoveryList();
			
		}
	}	
	
	private static void showdata(){
		System.out.println("\ngot data");
		for(int i=0; i<ModCom.ResponseData.length;i++){
			System.out.println(">"+ModCom.ResponseData[i]);
		}
	}
	
	private static boolean getconfigRefresh(){
		return SysUtils.getconfigRefresh();
	}
	
	private static void saveDevice(){
		//for Sigma SA systems
		if(ModCom.iDataSize==0x1E){
			showdata();
			String table="device_insta";
			String tn= table;
			int[] codes = meassureCodes();
			int codesum = codes[0]+codes[1];
			
			String[] args1= {"modbus_address","id","online","preserve",tn+".0",tn+".1",tn+".2",tn+".3",tn+".4",tn+".5",tn+".6",tn+".7",tn+".8",tn+".9",tn+".10",tn+".11",tn+".12",tn+".13",tn+".14", "code_min" , "code_max"};
			MySQL cn = new MySQL();
			cn.database="ampdb";
				
			cn.connect();
			String condition = " modbus_address="+ModCom.iAddressInt();
			String[] data= {""+ModCom.iAddressInt(),""+ModCom.ResponseData[0] ,""+1,""+1,""+ModCom.ResponseData[0],""+ModCom.ResponseData[1],""+ModCom.ResponseData[2],""+ModCom.ResponseData[3],""+ModCom.ResponseData[4],""+ModCom.ResponseData[5],""+ModCom.ResponseData[6],""+ModCom.ResponseData[7],""+ModCom.ResponseData[8],""+ModCom.ResponseData[9],""+ModCom.ResponseData[10],""+ModCom.ResponseData[11],""+ModCom.ResponseData[12],""+ModCom.ResponseData[13],""+ModCom.ResponseData[14], ""+codes[0], ""+codes[1]};
			cn.addUpdate(table, args1, condition, data);
			
			table="device_history";
			tn= table;
			String[] args2= {"id","online",tn+".0",tn+".1",tn+".2",tn+".3",tn+".4",tn+".5",tn+".6",tn+".7",tn+".8",tn+".9",tn+".10",tn+".11",tn+".12",tn+".13",tn+".14"};
			String[] data2= {"(SELECT id FROM device_insta WHERE modbus_address="+ModCom.iAddressInt()+")",""+1,""+ModCom.ResponseData[0],""+ModCom.ResponseData[1],""+ModCom.ResponseData[2],""+ModCom.ResponseData[3],""+ModCom.ResponseData[4],""+ModCom.ResponseData[5],""+ModCom.ResponseData[6],""+ModCom.ResponseData[7],""+ModCom.ResponseData[8],""+ModCom.ResponseData[9],""+ModCom.ResponseData[10],""+ModCom.ResponseData[11],""+ModCom.ResponseData[12],""+ModCom.ResponseData[13],""+ModCom.ResponseData[14]};
			cn.insert(table, args2, data2);
			
			if(codesum>0){
				
				table="device_alarms"; 
				String[] args3 = {"history_id","code_min","code_max","id"};
				String[] data3 = new String[4];
				data3[0]= " (SELECT MAX(history_id) FROM device_history) ";
				data3[1]= ""+codes[0];
				data3[2]= ""+codes[1];
				data3[3]= " (SELECT id FROM device_insta WHERE modbus_address="+ModCom.iAddressInt()+") ";
				cn.insert(table, args3, data3);
				
			}	
			
			
		}
	}
	
	private static void reportDeviceOffline(){
		String[] args1= {"modbus_address","online"};
		MySQL cn = new MySQL();
		cn.database="ampdb";
		String table="device_insta";	
		cn.connect();
		String condition = " modbus_address="+(ModCom.mAddressInt());
		String[] data= {""+ModCom.mAddressInt(),""+0};
		cn.update(table, args1, condition, data);
		pollingFlag =false;
		SystemIO.yellowWarning.high();
	}
	
	private static void packetReceived(){
		
		packetdebug();
		
		if(ModCom.gotResponse()){
			System.out.println("\n Package is a response to last sent command \n");
			SystemIO.yellowWarning.low();
			//save results here
		} else {
			System.out.println("\n Package is NOT a response to last sent command, probably a request \n");
			//process request here
		}
		
		switch(ModCom.iFunction){
			
			case 	(byte)0x03	:	//save data;
									saveDevice();
									break;								
		}
		
		ModCom.resetProtocol();
		
	}
	
	private static void idToTable(int busaddress, int productType, int unitId){
		int[] intdata = {productType , unitId }; 
		ModCom.modFunction.f16((byte)busaddress, 0, 2, intdata);
	}
	
	
	
	public static void idTableFill(){ 
		/*
		 * Fills up the PIC table for Modbus commands
		 * 
		 * Entry is formed by:   product type (16 bits) - Unit ID (16 bits)
		 * 
		 * Function used is 0x10 (16) 
		 * 
		 * Modbus Data address is not documented for this
		 * DataAddress=2*n (2n=> product type, 2n+1= unit ID) is assumed, 
		 * where n is the Modbus address number for the corresponding Device in the table
		 * 
		 * Data Address > Modbus info
		 * 
		 * 0 > Modbus 0 product type
		 * 1 > Modbus 0 unit ID
		 * 2 > Modbus 1 product type
		 * 3 > Modbus 1 unit ID
		 * .
		 * .
		 * 2n   > Modbus n product type
		 * 2n+1 > Modbus n unit ID
		 * .
		 * .
		 * */
		System.out.println("\nFilling ID Table...");
		sysInfo("Filling Table");
		
		try {
			for (int i=0; i<discoveryList.length;i++){
				while(ModCom.onHold){ModCom.checkMBT();}
				int busaddress = discoveryList[i][0];
				int productType =discoveryList[i][2];
				int unitId=discoveryList[i][1];
				idToTable(busaddress, productType, unitId);
				System.out.println("\n"+i);
			}
		} catch (Exception e) {
			SystemIO.redCritical.high();
			e.printStackTrace();
		}
	}
	
	private static void requestData(byte fAddress){
		ModCom.modFunction.f3(fAddress, 0, 15);		
	}
	
	public static void polling(){
		 	
		if(!ModCom.onHold){
			pollingFlag =true;
			if(p_counter<3){
				discovery=false;
				int pollingsize = 0;
				if (pollingList!=null) pollingsize=pollingList.length;
				if(pollingCounter<pollingsize ){
					byte fAddress=(byte)pollingList[pollingCounter][0]; ///iterar
					requestData(fAddress);
					int intfAddress= (fAddress & 0xFF);
					status = "polling "+intfAddress+"    ";
					pollingCounter++;
					p_counter++;
				} else {
					//check if list changed
					getDeviceList();//if yes, load
					pollingCounter=0; // and reset counter
				}
			} else {
				p_counter=0;
				discovery=true;
				if(d_counter>250 || d_counter>(discovery_max-discovery_min)) d_counter=0;
				requestData((byte)d_counter);
				status = "Discovery "+d_counter+"  ";
				d_counter++;
			}
			
		}	
		
	}
	
	public static void checkBuffer(){
		if(ModCom.eof_received){
			System.out.println("EOF");
			switch(ModCom.iFunction){
				
				case (byte)0x01		:	packetReceived();	
										break;
				case (byte)0x02		:	packetReceived();
										break;
				case (byte)0x03		:	packetReceived();	
										break;
				case (byte)0x04		:	packetReceived();	
										break;
				case (byte)0x05		:	packetReceived();	
										break;
				case (byte)0x06		:	packetReceived();	
										break;
				case (byte)0x0F		:	packetReceived();	
										break; 
				case (byte)0x10		:	packetReceived();	
										break;
				case (byte)0x81		:	checkError();	
										break;
				case (byte)0x82		:	checkError();	
										break;
				case (byte)0x83		:	checkError();	
										break;
				case (byte)0x84		:	checkError();	
										break;
				case (byte)0x85		:	checkError();	
										break;
				case (byte)0x86		:	checkError();	
										break;
				case (byte)0x8F		:	checkError();	
										break; 
				case (byte)0x90		:	checkError();	
										break;								
			
			}
		}		
	}
	
	private static void sysInfo(String message){
		SystemIO.sysInfo(message);
	}
	
	private static void ifoffline(){
		if(modbuserror && ModCom.iError==(byte)0x0C){
			modbuserror=false;
			if(!discovery) reportDeviceOffline();
		}
		else if(ModCom.checkMBT()){
			if(!discovery) reportDeviceOffline();
		}
	}
	
	private static void generateTable(){
		getConfig();
		getMinMax();
		setup();
		int iterateID= discovery_min;
		int maxID= discovery_max;
		int offset =0;
		System.out.println("\nGenerating tables...");
		
		String[] args1= {"mapid"};
		String[] data= new String[2];
		data[0]="1";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table = "device_insta";
		cn.deleteLess(table, args1, data);
		sysInfo("Gen. Tables     ");
		
		for(int i = 0 ; i <249 && iterateID<=maxID; i=i+offset){
			offset=generateTables(i,iterateID);
			iterateID++;
			System.out.println(""+i+" , "+iterateID+" , "+maxID);
		}
		System.out.println("\nGenerating tables Finished");
	} 
	
	public static void loop(){
		sysInfo("   Iniciando    ");
		getConfig();
		getMinMax();
		ModCom.modbusIni( Baud._19200, DataBits._8, Parity.EVEN, StopBits._1, FlowControl.NONE);
		generateTable();
		getAllDeviceList();
		idTableFill();
		getDeviceList();		
		System.out.println("\nID Table Ready");
		sysInfo("Table Ready");
		while(!Sigmation.resetSignal){
			if(getconfigRefresh()){
				generateTable();
				getAllDeviceList();
				idTableFill();  
				getDeviceList();
			}
			checkBuffer();
			ifoffline();
			polling();
			sysInfo(status); 
		}
	}
}
