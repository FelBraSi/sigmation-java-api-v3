package sigmation.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import sigmation.io.ComLayer;
import sigmation.io.SysCom;
import sigmation.io.WMXModCom;
import sigmation.utils.Definitions;
import sigmation.utils.MySQL;
import sigmation.utils.SysUtils; 
import sigmation.mail.SendEmail;
import sigmation.modbus.ModbusApp; 

/*
 *  Tagging and Tracking class
 *  Sigma telecom
 *  Felipe Bravo silva
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * */


public class TNT {
	
	/**
	 * ///////////////////////////////////////////////////////////////////////////
	 * 																			//
	 * 		SYSTEM FUNCTIONS, DONT CHANGE THIS UNLESS YOU KNOW WHAT YOU         //
	 *      ARE DOING.		                                                    //
	 *      -FBS-																//
	 * 																			//
	 * ///////////////////////////////////////////////////////////////////////////
	 *
	 *
	 * 	serverResquestsTag: server request the first tag available to register,
	 * 						if there's one, this remote server will respond with 
	 * 						tag data, tag index (in this remote server) and a flag 
	 * 						indicating if there are more tags available.
	 *  
	 *  serverNextTag	  : Server requests another tag, adding the last registered
	 *  					tag, so this server can mark it as registered and not 
	 *  					as available, until a new reading.
	 *  					If there's a tag, this remote server will respond with 
	 * 						tag data, tag index (in this remote server) and a flag 
	 * 						indicating if there are more tags available.
	 * 
	 * 	serverAck		  :	First command sent to open communication, this includes
	 * 						the current system's time stamp. This remote server
	 * 						will use the time stamp provided to set up its time, 
	 * 						and will answer with a flag indicating if there are
	 * 						tags available to send. USED ON RF MODE ONLY.
	 * 
	 * */
	
		static final byte serverResquestsTag   	= Definitions.TNT.serverResquestsTag;
		static final byte serverNextTag	  	 	= Definitions.TNT.serverNextTag;
		static final byte serverNextBusTag	  	= Definitions.TNT.serverNextBusTag;
		static final byte serverPing		  	= Definitions.TNT.serverPing;
		static final byte serverBusTag			= Definitions.TNT.serverBusTag; 
		
		// Same commands but received through Ethernet, the system acts differently when working through Ethernet
		static final byte serverResquestsTag_R  = Definitions.TNT.serverResquestsTag_R;
		static final byte serverNoTag_R		  	= Definitions.TNT.serverNoTag_R;
		static final byte serverPing_R		  	= Definitions.TNT.serverPing_R;  
		static final byte serverBusTag_r		= Definitions.TNT.serverBusTag_R; 
		
		static final byte thisAppid				= Definitions.TNT.thisAppid;
		
		static final byte MasterAddress			= Definitions.TNT.MasterAddress;
		
		public static final String thisDeviceTable= Definitions.TNT.thisDeviceTable;
		public static final String thisDeviceIndex= Definitions.TNT.thisDeviceIndex;
		
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   System variables								//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */	
		 
	public static int currentAddress=-1;
	public static int historyCount=0;
	public static boolean[] polling= new boolean[254];          //for every possible address, a bit indicates if it's registered on the database
	public static int[] pollingSerial= new int[254]; 
	public static boolean dailyCleanStart=false, alarmx=false;
	public static Calendar lastClean = Calendar.getInstance();
	public static int pollingIndex=0;
	public static boolean savedata=false;
	public static int savecount=0;
	public static int dataStorageFrequency=2;
	public static boolean next=true;
	public static boolean poll=false;
	public static int app_id=3;
	private static ModbusApp tagApp;
	public static byte[] tempdata;
	public static int maxcount=4;
	
	public static ArrayList<String[]> module_config;
	
	public static boolean WMX=false;
	public static boolean debugThis=false;
	
	private static ArrayList<String[]> readersList ;
	private static ArrayList<String[]> BusTagList;
	
	private static int pingCounter=0;
	
	private static byte currentAddressByte=0;

	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   Module Timers  								//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */	
	//settings in seconds
	private static long busTagCheckTimeSeconds= 60;						
	private static long updateTurnTimerSeconds= 30;  
	private static long alertsPersonasCheckTimeSeconds= 50; 
	private static long fillStaticSearchCheckTimeSeconds=60;
	//translations, do not change
	private static final long busTagCheckTime=busTagCheckTimeSeconds*1000;		
	private static final long updateTurnTimer=updateTurnTimerSeconds*1000;  
	private static final long alertsPersonasCheckTime=alertsPersonasCheckTimeSeconds*1000;
	private static final long fillStaticSearchCheckTime=fillStaticSearchCheckTimeSeconds*1000;
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   DATAPACKET variables							//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	 */	
	
	//fw data packet info
		static byte TNTaddress;
		static byte TNTzone;
		static byte TNTexecutiontask;
		static byte[] TNTdata;
		static byte[] TNTdatabuffer; 
		
	
	private static boolean packetAnalizer(){
		byte[] localdata = getPacketData();
		if(localdata==null){
			if(debugThis) System.out.println("[localdata null!]");
			resetProtocol();
			return false;
		} 
		TNTdata=localdata;
		TNTaddress= getCurrentAddress();
		if(SysUtils.byteToUnsignedInt(TNTaddress)>255 || SysUtils.byteToUnsignedInt(TNTaddress)<0) {
			if(debugThis) System.out.println("TNTAddress= "+SysUtils.byteToUnsignedInt(TNTaddress)+"!");
			return false;
		}
		TNTzone = getCurrentZone();
		TNTdatabuffer= getDataBuffer();
		resetProtocol();
		TNTexecutiontask=TNTdatabuffer[0]; 
		
		return true;
	}
	
	public static void resetProtocol(){
		ComLayer.resetProtocol();
	}
	
	private static byte[] getPacketData(){
		return ComLayer.getPacketData();
	}
	//
	private static byte[] getDataBuffer(){
		return ComLayer.getDataBuffer();
	}
	
	private static byte getCurrentAddress(){
		return ComLayer.getCurrentAddress();
	}
	
	private static byte getCurrentZone(){
		return ComLayer.getCurrentZone();
	}

	
	
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   TIMERS           							//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	*/	 
	
		public static void deleteGarbageIdStart(){
			//Timer timer = new Timer();
			//timer.schedule(new deleteGarbageId(), 0, 1500);
			//causa problemas
		}
		
		public static void updateTurnStart(){
			Timer timer = new Timer();
			
			timer.schedule(new UpdateTurn(), 0, updateTurnTimer); 
		}
	
		public static void busTagCheckStart(){
			Timer timer = new Timer();
			timer.schedule(new TNTBusCheck(), 0, busTagCheckTime);
		}
		
		
		public static void UpdateAlertsPersonasStart(){
			Timer timer = new Timer();
			timer.schedule(new UpdateAlertsPersonas(), 0, alertsPersonasCheckTime);
		} 
		
		public static void FillStaticSearchStart(){
			Timer timer = new Timer();
			timer.schedule(new FillStaticSearch(), 0, fillStaticSearchCheckTime);
		} 		 
		
		private static class TNTBusCheck extends TimerTask {
			
			public void run() { 
				processBusTag2(); 
				/*
				while(getAvailableBusTags(true,false)){
					
					String[] tagFrame= BusTagList.get(0);				
					String id		= tagFrame[0];
					String tagnum	= tagFrame[1];
					String readerid = tagFrame[2];   
					String tmstmp	= tagFrame[3];	
					String readernum= tagFrame[4];
					processBusTag(tagnum, readerid, tmstmp,readernum); 
				}			
				*/
			}
		}
		
		public static void onlineCheckStart(){
			Timer timer = new Timer();
			timer.schedule(new onlineCheck(), 0, 60000);
		}
		
		private static class onlineCheck extends TimerTask {
			
			public void run() { 
				checkOnlineReaders();
			}
		}
		
		private static class UpdateTurn extends TimerTask {
			
			public void run() { 
				if(debugThis) System.out.println("[updating turn]");
				updateTurn();
			}
		}
		
		private static class UpdateAlertsPersonas extends TimerTask {
			
			public void run() { 
				updateAlertsPersonas();
			}
		}
		
		private static class FillStaticSearch extends TimerTask {
			
			public void run() { 
				fillStaticSearch();
			}
		}
		
		
	/**
	 * ///////////////////////////////////////////////////////////
	 * 															//
	 * 			   FUNCTIONS          							//
	 * 															//
	 * ///////////////////////////////////////////////////////////
	*/			
		
		
	private static boolean updateTurn(){
		try {  
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "currentturnget";
			String[] data= {"''"};  
			cr.connect();  
			cr.callVoidProcedure(call, data);
			cr.close(); 
				return true; 
		} catch (Exception e) {
				return false;
		}			
	} 
	
	private static boolean fillStaticSearch(){
		try {  
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "get_list_personas_search_backend";
			String[] data= {"''"};  
			cr.connect();  
			cr.callVoidProcedure(call, data);
			cr.close(); 
				return true; 
		} catch (Exception e) {
				return false;
		}
			
	}
	
	private static boolean updateAlertsPersonas(){
		try {  
			MySQL cr = new MySQL();
			cr.database="ampdb";		
			String call = "get_alerts_personas_flags_backend";
			String[] data= {"''"};  
			cr.connect();  
			cr.callVoidProcedure(call, data);
			cr.close(); 
				return true; 
		} catch (Exception e) {
				return false;
		}
			
	}
	
	private static boolean checkOnlineReaders(){
		try {
				String[] argument= {"online"}; 
				MySQL cr = new MySQL(); 
				cr.database="ampdb";
				cr.connect(); 
				String table="rfidreaders";  
				String[] data= {"0"};  
				String condition = " last_mod< (CURRENT_TIMESTAMP -INTERVAL 2 MINUTE ) AND isethernet";
				cr.update(table, argument, condition, data); 
				cr.close(); 
				return true; 
		} catch (Exception e) {
				return false;
		}
			
	}
		
	public static void init(boolean rs485Ctrl, boolean wmxb){ 
		WMX = wmxb; 
		ComLayer.init(WMX,thisDeviceTable,thisDeviceIndex,thisAppid,MasterAddress); 
		busTagCheckStart(); 
		updateTurnStart();  
		UpdateAlertsPersonasStart(); 
		FillStaticSearchStart(); 
		loadDeviceList(); 
		SendEmail.init(); 
		loop(); 
	} 
	
	
	public static void getDeviceList(){
		if(debugThis) System.out.println("[loading device list]");
		ComLayer.packetDelay=1000;
		String argument= "idrfidreaders,serial,isethernet"; 
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="rfidreaders";
		readersList = cn.select(table, argument, " isethernet=0 ORDER BY serial ");
		try {
			if (!readersList.isEmpty()){
				ComLayer.pingTime= (ComLayer.packetDelay * readersList.size() * 2);
				ComLayer.offlineTime= 3*ComLayer.pingTime;
				ComLayer.deviceActive=new boolean[255];
				for(int i=0;i< readersList.size();i++){
					int series = SysUtils.returnInt(readersList.get(i)[1]);
					if(series>0)ComLayer.deviceActive[series]=true;
				}
			} else {
				ComLayer.offlineTime=120000;
				ComLayer.pingTime= 20000;
			}
		} catch (Exception e) {
			ComLayer.offlineTime=120000;
			ComLayer.pingTime= 20000;
		} 
		if(debugThis) {if(readersList!=null && !readersList.isEmpty()) System.out.println("[Found "+readersList.size()+" Devices]"); else System.out.println("[No devices found]");}
		cn.close(); 
	}
	

	
	public static void sendTagRequest(){
		
		byte[] localdata = {(byte)0x00};
		if(!tagApp.WMXprotocol){
			SysCom.output_buffer(app_id, currentAddress, 0xFF, localdata.length, localdata);
			SysCom.serialSend();
		}
		else{
			
		}
	}  

	
	private static void saveTag(String tagnum, int tagZone, int readernum, int tmstmp){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "registro_tag";
		cr.connect(); 
		String[] data= {"'"+tagnum+"'", Integer.toString(tagZone), Integer.toString(readernum)};  
		cr.callValueProcedure(call,data);
		cr.close(); 
	}	
	
	public static void processBusTag(String tagnum, String ip, String tmstmp, String readernum){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "processBusTagId";
		cr.connect(); 
		String[] data= {""+tmstmp, "'"+ip+"'","'"+tagnum+"'", "'"+readernum+"'"};  
		cr.callVoidProcedure(call,data);
		cr.close();
	}	
	
	private static void processBusTag2(){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "processbustag2";
		cr.connect();  
		cr.callVoidProcedure(call);
		cr.close();
	}	
	
	public void saveBusEPC(String tagnum, String client,String zone, String reader){  		 
		MySQL cn = new MySQL();
		cn.database="ampdb";					
		cn.connect(); 
		String procedure="saveTagBus";
		int score = 1; 
		String[] argument= {"'"+tagnum+"'", "'"+client+"'", ""+score+""};
		cn.callVoidProcedure(procedure, argument); 
		cn.close();
	}
	
	
	private static void saveBusTag(String tagnum, String readerId, int readernum, int tmstmp){
		MySQL cr = new MySQL();
		cr.database="ampdb";		
		String call = "saveTagBus";
		cr.connect(); 
		String[] data= {"'"+tagnum+"'", readerId, ""+1,Integer.toString(readernum), Integer.toString(tmstmp)};  
		cr.callValueProcedure(call,data);
		cr.close();
	}		
	
	public int confirmData(String localaddress){
		String localSource = WMXModCom.source_address;

		if(localSource==localaddress) {
			tempdata = WMXModCom.databuffer;
			return 100;
		}  
		else  {
			return 0;
		}		
	}

	public static void getTagReadings(){
		
		java.util.Date date1= new java.util.Date();
		java.util.Date date2= new java.util.Date();
		long difference = date2.getTime() - date1.getTime();
		
		for (int i=0; i < 3 ; i++){
			sendTagRequest();
			while(SysCom.eof_received==false && difference<300){
				date2= new java.util.Date();
				difference = date2.getTime() - date1.getTime();
			}
			if(SysCom.eof_received){
				//saveTags(SysCom.databuffer);
			}else{
				
			}
		}	
	}

	public static void saveTag(int tagnum, int zona, int reader, int tm){
		
		String[] args1= {""+tagnum,""+zona,""+reader,""+tm};	
		
		String procedure="registro_tag_tm";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.callVoidProcedure(procedure, args1);	
		cn.close();
	}
	 
	private static boolean packetExecuter(){
		if(debugThis) System.out.println("[executing packet]");
		boolean eof=packetAnalizer();
		if(eof){
			if(debugThis) System.out.println("[valid packet]");
			ComLayer.deviceKeepAlive[SysUtils.byteToUnsignedInt(TNTaddress)]=false;
			TNTSystem(TNTdatabuffer,TNTaddress,TNTzone); 		 		
		}
		else if(debugThis) System.out.println("[Bad packet]");
		return true;
	}
	
	private static boolean getEOF(){
		if(WMX){   
			return WMXModCom.eof_received;
		}
		else{ 
			return SysCom.eof_received;
		}
	}
	
	private static void sendingCheck(){	
		ComLayer.sendingCheck();
	}
	
	private static void checkOffline(){	
		ComLayer.checkOffline();
	}
	
	private static void standby(){
		SysUtils.currentTask="TNT In Standby  ";
	    SysUtils.sysInfo();
	}
	
	private static boolean extendPingTime(){

		if(currentAddressByte==TNTaddress) {
			if(debugThis) System.out.println("[Extended device "+currentAddressByte+" time ]");
			ComLayer.sendingExtend();
			return true;
		}
		return false;
	}
	
	private static boolean loadDeviceList(){
		if(readersList==null || readersList.isEmpty()){
			if(ComLayer.pingNextEnabled){
				
				getDeviceList();
				pingCounter=0;
			}
			return true;
		} else if(pingCounter>readersList.size()-1){
			if(ComLayer.pingNextEnabled){
				getDeviceList();
				pingCounter=0;
			}
			return true;
		}
		return false;
	}
	
	private static void pingCheck(){	
		ComLayer.pingCheck();
	}
	
	private static void pingStart(){	 
		ComLayer.pingStart();
	}
	
	private static void deviceInitTime(int dest){
		ComLayer.deviceInitTime(dest); 
		ComLayer.deviceSetOnline((byte)dest);
	}
	
	private static void ping(byte dest){	  
		ComLayer.sendingPacket=true;
		byte[] command = {serverPing}; 
		byte[] tmByte 	= SysUtils.getCurrentTimeStampBytes();
		byte[] allData	= new byte[command.length+tmByte.length];
		System.arraycopy(command , 0, allData	, 0			, command.length	);
		System.arraycopy(tmByte, 0, allData	, command.length, tmByte.length	);
		ComLayer.serialsend(dest,allData);
	}
	
	private static void getPingExecutions(){	
		pingCheck();											//revisa el tiempo entre ejecuciones de ping, una ronda de pings cada 20 segundos maximo
		if(loadDeviceList()) return;							//si cumple condiciones para volver a cargar lista, o ya cumplio ronda de pings, entonces sale de la funcion
		if(pingCounter==0) pingStart(); 
		String[] statsx=readersList.get(pingCounter); 				//obtiene datos de ping
		if(debugThis) System.out.println("<<<ping execution "+statsx[1]+" >>>");
		currentAddressByte=(byte)SysUtils.returnInt(statsx[1]); 
		ping(currentAddressByte);
		if(debugThis) System.out.println("<<<ping execution complete>>>");
		pingCounter++;
		if(pingCounter>readersList.size()) SysUtils.currentTask="Ping Exe End    ";
		SysUtils.sysInfo();
	}
	
	
	public static void loop(){
		//ComLayer.demo();		
		onlineCheckStart();
		while(!Sigmation.resetSignal){
			try {
				if(getEOF())packetExecuter();  			// si EOF, analiza paquete
				if(!ComLayer.sendingPacket){   			// si no esta enviando algo
					getPingExecutions();  				// si no hay tareas de usuario, realiza ping
				}
				sendingCheck(); 						// revisa tiempo de envio
				checkOffline(); 						// si pasa mas de 60 segundos para un MLA sin responder, se declara offline
				standby();
			} catch (Exception e) {
				if(debugThis)e.printStackTrace();
			}
		}	
	}
	

	public static boolean getAvailableBusTags(boolean limitOne, boolean groupByReader){  
		String argument= "id, tagnum, readerid, UNIX_TIMESTAMP(last_mod), tagreaderserial ";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="busrfid";
		String limitString=" ";
		String groupString=" ";
		if(limitOne) limitString=" LIMIT 1 ";
		if(groupByReader) groupString=" GROUP BY readerid ";
		BusTagList = cn.select(table, argument, "  score>0 AND last_mod < CURRENT_TIMESTAMP - INTERVAL 1 MINUTE "+groupString+ limitString );
		cn.close();
		if(BusTagList==null) return false;
		else if(BusTagList.isEmpty()) return false;
		else return true;
	}
	
	

	
	
	private static class TNTServerTasks{
		static byte[] datax;
		
		/**
		 * Estructura data
		 * 
		 * Transmision:
		 * 	-Ping
		 * 		data[0]= command (serverPing)
		 * 		data[1(MSB)][2][3][4(LSB)]= Timestamp
		 *  
		 *  -Request tag (serverResquestsTag)
		 *  	data[0]=command
		 *  
		 *  -Next tag
		 *  	data[0]=command (serverNextTag) 
		 *  	data[1(MSB)][2][3][4(LSB)]= Last Tag index
		 *  
		 * Recepcion:
		 * 	-send Tag
		 * 		data[0]=command (serverResquestsTag_R)
		 * 		data[1(MSB)]...[24(LSB)]= Tag number
		 * 		data[25(MSB)][26][27][28(LSB)]= Timestamp
		 * 		data[29(MSB)][30][31][32(LSB)]= Tag index
		 * 		data[33]=zone
		 * 		data[34(MSB)][35(LSB)]= Tags left
		 * 
		 *  -No tags
		 *  	data[0]=command (serverNoTag_R)  
		 */
		
		public static void serverResquestsTag_R(){
			if(debugThis) System.out.println("[TNT: got tags from device "+SysUtils.byteToUnsignedInt(TNTaddress)+"]");
			extractTagData();			
		}
		
		public static void serverNoTag_R(){ 
			//no hacer nada, fin de las tareas con este dispositivo
			if(debugThis) System.out.println("[TNT: no more tags on device "+SysUtils.byteToUnsignedInt(TNTaddress)+"]");
		}
		
		public static void serverPing_R(){ 
			//para posibles aplicaciones, no usado por ahora
			if(debugThis) System.out.println("[TNT: got ping answer]");
		}
		
		public static void serverBusTag_R(){ 
			//para posibles aplicaciones, no usado por ahora
			if(debugThis) System.out.println("[TNT: got tags from device "+SysUtils.byteToUnsignedInt(TNTaddress)+"]");
			extractBusTagData();
		}
		
		public static void extractTagData(){ 
			//local buffers
			if(datax.length>35){
				if(debugThis) System.out.println("[TNT: recived "+datax.length+" bytes]");
				byte[] tagNumB 	= Arrays.copyOfRange(datax, 1, 25);
				byte[] timeB	= Arrays.copyOfRange(datax, 25, 29);
				byte[] tagIndexB= Arrays.copyOfRange(datax, 29, 33);
				byte   zoneB 	= datax[33];
				//byte[] tagsleftB= Arrays.copyOfRange(datax, 34, 36);
				
				String tagnum 	= new String(tagNumB);
				int	 timestamp	= SysUtils.bytestoInts32(timeB)[0]; 
				int	 tagindex	= SysUtils.bytestoInts32(tagIndexB)[0]; 
				//int  tagsleft	= SysUtils.bytestoInts16(tagsleftB)[0];
				int  zona 		= SysUtils.byteToUnsignedInt(zoneB);
				int  reader		= SysUtils.byteToUnsignedInt(TNTaddress);
				
				if(debugThis) {
					System.out.println("[TNT: tag-index "+tagindex+"]");
					System.out.println("[TNT: tag "+tagnum+"]");
					Date datex = new Date ();
					datex.setTime((long)timestamp*1000);
					System.out.println("[ ("+timestamp+") "+datex.toString()+"]");
				}
				if(!Sigmation.noPiMode)saveTag(tagnum, zona, reader, timestamp);
				
				serverNextTag(tagIndexB);
			} else if(debugThis) System.out.println("[TNT: not enough arguments, data size too low - "+datax.length+" bytes]");
			
		}
		
		public static void extractBusTagData(){ 
			//local buffers
			if(datax.length>35){
				if(debugThis) System.out.println("[TNT: recived "+datax.length+" bytes]");
				byte[] tagNumB 	= Arrays.copyOfRange(datax, 1, 25);
				byte[] timeB	= Arrays.copyOfRange(datax, 25, 29);
				byte[] tagIndexB= Arrays.copyOfRange(datax, 29, 33);
				byte[] readerId = Arrays.copyOfRange(datax, 33, 37); 
				byte[] tagsleftB= Arrays.copyOfRange(datax, 37, 39);
				
				String tagnum 	= new String(tagNumB);
				String readerIdS= ""+SysUtils.bytestoInts32(readerId)[0]; 
				int	 timestamp	= SysUtils.bytestoInts32(timeB)[0]; 
				int	 tagindex	= SysUtils.bytestoInts32(tagIndexB)[0]; 
				int  reader		= SysUtils.byteToUnsignedInt(TNTaddress);
				int  tagsleft	= SysUtils.bytestoInts16(tagsleftB)[0]; 
				
				
				if(debugThis) {
					System.out.println("[TNT: tag-index "+tagindex+"]");
					System.out.println("[TNT: tag "+tagnum+"]");
					System.out.println("[TNT: busreader "+readerIdS+"]");
					System.out.println("[TNT: tagsleft "+tagsleft+"]");
					Date datex = new Date ();
					datex.setTime((long)timestamp*1000);
					System.out.println("[ ("+timestamp+") "+datex.toString()+"]");
				}
				if(!Sigmation.noPiMode)saveBusTag(tagnum, readerIdS, reader, timestamp);
				
				serverNextTag(tagIndexB,true);
			} else if(debugThis) System.out.println("[TNT: not enough arguments, data size too low - "+datax.length+" bytes]");
			
		}
		
		public static void serverNextTag(byte[] lastIndex){
			serverNextTag(lastIndex, false);
		}
		
		public static void serverNextTag(byte[] lastIndex, boolean busTag){
			if(extendPingTime()){
				if(debugThis) System.out.println("[TNT: Sending new request to "+SysUtils.byteToUnsignedInt(TNTaddress)+"]");
				byte[] command 	= {serverNextTag}; 		
				if(busTag) command[0]=serverNextBusTag; 
				byte[] allData	= new byte[command.length+lastIndex.length];
				int[] lengths ={command.length};
				System.arraycopy(command , 0, allData	, 0			, command.length	);
				System.arraycopy(lastIndex, 0, allData	, lengths[0], lastIndex.length	); 
				ComLayer.serialsend(TNTaddress, allData);
			} else if(debugThis) System.out.println("[TNT: Received not from "+SysUtils.byteToUnsignedInt(currentAddressByte)+"]");
			
		}		
	}
	
	public static void TNTSystem(byte[] datax, byte series, byte zone){
		if(debugThis) System.out.println("[TNT:command "+SysUtils.byteToUnsignedInt(datax[0])+"]");
		deviceInitTime(SysUtils.byteToUnsignedInt(series));
		TNTServerTasks.datax=datax;
		switch(datax[0]){
			// respuestas de ejecucion
			case serverResquestsTag_R	: 	TNTServerTasks.serverResquestsTag_R();
											break;
			case serverNoTag_R			: 	TNTServerTasks.serverNoTag_R();
											break;
			case serverPing_R			: 	TNTServerTasks.serverPing_R();
											break; 
			case serverBusTag_r			: 	TNTServerTasks.serverBusTag_R();
											break; 
			default		   				: 	if(debugThis) System.out.println("[TNT: "+SysUtils.byteToUnsignedInt(datax[0])+" is not a valid command]");
											break;				
		
		}

		
	}

}
