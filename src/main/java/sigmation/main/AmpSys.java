package sigmation.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import sigmation.io.LcdDevice;
import sigmation.io.SysCom;
import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;


public class AmpSys {

	public static byte[] c_address;
	public static int historyCount=0;
	public static ArrayList<String[]> AmpList;
	public static ArrayList<String[]> sys_config;	
	public static boolean current=true;
	public static int currentAddress=0;
	public static int highAddress= 		0b1111000000000000;
	public static int fullAddress= 	    0b0000001111111111;
	public static int highestAddress= 	highAddress+fullAddress;
	public static int highAddressMask= 	0b1111110000000000;
	public static boolean[] polling= new boolean[1024];
	public static boolean dailyCleanStart=false, alarmx=false;
	public static Calendar lastClean = Calendar.getInstance();
	public static int pollingIndex=0;
	public static boolean savedata=false;
	public static int savecount=0;
	public static int dataStorageFrequency=2;
	public static boolean next=true;
	public static boolean poll=false;
	
	
	
	//prevents the storage of irrelevant data
	//saves data only after certain frequency
	//or if data is relevant (errors, low/high measures and such)
	//saves database memory
	public static boolean savedataAsk(){
		if (savedata) {
			savedata=false;
			return true;
		} else if(savecount>=dataStorageFrequency){
			return true;
		}
		return false;
	}
	
	public static void registerOfflineAmp(){
		
		if(c_address!=null) System.out.println("<"+DatatypeConverter.printHexBinary(new byte[] {c_address[0],c_address[1]})+" OFFLINE>");
		else return;
		
			String[] data= new String[3];
			//data[0]="'"+DatatypeConverter.printHexBinary(new byte[] {c_address[0],c_address[1]})+"'";
			//System.out.println(data[0]);
			
			LcdDevice.setTask(DatatypeConverter.printHexBinary(new byte[] {c_address[0],c_address[1]})+" OFFLINE   ");
			data[0]="(SELECT id FROM amp_insta WHERE ampid='"+DatatypeConverter.printHexBinary(new byte[] {c_address[0],c_address[1]})+"')";
			data[1]="0";			
			data[2]="UNIX_TIMESTAMP()";
			String[] args1= {"ampid","online","time"};
			String[] args2= {"ampid","online"};
			MySQL cn = new MySQL();
			cn.database="ampdb";
			cn.connect();
			String table="amp_history";
			//if(savedataAsk()){			
			if(((historyCount & 1) == 1 )& c_address!=null) cn.insert(table, args1, data);
			//}
			data[0]="'"+DatatypeConverter.printHexBinary(new byte[] {c_address[0],c_address[1]})+"'";
			table="amp_insta";
			cn.addUpdate(table, args2, args2[0]+"="+data[0]+" ", data);	
	}
	
	public static void getAmpList(boolean restart){
		historyCount++;
		if(historyCount>10) historyCount=0;
		String argument= "ampid";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="amp_insta";
		String min= Integer.toHexString(highAddress);
		String max= Integer.toHexString(highestAddress);
		AmpList = cn.select(table, argument, " ampid>='"+min+"' and ampid<='"+max+"' ");
		if(AmpList==null | AmpList.isEmpty()){currentAddress=-1;}
		else if (currentAddress==-1 | restart) currentAddress=0;
		setPolling();
		cn = new MySQL();
		cn.database="config";
		cn.connect();
		table="sys_config";
		argument= "parameter,value";
		sys_config = cn.select(table, argument, " 1=1 ");
	}	
	
	public static void setPolling(){
		for(int i=0; i<AmpList.size();i++){
			String[] address= AmpList.get(i);
			int intaddress = Integer.parseInt(address[0],16);
			intaddress= intaddress -highAddress;
			polling[intaddress]=true;
		}
		
	}
	
	public static byte[] nextAmp(){
		
		current=false;
		if(currentAddress==-1 | currentAddress>= AmpList.size()) return null;
		String[] address= AmpList.get(currentAddress);
		currentAddress++;
		byte[] byteAddress= DatatypeConverter.parseHexBinary(address[0]);
		return byteAddress;
	}

	public static String nextAmpS(){
		if(currentAddress==-1 | currentAddress>= AmpList.size()) return null;
		String[] address= AmpList.get(currentAddress);
		currentAddress++;
		String StringAddress= address[0];
		return StringAddress;
	}
	


	public static void iniPolling(){
		Arrays.fill(polling, false);
	}
	
	public static void setalarm(float[] value, String[] data){
		alarmx=false;
		String[] datax = new String[2];
		float[] minpar= new float[5];
		float[] maxpar= new float[5];
		String alarmdetail= new String();
		String[] names= {"Vcc","outc","ampc","inc","gain"};
		for(int i=0;i<5;i++){
			datax=sys_config.get(SysUtils.returnArrayIndex(sys_config,names[i]+"max"));
			maxpar[i]=DatatypeConverter.parseFloat(datax[1]);
			datax=sys_config.get(SysUtils.returnArrayIndex(sys_config,names[i]+"min"));
			minpar[i]=DatatypeConverter.parseFloat(datax[1]);
		}
			
		for(int i=0;i<5;i++){
			if( value[i] < minpar[i] | value[i] > maxpar[i]  ){
				alarmx=true;
				String comp= new String();
				if( value[i] < minpar[i]) comp= "<"+minpar[i];
				else comp= " > "+maxpar[i];
				alarmdetail = alarmdetail + ""+names[i]+"="+value[i]+comp+" |"; 
			}
			
		}
		
		if(alarmx) alarm(1, data[0], alarmdetail);		
	}
	
	public static void dailyClean(){
		if (!dailyCleanStart) {	dailyCleanStart=true;
								lastClean = Calendar.getInstance();
								SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
								Date date= lastClean.getTime();
								System.out.println("<Old Data cleanin started at "+format1.format(date)+">");
								try {
									lastClean.add(Calendar.DAY_OF_MONTH, -1);
								} catch (Exception e) {
									System.out.println(e);
								}
							}
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		long currentTimeStamp = (cal.getTimeInMillis())/1000; //seconds
		long lastTimeStamp = (lastClean.getTimeInMillis())/1000; //seconds
		long timeDif = currentTimeStamp -lastTimeStamp;
		if (timeDif>82800 & hour == 00 ){ //timediff > 23 hours and it's 00:00 -> 00:59
			System.out.println("2>");
			cal.add(Calendar.YEAR, -1);
			long last_year=(cal.getTimeInMillis()/1000);
			//Delete all data older than 1 year
			String[] args1= {"time"};
			String[] data= new String[1];
			data[0]=String.valueOf((int)last_year);
			MySQL cn = new MySQL();
			cn.database="ampdb";
			cn.connect();
			String table = "amp_history";
			cn.deleteLess(table, args1, data);
			lastClean = Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			Date date= lastClean.getTime();
			System.out.println("<Data history clean at "+format1.format(date)+">");
			table = "alerts";
			cn.deleteLess(table, args1, data);	
			System.out.println("<alarms clean at "+format1.format(date)+">");
		}

		
	}
	
	public static void alarm(int tipo, String id, String cause){
		String[] args1= {"time","tipo","id","cause","al_state","history_id"};
		String[] data= new String[6];
		
		data[0]="UNIX_TIMESTAMP()";
		data[1]= Integer.toString(tipo);
		data[2]= id;
		data[3]= "'"+cause+"'";
		data[4]="0";
		data[5]="(SELECT MAX(ind) FROM amp_history)+1";
		
		String table="alerts";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.insert(table, args1, data);
		savedata=true;		
	}	
	
	public static String[] convData(String[] data, boolean save){
		float[] value= new float[5];
		value[0]= SysUtils.h2v(SysCom.buffer[6])*10;  //voltaje
		value[0] = SysUtils.cutDec(value[0] );
		data[1] = Float.toString(value[0] );
		
		value[1]= ((79*(float)22.5)-((float)SysCom.buffer[7]*22));     //corriente salida
		value[1] = SysUtils.cutDec(value[1]);
		data[2] = Float.toString(value[1]);
		
		value[2]= (((float)SysCom.buffer[8]*(float)11.74825175)-(float)(86*11.74825175)-(float)(11.74825175/2));	//corriente equipo
		value[2] = SysUtils.cutDec(value[2]);
		//if (value[2]<0) value[2]=0;
		data[3] = Float.toString(value[2]);
		
		value[3]= (((float)SysCom.buffer[9]*22)-(79*22));  //corriente entrada
		value[3] = SysUtils.cutDec(value[3]);
		data[4] = Float.toString(value[3]);
		
		value[4]= (SysUtils.h2v(SysCom.buffer[10])*((float)12.2449))+3;  //ganancia
		value[4] = SysUtils.cutDec(value[4]);
		data[5] = Float.toString(value[4]);
		
		if(save)setalarm(value, data);
		return data;		
	}
	
	public static int printvals(byte[] toprint, int size){
		int i=0;
		String[] data= new String[7];
		data[0]="";   		
		
		data=convData(data,false);	
		
		for (i=1;i<6;i++){     

    		System.out.print(data[i]  + " ["+(int)SysCom.buffer[5+i]+"] ");
    		if(i==5){
    					System.out.print("\r\n");
    					return 0;
    		}}
		return 0;
	}
	
}
