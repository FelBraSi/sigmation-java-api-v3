package sigmation.tagTcp;
import java.net.*;
import java.io.*;
import java.math.BigInteger;
import javax.xml.bind.DatatypeConverter;

import sigmation.remoteServer.*;
import sigmation.main.Sigmation;

public class PacketHandler {

	static Socket localSocket = null;
	static DataOutputStream os = null;
    static DataInputStream is = null;
    String localsname;
    int localp;
    static BufferedOutputStream out;
    static boolean tx = false;
	


    /** 
     * Packethandler( String ip/url, int port number);
     * default timeout is 20 ms.
     * Packet is init() at creation, you don't need to init() the object unless
     * you close() it.
     * use Packethandler( String ip/url, int port number, int timeout )
     * to setup a custom timeout in ms.
     * Or use the setTimeOut functions to set up the timeout in
     * seconds, milliseconds, microseconds or nanoseconds. 
     * */


    	private boolean initialized = false;
        private int debug_level =0;
        private static boolean debugThis= true;
        private int system_type=0;
        
        private String noinitMessage= "PacketHandler must be init() first";
        
        static byte[] databuffer;
        private static boolean data_received=false;
        
        private static long start_time = 0;
        private static long deltatime_ns = 1000 * 1000000;	//default value for timeout, *1000000 if milliseconds, default = 20 milliseconds
        
        /*----PROTOCOL ----*/
        byte header = (byte)0xbb;
        byte tail1 = (byte)0x0d;
        byte tail2 = (byte)0x0a;
        
        /** 
    	 * TcpIpHandler( String ip/url, int port number);
    	 * default timeout is 20 ms.
    	 * use TcpIpHandler( String ip/url, int port number, int timeout )
    	 * to setup a custom timeout in ms.
    	 * Or use the setTimeOut functions to set up the timeout in
    	 * seconds, milliseconds, microseconds or nanoseconds.
    	 * This function opens a new connection
    	 * unless the port is in use, or there are connection problems
    	 * */ 
        
        /** 
    	 * TcpIpHandler( String ip/url, int port number, int timeout )
    	 * This function opens a new connection
    	 * unless the port is in use, or there are connection problems. 
    	 * Timeout is in ms.
    	 * You can also use setTimeOut functions to set up the timeout in
    	 * seconds, milliseconds, microseconds or nanoseconds. 
    	 * */
        
        public static boolean gotData(){
        	return data_received;
        }
        
        public static byte[] getData(){
        	data_received=false;
        	return databuffer;
        }
        
        /** Set timeout in seconds */
        public void setTimeOut_s(int seconds){
        	
        	deltatime_ns = seconds * 1000000000;
        }
        
        /** Set timeout in milliseconds */
        public void setTimeOut_ms(int milliseconds){
        	
        	deltatime_ns = milliseconds * 1000000;
        }
        
        /** Set timeout in microseconds */
        public void setTimeOut_us(int microseconds){
        	
        	deltatime_ns = microseconds * 1000;
        }
        
        /** Set timeout in nanoseconds */
        public void setTimeOut_ns(long nanoseconds){
        	
        	deltatime_ns = nanoseconds;
        }
        
        
        /** get timeout in seconds */
        public long getTimeOut_s(){
        	
        	return deltatime_ns / 1000000000;
        }
        
        /** get timeout in milliseconds */
        public long getTimeOut_ms(){
        	
        	return deltatime_ns / 1000000;
        }
        
        /** get timeout in microseconds */
        public long getTimeOut_us(){
        	
        	return deltatime_ns / 1000;
        }
        
        /** get timeout in nanoseconds */
        public long getTimeOut_ns(){
        	
        	return deltatime_ns;
        }
        
        
        /** object is init() at creation
    	 *  Use this function only after close()
    	 *  or after using askAndClose() */
    	public void init(String socketName, int port){
    		
    		try {
    			localSocket = new Socket(socketName, port);
    			os = new DataOutputStream(localSocket.getOutputStream());
                is = new DataInputStream(localSocket.getInputStream());
                initialized=true;
                
    		} catch (IOException e) { 
    			e.printStackTrace();
    			initialized=false;
    		}
    	}
    	
    	/** Set debugging level of this object*/
    	public void setDebug(int level){
    		
    		debug_level=level;
    	}
    	
    	/** get debugging level of this object*/
    	public int getDebug(){
    		
    		return debug_level;
    	}
    	
    	/** Set System type of this object 
    	 * Default is 0
    	 * 0 means no system applied, so no data treatment is done
    	 * */
    	public void setSystemType(int level){
    		
    		system_type=level;
    	}
    	
    	/** Get System type of this object*/	
    	public int getSystemType(){

    		return system_type;
    	}
    	
    	public void getRFID(byte[] packetData,int pointer ){
    		int RFID_BYTES=12;
    		byte[] RFID_N = new byte[12];
    		
    		for(int i=0;i<RFID_BYTES; i++){
    			if(pointer+5+i<packetData.length) RFID_N[i]=packetData[pointer+5+i];			
    		}
    		int antenna= 0;
    		if(pointer+19<packetData.length) {
    			antenna= packetData[pointer+19];	
    			String hex = DatatypeConverter.printHexBinary(RFID_N); 
    			System.out.printf("RFID (antena %d):", antenna );
    			System.out.println(hex); 
            }
    	}
    	
    	public void rfid_sys(byte[] packetData,int[] pointers, int packetCounter, int counter){
    		switch(packetData[pointers[counter]+1]){
    		
    		case (byte)0x97 	: 	getRFID(packetData,pointers[counter]);
    								break;
    	
    		default				:    
    								break;
    	
    		}
    	}
    	
    	public void modbus_sys(byte[] packetData,int[] pointers, int packetCounter, int counter){
    		//do something choda dog
    		
    	}
    	
    	public void packetTreatment(byte[] packetData,int[] pointers, int packetCounter){
    				
    				
    		
    		
    				for (int i=0; i< packetCounter ; i++){
    					
    					
    					switch (system_type){
    						case 0 				:	//do nothings
    												break;
    						case 1 				:	rfid_sys(packetData, pointers, packetCounter, i);
    												break;
    						case 2				:	modbus_sys(packetData, pointers, packetCounter, i);
    												break;
    						default				:	break;					
    					
    					}	
    				}
    	}
    	
    	public void protocolChecker(byte[] packetData, int length){
    			
    		    int pointer = 0;
    		    int packetCounter=0;
    		    int[] pointers= new int[10000];
    		    while(pointer+2<length){
    		    	if(packetData[pointer]==header){
    					int localsize = packetData[2+pointer] + 6;				
    					pointers[packetCounter]=pointer;
    					packetCounter++;
    					pointer=pointer+localsize;
    				}
    		    	else pointer = length;
    		    }
    		    //System.out.printf("\n >>> (%d) packets ",packetCounter);
    		    packetTreatment(packetData, pointers, packetCounter);
    	}
    	
    	
    	public static void readSocket(){ 
    			int length;
    			try {
    				length = is.available();
    				if(length>0) {
    		            byte[] message = new byte[length];
    		            try {
    						is.readFully(message, 0, message.length);
    					} catch (IOException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					} // read the message
    		            String hex = DatatypeConverter.printHexBinary(message);
    		            System.out.printf("Received (%d): ",length);
    		            System.out.println(hex); 	
    		            databuffer=message;
    		            data_received=true;  
    		            RFThread rfthread = new RFThread(databuffer);
						rfthread.start();
    		        }
    			} catch (IOException e1) {
    				// TODO Auto-generated catch block
    				e1.printStackTrace();
    			}                    // read length of incoming message
    	        
    	}
    	
    	public static void writeSocket(byte[] commanding) {
    			try {
    				String hex = DatatypeConverter.printHexBinary(commanding);
    	            System.out.printf("sent ");
    	            System.out.println(hex); 
    				os.write(commanding);
    				checktimer(true);
    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}  
    			//Thread.sleep(20);
    	}
    	
    	public static void writeAndRead() {
    			//writeSocket(commanding);
    			readSocket();			
    	}
    	
    	public static void write() throws IOException {
    		
     
    		// The capital string before each colon has a special meaning to SMTP
    		// you may want to read the SMTP specification, RFC1822/3
                	 
    		        	//byte[] commanding = {(byte)0xbb,(byte)0x16,(byte)0x00,(byte)0x16,(byte)0x0D,(byte)0x0A};
    					byte[] commanding = {(byte)0xbb,(byte)0x17,(byte)0x02,(byte)0x00,(byte)0x00,(byte)0x19,(byte)0x0D,(byte)0x0A};
    					writeSocket(commanding);		        			        		
    		        		while (true) {			        		
    			        		writeAndRead();
    			        	}
            		
    	}
    	
    	public boolean checktimer(){
    		return checktimer(false);
    	}
    	
    	public static boolean checktimer(boolean start){
    		if(start){
    			start_time = System.nanoTime();
    			return true;
    		}
    		else {
    			long delta_time = System.nanoTime() - start_time;
    			if(delta_time>deltatime_ns) {
    					databuffer=null;
    					return false;
    			}
    			else if (data_received) return false;
    			else return true;
    		}
    	}
    	
    	public void close(){
    		if(initialized){
    			try {
    				localSocket.close();
    			   	os.close();
    			   	is.close();
    			   	initialized=false;
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		} else {
    			System.out.println(noinitMessage);
    		}	
    	}
    	
    	/** 
    	 * Sends a byte array data packet expecting a response,
    	 * if no response is received before the timeout
    	 * it returns a null byte array
    	 * else it returns the data received in byte array format.
    	 * Use setTimeOut functions to set up a custom timeout
    	 * */
    	public byte[] ask(byte[] toSend) throws IOException {
    		
    		if(initialized){
    			try {
    				if (localSocket != null && os != null && is != null) {     	
    			       	writeSocket(toSend);
    				   	while(checktimer()){
    				   		readSocket();
    				   	}				   	
    				}
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		} else{
    			System.out.println(noinitMessage);
    		}
    		
    		return databuffer;
    	}
    	
    	/** object must not be used after this function, 
    	  * this is intented to wait for an answer then terminate the connection.
    	  * if you need to use the object after this, use ask() instead.
    	  * You can also use init() after this function
    	  * to start a new connection with the same object. */
    	public byte[] askAndClose(byte[] toSend) throws IOException {	
    		 
    		
    		byte[] localbuffer = ask(toSend);
    		close();
    		return localbuffer;
    	}
    	
    	
    

  
	
	public static String toHex(String arg) {
	    return String.format("%040x", new BigInteger(1, arg.getBytes()));
	}
	
	public static void startServer(final int port) { 
        (new Thread() {
            @Override
            public void run() {
                ServerSocket ss;
                try {
                	
                    ss = new ServerSocket(port);

                    Socket s = ss.accept();

                    os = new DataOutputStream(s.getOutputStream());
                    is = new DataInputStream(s.getInputStream());
                    tx = true;   
                } catch (IOException e) {
                	System.out.println("Server opening @ port "+port +" > "+e.getMessage()+": System Exit. Program ended.");
                }
            }
        }).start();
    }
	
	public static void startSender() {
        (new Thread() {
            @Override
            public void run() {
                try {
                     
                	while(!tx){
                		Thread.sleep(0,1);
                	} 
                    
                    	write();
                    while (true) {
                        Thread.sleep(0,1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

	
	public void write2(){
		if (localSocket != null && os != null && is != null) {
            try {
		// The capital string before each colon has a special meaning to SMTP
		// you may want to read the SMTP specification, RFC1822/3
            	
		        try {
		        	byte[] commanding = {(byte)0xbb,(byte)0x16,(byte)0x00,(byte)0x16,(byte)0x0D,(byte)0x0A};
			        os.write(commanding); 
					Thread.sleep(2000);
 			        os.write(commanding); 
					Thread.sleep(2000);
 			        os.write(commanding); 
					Thread.sleep(2000);
 			        os.write(commanding); 
					Thread.sleep(2000);
 			        os.write(commanding); 
					Thread.sleep(2000);
 			        os.write(commanding); 
					Thread.sleep(2000);
				} catch (InterruptedException e) { 
					e.printStackTrace();
				}
		        
		        os.close();
                is.close();
                localSocket.close();   
            } catch (UnknownHostException e) {
                System.err.println("Trying to connect to unknown host: " + e);
            } catch (IOException e) {
                System.err.println("IOException:  " + e);
            }
        }
		
	}
	
	
	/////////////////////////// multi client ///////////////////////////////////
	
	private static void multiclientLoop(int port, int mode){
		ServerSocket ss2;
		try {
			ss2 = new ServerSocket(port);
			if(debugThis) System.out.println("Packet Handler: awaiting connections in port "+port+"with mode "+mode);
			while(!Sigmation.resetSignal){
		        try{		        	
		        	localSocket= ss2.accept();
		        	String thisClient= localSocket.getInetAddress().toString();
		        	if(debugThis) System.out.println("Packet Handler: Connection Established with"+localSocket.getInetAddress().toString()+" \n port:"+port+" mode:"+mode);
		            ServerThread st=new ServerThread(localSocket,thisClient,mode);
		            st.start();
		        }

			    catch(Exception e){
			        e.printStackTrace();
			        System.out.println("Connection Error");
		
			    }
		    }
		} catch (IOException e1) { 
			e1.printStackTrace();
		}
		
	}
	
	public static void multiclientServerStart(int port){
		multiclientServerStart(port,0);
	}
	
	public static void multiclientServerStart(int port,int mode){
		MulticlientThread multiclienthread = new MulticlientThread(port,mode);
		multiclienthread.start();
	}
	
	public static class MulticlientThread extends Thread {
		int serverport=8000;
		int mode=0;
		
		public MulticlientThread(){
		}
		
		public MulticlientThread(int asignPort){
			serverport=asignPort;
		}
		
		public MulticlientThread(int asignPort,int mode){
			serverport=asignPort;
			this.mode=mode;
		}
		
	    public void run(){
	    	System.out.println("port "+serverport);
	    	multiclientLoop(serverport,mode);
	    }
	  }
	
}
