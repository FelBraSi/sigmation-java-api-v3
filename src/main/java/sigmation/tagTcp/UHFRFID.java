package sigmation.tagTcp;

import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

import sigmation.utils.MySQL;
import sigmation.utils.SysUtils;
import sigmation.remoteServer.remoteTagServer;

public class UHFRFID {
	
	private byte[] localbuffer;
	private String thisClient="";
	private int thisReader=0;
	private int thisType=0;
	private int mode=0;
	private String groupId= "0";
	
	private final byte head = (byte)0xBB;
	
	//query commands
	private final byte multiQuery = (byte)0x17;
	private final byte singleQuery = (byte)0x16;
	
	//respuestas
	private final byte startedMultiquery = (byte)0x3A;
	private final byte gotCard = (byte)0x97;
	private final byte antennaStatus = (byte)0x90;
	private final byte EPCInform = (byte)0xE1;
	private final byte error = (byte)0xff;
	//antennas by zone
	int[] zone1 = {1,2};
	int[] zone2 = {3,4};
	
	//comandos predeterminados
	
	private final byte[] multiQueryCommand = {(byte)0xbb,(byte)0x17,(byte)0x02,(byte)0x00,(byte)0x00,(byte)0x19,(byte)0x0D,(byte)0x0A};
	private final byte[] singleQueryCommand = {(byte)0xbb,(byte)0x16,(byte)0x00,(byte)0x16,(byte)0x0D,(byte)0x0A};
	
	//parametros fijos
	
	private final int cardcommandsize=35;  
	private final int cardlocationstart=5; 
	private final int cardlocationend=cardlocationstart+12;
	
	private final boolean debugThis=true;
	
	class PacketLocations{
		/**
		 *  >>>>>>>>>>>>>>>>>Protocol<<<<<<<<<<<<<<<<<<<<
		 *  
		 *  SLAVE UPLINK [PACKET]
		 *  
		 *  >>>>>>>>>>[PACKET]
		 *  <0xBB>: head
		 *  <0xE1>: Type
		 *  LN: data length 
		 *  [DATA]
		 *  CS: checksum
		 *  <0x0D0A>: Tail
		 *  
		 *  >>>>>>>>>>[DATA]
		 *  
		 *  CM: command
		 *  CC: command packet count (should be incremental (?))
		 *  [COMMAND data]
		 *  
		 *  >>>>>>>>>[COMMAND DATA]
		 *  
		 *  >>>command= 01
		 *  
		 *  ET1: EPC total number (MSB)
		 *  ET2: EPC total number (LSB)
		 *  
		 *  >>>command= 02
		 *  
		 *  TC:  total count (natural number counter of total packets to be received)
		 *  CP:  current package count (index counter, starts from 0, ends on TC-1)
		 *  ET1: EPC total number (MSB)
		 *  ET2: EPC total number (LSB)
		 *  [EPC DATA] 12 bytes x N. of EPC (max 20 EPC)
		 * 
		 * */
		
		public final static int headByte=0;
		public final static int typeByte=1;
		public final static int dataLengthByte=2;
		
		public final static int dataBytesStart=3;
		public final static int cmByte=3;
		public final static int ccByte=4;
		
		public final static int commandDataStart=5;
		
		public class com1{
			public final static int ET1Byte=5;
			public final static int ET2Byte=6;
		}
		
		public class com2{
			public final static int TCByte=5;
			public final static int CPByte=6;
			public final static int ET1Byte=7;
			public final static int ET2Byte=8;
			public final static int EPCBytesStart=9;
			public final static int com2HEaderSize=EPCBytesStart-dataBytesStart;
			public final static int EPCLength=12;
			public final static int maxEPC=20;
		}
	}
	
	public byte[] getMultiQueryCommand(){
		return multiQueryCommand;
	}
	
	public byte[] getSingleQueryCommand(){
		return singleQueryCommand;
	}
	
	public void fillBuffer(byte[] aBuffer){
		localbuffer=aBuffer;
	}
	
	public ArrayList< String[] > identifyCards(int cards){
		return identifyCards(cards,false);
	}
	
	public ArrayList< String[] > identifyCards(int cards, boolean mode){
		ArrayList< String[] > CardArray = new ArrayList<String[]>(); 
		if(mode){
			int framesize=PacketLocations.com2.EPCLength; 
			//if(debugThis)System.out.println("Looking for EPCs: \n frame size:"+framesize+" bytes \n Estimated EPC quantity: "+cards+" ");
			for(int frame=0;frame<cards;frame++){ 
				int displacement=PacketLocations.com2.EPCBytesStart+(framesize*frame);
				//if(debugThis)System.out.println("current displacement:"+displacement+" bytes "); 
			    byte[] smallBuffer = Arrays.copyOfRange(localbuffer, displacement, displacement+framesize); 
				byte[] antennabyte= {(byte)0xAA};
				String[] localstring= {DatatypeConverter.printHexBinary(smallBuffer),""+SysUtils.byteToUnsignedInt(antennabyte[0]), ""+thisReader};
				CardArray.add(localstring); 			
			}
			
		} else {
			for(int frame=0;frame<cards;frame++){
				int displacement=cardcommandsize*frame;
				if(localbuffer[1+displacement]==gotCard){
					byte[] smallBuffer = Arrays.copyOfRange(localbuffer, cardlocationstart+displacement, cardlocationend+displacement); 
					byte[] antennabyte= {localbuffer[19+displacement]};
					String[] localstring= {DatatypeConverter.printHexBinary(smallBuffer),DatatypeConverter.printHexBinary(antennabyte), ""+thisReader};
					CardArray.add(localstring);
				}			
			}
		}
		return CardArray;
	}
	
	
	/**
	 * IP: thisStats[0].thisStats[1].thisStats[2].thisStats[3]
	 * <br>index 2 : tipo de lector (1 subida, 2 bajada)
	 * <br>index 3 : numero de serie lector
	 * 	
	 * */
	public void getReaderStats(){
		 
		String[] thisStats = SysUtils.splitString(thisClient, "\\.");
		if(debugThis) System.out.println("\nsplit size:"+thisStats.length+" - Split String: "+thisClient);
		if(thisStats.length<4) return;
		this.thisReader=SysUtils.returnInt(thisStats[3]);
		this.thisType=SysUtils.returnInt(thisStats[2]);
		if(debugThis) System.out.println("\nreader: "+thisReader+" - type: "+thisType);
	}
	
	public int analyzeData(byte[] aBuffer,String thisClient,int mode, String groupId){
		this.mode=mode;
		this.thisClient=thisClient;
		this.groupId= groupId;
		getReaderStats();
		fillBuffer(aBuffer);
		return analyzeData();
	}
	
	public int analyzeData(){
		switch(localbuffer[1]){
			case gotCard 			:  	analyzeCardData();
										break;
			case startedMultiquery	: 	if(debugThis)System.out.println("multiquery started");
										break;
			case antennaStatus		:	if(debugThis)System.out.println("Antenna status");
										break;
			case error				:	if(debugThis)System.out.println("Error(X)");
										break;
			case EPCInform			:	return analyzeInform(); 
			
			default					:	if(debugThis)System.out.println("Unknown status (X)");
										break;							
		}
		return 0;
	}
	
	public int analyzeInform(){
		 int length = SysUtils.byteToUnsignedInt(localbuffer[PacketLocations.dataLengthByte]);
		 int command = SysUtils.byteToUnsignedInt(localbuffer[PacketLocations.cmByte]);
		 if(debugThis)System.out.println("packet has "+length+" bytes and it's command "+command);
		 
		 switch(command){
		 case 1				:	return analyzeCommand(); 
		 case 2				:	return analyzeEPCcommand(length); 
		 default			:	if(debugThis) System.out.println("Unknown status (X)");
								break;
		 }
		 return 0;
	}
	
	private void getEPC(int EPCQuantity){ 
		int localzone = 0;
		
		if(thisType==1 || thisType == 2) {
			busEPC(EPCQuantity) ;
		}
		else if(thisType==4 || thisType == 5)  { 
			localzone=thisType-3;
			specialEPC(EPCQuantity, localzone);
		} 
	}
	
	private void specialEPC(int EPCQuantity, int thiszone) {
		boolean BusMode=true;
		ArrayList<String[]> localarray= identifyCards(EPCQuantity,BusMode);
		if(debugThis)System.out.println(localarray.size()+" EPC were retrieved");
		for(String[] strArr: localarray){
			System.out.println("CARD: "+Arrays.toString(strArr));
			String tagnum= strArr[0];
			String zone=""+thiszone; 
			saveCards(tagnum, zone);
		}
	}
	
	private void busEPC(int EPCQuantity) {
		boolean BusMode=true;
		ArrayList<String[]> localarray= identifyCards(EPCQuantity,BusMode);
		if(debugThis)System.out.println(localarray.size()+" EPC were retrieved");
		for(String[] strArr: localarray){
			System.out.println("CARD: "+Arrays.toString(strArr));
			String tagnum= strArr[0];
			String zone="0";
			if(Integer.parseInt(strArr[1])>2) zone="2";
			else zone ="1";
			saveBusEPC(tagnum, zone);
		}
	}
	
	private int analyzeEPCcommand(int length){
		if(debugThis)System.out.println("analyzing EPC command ");
		byte[] ETbytes={localbuffer[PacketLocations.com2.ET1Byte],localbuffer[PacketLocations.com2.ET2Byte]};
		int EPCtotal = SysUtils.bytestoInts16(ETbytes)[0];
		int EPCByteSize = length-PacketLocations.com2.com2HEaderSize;
		int EPCQuantity = EPCByteSize/PacketLocations.com2.EPCLength;
		int EPCtotalpackets = SysUtils.byteToUnsignedInt(localbuffer[PacketLocations.com2.TCByte]);
		int EPCcurrentpacket = SysUtils.byteToUnsignedInt(localbuffer[PacketLocations.com2.CPByte]);
		if(debugThis)System.out.println("the packet contains "+EPCQuantity+" EPC, packet "+(EPCcurrentpacket+1)+" of "+EPCtotalpackets+" retrieving ...");
		if(thisType==1 || thisType == 2) {
			if(EPCcurrentpacket+1==EPCtotalpackets) writePreMessage(EPCtotal);
		}
		getEPC(EPCQuantity);
		return 2;
	}
	
	private int analyzeCommand(){
		byte[] ETbytes={localbuffer[PacketLocations.com1.ET1Byte],localbuffer[PacketLocations.com1.ET2Byte]};
		int EPCtotal = SysUtils.bytestoInts16(ETbytes)[0];
		if(debugThis)System.out.println("the packet says there's "+EPCtotal+" EPC stored ...");
		if (EPCtotal>0) return 1;
		else return 0;
	}
	
	public void analyzeCardData(){
		int cards = Math.abs(localbuffer.length/cardcommandsize); 
		ArrayList<String[]> localarray= identifyCards(cards);
		for(String[] strArr: localarray){
			System.out.println("CARD: "+Arrays.toString(strArr));
			String tagnum= strArr[0];
			String zone="0";
			if(mode==0) {
				if(Integer.parseInt(strArr[1])>2) zone="2";
				else zone ="1";
			} else if(mode==6 || mode==7) {
				try {
					remoteTagServer.cardcounter[thisReader-1]++;
					System.out.println("[::: "+remoteTagServer.cardcounter[thisReader-1]+" :::]");
				} catch (Exception e) {
					System.out.println("[::: card counter, bad index :::]");
				}
				if(thisReader>2) zone="2";
				else zone ="1";
			}
			
			saveCards(tagnum, zone);
		}		
	}
	 
	
	private static void writePreMessage(int EPCtotal){
		String message="'BUS TAGS:"+EPCtotal+"    Registros'";
		String table="busmessage"; 			
		String[] args1= {"messagetext","stat"};
		String[] vals= {message,""+1};
		MySQL cn = new MySQL();
		cn.database="remotedb";					
		cn.connect();  
		cn.insert(table, args1, vals);	
		cn.close();
	}
	
	public void saveBusEPC(String tagnum, String zone){  		 
		MySQL cn = new MySQL();
		cn.database="remotedb";					
		cn.connect(); 
		String procedure="saveTagBus";
		int score = 0;
		if(mode==1) score=1;
		else if(mode==2) score=-1;
		String[] argument= {"'"+tagnum+"'", "'"+thisReader+"'", ""+score+"", "'"+groupId+"'" };
		cn.callVoidProcedure(procedure, argument); 
		cn.close();
	}
	
	public void saveCards(String tagnum, String zone){  	
		MySQL cn = new MySQL();
		cn.database="remotedb";					
		cn.connect(); 
		String procedure="saveTag";
		String[] argument= {"'"+tagnum+"'", zone};
		cn.callVoidProcedure(procedure, argument); 
		cn.close();
	}

}
