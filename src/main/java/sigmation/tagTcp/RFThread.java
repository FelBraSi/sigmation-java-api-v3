package sigmation.tagTcp;

import sigmation.remoteServer.ServerThread;

public class RFThread extends Thread{
	byte[] localbuffer;
	String thisClient="";
	int mode=0;
	ServerThread thisServerThread;
	boolean debugThis=false;
	String groupId="0";
	
	public RFThread(byte[] localbuffer, String thisClient, int mode, ServerThread thisServerThread) {
		// TODO Auto-generated constructor stub
	    	this.localbuffer = localbuffer;
	    	this.thisClient=thisClient;
	    	this.mode=mode;
	    	this.thisServerThread=thisServerThread;
	    	this.groupId = thisServerThread.groupId;
	}
	
	public RFThread(byte[] localbuffer, String thisClient, int mode) {
		// TODO Auto-generated constructor stub
	    	this.localbuffer = localbuffer;
	    	this.thisClient=thisClient;
	    	this.mode=mode;
	}
	
	public RFThread(byte[] localbuffer, String thisClient) {
		// TODO Auto-generated constructor stub
	    	this.localbuffer = localbuffer;
	    	this.thisClient=thisClient;
	}
	    
	    public RFThread(byte[] localbuffer ) {
			// TODO Auto-generated constructor stub
		    	this.localbuffer = localbuffer;
		}

		public void run(){	    	
			UHFRFID rfdata = new UHFRFID();
			int commandType=rfdata.analyzeData(localbuffer,thisClient,mode,groupId);
			if (mode>0 && thisServerThread!=null){
				thisServerThread.nextMove(commandType);
				if(debugThis) System.out.println("nextMove is "+commandType);
			} else if (mode>0){
				if(debugThis) System.out.println("Null server thread, check code ");
			}
	    }	  
}
