package sigmation.tagTcp;
 
import java.util.ArrayList;

import sigmation.utils.MySQL;
import sigmation.utils.RunShellCommandFromJava;
import sigmation.main.Sigmation;;


public class MessageHandler extends Thread{
		
		public static ArrayList<String[]> TagList;
		static long lastTime = System.currentTimeMillis();
		static long messagetime = 2500; //milliseconds
		static long currentmessagetime=0;
		static long shortMessageTime=500;
		private static String message;
		private static boolean fastloop=false;
		private static boolean alreadynull=false;
	
		public void run(){	    	
			//do something shodadog
			loop();			
	    }
		
		private static void startScreen(){
			sigmation.io.LedDisplay.init();
		}
		
		private static void loop(){
			currentmessagetime=messagetime;
			if(!Sigmation.noPiMode) startScreen();
			while(!Sigmation.resetSignal){
				if(isItTime()){
					savetime();
					setTags();
					getTagsToDisplay();
					prepareMessage();
					if(!fastloop)setMessage(); 
				} 
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
		}
		
		private static boolean checkBusReadings(){
			String argument= "messagetext";
			boolean gotReadings=false;
			MySQL cn = new MySQL();
			cn.database="remotedb";
			cn.connect();
			String table="busmessage";
			ArrayList<String[]> thisList= cn.select(table, argument, "  stat=1 ORDER BY last_mod ASC LIMIT 1");
			if(thisList==null | thisList.isEmpty()) gotReadings=false;// do something
			else {
					gotReadings=true;
					message=thisList.get(0)[0];}
			cn.close();
			return gotReadings;
		}
		
		private static void deleteMessage(){
			String table="busmessage"; 			
			String[] args1= {"messagetext","stat"};
			MySQL cn = new MySQL();
			cn.database="remotedb";					
			cn.connect();
			String[] values = {"'"+message+"'",""+1};  //tags en display son seteados
			cn.delete(table, args1, values);	
			cn.close(); 	
		}
		
		private static void prepareMessage(){
			message="";
			if(TagList.size()>0){
				for(int i=0; i < TagList.size() && i<2 ; i++){
					String[] localstring = TagList.get(i);
					message= message + ">" +localstring[2]+" "+localstring[1].substring(17)+" ";
					currentmessagetime=messagetime;
					fastloop=false;
					alreadynull=false;
				}
			} else if(checkBusReadings()){
				currentmessagetime=messagetime;
				fastloop=false;
				alreadynull=false;     
				setMessage();
				deleteMessage();
			}else{
				if (!alreadynull) message="null";
				else fastloop=true;
				alreadynull=true;
				currentmessagetime=shortMessageTime;
			}
		}
		
		private static void savetime(){
			lastTime = System.currentTimeMillis();
		}
		
		private static boolean isItTime(){
			long thisTime= System.currentTimeMillis();
			if (thisTime-lastTime>=currentmessagetime) return true;
			return false;
		} 
		
		public static void getTagsToDisplay(){    // get all the registered readers

			String argument= "id, tagnum, initials ";
			MySQL cn = new MySQL();
			cn.database="remotedb";
			cn.connect();
			String table="rfid";
			TagList = cn.select(table, argument, "  display=2 LIMIT 2");
			if(TagList==null | TagList.isEmpty()) ;// do something
			cn.close();
			
		}
		
		private static void setTags(){

				String table="rfid"; 
				
				String[] args1= {"display"};
				MySQL cn = new MySQL();
				cn.database="remotedb";					
				cn.connect();
				String condition = " display=2 ";  //ya mostrados son sacados
				String[] data= {"3"};
				if (!alreadynull) cn.update(table, args1, condition, data);
				
				cn = new MySQL();
				cn.database="remotedb";					
				cn.connect();
				condition = " display=1 LIMIT 2 ";  //tags en display son seteados para mostrarse
				String[] data2= {"2"};
				cn.update(table, args1, condition, data2);
				cn.close();
				try {
					cn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		
		public static void setResetMessage(){
			message=" Reset by     Config.";
			setMessage();
		}
	
		public static void setMessage(String mess){
			message=mess;
			setMessage();
		}
		
		private static void setMessage(){

			String table="message"; 			
			String[] args1= {"messagetext"};
			MySQL cn = new MySQL();
			cn.database="remotedb";					
			cn.connect();
			String condition = " messagetext!= '"+message+"' ";  //tags en display son seteados
			String[] data= {"'"+message+"'"};
			cn.update(table, args1, condition, data);	
			cn.close(); 
		}
		 
}
