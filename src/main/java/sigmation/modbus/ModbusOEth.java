package sigmation.modbus;

import sigmation.io.TcpIpHandler;
import sigmation.utils.SysUtils;

public class ModbusOEth extends Modbus {
	private boolean packageAsModbus = false;  
	private byte[] packageCommand= {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x06};  //modbus command ethernet wraper
	private byte[] packageAnswer = {(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x09};  //modbus answer  ethernet wraper
	private byte[] outputData;
	private byte[] inputData;	
	
	public int responseAddress=0;
	public int responseFunction=0;
	public int responsedataQuantity=0;
	public int[] responseDataInts;
	public boolean[] responseDatabooleans;
	public byte[] responseDatabytes;
	
	private boolean gotData=false;
	
	private boolean modbusOethInit=false;
	
	public boolean hasData(){
		return gotData;
	}
	
	public boolean isInit(){
		return modbusOethInit;
	}
	
	private TcpIpHandler localConnection;
	
	/** 
	 * Sets if data must be sent as a modbus package
	 * This must be set to false when using raw data
	 * adapter modules for Ethernet to rs422/485/232.
	 * Must be set true if using ethernet modbus modules.
	 * */	
	public void setModbusPackage(boolean packset){
		packageAsModbus = packset;
	}  
	
	/** 
	 * Indicates if data is being sent as a modbus package
	 * This should be set to false when using raw data
	 * adapter modules for Ethernet to rs422/485/232.
	 * Should be set true if using ethernet modbus modules.
	 * */
	public boolean getModbusPackage(){
		return packageAsModbus;
	}  
	
	/** 
	 * Creates an ethernet modbus object
	 * using a custom port.	 
	 * */
	public ModbusOEth(String localIp, int port){
		super();
		localConnection = new TcpIpHandler(localIp, port);
	}  
	
	/** 
	 * Creates an ethernet modbus object
	 * using the default port 502.
	 * Use ModbusOEth(String localIp, int port) 
	 * for a custom port.
	 * */
	public ModbusOEth(String localIp){
		super();
		localConnection = new TcpIpHandler(localIp, 502);
	}  
	
	/** 
	 * Creates an ethernet modbus object
	 * using a custom port.	 
	 * */
	public ModbusOEth(String localIp, int port, boolean packageAsModbus){
		super();
		this.packageAsModbus = packageAsModbus;
		localConnection = new TcpIpHandler(localIp, port);
		modbusOethInit =localConnection.isInit();
		System.out.println("Ethernet state --"+modbusOethInit); 
	}  
	
	/** 
	 * Creates an ethernet modbus object
	 * using the default port 502.
	 * Use ModbusOEth(String localIp, int port) 
	 * for a custom port.
	 * */
	public ModbusOEth(String localIp,boolean packageAsModbus){
		super();
		this.packageAsModbus = packageAsModbus;
		localConnection = new TcpIpHandler(localIp, 502);
		modbusOethInit =localConnection.isInit();
		System.out.println("Ethernet state --"+modbusOethInit); 
	}  
	
	
	public void init(String localIp, int port){ 
		localConnection = new TcpIpHandler(localIp, port);
	}
	
	public void init(String localIp){ 
		localConnection = new TcpIpHandler(localIp, 502);
		gotData=false;
	}
	
	public void close(){
		localConnection.close();
	}
	
	public byte[] ask(){
		try {
			arrangeOutputData();
			arrangeInputData(localConnection.ask(outputData));
			readPacket();
			return inputData;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}  
	
	public byte[] askAndClose(){
		try {
			arrangeOutputData();
			arrangeInputData(localConnection.askAndClose(outputData));
			readPacket();
			return inputData;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("askandclose error");
		}
		return null;
	}  
	
	private void arrangeOutputData(){
		if(packageAsModbus && data!=null){
			int datalength= data.length-2;  // datalength-2 to skip the crc
			int commandlength= packageCommand.length;
			packageCommand[commandlength-1]=(byte)(datalength);
			outputData = new byte[datalength+commandlength];
			for(int i=0;i<commandlength; i++){
				outputData[i]=packageCommand[i];
			}
			for(int i=0;i<datalength; i++){      
				outputData[i+commandlength]=data[i];
			}
		} else if (data!=null){
			outputData=data;
		} else System.out.println("MBOETH: No data!");
	}  
	
	private void readBits(){
		responseDatabooleans=SysUtils.toBooleanArray(responseDatabytes);
	}
	
	private void readInts(){
		responseDataInts=SysUtils.bytestoInts16(responseDatabytes);
	}
	
	private void readPacket(){
		if(inputData!=null){
			int psize=0;
			if (packageAsModbus) psize=inputData.length;
			else psize=inputData.length-2;
			
			if(psize>=3){
				responseAddress=inputData[0];
				responseFunction=inputData[1];
				responsedataQuantity=inputData[2];		
				if(psize>3){
					responseDatabytes = new byte[psize-3];
					for(int i=3; i<psize;i++){
						responseDatabytes[i-3]=inputData[i];
					}
					
					switch(responseFunction){
						case 1		:		readBits();		break; 
						case 2		:		readBits();		break; 
						case 3		:		readInts();		break; 
						case 4		:		readInts();		break; 
						case 5		:		readInts();		break; 
						case 6		:		readInts();		break; 
						case 15		:		readInts();		break; 
						case 16		:		readInts();		break; 
						default		:						break;  				
					}
					gotData=true;
				} else gotData=false; 				
			}
		}
	}
		
	private void arrangeInputData(byte[] byteinput){
		if(packageAsModbus && byteinput!=null){
			int datalength= byteinput.length;
			int answerlength= packageAnswer.length;
			int modbusdatalength= datalength-answerlength;
			boolean valid = true;
			if(modbusdatalength<0) return;
			int datasize = 0;
			inputData = new byte[modbusdatalength];
			for(int i=0;i<answerlength && valid ; i++){				//checks if it is a valid ethernet modbus response
				if(packageAnswer[i]==byteinput[i]) valid = true;
				datasize = (byteinput[i] & 0xff);
			}
			if (datasize<4) {
				valid=false; 
				System.out.printf("Validating failed ");
				System.out.println(datasize);
			}
			for(int i=0;i<modbusdatalength && valid ; i++){
				inputData[i]=byteinput[i+answerlength];
			}
			if(!valid) inputData=null;
		} else {
			if(checkModbusCRC16(byteinput)) inputData=byteinput;
			else inputData=null;
		}
	}
}