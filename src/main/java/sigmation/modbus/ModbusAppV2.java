package sigmation.modbus;
 
import java.util.ArrayList;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
 
import sigmation.drivers.Helpers; 
import sigmation.drivers.StatObject;
import sigmation.drivers.VentStats;
import sigmation.io.SysCom;
import sigmation.io.UDPClient;
import sigmation.io.UDPHandler;
import sigmation.io.WMXModCom;
import sigmation.utils.MySQL;
import sigmation.utils.PM130EH;
import sigmation.utils.SigmationVarTopic;
import sigmation.utils.SysUtils;
import sigmation.utils.SysUtils.jsonArray;
import sigmation.utils.SysUtils.jsonObject;
import sigmation.main.Sigmation;
import sigmation.mongodb.MongoDriver;
import sigmation.mqtt.pahoMqtt;
import sigmation.mqtt.pahoMqtt.Responder;;
 
public class ModbusAppV2 {
	
	/*==========================================
	 * 
	 * MONGODB CONFIG
	 * 
	 ==========================================*/
	
	private final static String mongoServer="mongodb://127.0.0.1:3001/meteor";
	private final static String databaseName= "meteor"; 
	private final static String collectionName= "mqttcollection";  

	/*==========================================
	 * 
	 * MQTT CONFIG
	 * 
	 ==========================================*/
	
	private static String SmgSys = "____DOLF";
	private static String res = "__dev";
	private static String param= "__set";
	private static Helpers helpers = new Helpers();
	
	/*==========================================
	 * 
	 * SUBSYS VARS
	 * 
	 ==========================================*/
	
	private static MongoDriver mdbMongoDB;
	private static pahoMqtt mdbMqtt;
	private static UDPHandler udphandler = new UDPHandler();
	private static UDPClient udpClient;

	/*==========================================
	 * 
	 * SYSTEM CONFIG
	 * 
	 ==========================================*/
	
	private static ArrayList<String[]> AmpList ;
	private  ModbusDevice[] modbusDevices; 
	private  PM130EH[] pm130list; 
	private int PM130EHindex=0;
	private int[] devicemap= new int[255];
	private int maxcount = 5;
	public boolean WMXprotocol = true;
	public boolean rs485Ctrl = false;
	public boolean confirmACK = true;
	 

	/*==========================================
	 * 
	 * FUNCTIONS
	 * 
	 ==========================================*/
	
	public ModbusAppV2(boolean rs485Ctrl, boolean WMX) {
		this.rs485Ctrl=rs485Ctrl;
		WMXprotocol=WMX;
		udphandler.start();
		startMQTT();
		startVentObject("test", "1", "2", "noip"); 
	}
	
	private void displayStatus(String status){
		System.out.println("< "+status+" >");
		SysUtils.currentTask=status;
		SysUtils.sysInfo();
	}
	
	private void getPM130EHIndex(){
		String argument= "id";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_devices_types";
		ArrayList<String[]> stats = cn.select(table, argument, " name= 'PM130EH'");
		if(stats.size()==1){
			String[] strindex= stats.get(0);
			PM130EHindex = Integer.parseInt(strindex[0]);
			
		} 		
	}
	
	private void getcom(byte[] sendData, int deviceIndex){
		getcom(sendData, deviceIndex, 1);
	}
	
	private int getcom(int valueindex, int deviceIndex ,byte[] sendData){
		return getcom(valueindex, deviceIndex ,sendData, 1);
	}
	
	private boolean getEOF(){
		if(WMXprotocol){
			return WMXModCom.eof_received;
		}
		else{
			return SysCom.eof_received;
		}
	}

	private int getcom(int valueindex, int deviceIndex, int extended){
		int j=0, noresponse=1;
		if(modbusDevices[deviceIndex].isEthernet()>0){
			System.out.println("Ethernet com --");			
			try {
				ModbusOEth localDevice = modbusDevices[deviceIndex].getEthVarsData(valueindex);
				if(localDevice==null) {noresponse=noresponse+2;}
				else noresponse = confirmData(localDevice, deviceIndex, valueindex);
			} catch (Exception e) {
				noresponse++;
			}				
			j=j+ noresponse;
		} else {
			byte localaddress=modbusDevices[deviceIndex].address ;
			byte[] sendData = modbusDevices[deviceIndex].getVarsData(valueindex); 
			Modbus localMB = modbusDevices[deviceIndex].getModbusObj(valueindex,true); 
			if(WMXprotocol){
				WMXModCom.o_destination_address=modbusDevices[deviceIndex].wmxaddress ;
			}
			for (j=0;j<maxcount;j++) {
				sendModbusData(localMB); 
				boolean localEOF=false, localMBT= false;
				confirmACK= true;  
				while(!localEOF && !localMBT){
					if (!confirmACK) confirmACK = WMXModCom.confirmACK(); 
					localEOF=getEOF();
					localMBT=WMXModCom.checkMBT();
				}
				if(localEOF){
					if(WMXprotocol){
						j = confirmData(WMXModCom.o_destination_address,j,deviceIndex, valueindex);
						WMXModCom.resetProtocol();
					}
					else{
						j = j+confirmData(localaddress,j,deviceIndex);
					}	
					return j;
				} else if (localMBT) {
					System.out.println("timeout>");
				}	
				WMXModCom.resetModem();
			} 
		}			
		return j;
	}
	
	private int getcom(int valueindex, int deviceIndex, byte[] sendData ,int extended){
		int j=0, noresponse=1;
		if(modbusDevices[deviceIndex].isEthernet()>0){
			System.out.println("Ethernet com --");
			for ( j=0;j<maxcount && noresponse!=0;j++) {
				try {
					ModbusOEth localDevice = modbusDevices[deviceIndex].setEthVarsStat(deviceIndex,sendData);
					if(localDevice==null) {noresponse=noresponse+2;}
					else noresponse = confirmData(localDevice, deviceIndex, valueindex);
				} catch (Exception e) {
					noresponse++;
				}				
				j=j+ noresponse;
			}			
		} else {
			byte localaddress=modbusDevices[deviceIndex].address ;
			byte[] sendData1 = modbusDevices[deviceIndex].getVarsData(valueindex); 
			if(WMXprotocol){
				WMXModCom.o_destination_address=modbusDevices[deviceIndex].wmxaddress ;
			}
			for ( j=0;j<maxcount;j++) {
				java.util.Date date1= new java.util.Date();
				java.util.Date date2= new java.util.Date();
				long difference = date2.getTime() - date1.getTime();
				sendModbusData(localaddress, sendData1); 
				while(getEOF()==false && difference<(400*extended)){
					date2= new java.util.Date();
					difference = date2.getTime() - date1.getTime();
	 	
				}
				if(getEOF()){
					if(WMXprotocol){
						j = j+confirmData(WMXModCom.o_destination_address,j,deviceIndex, valueindex);
					}
					else{
						j = j+confirmData(localaddress,j,deviceIndex);
					}	
					return j;
				}	
			} 
		}			
		return j;
	}
	
	//ModbusDefinitions.PRESET_SINGLE_REGISTER
	
	private int getConfirm(byte[] sendData, int deviceIndex, int extended){
		
		byte localaddress=modbusDevices[deviceIndex].address ; 
		int j=0;
		for (j=0;j<maxcount;j++) {
				if(WMXprotocol) sendModbusData(modbusDevices[deviceIndex].wmxaddress, sendData); 
				else sendModbusData(localaddress, sendData); 
				boolean localEOF=false, localMBT= false;
				confirmACK= true;  
				while(!localEOF && !localMBT){
					if (!confirmACK) confirmACK = WMXModCom.confirmACK(); 
					localEOF=getEOF();
					localMBT=WMXModCom.checkMBT();
				}
				if(localEOF){
					if(WMXprotocol){
						j = confirmCommand(modbusDevices[deviceIndex].wmxaddress, j, deviceIndex);
						WMXModCom.resetProtocol();
					}
					else{
						j = j+confirmData(localaddress,j,deviceIndex);
					}	
					return j;
				} else if (localMBT) {
					System.out.println("timeout>");
				}	
				WMXModCom.resetModem();
		} 	
		return j;
	}
	
	private int getcom(byte[] sendData, int deviceIndex, int extended){
		
		byte localaddress=modbusDevices[deviceIndex].address ; 
		int j=0;
		for (j=0;j<maxcount;j++) {
				if(WMXprotocol) sendModbusData(modbusDevices[deviceIndex].wmxaddress, sendData); 
				else sendModbusData(localaddress, sendData); 
				boolean localEOF=false, localMBT= false;
				confirmACK= false;  
				while(!localEOF && !localMBT){
					if (!confirmACK) confirmACK = WMXModCom.confirmACK(); 
					localEOF=getEOF();
					localMBT=WMXModCom.checkMBT();
				}
				if(localEOF){
					if(WMXprotocol){
						j = confirmData(localaddress,j,deviceIndex);
						WMXModCom.resetProtocol();
					}
					else{
						j = j+confirmData(localaddress,j,deviceIndex);
					}	
					return j;
				} else if (localMBT) {
					System.out.println("timeout>");
				}	
				WMXModCom.resetModem();
		} 	
		return j;
	}
	
	public int confirmData(byte localaddress, int counter, int deviceIndex){
		int localdata = storeData(SysCom.currentAddress);

		if(localdata==localaddress) {
			                       counter=maxcount; 
			                       byte[] localbuf= SysCom.databuffer;
			                       
			                       if (localbuf[1]==(byte)0xB1) setDeviceOnline(localaddress,deviceIndex,2);
			                       else		setDeviceOnline(localaddress,deviceIndex,1);
								   }  // si respuesta es del dispositivo esperado, continua con el siguiente
		else if(counter==maxcount-1){
			setDeviceOffline(localaddress,deviceIndex); //si van 3 intentos sin respuesta del dispositivo, registra offline
		}		
		return counter;
	}
	
	public int confirmCommand(String localaddress, int counter, int deviceIndex){
		String localSource = WMXModCom.source_address;
		System.out.println("Address "+localSource + " expected "+localaddress );
		if(localSource.equals(localaddress)) WMXModCom.retrieveData();
		try {
			if(localSource.equals(localaddress) && WMXModCom.iFunction==ModbusDefinitions.PRESET_MULTIPLE_REGISTERS) {	
				System.out.println("Command comfirmed");
				return 0;
			} else  {
				System.out.println("commands are "+WMXModCom.iFunction+" vs "+ModbusDefinitions.PRESET_MULTIPLE_REGISTERS+" not equal");
				return maxcount;//
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return maxcount;
	}
	
	public int confirmData(String localaddress, int counter, int deviceIndex, int valueindex){
		String localSource = WMXModCom.source_address;
		System.out.println("Address "+localSource + " expected "+localaddress );
		if(localSource.equals(localaddress))WMXModCom.retrieveData();
		try {
			if(localSource.equals(localaddress) && WMXModCom.iError==0 && WMXModCom.data_received) {	
				System.out.println("Data confirmed");
				WMXModCom.retrieveData();
				modbusDevices[deviceIndex].insertInstaValue(valueindex, WMXModCom.ResponseData[0]); 
				return 0;
			} else if(localSource.equals(localaddress) && WMXModCom.iError==0 && WMXModCom.ack_control){
				System.out.println("Data skipped");
			} 
			else  {
				//setDeviceOffline(localaddress,deviceIndex); //si van 3 intentos sin respuesta del dispositivo, registra offline
				System.out.println("No Data");
				return maxcount;//
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		//setDeviceOffline(localaddress,deviceIndex); //si van 3 intentos sin respuesta del dispositivo, registra offline
		return 0;
	}
	
	public int confirmData(ModbusOEth localMOEObject, int deviceIndex, int valueindex){
		
		if(localMOEObject.hasData()) { 
			try {
				if (localMOEObject.responseFunction==6) System.out.println("Parameter written");
				else if(localMOEObject.responseFunction==3) modbusDevices[deviceIndex].insertInstaValue(valueindex,localMOEObject.responseDataInts[0]);  
				return 0;
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("index "+deviceIndex);
				System.out.println("responseDataInts len "+localMOEObject.responseDataInts.length);
			}			
			return 1;
		}  
		else  {
			return 1;//setDeviceOffline(localaddress,deviceIndex); //si van 3 intentos sin respuesta del dispositivo, registra offline
		}
		
		
	}
	
	private static void updatedeploy(int address){
		if(address>0){
			String[] args1= {"deploy"};
			String[] dat= new String[1];
			
			dat[0]= ""+2+"";
			
			String condition = " address= "+address+" ";
			String table="modbus_devices";
			MySQL cn = new MySQL();
			cn.database="ampdb";
			cn.connect();
			cn.update(table, args1,condition, dat);
		}
	}
	
	private static void updatedeploy(String address){
		
		if(address!=null){
			if(address.length()==0) return;
			String[] args1= {"deploy"};
			String[] dat= new String[1];			
			dat[0]= ""+2+"";
			
			String condition = " address= '"+address+"' ";
			String table="modbus_devices";
			MySQL cn = new MySQL();
			cn.database="ampdb";
			cn.connect();
			cn.update(table, args1,condition, dat);
		}
	}
	
	private static void updateInstaData(int id,double last_value,double alarm){
		if(alarm!=10){
			
			String[] args1= {"last_value","alarm"};
			String[] dat= new String[args1.length];
			
			dat[0]= ""+last_value+"";
			dat[1]= ""+(int)alarm+"";
			
			String condition = " id= "+id+" ";
			String table="modbus_devices_vars";
			MySQL cn = new MySQL();
			cn.database="ampdb";
			cn.connect();
			cn.update(table, args1,condition, dat);	
		}		
		
	}
	
	public static void insertHistory(int varsid, double val, double alarm){
		if(alarm!=10){
			String[] args1= {"vars_id","value","alarm"};
			String[] data= new String[args1.length];
			
			//mapValues
			
			data[0]=""+varsid;
			data[1]=""+val;
			data[2]=""+(int)alarm;
			
			String table="modbus_devices_history";
			MySQL cn = new MySQL();
			cn.database="ampdb";
			cn.connect();
			cn.insert(table, args1, data);		
		}
	}	
	
	public void setWorkingTime(int addrs,int onservice){
		String[] args1= {""+addrs,""+onservice};	
		System.out.print("\n<Onservice: " + onservice + " , " + addrs +">");
		String procedure="addServiceTime";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.callVoidProcedure(procedure, args1);
	}
	
	public ArrayList<double[]> extractDataFrames(byte[] datax, int index){
		ArrayList<double[]> dataFrameList = new ArrayList<double[]>();
		int frames=datax[2];
		int general_alarms=1;
		// 2/2/x bytes: address-size-data
		int bytecounter=3;
		for(int i=0;i<frames;i++){
			
			int addrs = (((datax[bytecounter] & 0xff ) << 8 ) | (datax[bytecounter+1] & 0xff ));
			int qunty = (((datax[bytecounter+2] & 0xff ) << 8 ) | (datax[bytecounter+3] & 0xff ));
			int value = 0;
			
				
				if(qunty==1) value = (((datax[bytecounter+4] & 0xff ) << 8 ) | (datax[bytecounter+5] & 0xff ));
				else value = (((datax[bytecounter+4] & 0xff ) << 24 ) | ((datax[bytecounter+5] & 0xff ) << 16 ) | ((datax[bytecounter+6] & 0xff ) << 8 ) | (datax[bytecounter+7] & 0xff ));
				
				double valuedob = value;
				int alarm=0;
				if(modbusDevices[index].devicetype==PM130EHindex){
					valuedob=pm130list[index].mapValues(addrs, value);
				}
				
				for(int j=0;j<modbusDevices[index].valuesToRead.size();j++){
					int localaddressint = modbusDevices[index].valuesToRead.get(j)[1];
					if(localaddressint==addrs){
						int localmax = modbusDevices[index].valuesToRead.get(j)[3];
						int localmin = modbusDevices[index].valuesToRead.get(j)[4];
						if(valuedob>localmax || valuedob<localmin ){
							alarm=1;
							general_alarms=0;
						}
					}
				}
				
				if (((byte)datax[bytecounter+4] )==(byte)0xff && ((byte)datax[bytecounter+5])==(byte)0xff ) { 
					alarm =10 ;
					} 
				double[] intframe = {addrs, qunty, valuedob,alarm}; //mapValues
				dataFrameList.add(intframe);
				
			
			
			if(qunty==1) bytecounter=bytecounter+6;
			else bytecounter=bytecounter+8;
		}
		setWorkingTime(modbusDevices[index].address,general_alarms);
		return dataFrameList;
	}
	
	public void storeInDB(ArrayList<double[]> framelist,int[] valIdIndex){
		System.out.print("\nStoring Data:"+"--frames"+framelist.size()+"--frames"+valIdIndex.length+"\n");
		for(int i=0;i<valIdIndex.length;i++){
			double[] localint = framelist.get(i);
			updateInstaData(valIdIndex[i],localint[2],localint[3]);
			insertHistory(valIdIndex[i],localint[2],localint[3]);			
		}
	}
	
	public int storeData(byte deviceaddress){
		byte[] localpdata = SysCom.packetCheck();
		byte[] localdata= SysCom.databuffer;
		System.out.print("\nCommand Received:");
		SysUtils.printbyte2hex(localdata[1]);
		System.out.print("\n");
		if(localdata[1]==(byte)0xA1){
			System.out.println("\npreparing to store data");
			int index = devicemap[deviceaddress];	
			ArrayList<double[]> framelist = extractDataFrames(localdata,index);
			int intvaltoread = modbusDevices[index].valuesToRead.size();
			int[] valIdIndex = new int[intvaltoread];
			for (int i=0; i<intvaltoread ;i++){
				valIdIndex[i]=modbusDevices[index].valuesToRead.get(i)[0];
			}
			storeInDB(framelist,valIdIndex);		
		}	
		else if(localdata[1]==(byte)0xB1){
			System.out.println("\n485 cable is disconnected from the remote Modbus Device");
			 
		}	
		return localpdata[3];
	}
	
	
	public void setDeviceOffline(byte deviceaddress1, int deviceIndex){
		displayStatus("Modbus " +modbusDevices[deviceIndex].address+" OFFLINE    ");
		setDeviceComstat(deviceaddress1,0);
	}
	
	public void setDeviceOnline(byte deviceaddress1, int deviceIndex, int status){
		displayStatus("Modbus " +modbusDevices[deviceIndex].address+" ONLINE    ");
		setDeviceComstat(deviceaddress1,status);
	}
	
	public void setDeviceOffline(String deviceaddress1, int deviceIndex){
		displayStatus("Modbus " +modbusDevices[deviceIndex].wmxaddress+" OFFLINE    ");
		setDeviceComstat(deviceaddress1," online>0 "," online-1 ", " 0 ");
	}
	
	public void setDeviceDisconnected(String deviceaddress1, int deviceIndex){
		displayStatus("Modbus " +modbusDevices[deviceIndex].wmxaddress+" OFFLINE    ");
		setDeviceComstat(deviceaddress1," online>-1 "," -1 ", " -1 ");
	}
	
	public void setDeviceOnline(String deviceaddress1, int deviceIndex, int status){
		displayStatus("Modbus " +modbusDevices[deviceIndex].wmxaddress+" ONLINE    ");
		setDeviceComstat(deviceaddress1,status);
	}
	
	public void setDeviceComstat(String deviceaddress1, int state){
		setDeviceComstat(deviceaddress1, ""+state, " 1 ");
	}
	
	public void addServiceTime(String deviceaddress1){
		String procedure= "addServiceTime"; 	 		 
		String[] argument={"'"+deviceaddress1+"'"};
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.callVoidProcedure(procedure, argument);
	}
	
	public void setDeviceComstat(String deviceaddress1, String specialcondition, String thenValue, String elseValue ){
		String variable= "online"; 	 		 
		String table="modbus_devices";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.updateCase(table, variable, specialcondition, thenValue, elseValue, " address= '"+deviceaddress1+"'");	
		addServiceTime(deviceaddress1);
	}
	
	public void setDeviceComstat(String deviceaddress1, String state, String specialcondition){
		String[] args1= {"online"};
		String[] dat= new String[args1.length];
		
		dat[0]= ""+state+"";
		
		String condition = " address= '"+deviceaddress1+"' AND "+specialcondition;
		String table="modbus_devices";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.update(table, args1,condition, dat);	
		addServiceTime(deviceaddress1);
	}
	
	public void setDeviceComstat(byte deviceaddress1, int state){
		String[] args1= {"online"};
		String[] dat= new String[args1.length];
		
		dat[0]= ""+state+"";
		
		String condition = " address= "+deviceaddress1+" ";
		String table="modbus_devices";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		cn.update(table, args1,condition, dat);	
	}
		
	
	
	public static ArrayList<String[]>  getDeployList(){

		String argument= "address,deploy";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_devices";
		return cn.select(table, argument, " 1 ");
	}
	
	public static void getDeviceList(){

		String argument= "*";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_devices";
		AmpList = cn.select(table, argument, " deploy>0 ");
	}
	
	public static int [][] getDeviceVariables(int device){
		String argument= "*";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_devices_vars";
		ArrayList<String[]> stats = cn.select(table, argument, " device_id= "+device);
		int [][] devicevarsint = new int[stats.size()][3];
		for(int i=0; i<stats.size();i++){
			String[] deviceVars= stats.get(i);
			devicevarsint[i][0]  = Integer.parseInt(deviceVars[2]);
			devicevarsint[i][1]  = Integer.parseInt(deviceVars[3]);
			devicevarsint[i][2]  = Integer.parseInt(deviceVars[4]);
		}
		return devicevarsint;
	}	
	//select  from  where ;

	public static ArrayList<int[]> getDeviceVariablesList(int device){
		String argument= "modbus_devices_vars.id,startaddress,quantity,maxvaluex,minvaluex,last_value";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_devices_vars LEFT JOIN modbus_meassurement_templates ON modbus_meassurement_templates.id=modbus_devices_vars.mes_template_id";
		ArrayList<String[]> stats = cn.select(table, argument, " device_id= "+device+" LIMIT 14");
		ArrayList<int[]>  devicevarsint = new ArrayList<int[]>();
		for(int i=0; i<stats.size();i++){
			String[] deviceVars= stats.get(i);
			int[] localint = new int[6];
			for(int j=0;j<6;j++){
				localint[j]=(int)Double.parseDouble(deviceVars[j]);
			}
			devicevarsint.add(localint);
		}
		return devicevarsint;
	}
	
	public static ArrayList<String[]> getSpecialPars(int device){
		String argument= "startaddress,quantity,specialpar";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_devices_vars LEFT JOIN modbus_meassurement_templates ON modbus_meassurement_templates.id=modbus_devices_vars.mes_template_id";
		ArrayList<String[]> stats = cn.select(table, argument, " device_id= "+device+" LIMIT 14");
		return stats;
	}
	
	private void debugarray(ArrayList<String[]> parametersList){
		System.out.println("\nArray -------");
		for(int i=0;i<parametersList.size();i++){			
			String[] localstring = parametersList.get(i);
			System.out.println("\nLine:"+i+ "  size : "+localstring.length);
			for(int j=0; j < localstring.length; j++){
				System.out.print(" - "+localstring[j]);
			}
			
		}
		System.out.println("\nEND -------");
	}
	
	
	
	public void createPM130EH(int ind, byte address, int deviceid){
		createPM130EH(ind, address, deviceid, false);
	}
	
	public void createPM130EH(int ind, byte address, int deviceid, boolean ethernet){
		String argument= "modbus_device_templates.varname,modbus_device_templates.address,modbus_device_config.*";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_device_config LEFT JOIN modbus_device_templates ON modbus_device_templates.id=modbus_device_config.template_id ";
		ArrayList<String[]> parametersList = cn.select(table, argument, " device_id= "+deviceid);
		debugarray(parametersList);
		
		try {
			if (ethernet) address =(byte)1;
			pm130list[ind] = new PM130EH(parametersList, address)	;
			pm130list[ind].setPostSetup(getPM130EHSetpoints(ind, address, deviceid));
			pm130list[ind].setSpecialPars(getSpecialPars(deviceid));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.print(pm130list[ind].getString());
	}
	
	public ArrayList<String[]> getPM130EHSetpoints(int index, byte address, int deviceid){ 
		String argument= "modbus_postsetup_templates.address, modbus_device_postsetup.value";
		MySQL cn = new MySQL();
		cn.database="ampdb";
		cn.connect();
		String table="modbus_device_postsetup LEFT JOIN modbus_postsetup_templates ON modbus_postsetup_templates.id=modbus_device_postsetup.template_id ";
		ArrayList<String[]> parametersList = cn.select(table, argument, " modbus_postsetup_templates.address>2575 AND modbus_postsetup_templates.address<2704 AND device_id= "+deviceid);		
		return parametersList;
	}
	
	public boolean DeviceList(){
		getDeviceList();
		int listSize = AmpList.size();
		System.out.println("Items found "+listSize);
		if (listSize==0) {
			System.out.println("no devices found");
			return false;
		}
		modbusDevices = new ModbusDevice[listSize];
		pm130list = new PM130EH[listSize];
		for(int i=0; i<AmpList.size();i++){
			String[] deviceValues= AmpList.get(i);
			int deviceid = Integer.parseInt(deviceValues[0]);
			int isEthernet = Integer.parseInt(deviceValues[11]);
			int intaddress = 1;
			if(isEthernet==0) intaddress =Integer.parseInt(deviceValues[1]);
			String stringAddress =deviceValues[1];
			String name = deviceValues[2];  
			int type = Integer.parseInt(deviceValues[8]);	
			int deploy = Integer.parseInt(deviceValues[9]);	
			
			devicemap[intaddress]=i;
			if (PM130EHindex==type) {
						System.out.println("\n device is type: "+deviceValues[8]+" PM130EH");
						if(isEthernet==0) createPM130EH(i,(byte)intaddress, deviceid);		
						else createPM130EH(i,(byte)intaddress, deviceid,true);
			}
			try {
				
				if(isEthernet==0){
					if(WMXprotocol){
						modbusDevices[i]= new ModbusDevice(stringAddress,getDeviceVariablesList(deviceid),name,type,deploy);
					}
					else {
						modbusDevices[i]= new ModbusDevice((byte)intaddress,getDeviceVariablesList(deviceid),name,type,deploy);
					}
				} else {
					modbusDevices[i]= new ModbusDevice(stringAddress,getDeviceVariablesList(deviceid),name,type,deploy);
					modbusDevices[i].isEthernet(isEthernet, stringAddress);
				}
				
			} catch (Exception e) {
				e.printStackTrace();  
				return false;
			}
			
		}
		return true;
	}
	
	public void sendModbusData(byte address,byte[] data){

		SysCom.output_buffer( 3, address,  0xFF, data.length, data);  
		SysCom.serialSend(); 			 
	}
	
	public void sendModbusData(String address,byte[] data){
		WMXModCom.o_destination_address=address;
		WMXModCom.modFunction.sendMBD(data);		 
	}
	
	public void sendModbusData(Modbus data){
		WMXModCom.modFunction.sendMB(data);
	}
	
	public void storeInstaVars(int devIndex){
		setDeviceOnline(modbusDevices[devIndex].wmxaddress, devIndex,2);
		for (int j=0; j < modbusDevices[devIndex].valuesToRead.size(); j++){
			int alarm =0;
			int localaddressint = modbusDevices[devIndex].valuesToRead.get(j)[1];
			int value = modbusDevices[devIndex].instaValues[j];
			double valuedob=pm130list[devIndex].mapValues(localaddressint, value); 
			int localmax = modbusDevices[devIndex].valuesToRead.get(j)[3];
			int localmin = modbusDevices[devIndex].valuesToRead.get(j)[4];
			if(valuedob>localmax || valuedob<localmin ){
				alarm= 1;
			} else alarm = 0;
			int localid = modbusDevices[devIndex].valuesToRead.get(j)[0];
			if(modbusDevices[devIndex].instaValuesSet[j]){
				updateInstaData(localid, valuedob, alarm);
				insertHistory(localid, valuedob, alarm);
			}			
		}	
		modbusDevices[devIndex].restoreInstaValues();
	}
	
	
	public void getDevicesVars(){
		try {
			
			System.out.println("-----");
			for(int i=0;i<modbusDevices.length;i++){
							if(modbusDevices[i].deploy==2){
								if (WMXprotocol || modbusDevices[i].isEthernet()>0){
									System.out.println("\n----WMX: getting variables");
									int offlinecounts =0, onlinecounts=0;
									confirmACK=false;
									for (int j=0; j<modbusDevices[i].valuesToRead.size() && offlinecounts< maxcount; j++){
										offlinecounts=getcom(j,i,2);
										if(offlinecounts<maxcount) onlinecounts++;
									}
									if (onlinecounts> 0) storeInstaVars(i);
									else if(confirmACK) setDeviceDisconnected(modbusDevices[i].wmxaddress, i);
									else if(onlinecounts==0) {
										if(WMXprotocol)	setDeviceOffline(modbusDevices[i].wmxaddress, i);
										else if(modbusDevices[i].isEthernet()>0) setDeviceOffline(modbusDevices[i].ipAddress, i);
									}								
									if(offlinecounts!=0)System.out.println("\n----WMX: device unresponsive "+offlinecounts+" times");
								}
								else {
									System.out.println("\n---- getting variables");  
									getcom(modbusDevices[i].varsdata,i,2);  
								}
							}
							else {
								System.out.println("\n---- Device not deployed "+modbusDevices[i].devicename); 
							}
									
			}
		} catch (Exception e) {
			e.printStackTrace();  
		}		
	}
	
	public void deploy(){
			for(int i=0;i<modbusDevices.length;i++){
				if(modbusDevices[i].deploy==1){
					if(modbusDevices[i].devicetype==PM130EHindex){
							System.out.println("\n---- setting config");
							int offlinecounts =0;
							for(int j=0;j<pm130list[i].configsetup.size() && offlinecounts<maxcount;j++){
								if (offlinecounts< maxcount){
									if(modbusDevices[i].isEthernet()>0) offlinecounts=getcom(j, i ,pm130list[i].setConfig(j));
									else offlinecounts= getConfirm(pm130list[i].setConfig(j,true),i,2);
								}
								if(offlinecounts>=maxcount)WMXModCom.resetModem();
							}
							System.out.println("\n---- postsetup");
							for(int j=0;j<pm130list[i].setpointsetup.size() && offlinecounts<maxcount;j++){
								if (offlinecounts< maxcount){
									if(modbusDevices[i].isEthernet()>0) getcom(j, i ,pm130list[i].setConfig(j));
									else offlinecounts = getConfirm(pm130list[i].setSetPoints(j,true),i,2);
									if(offlinecounts>=maxcount)WMXModCom.resetModem();
								}																		
							}
							if(modbusDevices[i].isEthernet()==0){
								if(WMXprotocol){
									if (offlinecounts< maxcount) updatedeploy(modbusDevices[i].wmxaddress);
								} else {
									if (offlinecounts< maxcount) updatedeploy(modbusDevices[i].address);
								}								
							}
							else if (offlinecounts< maxcount) updatedeploy(modbusDevices[i].ipAddress);
					}  
				}			 	
			}  
	}  
	

	private void startSerial(){
		//if (WMXprotocol) WMXModCom.modbusIni();
		//else SysCom.serialIni();
	}
	
	public void loop(){
		/*try {
			startSerial();			
			getPM130EHIndex();
			
	     	while(!Sigmation.resetSignal){
	     		
	     		if(DeviceList()){
	     			deploy();
	     			WMXModCom.resetModem();
				    getDevicesVars();
				    WMXModCom.checkMBT();
	     		} else WMXModCom.checkMBT();
	     		
	     			     		
	     	}					
		} catch (Exception e) {
			e.printStackTrace();
		}
		 try {
			 Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}	*/	
	}
	
	private static void startMQTT() {
		mdbMqtt = new pahoMqtt(SmgSys, res, param);
		Responder thisresponder = mdbMqtt.new Responder() {
			@Override
			public void mqttMessageReceived(SigmationVarTopic stopic, MqttClient publisher, MqttDeliveryToken token) {
				MqttTopic mqttTopic = publisher.getTopic(stopic.getCurrentTopic()); 
				 try {
				   if(!stopic.getCurrentValString().contentEquals("__first_set")) {
					   if(topicProcessing(stopic)) {
							token = mqttTopic.publish(stopic.getCurrentVal(),0,false);
						} else {
							//TODO avisar falla
						}
				   }					
				} catch (MqttPersistenceException e) { 
					e.printStackTrace();
				} catch (MqttException e) { 
					e.printStackTrace();
				}
			}
		};
		mdbMqtt.addListener(thisresponder); 
		mdbMqtt.start();
	}
	
	private static boolean topicProcessing(SigmationVarTopic stopic) {  
		return true;
	}
	
	private static void statToMqtt(StatObject thisStatObject, String objectName, String varname, String groupname, String group) {
		helpers.statToMqtt(mdbMqtt,thisStatObject, objectName, varname, groupname, group, null); 
	}
	
	private static void statToMqtt(StatObject thisStatObject, String objectName, String varname, String groupname, String group, String setPar) {
		helpers.statToMqtt(mdbMqtt,thisStatObject, objectName, varname, groupname, group, setPar); 
	}
	
	private static void startVentObject(String objectName, String modbusAddress, String modemAddress, String ipAddress) {
				
		statToMqtt(VentStats.ParametrosRed.deviceModbusAddress, objectName,"deviceModbusAddress",VentStats.ParametrosRed.cln,VentStats.ParametrosRed.clng,modbusAddress);
		statToMqtt(VentStats.ParametrosRed.deviceModemAddress, objectName,"deviceModemAddress",VentStats.ParametrosRed.cln,VentStats.ParametrosRed.clng,modemAddress);
		statToMqtt(VentStats.ParametrosRed.deviceIPAddress, objectName,"deviceIPAddress",VentStats.ParametrosRed.cln,VentStats.ParametrosRed.clng,ipAddress); 
		statToMqtt(VentStats.Controles.relayVentilador, objectName,"relayVentilador",VentStats.Controles.cln,VentStats.Controles.clng,"false"); 
		helpers.metaObjectComp(mdbMqtt,objectName, "Objeto prueba", "por definir","rfDevice", getMetaParams());		
	}
	
	private static ArrayList<jsonObject[]> getSecondaryInfo() {
		String[][] deviceModbusAddress = {{"groupname",VentStats.ParametrosRed.clng},{"varname","deviceModbusAddress"}};
		String[][] deviceModemAddress = {{"groupname",VentStats.ParametrosRed.clng},{"varname","deviceModemAddress"}};
		String[][] deviceIPAddress = {{"groupname",VentStats.ParametrosRed.clng},{"varname","deviceIPAddress"}};
		ArrayList<jsonObject[]> items = new ArrayList<SysUtils.jsonObject[]>();	
		items.add(helpers.itemObject(deviceModbusAddress));
		items.add(helpers.itemObject(deviceModemAddress));
		items.add(helpers.itemObject(deviceIPAddress));
		return items;
	}
	
	private static ArrayList<jsonObject[]> getcontrols() {
		String[][] relayVentilador = {{"groupname",VentStats.Controles.clng},{"varname","relayVentilador"}}; 
		ArrayList<jsonObject[]> items = new ArrayList<SysUtils.jsonObject[]>();	
		items.add(helpers.itemObject(relayVentilador));  
		return items;
	}
	
	private static ArrayList<jsonArray> getMetaParams(){
		ArrayList<jsonArray> metaParams = new ArrayList<SysUtils.jsonArray>();
		jsonArray metaParamsInfo = new SysUtils().new jsonArray("secondaryInfo", getSecondaryInfo());
		metaParams.add(metaParamsInfo);
		jsonArray metaParamsControls = new SysUtils().new jsonArray("controls", getcontrols()); 
		metaParams.add(metaParamsControls); 
		return metaParams; 
	}
	
	private static void deployModbusDevices() {
		MongoDriver localMongoDB = new MongoDriver(mongoServer, databaseName);
		
		
	}
}

