package sigmation.modbus;

import java.util.ArrayList;

public class ModbusDevice {

	byte address;
	String wmxaddress="0";
	String ipAddress="";
	String devicename="unknown";
	private int isEthernetDevice=0;
	int devicetype;
	int deploy=0;
	ArrayList<int[]> valuesToRead;
	int[] instaValues;
	boolean[] instaValuesSet;
	byte[] varsdata;
	byte[] rawData;
	
	public ModbusDevice(byte address ){
		this.address = address;	
		this.devicetype=0;
	}
	
	public ModbusDevice(byte address, ArrayList<int[]> valuesToRead ){
		this.address = address;	
		this.valuesToRead =valuesToRead;
	}
	
	public ModbusDevice(byte address, ArrayList<int[]> valuesToRead , String name, int devicetype, int deploy){
		this.address = address;
		this.devicename = name;
		this.valuesToRead =valuesToRead;
		this.instaValues = new int[valuesToRead.size()];
		this.instaValuesSet = new boolean[valuesToRead.size()];
		this.devicetype=devicetype;
		this.deploy = deploy;
		this.varsdata=getVarsData();
	}
	
	public ModbusDevice(String address, ArrayList<int[]> valuesToRead ){
		this.address = (byte)0x01;		
		setWmxAddress(address);
		this.valuesToRead =valuesToRead;
	}
	
	public ModbusDevice(String address, ArrayList<int[]> valuesToRead , String name, int devicetype, int deploy){
		this.address = (byte)0x01;	
		setWmxAddress(address);
		this.devicename = name;
		this.valuesToRead =valuesToRead;
		this.instaValues = new int[valuesToRead.size()];
		this.instaValuesSet = new boolean[valuesToRead.size()];
		this.devicetype=devicetype;
		this.deploy = deploy;
		this.varsdata=getVarsData();
	}
	
	/** 
	 * isEthernet
	 * 0 = non ethernet device (serial device)
	 * 1 = raw ethernet serial device (data sent as it is, no wrapers)
	 * 2 = modbus ethernet device (uses ethernet modbus wraper)
	 * */
	public void isEthernet(int isEthernetDevice){
		this.isEthernetDevice = isEthernetDevice;
	}
	
	/** 
	 * isEthernet
	 * 0 = non ethernet device (serial device)
	 * 1 = raw ethernet serial device (data sent as it is, no wrapers)
	 * 2 = modbus ethernet device (uses ethernet modbus wraper)
	 * 
	 * ipAddress = sets ip address string
	 * */
	public void isEthernet(int isEthernetDevice, String ipAddress){
		this.ipAddress = ipAddress;
		this.isEthernetDevice = isEthernetDevice;
	}
	
	/** 
	 * isEthernet
	 * 0 = non ethernet device (serial device)
	 * 1 = raw ethernet serial device (data sent as it is, no wrapers)
	 * 2 = modbus ethernet device (uses ethernet modbus wraper)
	 * */
	public int isEthernet(){
		return this.isEthernetDevice;
	}
	
	public void setWmxAddress(String WmxAddress){
		wmxaddress=WmxAddress;
		this.address = (byte)0x01;	
	}
	
	public void setWmxAddress(int WmxAddress){
		wmxaddress=""+WmxAddress;
		this.address = (byte)0x01;	
	}
	
	public void setWmxAddress(byte WmxAddress){
		int intWmxAddress = WmxAddress & 0xFF;
		wmxaddress=""+intWmxAddress;
		this.address = (byte)0x01;	
	}
	
	public String getName(){
		return devicename;
	}
	
	public String getDeviceDetails(){
		return devicename + " - Address: " + address + " - vars to read: " + valuesToRead.size();
	}
	
	public byte[] getVarsData(int index){
		Modbus myModbus = new Modbus();
		int[] localint = valuesToRead.get(index);
		int localaddress = localint[1];
		int localvalue = localint[2];
		myModbus.readInputRegisters((byte)address, localaddress, localvalue);
		return myModbus.data;
	}
	
	public Modbus getModbusObj(int index){
		Modbus myModbus = new Modbus();
		int[] localint = valuesToRead.get(index);
		int localaddress = localint[1];
		int localvalue = localint[2];
		myModbus.readInputRegisters((byte)address, localaddress, localvalue);
		return myModbus;
	}
	
	public Modbus getModbusObj(int index, boolean noaddress){
		Modbus myModbus = new Modbus();
		int[] localint = valuesToRead.get(index);
		int localaddress = localint[1];
		int localvalue = localint[2];
		myModbus.readInputRegisters((byte)0x01, localaddress, localvalue);
		return myModbus;
	}
	
	public ModbusOEth getEthVarsData(int index){
		boolean packAsMB = false;
		int[] localint = valuesToRead.get(index);
		int localaddress = localint[1];
		int localvalue = localint[2];
		if (this.isEthernetDevice==2) packAsMB = true;
		System.out.println("Ethernet starting --");	
		ModbusOEth localEthObject = new ModbusOEth(ipAddress, 9001, packAsMB);
		if(localEthObject.isInit()){
			System.out.println("Ethernet sending --");	
			localEthObject.readHoldingRegisters((byte)1, localaddress, localvalue);
			localEthObject.askAndClose();			
			return localEthObject;
		}else {
			System.out.println("Ethernet did not init --");
			return null;
			}		
	}
	
	public ModbusOEth setEthVarsStat(int index, byte[] tosend){
		boolean packAsMB = false; 
		if (this.isEthernetDevice==2) packAsMB = true;
		ModbusOEth localEthObject = new ModbusOEth(ipAddress, 9001, packAsMB); 
		if(localEthObject.isInit()){
			localEthObject.data=tosend;
			localEthObject.askAndClose();			
			return localEthObject;
		}else return null;
		
	}
	
	public byte[] getVarsData(){
		byte[] datax= new byte[(valuesToRead.size()*4)+3];
		datax[0]=(byte)address;
		datax[1]=(byte) 0xA1;
		datax[2]=(byte)valuesToRead.size();
		for(int index=0; index<valuesToRead.size();index++){
			int[] localint = valuesToRead.get(index);
			int localaddress = localint[1];
			int localvalue = localint[2];	
			datax[4+(4*index)] = (byte) ((localaddress & 0x000000ff));
			datax[3+(4*index)] = (byte) ((localaddress & 0x0000ff00) >>> 8);	
			datax[6+(4*index)] = (byte) ((localvalue & 0x000000ff));
			datax[5+(4*index)] = (byte) ((localvalue & 0x0000ff00) >>> 8);
		}
		return datax;
	}
	
	public void insertInstaValue(int index, int instavalue){
		instaValues[index] = instavalue;
		instaValuesSet[index] = true;
	}
	
	public void restoreInstaValues(){ 
		instaValuesSet = new boolean[valuesToRead.size()];
	}
}
