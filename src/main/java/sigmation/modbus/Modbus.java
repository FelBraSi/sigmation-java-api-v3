package sigmation.modbus;



public class Modbus {

	public byte[] data;		
	public int address=0;
	public int function=0;
	public int startAddress = 0;
	public int quantityOfRegisters = 0;
	
	//calculate CRC for Modbus
	
	private int modbusCRC16()
	{		 
		return modbusCRC16(data,true);  
	}
	
	private int modbusCRC16(byte[] databytes)
	{		 
		return modbusCRC16(databytes,false);  
	}
	
	private int modbusCRC16(byte[] databytes, boolean fillcrc)
	{
		int crc = 0xFFFF;

	  	for (int pos = 0; pos < databytes.length-2; pos++) {
		    crc ^= (int)databytes[pos] & 0xFF;   // XOR byte into least sig. byte of crc
	
		    for (int i = 8; i != 0; i--) {    // Loop over each bit
		      if ((crc & 0x0001) != 0) {      // If the LSB is set
		        crc >>= 1;                    // Shift right and XOR 0xA001
		        crc ^= 0xA001;
		      }
		      else                            // Else LSB is not set
		        crc >>= 1;                    // Just shift right
		    }
	  	}
		// Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
		if(fillcrc){
			databytes[databytes.length-2] = (byte) ((crc & 0x000000ff));
			databytes[databytes.length-1] = (byte) ((crc & 0x0000ff00) >>> 8);
		}
		return crc;  
	}
	
	
	public boolean checkModbusCRC16(byte[] databytes)
	{
		if(databytes!=null){
			if(databytes.length>3){
				int crc = modbusCRC16(databytes);
			  	int pakCRC = ((databytes[databytes.length-1] & 0xff) << 8) | (databytes[databytes.length-2] & 0xff);
				if(pakCRC==crc){
					return true;
				}else{
					System.out.println("CRC fail - pkt "+pakCRC+" - calc "+crc);
					return false;
				}
			} else {
				System.out.println("invalid packet! ");
				return false;
			}			
		}
		System.out.println("CRC: null array! ");
		return false;
	}
	
	//Some code reduction for Modbus functions
	
	private void prepareAddress(int startAddress, int quantityOfRegisters){
		data[3] = (byte) (startAddress & 0xFF);
		data[2] = (byte) ((startAddress >> 8) & 0xFF);
		data[5] = (byte) (quantityOfRegisters & 0xFF);
		data[4] = (byte) ((quantityOfRegisters >> 8) & 0xFF);
		this.quantityOfRegisters=quantityOfRegisters;
		this.startAddress=startAddress;
		this.address=(data[0] & 0xff);
		this.function=(data[1] & 0xff);
	}
	
	private void finishpacket(int startAddress, int quantityOfRegisters){
		prepareAddress(startAddress, quantityOfRegisters);
		modbusCRC16();
	}
	
	private void injectdata(int[] datax){
		for (int i=0;i<datax.length;i++){
			data[8+(2*i)] = (byte) ((datax[i] & 0x000000ff));
			data[7+(2*i)] = (byte) ((datax[i] & 0x0000ff00) >>> 8);		
		}
	}
	
	private void injectdata(byte[] datax){
		for (int i=0;i<datax.length;i++){
			data[8+i] = datax[i]; 	
		}
	}
	
	private void finishpacket(int startAddress, int quantityOfRegisters, int[] datax){
		prepareAddress(startAddress, quantityOfRegisters);
		injectdata(datax);
		modbusCRC16();
	}
	
	private void finishpacket(int startAddress, int quantityOfRegisters, byte[] datax){
		prepareAddress(startAddress, quantityOfRegisters);
		injectdata(datax);
		modbusCRC16();
	}
	
	private void commonFunction(byte function,byte address, int startAddress, int quantityOfRegisters){
		data = new byte[8];
		data[0] = address;
		data[1] = function;
		finishpacket(startAddress,quantityOfRegisters);
	}
	
	private void commonFunction(byte function,byte address, int startAddress, int quantityOfRegisters, int[] datax){
		data = new byte[9 + (datax.length*2)];
		data[0] = address;
		data[1] = function;
		data[6] = (byte) (datax.length*2);
		finishpacket(startAddress,quantityOfRegisters,datax);
	}
		
	private void commonFunction(byte function,byte address, int startAddress , int[] datax){
		data = new byte[9 + (datax.length*2)];
		data[0] = address;
		data[1] = function;
		data[6] = (byte) (datax.length*2);
		finishpacket(startAddress,datax.length,datax);
	}
	
	private void commonFunction(byte function,byte address, int startAddress , byte[] datax){
		data = new byte[9 + (datax.length*2)];
		data[0] = address;
		data[1] = function;
		data[6] = (byte) (datax.length*2);
		finishpacket(startAddress,datax.length,datax);
	}
	
//Some Modbus functions
	
	public void readCoilStatus(byte address, int startAddress, int quantityOfRegisters){  //1
		commonFunction(ModbusDefinitions.READ_COIL_STATUS,address, startAddress, quantityOfRegisters);
	}
	
	public void readDiscreteInputs(byte address, int startAddress, int quantityOfRegisters){  //2
		commonFunction(ModbusDefinitions.READ_INPUT_STATUS,address, startAddress, quantityOfRegisters);
	}
	
	public void readHoldingRegisters(byte address, int startAddress, int quantityOfRegisters){ //3
		commonFunction(ModbusDefinitions.READ_HOLDING_REGISTERS,address, startAddress, quantityOfRegisters);
	}
	
	public void readInputRegisters(byte address, int startAddress, int quantityOfRegisters){  //4
		commonFunction(ModbusDefinitions.READ_INPUT_REGISTERS,address, startAddress, quantityOfRegisters);
	}
	
	public void writeSingleCoil(byte address, int startAddress, int quantityOfRegisters){  //5
		commonFunction(ModbusDefinitions.FORCE_SINGLE_COIL,address, startAddress, quantityOfRegisters);
	}
	
	public void writeSingleRegister(byte address, int startAddress, int quantityOfRegisters){  //6
		commonFunction(ModbusDefinitions.PRESET_SINGLE_REGISTER,address, startAddress, quantityOfRegisters);
	}
	
	public void writeMultipleCoils(byte address, int startAddress, int quantityOfRegisters){ 
		commonFunction(ModbusDefinitions.FORCE_MULTIPLE_COILS,address, startAddress, quantityOfRegisters);
	}
	
	public void writeMultipleCoils(byte address, int startAddress, int quantityOfRegisters , int[] datax){ 
		commonFunction(ModbusDefinitions.FORCE_MULTIPLE_COILS,address, startAddress, quantityOfRegisters , datax);
	}
	
	public void writeMultipleRegisters(byte address, int startAddress, int quantityOfRegisters , int[] datax){
		commonFunction(ModbusDefinitions.PRESET_MULTIPLE_REGISTERS,address, startAddress, quantityOfRegisters , datax);
	}
	
	public void writeMultipleRegisters(byte address, int startAddress , int[] datax){
		commonFunction(ModbusDefinitions.PRESET_MULTIPLE_REGISTERS,address, startAddress, datax);
	}
	
	/**========================= MODBUS EXTENDED =====================================
	 * 
	 * EXTENSIONS REQUIRED FOR SPECIAL FUNCTIONS
	 * 
	 * ==============================================================================*/
	
	public void getAndSetTimestamp(byte address, int startAddress , byte[] datax){
		/**
		 * 
		 * CUSTOM FUNCTION TO SET TIMESTAMP WHILE GETTING A REGISTER, 
		 * EXPECTED TO RECEIVE DATA + INDEX, ERROR CODE EC_NEGATIVE_ACKNOWLEDGE(0x07) MEANS NO REGISTERS LEFT TO SEND 
		 * 
		 * */
		commonFunction(ModbusDefinitions.GET_AND_SET_TIMESTAMP,address, startAddress, datax);
	}
	
	public void getNextDeleteLast(byte address, int startAddress , byte[] datax){
		/**
		 * 
		 * CUSTOM FUNCTION TO GET THE NEXT VALUE OF THE SAME REGISTER AND CONFIRM THE LAST ONE (index) HAS BEEN RECEIVED
		 * EXPECTED TO RECEIVE DATA + INDEX, ERROR CODE EC_NEGATIVE_ACKNOWLEDGE(0x07) MEANS NO REGISTERS LEFT TO SEND 
		 * 
		 * */
		commonFunction(ModbusDefinitions.GET_NEXT_DELETE_LAST,address, startAddress, datax);
	}
	
}
