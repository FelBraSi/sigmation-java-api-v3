package sigmation.modbus;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory; 
import com.pi4j.io.serial.StopBits;

import sigmation.utils.SysUtils;


public class ModCom {
	public static byte[] out_buffer = new byte[1] ;
	public static byte[] in_buffer = new byte[300] ;
	public static byte[] buffer = new byte[300] ;
	public static byte[] databuffer = new byte[300] ;
	public static byte currentError=0;
	public static byte currentFunction = 0 ;
	public static byte destAddress = 0 ;
	public static byte currentAddress = 0 ;
	public static byte currentZone = 0 ;
	public static boolean head_received=false;
	public static boolean eof_received=false;
	public static int currentDataSize=0;  
	public static int packet_state=0;
	public static int in_b_counter=0;
	public static java.util.Date date1= new java.util.Date();
	public static java.util.Date date2= new java.util.Date();
	public static Serial serial;
	public static int[] ResponseData;
	
	//serial setup variables
	
	public static String serialname = "/dev/ttyAMA0";
	public static Baud serialBaud = Baud._2400;
	public static DataBits serialDataBits =DataBits._8;
	public static Parity serialParity= Parity.EVEN;
	public static StopBits serialStopBits=StopBits._1;
	public static FlowControl serialFlowControl=FlowControl.NONE;
	
	//modbus output variables
	
	public static byte mAddress = 0;
	public static int mFunction =0;
	public static int mDataAddress=0;
	public static int mCoils=0;
	public static int mRegs =0;
	
	//modbus input variables
	
	public static byte iAddress = 0;
	public static byte iFunction =0;
	public static byte iError=0;
	public static int iDataSize =0;	
	
	public static final int COIL_ON   = 0xFF00;
	public static final int COIL_OFF  = 0x0000;
	
	private static final long timeoutMB=2000;
	
	public static boolean onHold = false;
	
	public static class modFunction{
		
		private static void sendMB(Modbus modbusOutput){
			out_buffer = modbusOutput.data; 
	  	    System.out.print("\n\n sending  ");
	  		SysUtils.printhex(out_buffer, out_buffer.length);
	  		System.out.println("> ");
			serialSend();
			startMBT();
			onHold=true;
		}
		
		
		
		private static void singleMB(byte fAddress, int fDataAddress, int fCoils){
			mAddress = fAddress;
			mDataAddress = fDataAddress;
			if(mFunction<3 || mFunction ==5)mCoils = fCoils;
			else mRegs = fCoils;
			
			Modbus modbusOutput = new Modbus(); 
			if(mFunction==1) modbusOutput.readCoilStatus(fAddress, fDataAddress, fCoils);
			if(mFunction==2) modbusOutput.readDiscreteInputs(fAddress, fDataAddress, fCoils);
			if(mFunction==3) modbusOutput.readHoldingRegisters(fAddress, fDataAddress, fCoils);
			if(mFunction==4) modbusOutput.readInputRegisters(fAddress, fDataAddress, fCoils);
			if(mFunction==5) modbusOutput.writeSingleCoil(fAddress, fDataAddress, fCoils);
			if(mFunction==6) modbusOutput.writeSingleRegister(fAddress, fDataAddress, fCoils);
			sendMB(modbusOutput);
		}
		
		public static void f1(byte fAddress, int fDataAddress, int fCoils){
			mFunction = 0x01;
			singleMB(fAddress, fDataAddress, fCoils);
		}
		
		public static void f2(byte fAddress, int fDataAddress, int fCoils){
			mFunction = 0x02; 
			singleMB(fAddress, fDataAddress, fCoils);
		}
		
		public static void f3(byte fAddress, int fDataAddress, int fRegs){
			mFunction = 0x03; 
			singleMB(fAddress, fDataAddress, fRegs);
		}
		
		public static void f4(byte fAddress, int fDataAddress, int fRegs){
			mFunction = 0x04; 
			singleMB(fAddress, fDataAddress, fRegs);
		}
		
		public static void f5(byte fAddress, int fDataAddress, boolean coilState){
			mFunction = 0x05; 
			int coilValue = COIL_OFF;
			if(coilState) coilValue = COIL_ON;
			singleMB(fAddress, fDataAddress, coilValue);
		}
		
		public static void f6(byte fAddress, int fDataAddress, int regValue){
			mFunction = 0x06; 
			singleMB(fAddress, fDataAddress, regValue);
		}
		
		public static void f15(byte fAddress, int fDataAddress, int fCoils, int[] datax){
			mFunction = 0x0F; 
			mAddress = fAddress;
			mDataAddress = fDataAddress;
			mCoils = fCoils;
			Modbus modbusOutput = new Modbus(); 
			modbusOutput.writeMultipleCoils(fAddress, fDataAddress, fCoils, datax);
			sendMB(modbusOutput);
		}
		
		public static void f16(byte fAddress, int fDataAddress, int fRegs, int[] datax){
			mFunction = 0x10; 
			mAddress = fAddress;
			mDataAddress = fDataAddress;
			mRegs = fRegs;
			Modbus modbusOutput = new Modbus(); 
			modbusOutput.writeMultipleRegisters(fAddress, fDataAddress, fRegs, datax);
			sendMB(modbusOutput);
		}
	}
	
 
	
	public static void modbusIni(	String serialname,
					Baud serialBaud,
	                DataBits serialDataBits,
	                Parity serialParity,
	                StopBits serialStopBits,
	                FlowControl serialFlowControl)
	{
		ModCom.serialname=serialname;
		modbusIni(serialBaud, serialDataBits, serialParity, serialStopBits, serialFlowControl);
		modbusIni();
	}
	
	public static void modbusIni(	 
			Baud serialBaud,
            DataBits serialDataBits,
            Parity serialParity,
            StopBits serialStopBits,
            FlowControl serialFlowControl)
	{ 
	ModCom.serialBaud=serialBaud;
	ModCom.serialDataBits=serialDataBits;
	ModCom.serialParity=serialParity;
	ModCom.serialStopBits=serialStopBits;
	ModCom.serialFlowControl=serialFlowControl;
	modbusIni();
	}
	
	public static int iAddressInt(){
		return iAddress & 0xFF;
	}
	
	public static int mAddressInt(){
		return mAddress & 0xFF;
	}
	
	public static boolean gotResponse(){
		if(iAddress==mAddress && iFunction==mFunction) return true;
		return false;
	}

	public static void flush_in_buffer(){
		int i=0;
		for (i=0;i<in_buffer.length;i++){
			in_buffer[i]=0;
		}		
	}	
	public static void flush_in_databuffer(){
		int i=0;
		for (i=0;i<databuffer.length;i++){
			databuffer[i]=0;
		}		
	}		

	
	public static void buffercopy(){
		for(int i=0; i<in_buffer.length;i++){
			buffer[i]=in_buffer[i];
		}
		
	}
	
	public static boolean chcks(int value){
		int chs =0;
		for(int i=1;i<value;i++){
			chs= chs ^ in_buffer[i];			
		}		
		if(chs==(int)in_buffer[value]) { 
					System.out.print("[chk:ok]");
			       return true;
		}
		else {
			  System.out.print("[chk:fail] ");
			  return false;}
	}	
	
	
	public static void getAddress(Byte data){
		currentAddress= data; ///
		packet_state=1;
		System.out.println("[State:"+packet_state+"] ");
		in_b_counter++;
	}

	public static void getFunction(Byte data){
		currentFunction=(byte)data;
		in_b_counter++;
		if((currentFunction & 0xFF)>0x81){
			byte[] toprint = {currentFunction};
		  	    System.out.print("[currentFunction] ");
		  		SysUtils.printhex(toprint, 1);
			packet_state=4;
			System.out.println("[State:"+packet_state+"] ");
		} else if (currentFunction==(byte)0x10) {
			packet_state=10;
			System.out.println("[TableSetting] ");
		} else {
			packet_state=2;
			System.out.println("[State:"+packet_state+"] ");
		}
		 
	}	
	
	public static void getByteSize(Byte data){ 
		currentDataSize=(byte)data; 
		in_b_counter++;
		packet_state=3;
		System.out.println("[State:"+packet_state+"] ");
	}

	public static void getDataBytes(Byte data){
		if(in_b_counter<=currentDataSize+2){
			in_buffer[in_b_counter]=(byte)data;
			packet_state=3;
			in_b_counter++;
		} 
		
		if(in_b_counter>(int)currentDataSize+2){
			packet_state=5;
			System.out.println("[State:"+packet_state+"] ");
			in_b_counter++;
		}
	}
	
	public static void getErrorCode(Byte data){
		currentError=(byte)data;
		packet_state=5;
		//debug_modbus_error();
		in_b_counter++;
	}	
	
	public static void getIntData(){
		ResponseData = new int[currentDataSize/2];
		for(int i=0; i<currentDataSize; i=i+2){
			ResponseData[i/2]= ((in_buffer[3+i] & 0xff) << 8 ) | (in_buffer[4+i] & 0xff);
		}
	}
	
	public static void getCRC(Byte data, Byte data2){ ////////////////////
		//hacer algo con CRC
		iAddress = currentAddress;
		iFunction =currentFunction;
		iError=currentError;
		iDataSize =currentDataSize;	
		if (iFunction==(byte)0x03 && iDataSize>0){
			getIntData();
		}
		packet_state=0;
		eof_received=true;
		in_b_counter=0;
		
	}	
	
	

	public static void resetProtocol(){
		eof_received=false;
		packet_state=0;
		in_b_counter=0; 
		flush_in_buffer();
		startMBT();
		onHold=false;
		// 	queda en estado 8 hasta no ser reseteado por checkpacket
	}	
	
	public static void serialSend(){
		try {
			Thread.sleep(50);
			serial.write(out_buffer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public static void debug_modbus_error(){
		System.out.println("Modbus Error:");
		System.out.println("[State <error>:"+packet_state+"] ");
	}
	
	public static void startMBT(){
		//comenzar timers aca
		date1= new java.util.Date();
		date2= new java.util.Date();
	}
	
	public static boolean checkMBT(){
		//comparar timers aca
		date2= new java.util.Date();
		long difference = date2.getTime() - date1.getTime();
		if(difference<timeoutMB) return false;
		resetProtocol();
		return true;
	}
	
	private static void serialDebug(){
		byte[] toprint = {in_buffer[in_b_counter]};
	  	    System.out.print("["+in_b_counter+"] ");
	  		SysUtils.printhex(toprint, 1);
	}
	
	public static void modbusIni(){
		/*
		 *   PROTOCOLO MODBUS
		 *   
		 * */
		startMBT();
		serial = SerialFactory.createInstance();
      	try {
      		SerialConfig config = new SerialConfig();
      		//config.device(SerialPort.getDefaultPort())
      		config.device(serialname) 
                  .baud(serialBaud)
                  .dataBits(serialDataBits)
                  .parity(serialParity)
                  .stopBits(serialStopBits)
                  .flowControl(serialFlowControl);

            // open the default serial device/port with the configuration settings
            serial.open(config);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 

		serial.addListener(new SerialDataEventListener() {
            @Override
            public void dataReceived(SerialDataEvent event) {
          	  try {
          		  	byte[] response = event.getBytes();
          		  	int res_s = response.length;
          		  	int i=0;
          		  	for (i=0;i<res_s;i++){ 
          		  		startMBT();
          		  		in_buffer[in_b_counter]=(byte)response[i];
          		  		serialDebug();
          		  		Byte value = (byte)response[i];
          		  		Byte value2 =0;
          		  		if(packet_state==5) { 
          		  				
          		  				value2 = (byte)response[i+1];
          		  			    in_b_counter++;
          		  			    i++;
          		  			}
          		  		
          		  		switch(packet_state){
          		  			case	0:	getAddress(value);
				  						break;
          		  			case	1:	getFunction(value);
          		  						break;
	          		  		case	2:	getByteSize(value);
	          		  					break;
	          		  		case	3:	getDataBytes(value);
			  							break;
	          		  		case	4:	getErrorCode(value);
			  							break;
	          		  		case	5:	getCRC(value, value2);
			  							break;
		          		  	case   10:	in_b_counter++;
		          		  				if(in_b_counter>7) resetProtocol();
										break;
				  			default	 :	break; 
          		  		}
          		  	}				
				} catch (Exception e) {
					head_received=false;
					in_b_counter=0;
					packet_state=0;
					flush_in_buffer();
				}
   	
                   
            	
            }            
        });		
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}	
	
}
