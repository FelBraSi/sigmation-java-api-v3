-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `idpersonas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre1` varchar(45) NOT NULL,
  `nombre2` varchar(45) DEFAULT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) DEFAULT NULL,
  `idn` varchar(45) NOT NULL,
  `tag` int(11) NOT NULL DEFAULT '0',
  `email` varchar(60) DEFAULT '',
  `area` varchar(45) DEFAULT NULL,
  `empresa` int(11) NOT NULL DEFAULT '0',
  `ultima_zona` int(11) NOT NULL DEFAULT '0',
  `ultimo_ingreso` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ultima_salida` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `foto` varchar(45) DEFAULT '0.png',
  `leasing_start` int(11) DEFAULT '0',
  `leasing_end` int(11) DEFAULT '0',
  `permanente` int(11) DEFAULT '0',
  `vehiculo` int(11) DEFAULT '0',
  `tipo_vehiculo` int(11) NOT NULL DEFAULT '0',
  `hasreader` int(11) DEFAULT '0',
  `busreaderid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpersonas`),
  KEY `empresa_idx` (`empresa`),
  KEY `tag_idx` (`tag`),
  CONSTRAINT `empresa` FOREIGN KEY (`empresa`) REFERENCES `empresas` (`idempresas`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,'Felipe','Eduardo','Bravo','Silva','15751070-3',18,'test@test.cl','Ingeniero de sistemas',1,1,'2018-10-05 16:17:05','2018-10-05 16:17:21','1',0,0,0,0,0,0,NULL),(2,'Mario','Esteban','Gomez','Pablo','154568772',5,'','Ingeniero prevencion de riesgos',0,1,'2016-12-22 19:31:20','2016-12-22 19:31:03','2',1487818800,1501300800,0,0,0,0,NULL),(10,'Maria','Magdalena','Miranda','Lopez','20445689',6,'','Ingeniero Comercial',0,3,'2016-12-30 13:56:35','2016-12-29 20:19:53','10',1487818800,1487905200,0,0,0,0,NULL),(11,'Nicolas','','Ramirez','','99999999-9',4,'','ingeniero de sistemas ',3,2,'0000-00-00 00:00:00','0000-00-00 00:00:00','11',0,0,0,0,0,0,NULL),(14,'Bob','','Esponja','','12345678-9',1,'','Ingeniero Electronico',2,1,'2017-01-06 17:21:01','2017-01-06 17:21:01','14',0,0,0,0,0,0,NULL),(15,'Nicanor','','Errazuriz','','12345678-9',6,'','Gerente General',0,1,'2017-07-21 14:39:21','2017-07-21 14:39:49','0',0,0,0,0,0,0,NULL),(16,'Mazda','Sieriaona','Rojo','0','ABCD1234',18,'','Camioneta',0,2,'2017-02-23 18:36:52','2017-02-23 18:36:13','16',0,0,0,1,0,1,1);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
