-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alarm_insta`
--

DROP TABLE IF EXISTS `alarm_insta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarm_insta` (
  `alarm_id` int(11) NOT NULL AUTO_INCREMENT,
  `series` varchar(12) NOT NULL,
  `alarm_state` int(1) NOT NULL DEFAULT '0',
  `alarm_s_test` int(1) NOT NULL DEFAULT '0',
  `alarm_radius` int(4) DEFAULT NULL,
  `mapx` int(4) DEFAULT NULL,
  `mapy` int(4) DEFAULT NULL,
  `mapid` int(6) NOT NULL DEFAULT '0',
  `online` int(1) NOT NULL DEFAULT '1',
  `state` int(2) NOT NULL DEFAULT '0',
  `zone` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`alarm_id`),
  UNIQUE KEY `series` (`series`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alarm_insta`
--

LOCK TABLES `alarm_insta` WRITE;
/*!40000 ALTER TABLE `alarm_insta` DISABLE KEYS */;
INSERT INTO `alarm_insta` VALUES (12,'3',0,0,NULL,1915,1141,1,0,0,1),(22,'-61',0,0,NULL,NULL,NULL,0,1,0,1),(23,'11',0,0,NULL,NULL,NULL,0,0,0,1),(24,'15',0,0,NULL,NULL,NULL,0,0,0,1),(25,'67',0,0,NULL,NULL,NULL,0,0,0,1),(26,'7',0,0,NULL,NULL,NULL,0,0,0,1),(27,'4',0,0,NULL,2065,864,1,1,0,4),(28,'5',0,0,NULL,668,1139,1,0,0,0);
/*!40000 ALTER TABLE `alarm_insta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
