-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `modbus_devices_vars`
--

DROP TABLE IF EXISTS `modbus_devices_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modbus_devices_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_value` float(10,1) NOT NULL DEFAULT '0.0',
  `device_id` int(11) NOT NULL,
  `maxvaluex` float(10,1) NOT NULL DEFAULT '0.0',
  `minvaluex` float(10,1) NOT NULL DEFAULT '0.0',
  `mes_template_id` int(11) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alarm` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `deviceid_idx` (`device_id`),
  KEY `templates_idx` (`mes_template_id`),
  CONSTRAINT `deviceid` FOREIGN KEY (`device_id`) REFERENCES `modbus_devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `templates` FOREIGN KEY (`mes_template_id`) REFERENCES `modbus_meassurement_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modbus_devices_vars`
--

LOCK TABLES `modbus_devices_vars` WRITE;
/*!40000 ALTER TABLE `modbus_devices_vars` DISABLE KEYS */;
INSERT INTO `modbus_devices_vars` VALUES (26,0.0,21,999999.0,0.0,1,'2018-09-05 18:08:00',0),(28,0.0,23,999999.0,0.0,1,'2018-09-06 11:42:40',0),(29,0.0,24,999999.0,0.0,1,'2018-09-06 11:49:43',0),(30,0.0,25,999999.0,0.0,1,'2018-11-05 19:16:20',0),(31,0.0,25,100000000.0,0.0,2,'2018-11-05 19:16:20',0);
/*!40000 ALTER TABLE `modbus_devices_vars` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
