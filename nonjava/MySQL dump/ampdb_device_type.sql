-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device_type`
--

DROP TABLE IF EXISTS `device_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_type` (
  `type` int(11) NOT NULL,
  `0` varchar(45) NOT NULL DEFAULT 'null',
  `1` varchar(45) NOT NULL DEFAULT 'null',
  `2` varchar(45) NOT NULL DEFAULT 'null',
  `3` varchar(45) NOT NULL DEFAULT 'null',
  `4` varchar(45) NOT NULL DEFAULT 'null',
  `5` varchar(45) NOT NULL DEFAULT 'null',
  `6` varchar(45) NOT NULL DEFAULT 'null',
  `7` varchar(45) NOT NULL DEFAULT 'null',
  `8` varchar(45) NOT NULL DEFAULT 'null',
  `9` varchar(45) NOT NULL DEFAULT 'null',
  `10` varchar(45) NOT NULL DEFAULT 'null',
  `11` varchar(45) NOT NULL DEFAULT 'null',
  `12` varchar(45) NOT NULL DEFAULT 'null',
  `13` varchar(45) NOT NULL DEFAULT 'null',
  `14` varchar(45) NOT NULL DEFAULT 'null',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_type`
--

LOCK TABLES `device_type` WRITE;
/*!40000 ALTER TABLE `device_type` DISABLE KEYS */;
INSERT INTO `device_type` VALUES (8,'Unit ID','Base Current','Unit Current','Line Voltage','Tunnel Current','Com Uplink AGC','Com Uplink Current','Com. Downlink Current','Com. Downlink AGC','Data Uplink AGC','Data Downlink AGC','Data Downlink Current','Data Uplink Current','Digital','Product type'),(9,'Unit ID','Mains OK','PS Voltage','PS current','Battery Voltage','Battery Current','null','null','null','null','null','null','null','null','Product type'),(20,'Unit ID','Base Current','Unit Current','Line Voltage','Tunnel Current','Com Uplink AGC','null','null','Com. Downlink AGC','null','null','null','null','null','Product type'),(21,'Unit ID','Base Current','Unit Current','Line Voltage','Tunnel Current','Com Uplink AGC','null','null','Com. Downlink AGC','null','null','null','null','null','Product type'),(22,'Unit ID','Base Current','Unit Current','Line Voltage','Tunnel Current','Com Uplink AGC','null','null','Com. Downlink AGC','null','null','null','null','null','Product type'),(23,'Unit ID','Base Current','Unit Current','Line Voltage','Tunnel Current','Com Uplink AGC','null','null','Com. Downlink AGC','null','null','null','null','null','Product type');
/*!40000 ALTER TABLE `device_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:05
