-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `modbus_devices`
--

DROP TABLE IF EXISTS `modbus_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modbus_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(20) DEFAULT '0',
  `name` varchar(60) DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mapx` int(11) NOT NULL DEFAULT '0',
  `mapy` int(11) NOT NULL DEFAULT '0',
  `mapid` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) DEFAULT NULL,
  `deploy` int(11) NOT NULL DEFAULT '1',
  `onservice` int(11) DEFAULT '0',
  `isethernet` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idmodbus_devices_UNIQUE` (`id`),
  UNIQUE KEY `address_UNIQUE` (`address`),
  KEY `fk_modbus_devices_1_idx` (`type_id`),
  CONSTRAINT `fk_modbus_devices_1` FOREIGN KEY (`type_id`) REFERENCES `modbus_devices_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modbus_devices`
--

LOCK TABLES `modbus_devices` WRITE;
/*!40000 ALTER TABLE `modbus_devices` DISABLE KEYS */;
INSERT INTO `modbus_devices` VALUES (21,'0001','1',0,'2018-09-06 12:46:14',485,366,1,2,1,0,0),(23,'0002','2',0,'2018-09-06 13:52:32',809,288,1,2,1,0,0),(24,'0003','3',0,'2018-10-24 19:21:01',748,1130,1,2,1,0,0),(25,'0004','test',0,'2018-11-05 19:16:32',838,1406,1,2,1,0,0);
/*!40000 ALTER TABLE `modbus_devices` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
