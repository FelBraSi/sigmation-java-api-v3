-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'ampdb'
--
/*!50003 DROP PROCEDURE IF EXISTS `activate_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `activate_persona`(idx INT, activate INT)
BEGIN

    IF activate>0 THEN
INSERT INTO `ampdb`.`zonas_permisos` (`idzona`, `idpersona`) VALUES (1, idx);
    ELSE 
DELETE FROM `ampdb`.`zonas_permisos` WHERE idpersona=idx AND idzona=1;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addServiceTime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `addServiceTime`(addrs VARCHAR(45), onservic INT)
BEGIN
DECLARE idv,stat INT;
    SELECT id INTO idv FROM modbus_devices WHERE address=addrs;
    
    IF idn IS NOT NULL THEN
SELECT onservice INTO stat FROM modbus_devices WHERE id=idv;
        
        IF stat != onservic THEN
UPDATE modbus_devices SET onservice = onservic WHERE id=idv;
            INSERT INTO servicetimes(state, device_id) values(onservic,idv);
        END IF;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `asign_bus_readers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `asign_bus_readers`(idx int, vehicle int)
BEGIN 

UPDATE `busreaders` SET bustagid=NULL WHERE bustagid=vehicle;
UPDATE `personas` SET busreaderid=NULL, hasreader=0 WHERE busreaderid=idx;

UPDATE `busreaders` SET bustagid=vehicle WHERE idbusreaders=idx;
UPDATE `personas` SET busreaderid=idx, hasreader=1 WHERE idpersonas=vehicle;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `asign_cred` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `asign_cred`(idx INT, tag_ INT, estado_ INT, leasing_start_ DATE, leasing_end_ DATE)
BEGIN
UPDATE tags SET estado=5 , idasig=0 WHERE idasig=idx;
    if estado_!=4 THEN
UPDATE personas SET tag=tag_  , leasing_start= UNIX_TIMESTAMP(leasing_start_ ), leasing_end=UNIX_TIMESTAMP(leasing_end_) WHERE idpersonas = idx; 
UPDATE tags SET estado= estado_ , leasing_start= UNIX_TIMESTAMP(leasing_start_ ), leasing_end=UNIX_TIMESTAMP(leasing_end_), idasig=idx , ignoreflag=0 WHERE idtags=tag_;
    ELSE
      UPDATE personas SET tag=tag_ , leasing_start= leasing_start_ , leasing_end=leasing_end_ WHERE idpersonas = idx; 
  UPDATE tags SET estado= estado_ , idasig=idx, ignoreflag=0  WHERE idtags=tag_;
    END IF;
    
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `asign_cred_manual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `asign_cred_manual`(idx INT, tag_ VARCHAR(45), estado_ INT, leasing_start_ DATE, leasing_end_ DATE)
BEGIN
DECLARE tag_id INT;
    UPDATE tags SET estado=5, idasig=0 WHERE idasig=idx;
    SELECT count(*) INTO tag_id FROM tags WHERE serial=tag_;
    
    IF tag_id != 0 THEN
SELECT idtags INTO tag_id FROM tags WHERE serial=tag_;
        if estado_!=4 THEN
UPDATE personas SET tag=tag_id , leasing_start= UNIX_TIMESTAMP(leasing_start_ ), leasing_end=UNIX_TIMESTAMP(leasing_end_) WHERE idpersonas = idx; 
UPDATE tags SET estado= estado_ , idasig=idx , leasing_start= UNIX_TIMESTAMP(leasing_start_ ), leasing_end=UNIX_TIMESTAMP(leasing_end_) WHERE idtags=tag_id;
ELSE
  UPDATE personas SET tag=tag_id , leasing_start= leasing_start_ , leasing_end=leasing_end_ WHERE idpersonas = idx; 
  UPDATE tags SET estado= estado_ , idasig=idx  WHERE idtags=tag_id;
END IF;   
ELSE
        SELECT 'Tag No existe / Tag doesn\'t exist' as result;
END IF;
    
    
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_bus_readers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_bus_readers`( idx int)
BEGIN
UPDATE busreaders SET del=1 WHERE idbusreaders=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_condic_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_condic_zona`(idx INT)
BEGIN
DELETE FROM `ampdb`.`condic_zonas`
    WHERE id_cond=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_cond_doc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_cond_doc`(idx INT)
BEGIN
DELETE FROM `ampdb`.`condic_zonas`
WHERE id_cond=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_doc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_doc`(idx INT)
BEGIN
DELETE FROM `ampdb`.`conf_docs`
    WHERE idconf_docs=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_subdoc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_subdoc`(idx INT)
BEGIN
DELETE FROM `ampdb`.`conf_documentos_sub`
WHERE idsubdocs=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tags`(idx INT)
BEGIN
UPDATE tags SET ignoreflag=1, idasig=null, leasing_start=0, leasing_end=0, estado=5 WHERE idtags=idx ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tipo_vehiculos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tipo_vehiculos`(idx INT)
BEGIN
DELETE FROM `ampdb`.`tipo_vehiculos` WHERE idtipo_vehiculos=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tnt_alerts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tnt_alerts`()
BEGIN
UPDATE registro_personas
    SET tipo_evento = (tipo_evento+1000)
    WHERE tipo_evento BETWEEN 4 AND 499;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tnt_alerts_p` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tnt_alerts_p`(idx INT)
BEGIN
UPDATE registro_personas
    SET tipo_evento = (tipo_evento+1000)
    WHERE idregistro_personas=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tnt_alerts_t` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tnt_alerts_t`(idx INT)
BEGIN
UPDATE registro_personas
    SET tipo_evento = (tipo_evento+1000)
    WHERE idregistro_personas=idx AND tipo_evento<1000;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tnt_reader` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tnt_reader`(idx INT)
BEGIN
DELETE FROM
`ampdb`.`rfidreaders` 
    WHERE idrfidreaders=idx; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_tnt_setting` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_tnt_setting`(idsetting_ INT)
BEGIN
DELETE FROM `ampdb`.`tnt_settings` 
    WHERE idsetting=idsetting_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `del_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_zona`(idzonas INT)
BEGIN
    DELETE FROM 
    `ampdb`.`zonas` 
    WHERE `zonas`.`idzonas`= idzonas AND numero > 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_condic_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_condic_zona`(idx INT,iddoc INT, idconf_doc INT)
BEGIN
UPDATE `ampdb`.`condic_zonas`
SET  `condic_zonas`.`iddoc`=iddoc
    WHERE id_cond=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_doc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_doc`(idx INT, doc_nomb VARCHAR(45))
BEGIN
UPDATE `ampdb`.`conf_docs`
    SET nombre = doc_nomb
    WHERE idconf_docs=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_empresa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_empresa`( idempres INT,nombre_empres VARCHAR(60), ru VARCHAR(60))
BEGIN
UPDATE `ampdb`.`empresas` SET nombre_empresa=nombre_empres , rut=ru WHERE idempresas=idempres;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_persona`( idx INT, nombr1 VARCHAR(25), nombr2 VARCHAR(25), apellid1 VARCHAR(25), apellid2 VARCHAR(25), id VARCHAR(45), ta INT, emai VARCHAR(60), are VARCHAR(25), empres INT,fot VARCHAR(6))
BEGIN
UPDATE `ampdb`.`personas`  
SET 
`nombre1`=nombr1 , 
`nombre2`= nombr2, 
`apellido1`= apellid1, 
`apellido2`= apellid2, 
`idn`= id, 
    `email`= emai, 
`area`= are, 
`empresa`= empres, 
`foto`= fot 
WHERE idpersonas=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_persona_pic` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_persona_pic`( idx INT, fot VARCHAR(6))
BEGIN
UPDATE `ampdb`.`personas`  
SET 
`foto`= fot 
WHERE idpersonas=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_subdoc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_subdoc`(idx INT,subdoc_nombr VARCHAR(45), idconf_doc INT)
BEGIN
UPDATE `ampdb`.`conf_documentos_sub`
SET  `subdoc_nombre`=subdoc_nombr, idconf_docs =idconf_doc
    WHERE idsubdocs=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_tag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_tag`(idx INT, serial_ INT, estado_ INT)
BEGIN
UPDATE `ampdb`.`tags` 
    SET 
    `tag_estado`.`serial`=serial_, 
    `tag_estado`.`estado`=estado_
WHERE idtags=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_tags`(idx INT, serial_ VARCHAR(45), estado_ INT)
BEGIN
UPDATE `ampdb`.`tags` 
    SET 
    `tags`.`serial`=serial_, 
    `tags`.`estado`=estado_,
    `tags`.`ignoreflag`=0
WHERE idtags=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_tipo_vehiculos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_tipo_vehiculos`(idx INT, nombre_tipo_ VARCHAR(45))
BEGIN
UPDATE `ampdb`.`tipo_vehiculos` SET `nombre_tipo`=nombre_tipo_ WHERE idtipo_vehiculos=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_tnt_reader` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_tnt_reader`(idx INT, serial_ VARCHAR(45), zona_a_ INT, zona_b_ INT, isethernet_ INT)
BEGIN
UPDATE 
`ampdb`.`rfidreaders` 
    SET `serial`=serial_, `zona_a`=zona_a_, `zona_b` = zona_b_  , `isethernet` = isethernet_
WHERE idrfidreaders=idx; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_tnt_reader_map` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_tnt_reader_map`(idx INT, mapx_ INT, mapy_ INT, mapid_ INT)
BEGIN
UPDATE 
`ampdb`.`rfidreaders` 
    SET `mapx`=mapx_, `mapy`=mapy_, `mapid` = mapid_
WHERE idrfidreaders=idx; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_tnt_setting` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_tnt_setting`(idsetting_ INT, setting_nam VARCHAR(45), setting_des VARCHAR(200), setting_va VARCHAR(200))
BEGIN
UPDATE `ampdb`.`tnt_settings` 
    SET 
    `tnt_settings`.`setting_name`=setting_nam, 
    `tnt_settings`.`setting_desc`=setting_des, 
    `tnt_settings`.`setting_val`=setting_va
WHERE idsetting=idsetting_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_zona`(idzonas INT,numero INT ,nombre VARCHAR(45),descripcion VARCHAR(200))
BEGIN
IF numero > 0 THEN
UPDATE 
`ampdb`.`zonas` 
SET
`numero` =numero, `nombre` = nombre, `descripcion` = descripcion
WHERE `zonas`.`idzonas`= idzonas  ;
    ELSE
UPDATE 
`ampdb`.`zonas` 
SET
`nombre` = nombre, `descripcion` = descripcion
WHERE `zonas`.`idzonas`= idzonas ;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_zona_map` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_zona_map`(idzonas INT,mapx INT ,mapy INT ,mapid INT)
BEGIN
UPDATE 
    `ampdb`.`zonas` 
    SET
    `zonas`.`mapx` =mapx, `zonas`.`mapy` = mapy, `zonas`.`mapid` = mapid
    WHERE `zonas`.`idzonas`= idzonas;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getReportOptions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getReportOptions`()
BEGIN
SELECT `id` FROM  `ampdb`.`report_options` WHERE `option`=1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getServiceTime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getServiceTime`(idx INT)
BEGIN
select IF(timesum<0,timesum+unix_timestamp(NOW()),IFNULL(timesum,0)) as tmsum FROM  (select SUM(IF(state=1,- UNIX_TIMESTAMP(time),UNIX_TIMESTAMP(time))) as timesum  from servicetimes WHERE device_id=idx) as T;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_active_access_errors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_active_access_errors`()
BEGIN
SELECT
*
    FROM registro_personas
    WHERE tipo_evento BETWEEN 4 AND 99;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_alerts_personas_flags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_alerts_personas_flags`(itemx VARCHAR(60))
BEGIN
    DECLARE last_event, hours , max_hours, overwork_num, docven_num INT;
DECLARE time_data1, time_data2 TIMESTAMP;
    SELECT (CAST(setting_val AS UNSIGNED) * 3600 ) INTO max_hours from tnt_settings WHERE setting_name= 'max_work_hours';
    IF max_hours = null THEN
SET max_hours=87000;
END IF;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona
    );

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE vehiculo=0 AND ( CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%')) 
);



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY table_filter_personas.idpersonas) ;

  
  DROP TEMPORARY TABLE IF EXISTS person_search;
  

  CREATE TEMPORARY TABLE IF NOT EXISTS person_search AS (
  SELECT
IFNULL (doc_personas.ven_flag,0) as venflag,
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
GROUP BY table_filter_personas.idpersonas);

  DROP TEMPORARY TABLE IF EXISTS pre_person_alerts;
  

  CREATE TEMPORARY TABLE IF NOT EXISTS pre_person_alerts AS (
    SELECT * from person_search WHERE venflag>0 OR over_hours>0
  )  ;

  select count(*) into overwork_num from pre_person_alerts WHERE over_hours >0;
  select count(*) into docven_num from pre_person_alerts WHERE venflag >0;
  
  DROP TEMPORARY TABLE IF EXISTS person_alerts;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS person_alerts AS (
    SELECT overwork_num, docven_num
  );
  
  select * from person_alerts;
 









END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_bus_readers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_bus_readers`()
BEGIN
SELECT * from busreaders LEFT JOIN personas ON personas.idpersonas=busreaders.bustagid WHERE del=0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_condic_zonas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_condic_zonas`()
BEGIN
SELECT 
    condic_zonas.* ,
    zonas.nombre as nombrezona,
    zonas.idzonas,
    zonas.numero,
    zonas.descripcion,
    conf_docs.*,
    conf_documentos_sub.*
    FROM
    condic_zonas 
    LEFT JOIN zonas ON zonas.idzonas = condic_zonas.idzona
    LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = condic_zonas.iddoc
    LEFT JOIN conf_docs ON conf_docs.idconf_docs = conf_documentos_sub.idconf_docs
    where idzonas>1 ORDER BY zonas.numero ASC;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_docs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_docs`(idx INT)
BEGIN
SELECT 
    * ,
    IF(vencimiento<=curdate(),1,0) as ven_flag
    FROM documentos 
    LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
    LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
    WHERE idpersona=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_docs_cat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_docs_cat`()
BEGIN
SELECT * FROM conf_docs 
    ORDER BY idconf_docs ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_documents_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_documents_persona`(idx INT)
BEGIN
SELECT 
    * ,
    conf_documentos_sub.*,
    conf_docs.*
    FROM
    documentos 
    LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
    LEFT JOIN conf_docs ON conf_docs.idconf_docs = conf_documentos_sub.idconf_docs
    WHERE idpersona=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_doc_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_doc_list`()
BEGIN
SELECT 
    conf_documentos_sub.*,
    conf_docs.*
    FROM
    conf_documentos_sub 
    LEFT JOIN conf_docs ON conf_docs.idconf_docs = conf_documentos_sub.idconf_docs
    ORDER BY conf_docs.idconf_docs ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_empresa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_empresa`(idx INT)
BEGIN
SELECT * FROM empresas WHERE idx= idempresas;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_empresa_h` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_empresa_h`(idx INT, time_data1 DATE, time_data2 DATE )
BEGIN

    DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
id_persona, 
registro_personas.empresa,
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND empresa = 0
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
id_persona, 
registro_personas.empresa,
max(tiempo) as last_timey
FROM registro_personas
WHERE tiempo < time_data2 
AND empresa = 0
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0  ,
IF (last_timex<time_data1,UNIX_TIMESTAMP(DATE(last_timex + INTERVAL 1 DAY) ),UNIX_TIMESTAMP(time_data1)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.empresa = 0
    GROUP BY id_persona);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.empresa = 0
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.empresa = 0
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
id_persona,
empresa 
FROM registro_personas 
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND empresa = 0
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY id_persona) ;

SELECT
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,unix_timestamp(current_timestamp)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
nombre_empresa,
rut
FROM
personas
LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= personas.idpersonas
LEFT JOIN empresas ON empresas.idempresas= table_last_before.empresa
WHERE personas.empresa = 0 AND vehiculo=0;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_empresa_p` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_empresa_p`(idx INT)
BEGIN
     DECLARE last_event, hours INT;

DECLARE time_data1, time_data2 TIMESTAMP;

SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;


DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
registro_personas.empresa,
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.empresa = idx
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona, 
registro_personas.empresa,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND registro_personas.empresa = idx
AND (tipo_evento=0 OR tipo_evento=3) 
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=0) 
AND registro_personas.empresa = idx
    GROUP BY registro_personas.id_persona);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=0) 
AND registro_personas.empresa = idx
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 ) 
AND registro_personas.empresa = idx
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona,
registro_personas.empresa 
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND registro_personas.empresa = idx
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY registro_personas.id_persona) ;

SELECT
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as tiempo_hoy,
    personas.*,
    DATE(FROM_UNIXTIME(tags.leasing_start)) as active_start,
    DATE(FROM_UNIXTIME(tags.leasing_end)) as active_end,
    IF((estado=1 AND CURDATE() BETWEEN DATE(FROM_UNIXTIME(tags.leasing_start)) AND DATE(FROM_UNIXTIME(tags.leasing_end))) OR estado =4, 1, 0 ) as active,
    zonas.*,
    tags.*,
    empresas.nombre_empresa
FROM
personas
LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= personas.idpersonas
    LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
LEFT JOIN empresas ON empresas.idempresas= personas.empresa
    LEFT JOIN tags ON personas.tag= tags.idtags
WHERE personas.empresa = idx  AND vehiculo=0
    GROUP BY idpersonas;








END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_inactive_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_inactive_tags`()
BEGIN
select tags.*, tag_estado.* from tags left join tag_estado on tags.estado = tag_estado.idtag_estado 
    WHERE ignoreflag=0 
    AND NOT ((estado=1 AND CURDATE() BETWEEN DATE(FROM_UNIXTIME(tags.leasing_start)) AND DATE(FROM_UNIXTIME(tags.leasing_end))) OR estado =4) 
    ORDER BY `serial`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_empresas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_empresas`()
BEGIN
select * from empresas ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_empresas_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_empresas_search`( itemx VARCHAR(60) )
BEGIN
select * from empresas 
    WHERE ( 
    rut LIKE CONCAT('%', itemx , '%') 
            OR  nombre_empresa LIKE CONCAT('%', itemx , '%') 
);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_personas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_personas`(pag INT)
BEGIN
    DECLARE last_event, hours , max_hours INT;
    DECLARE pag_count_ini, pag_count_end INT;
DECLARE time_data1, time_data2 TIMESTAMP;
    SELECT (CAST(setting_val AS UNSIGNED) * 3600 ) INTO max_hours from tnt_settings WHERE setting_name= 'max_work_hours';
    IF max_hours = null THEN
SET max_hours=87000;
END IF;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;

    SET pag_count_ini = (pag * 100)+1;
    SET pag_count_end = pag_count_ini+100;

DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
registro_personas.empresa,
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona BETWEEN pag_count_ini AND pag_count_end
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona, 
registro_personas.empresa,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND registro_personas.id_persona BETWEEN pag_count_ini AND pag_count_end
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona BETWEEN pag_count_ini AND pag_count_end
    GROUP BY registro_personas.id_persona);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona BETWEEN pag_count_ini AND pag_count_end
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona BETWEEN pag_count_ini AND pag_count_end
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona,
registro_personas.empresa 
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND registro_personas.id_persona BETWEEN pag_count_ini AND pag_count_end
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY registro_personas.id_persona) ;

    
    
SELECT
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    personas.*,
    zonas.*,
    empresas.nombre_empresa
FROM
personas
LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= personas.idpersonas
LEFT JOIN empresas ON empresas.idempresas= table_last_before.empresa
    LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE personas.idpersonas BETWEEN pag_count_ini AND pag_count_end
    GROUP BY idpersonas;








END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_personas_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_personas_search`(itemx VARCHAR(60))
BEGIN
    DECLARE last_event, hours , max_hours INT;
DECLARE time_data1, time_data2 TIMESTAMP;
    SELECT (CAST(setting_val AS UNSIGNED) * 3600 ) INTO max_hours from tnt_settings WHERE setting_name= 'max_work_hours';
    IF max_hours = null THEN
SET max_hours=87000;
END IF;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
        count(*) as docq,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona
    );

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE vehiculo=0 AND (CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%')) 
);



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY table_filter_personas.idpersonas) ;

SELECT
    IFNULL (doc_personas.docq,0) as docq,
IFNULL (doc_personas.ven_flag,0) as venflag,
    IF((estado=1 AND CURDATE() BETWEEN DATE(FROM_UNIXTIME(tags.leasing_start)) AND DATE(FROM_UNIXTIME(tags.leasing_end))) OR estado =4, 1, 0 ) as active,
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    table_filter_personas.*
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
    LEFT JOIN tags ON table_filter_personas.idpersonas= tags.idasig
GROUP BY table_filter_personas.idpersonas;










END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_personas_search_date` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_personas_search_date`(itemx VARCHAR(60), date1 DATE, date2 DATE)
BEGIN
    DECLARE last_event, hours , max_hours INT;
DECLARE time_data1, time_data2 TIMESTAMP; 
    SET max_hours=9999999999;
    SET time_data1 = timestamp(date1);
    SET time_data2 = timestamp(date2);
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
        count(*) as docq,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona
    );

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE vehiculo=0 AND (CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%')) 
);



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY table_filter_personas.idpersonas) ;

SELECT
    IFNULL (doc_personas.docq,0) as docq,
IFNULL (doc_personas.ven_flag,0) as venflag,
    IF((estado=1 AND CURDATE() BETWEEN DATE(FROM_UNIXTIME(tags.leasing_start)) AND DATE(FROM_UNIXTIME(tags.leasing_end))) OR estado =4, 1, 0 ) as active,
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    table_filter_personas.*
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
    LEFT JOIN tags ON table_filter_personas.idpersonas= tags.idasig
GROUP BY table_filter_personas.idpersonas;










END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_personas_search_date_zone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_personas_search_date_zone`(itemx VARCHAR(60), date1 DATE, date2 DATE, thiszone INT)
BEGIN
    DECLARE last_event, hours , max_hours INT;
DECLARE time_data1, time_data2 TIMESTAMP; 
    SET max_hours=9999999999;
    SET time_data1 = timestamp(date1);
    SET time_data2 = timestamp(date2);
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
        count(*) as docq,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona);

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE 
vehiculo=0 
AND (
idpersonas IN (SELECT id_persona FROM (SELECT id_persona,zona,max(tiempo) FROM registro_personas where tiempo<time_data1 GROUP BY id_persona) as l  WHERE zona=thiszone)
OR
idpersonas IN (SELECT id_persona FROM registro_personas where tiempo BETWEEN time_data1  AND time_data2 AND  zona=thiszone GROUP BY id_persona, zona)
)
AND 
(CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%')) 
);



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY table_filter_personas.idpersonas) ;

SELECT
    IFNULL (doc_personas.docq,0) as docq,
IFNULL (doc_personas.ven_flag,0) as venflag,
    IF((estado=1 AND CURDATE() BETWEEN DATE(FROM_UNIXTIME(tags.leasing_start)) AND DATE(FROM_UNIXTIME(tags.leasing_end))) OR estado =4, 1, 0 ) as active,
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    table_filter_personas.*
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
    LEFT JOIN tags ON table_filter_personas.idpersonas= tags.idasig
GROUP BY table_filter_personas.idpersonas;










END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_personas_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_personas_zona`(zonaid INT)
BEGIN
DECLARE time_data1, time_data2 TIMESTAMP;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
id_persona, 
registro_personas.empresa,
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND ultima_zona = zonaid
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
id_persona, 
registro_personas.empresa,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND ultima_zona = zonaid
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND ultima_zona = zonaid
    GROUP BY id_persona);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
AND ultima_zona = zonaid
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND ultima_zona = zonaid
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona,
registro_personas.empresa 
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND ultima_zona = zonaid
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY id_persona) ;

SELECT
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,unix_timestamp(current_timestamp)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    idpersonas,
    nombre1,
    nombre2,
    apellido1,
    apellido2,
    idn
FROM
personas
LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= personas.idpersonas
LEFT JOIN empresas ON empresas.idempresas= table_last_before.empresa
WHERE ultima_zona = zonaid  AND vehiculo=0
    GROUP BY idpersonas;








END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_vehicles_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_vehicles_search`(itemx VARCHAR(60))
BEGIN
    DECLARE last_event, hours , max_hours INT;
DECLARE time_data1, time_data2 TIMESTAMP;
    SELECT (CAST(setting_val AS UNSIGNED) * 3600 ) INTO max_hours from tnt_settings WHERE setting_name= 'max_work_hours';
    IF max_hours = null THEN
SET max_hours=87000;
END IF;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona
    );

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE vehiculo=1 AND (CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%') 
) );



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=103 OR tipo_evento=100) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=100  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=100 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=103,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=100, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=103 OR tipo_evento=100)
GROUP BY table_filter_personas.idpersonas) ;

SELECT
IFNULL (doc_personas.ven_flag,0) as venflag,
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    table_filter_personas.*
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
GROUP BY table_filter_personas.idpersonas;










END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_vehiculos_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_list_vehiculos_zona`(zonaid INT)
BEGIN
DECLARE time_data1, time_data2 TIMESTAMP;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
id_persona, 
registro_personas.empresa,
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND ultima_zona = zonaid
    AND personas.vehiculo =1
AND (tipo_evento=103 OR tipo_evento=100) 
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
id_persona, 
registro_personas.empresa,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND ultima_zona = zonaid
AND (tipo_evento=103 OR tipo_evento=100) 
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=100  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=103 OR tipo_evento=100) 
AND ultima_zona = zonaid
    GROUP BY id_persona);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=100 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=103 OR tipo_evento=100) 
AND ultima_zona = zonaid
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF( 
tipo_evento=103,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=103 OR tipo_evento=100) 
AND ultima_zona = zonaid
    GROUP BY id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=100, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona,
registro_personas.empresa 
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND ultima_zona = zonaid
AND (tipo_evento=103 OR tipo_evento=100)
GROUP BY id_persona) ;

SELECT
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,unix_timestamp(current_timestamp)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    idpersonas,
    nombre1,
    nombre2,
    apellido1,
    apellido2,
    idn
FROM
personas
LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= personas.idpersonas
LEFT JOIN empresas ON empresas.idempresas= table_last_before.empresa
WHERE ultima_zona = zonaid  AND vehiculo=1
    GROUP BY idpersonas;








END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_persona`(idx INT)
BEGIN
     DECLARE last_event, hours INT;

DECLARE time_data1, time_data2 TIMESTAMP;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;


DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
registro_personas.empresa,
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona = idx
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona, 
registro_personas.empresa,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND registro_personas.id_persona = idx
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona = idx
    GROUP BY registro_personas.id_persona);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=0) 
AND registro_personas.id_persona = idx
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
registro_personas.empresa,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
AND registro_personas.id_persona = idx
    GROUP BY registro_personas.id_persona);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona,
registro_personas.empresa 
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND registro_personas.id_persona = idx
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY registro_personas.id_persona) ;

SELECT
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as tiempo_hoy,
    personas.*,
    DATE(FROM_UNIXTIME(tags.leasing_start)) as active_start,
    DATE(FROM_UNIXTIME(tags.leasing_end)) as active_end,
    IF((estado=1 AND CURDATE() BETWEEN DATE(FROM_UNIXTIME(tags.leasing_start)) AND DATE(FROM_UNIXTIME(tags.leasing_end))) OR estado =4, 1, 0 ) as active,
    zonas.*,
    tags.*,
    empresas.nombre_empresa
FROM
personas
LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= personas.idpersonas
    LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
LEFT JOIN empresas ON empresas.idempresas= personas.empresa
    LEFT JOIN tags ON personas.idpersonas= tags.idasig
WHERE personas.idpersonas = idx
    GROUP BY idpersonas;








END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_persona_active` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_persona_active`(idx INT)
BEGIN
   SELECT IFNULL(idzona,0) as active FROM zonas_permisos WHERE idpersona=idx AND idzona=1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_persona_t` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_persona_t`(idx INT, time_data DATE, time_type INT)
BEGIN
    DECLARE last_event, first_event, hours INT;
    DECLARE time1, time2, first_time TIMESTAMP;
    DECLARE curdate INT;
    
    set time1 = time_data;
    DROP TEMPORARY TABLE IF EXISTS table_time;
    IF time_type=0 THEN
        set time2 = time1 + INTERVAL 1 DAY;        
    ELSEIF time_type=1 THEN
        set time2 = time_data + INTERVAL 1 MONTH;
    ELSEIF time_type=2 THEN
        set time2 = time_data + INTERVAL 1 YEAR;
    END IF;
    
    IF time2=CURDATE() THEN
SET time2 = CURRENT_TIMESTAMP;
ELSEIF time2>CURDATE() THEN
SET time2 = CURRENT_TIMESTAMP;
    END IF;
        
    SELECT tipo_evento INTO last_event FROM registro_personas 
    WHERE id_persona=idx
    AND tiempo >= time1
    AND tiempo < time2
AND (tipo_evento=3 OR tipo_evento=0) 
    ORDER BY idregistro_personas DESC LIMIT 1;

    SELECT tipo_evento INTO first_event FROM registro_personas 
    WHERE id_persona=idx
    AND tiempo >= time1
    AND tiempo < time2
    ORDER BY idregistro_personas ASC LIMIT 1;
    
    IF first_event>0 THEN 
SELECT tiempo INTO first_time FROM registro_personas 
WHERE id_persona=idx
AND tiempo >= time1
AND tiempo < time2
AND (tipo_evento=3 OR tipo_evento=0) 
ORDER BY idregistro_personas ASC LIMIT 1;
            SET curdate= UNIX_TIMESTAMP(DATE(first_time));
    ELSE SET curdate=0;
    END IF;

IF last_event=0 THEN 
CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS (SELECT 
TIME_FORMAT(
SEC_TO_TIME(
sum(
IF(tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) )) - curdate +UNIX_TIMESTAMP(time2)),
                            '%Hh : %im : %ss') as time_work,
id_persona
        FROM registro_personas 
        WHERE (tipo_evento=3 OR tipo_evento=0) 
        AND id_persona=idx
        AND tiempo >= time1
        AND tiempo < time2);
ELSE
        CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS (SELECT 
TIME_FORMAT(
SEC_TO_TIME(
sum(
IF(tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
                            UNIX_TIMESTAMP(`tiempo`) ))- curdate),
                            '%Hh : %im : %ss') as time_work,
id_persona
FROM registro_personas 
        WHERE (tipo_evento=3 OR tipo_evento=0)
        AND id_persona=idx
        AND tiempo >= time1
AND tiempo < time2);
    END IF;

SELECT 
    personas.*,
    IFNULL(table_time.time_work,0) as tiempo_hoy,
    nombre_empresa
    FROM `ampdb`.`personas`
    LEFT JOIN table_time ON table_time.id_persona= personas.idpersonas
    LEFT JOIN empresas ON empresas.idempresas=personas.empresa
    WHERE idpersonas=idx;
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_regist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_regist`(idx INT)
BEGIN
select * from registro_personas 
    LEFT JOIN tipo_evento ON tipo_evento.idevento = registro_personas.tipo_evento OR tipo_evento.idevento = (registro_personas.tipo_evento-1000) 
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    where id_persona=idx ORDER BY tiempo DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_regist_date` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_regist_date`(idx INT, date1 DATE, date2 DATE)
BEGIN
select * from registro_personas 
    LEFT JOIN tipo_evento ON tipo_evento.idevento = registro_personas.tipo_evento OR tipo_evento.idevento = (registro_personas.tipo_evento-1000) 
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    where id_persona=idx 
    AND tiempo BETWEEN date1 AND date2
    ORDER BY tiempo DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_regist_emp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_regist_emp`(idx INT)
BEGIN
select * from registro_personas 
    LEFT JOIN tipo_evento ON tipo_evento.idevento = registro_personas.tipo_evento 
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    LEFT JOIN personas ON personas.idpersonas= registro_personas.id_persona
    where registro_personas.empresa=idx ORDER BY tiempo DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_search_personas_flags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_search_personas_flags`(itemx VARCHAR(60))
BEGIN
    DECLARE last_event, hours , max_hours INT;
DECLARE time_data1, time_data2 TIMESTAMP;
    SELECT (CAST(setting_val AS UNSIGNED) * 3600 ) INTO max_hours from tnt_settings WHERE setting_name= 'max_work_hours';
    IF max_hours = null THEN
SET max_hours=87000;
END IF;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona
    );

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE vehiculo=0 AND (CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%') 
));



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=3 OR tipo_evento=0) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=0 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=3,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=3 OR tipo_evento=0) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=0, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=3 OR tipo_evento=0)
GROUP BY table_filter_personas.idpersonas) ;

  
  DROP TEMPORARY TABLE IF EXISTS person_search;
  

  CREATE TEMPORARY TABLE IF NOT EXISTS person_search AS (
  SELECT
IFNULL (doc_personas.ven_flag,0) as venflag,
    IF (
 sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
) > max_hours,
            1,
            0
) as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    table_filter_personas.*
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
GROUP BY table_filter_personas.idpersonas);

  SELECT * from person_search WHERE venflag>0 OR over_hours>0;










END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_search_vehicles_flags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_search_vehicles_flags`(itemx VARCHAR(60))
BEGIN
    DECLARE last_event, hours , max_hours INT;
DECLARE time_data1, time_data2 TIMESTAMP;
    SELECT (CAST(setting_val AS UNSIGNED) * 3600 ) INTO max_hours from tnt_settings WHERE setting_name= 'max_work_hours';
    IF max_hours = null THEN
SET max_hours=87000;
END IF;
SET time_data1 = CURDATE();
    SET time_data2 = CURDATE() + INTERVAL 1 DAY;
    
DROP TEMPORARY TABLE IF EXISTS doc_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS doc_personas as(
SELECT 
documentos.idpersona as idperdoc ,
sum(IF(vencimiento<=curdate(),1,0)) as ven_flag
FROM documentos 
LEFT JOIN conf_documentos_sub ON conf_documentos_sub.idsubdocs = documentos.idtipos
LEFT JOIN conf_docs ON conf_documentos_sub.idconf_docs = conf_docs.idconf_docs
GROUP BY idpersona
    );

DROP TEMPORARY TABLE IF EXISTS table_filter_personas;

CREATE TEMPORARY TABLE IF NOT EXISTS table_filter_personas as (
SELECT * 
FROM personas
LEFT JOIN empresas ON personas.empresa=empresas.idempresas
LEFT JOIN zonas ON zonas.idzonas= personas.ultima_zona
WHERE vehiculo=1 AND (CONCAT('Zona ', zonas.numero , ' : ',zonas.nombre) LIKE CONCAT('%', itemx , '%') OR nombre_empresa LIKE CONCAT('%', itemx , '%') OR  rut LIKE CONCAT('%', itemx , '%') OR nombre1 LIKE CONCAT('%', itemx , '%') OR  nombre2 LIKE CONCAT('%', itemx , '%') OR  apellido1 LIKE CONCAT('%', itemx , '%') OR  apellido2 LIKE CONCAT('%', itemx , '%') OR  idn LIKE CONCAT('%', itemx , '%') OR  area LIKE CONCAT('%', itemx , '%') 
));



DROP TEMPORARY TABLE IF EXISTS table_last_first;

CREATE TEMPORARY TABLE IF NOT EXISTS table_last_first as (
SELECT 
registro_personas.id_persona, 
min(tiempo) as first_timex,
max(tiempo) as last_timex
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE (tiempo BETWEEN time_data1 AND time_data2 )
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last_before_pre;
CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before_pre as (
SELECT 
registro_personas.id_persona,
max(tiempo) as last_timey
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE tiempo < time_data1  
AND (tipo_evento=103 OR tipo_evento=100) 
    GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_last;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last as (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=100  ,
UNIX_TIMESTAMP(IF(current_timestamp<= time_data2, current_timestamp,time_data2)),
0
)
,0) as last_time_add
FROM registro_personas 
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
    WHERE  tiempo = last_timex
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

DROP TEMPORARY TABLE IF EXISTS table_last_before;
    CREATE TEMPORARY TABLE IF NOT EXISTS table_last_before as (
SELECT 
table_last_before_pre.id_persona, 
tipo_evento,
IFNULL(
IF(
tipo_evento=100 AND tiempo < time_data1,
UNIX_TIMESTAMP(time_data1)*-1,
0
)
,0) as last_time_before_add
FROM registro_personas 
LEFT JOIN table_last_before_pre ON table_last_before_pre.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = last_timey
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_first;
CREATE TEMPORARY TABLE IF NOT EXISTS table_first AS (
SELECT 
table_last_first.id_persona, 
tipo_evento,
IFNULL(
IF( 
tipo_evento=103,
IF(first_timex > time_data1,UNIX_TIMESTAMP(DATE(first_timex)) * -1,UNIX_TIMESTAMP(time_data1) * -1),
0
)
,0) as first_time_add
FROM registro_personas 
LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
    WHERE  tiempo = first_timex
AND (tipo_evento=103 OR tipo_evento=100) 
GROUP BY table_filter_personas.idpersonas);

    DROP TEMPORARY TABLE IF EXISTS table_time;


CREATE TEMPORARY TABLE IF NOT EXISTS table_time AS ( 

SELECT 
IFNULL(
(
sum( 
IF(
registro_personas.tipo_evento=100, 
UNIX_TIMESTAMP(`tiempo`) *-1 , 
UNIX_TIMESTAMP(`tiempo`) 
   )
) 
)
,0
)
as time_work,
tiempo,
registro_personas.id_persona
FROM registro_personas 
    LEFT JOIN table_last_first ON table_last_first.id_persona = registro_personas.id_persona
LEFT JOIN table_filter_personas ON table_filter_personas.idpersonas = registro_personas.id_persona
WHERE tiempo >= time_data1
AND tiempo < time_data2
AND (tipo_evento=103 OR tipo_evento=100)
GROUP BY table_filter_personas.idpersonas) ;

  
  DROP TEMPORARY TABLE IF EXISTS person_search;
  

  CREATE TEMPORARY TABLE IF NOT EXISTS person_search AS (
  SELECT
IFNULL (doc_personas.ven_flag,0) as venflag,
    0 as over_hours,
TIME_FORMAT( 
            SEC_TO_TIME(
sum(
IFNULL(IFNULL(time_work,0)+ if(IFNULL(first_time_add,0)!=0,first_time_add ,IFNULL(last_time_before_add,0)) + IFNULL(last_time_add,IF((IFNULL(first_time_add,0)+IFNULL(last_time_before_add,0)+IFNULL(last_time_add,0))!=0,unix_timestamp(current_timestamp),0)),0)
)
)
,
'%Hh : %im : %s'
) 
as time_total,
    table_filter_personas.*
FROM
table_filter_personas
LEFT JOIN table_time ON table_time.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_first ON table_first.id_persona= table_filter_personas.idpersonas
LEFT JOIN table_last ON table_last.id_persona= table_filter_personas.idpersonas
    LEFT JOIN table_last_before ON table_last_before.id_persona= table_filter_personas.idpersonas
    LEFT JOIN doc_personas ON doc_personas.idperdoc= table_filter_personas.idpersonas
GROUP BY table_filter_personas.idpersonas);

  SELECT * from person_search WHERE venflag>0;










END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_subdocs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_subdocs`()
BEGIN
SELECT * FROM conf_documentos_sub 
    LEFT JOIN conf_docs 
    ON conf_docs.idconf_docs= conf_documentos_sub.idconf_docs 
    ORDER BY conf_documentos_sub.idconf_docs ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tags`()
BEGIN
select tags.*, tag_estado.* from tags left join tag_estado on tags.estado = tag_estado.idtag_estado WHERE tags.ignoreflag=0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tipo_vehiculos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tipo_vehiculos`()
BEGIN
SELECT * from tipo_vehiculos;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts`()
BEGIN
SELECT
*
    FROM registro_personas
    WHERE tipo_evento BETWEEN 4 AND 499;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts_num` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts_num`()
BEGIN
SELECT
count(*) as accnum
    FROM registro_personas
    WHERE tipo_evento BETWEEN 4 AND 99;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts_p` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts_p`()
BEGIN
SELECT
*
    FROM registro_personas
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    LEFT JOIN rfidreaders ON rfidreaders.idrfidreaders = registro_personas.rfidreader
    LEFT JOIN empresas ON empresas.idempresas = registro_personas.empresa
    LEFT JOIN tipo_evento ON tipo_evento.idevento = registro_personas.tipo_evento
    WHERE tipo_evento BETWEEN 6 AND 99 LIMIT 1000;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts_p_r` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts_p_r`()
BEGIN
SELECT
*
    FROM registro_personas
    LEFT JOIN personas ON personas.idpersonas = registro_personas.id_persona
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    LEFT JOIN rfidreaders ON rfidreaders.idrfidreaders = registro_personas.rfidreader
    LEFT JOIN empresas ON empresas.idempresas = registro_personas.empresa
    LEFT JOIN tipo_evento ON tipo_evento.idevento = (registro_personas.tipo_evento-1000)
    WHERE tipo_evento BETWEEN 1006 AND 1099 LIMIT 1000;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts_t` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts_t`()
BEGIN
SELECT
    registro_personas.*,
zonas.*,
    `rfidreaders`.`serial`,
    tipo_evento.*,
    `tags`.`serial` as tagserial
    FROM registro_personas
    LEFT JOIN tags ON tags.idtags = registro_personas.id_persona
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    LEFT JOIN rfidreaders ON rfidreaders.idrfidreaders = registro_personas.rfidreader
    LEFT JOIN tipo_evento ON tipo_evento.idevento = registro_personas.tipo_evento
    WHERE tipo_evento BETWEEN 4 AND 5 LIMIT 1000;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts_t_a` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts_t_a`()
BEGIN
SELECT
    registro_personas.*,
zonas.*,
    `rfidreaders`.`serial`,
    tipo_evento.*,
    `tags`.`serial` as tagserial
    FROM registro_personas
    LEFT JOIN tags ON tags.idtags = registro_personas.id_persona
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    LEFT JOIN rfidreaders ON rfidreaders.idrfidreaders = registro_personas.rfidreader
    LEFT JOIN tipo_evento ON tipo_evento.idevento = (registro_personas.tipo_evento - 1000)
    WHERE `registro_personas`.`tiempo`>= (now() - INTERVAL 1 DAY) AND (tipo_evento BETWEEN 1004 AND 1005 OR tipo_evento BETWEEN 4 AND 5);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_alerts_t_r` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_alerts_t_r`()
BEGIN
SELECT
    registro_personas.*,
zonas.*,
    `rfidreaders`.`serial`,
    tipo_evento.*,
    `tags`.`serial` as tagserial
    FROM registro_personas
    LEFT JOIN tags ON tags.idtags = registro_personas.id_persona
    LEFT JOIN zonas ON zonas.idzonas = registro_personas.zona
    LEFT JOIN rfidreaders ON rfidreaders.idrfidreaders = registro_personas.rfidreader
    LEFT JOIN tipo_evento ON tipo_evento.idevento = (registro_personas.tipo_evento - 1000)
    WHERE tipo_evento BETWEEN 1004 AND 1005 LIMIT 1000;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_connectiontype` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_connectiontype`()
BEGIN
select * from tnt_connection_type;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_mailvars` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_mailvars`()
BEGIN
select * from tnt_report_config limit 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_reader` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_reader`(idx INT)
BEGIN
SELECT 
    rfidreaders.*,
    zonas_a.nombre as nombre_a,
    zonas_a.numero as numero_a,
    zonas_b.nombre as nombre_b,
    zonas_b.numero as numero_b,
    rfid_estado.*
    FROM rfidreaders
    LEFT JOIN rfid_estado ON rfid_estado.idrf_estado = rfidreaders.online
    LEFT JOIN zonas as zonas_a ON rfidreaders.zona_a = zonas_a.idzonas
    LEFT JOIN zonas as zonas_b ON rfidreaders.zona_b = zonas_b.idzonas
    WHERE rfidreaders.idrfidreaders = idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_readers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_readers`()
BEGIN
SELECT 
    *
    FROM rfidreaders
    LEFT JOIN rfid_estado ON rfid_estado.idrf_estado = rfidreaders.online
    ORDER BY `serial`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_readers_alerts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_readers_alerts`()
BEGIN
SELECT 
    rfidreaders.*,
    zonas_a.nombre as nombre_a,
    zonas_a.numero as numero_a,
    zonas_b.nombre as nombre_b,
    zonas_b.numero as numero_b,
    rfid_estado.*
    FROM rfidreaders
    LEFT JOIN rfid_estado ON rfid_estado.idrf_estado = rfidreaders.online
    LEFT JOIN zonas as zonas_a ON rfidreaders.zona_a = zonas_a.idzonas
    LEFT JOIN zonas as zonas_b ON rfidreaders.zona_b = zonas_b.idzonas
    WHERE rfidreaders.online <> 1 AND rfidreaders.mapid>0
    ORDER BY `serial`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_readers_bymap` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_readers_bymap`(idx INT)
BEGIN
SELECT 
    rfidreaders.*,
    zonas_a.nombre as nombre_a,
    zonas_a.numero as numero_a,
    zonas_b.nombre as nombre_b,
    zonas_b.numero as numero_b,
    rfid_estado.*
    FROM rfidreaders
    LEFT JOIN rfid_estado ON rfid_estado.idrf_estado = rfidreaders.online
    LEFT JOIN zonas as zonas_a ON rfidreaders.zona_a = zonas_a.idzonas
    LEFT JOIN zonas as zonas_b ON rfidreaders.zona_b = zonas_b.idzonas
    WHERE rfidreaders.mapid = idx
    ORDER BY `serial`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_setting` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_setting`()
BEGIN
select * from `ampdb`.`tnt_settings` ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tnt_tag_states` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tnt_tag_states`()
BEGIN
select * from tag_estado;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zonas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zonas`()
BEGIN
select * from zonas;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zonas_byid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zonas_byid`(idx INT)
BEGIN
select * from zonas WHERE mapid=idx and numero>0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zonas_unused` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zonas_unused`()
BEGIN
select * from zonas WHERE mapid=0 AND numero>0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zones` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zones`()
BEGIN
select * from personas where ultima_zona>0 ORDER by ultima_zona;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zones_byidmap` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zones_byidmap`(idx INT)
BEGIN
select * from personas where ultima_zona>0  AND vehiculo=0 AND EXISTS (select idzonas as ultima_zona from zonas where mapid=idx AND idzonas= personas.ultima_zona) ORDER by ultima_zona;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zone_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zone_count`(idx INT)
BEGIN
    DECLARE counter, v_counter INT;
    select count(*) INTO counter from personas where ultima_zona=idx AND vehiculo=0;
    select count(*) INTO v_counter from personas where ultima_zona=idx  AND vehiculo=1;
select counter, v_counter, zonas.* from zonas where idzonas=idx;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_bus_readers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_bus_readers`(_serial VARCHAR(45),_ip1 VARCHAR(45), _ip2 VARCHAR(45))
BEGIN
DECLARE auxint1, auxint2 int;

SELECT count(*) INTO auxint1 FROM `busreaders` WHERE `serial`=_serial AND del=1;
SELECT count(*) INTO auxint2 FROM `busreaders` WHERE del=0 AND (`serial`=_serial OR ip1=_ip1 OR ip2=_ip2 OR ip1=_ip2 OR ip2=_ip1);
    
IF auxint1>0 AND auxint2=0 THEN
UPDATE `busreaders` SET del=0 , `ip1`=_ip1 , `ip2`=_ip2 WHERE `serial`=_serial;
ELSEIF auxint2=0 THEN
INSERT INTO `busreaders`(`serial`,`ip1`,`ip2`) values(_serial,_ip1,_ip2);
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_condic_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_condic_zona`(idx INT, iddc INT)
BEGIN
INSERT INTO `ampdb`.`condic_zonas` ( `idzona`, `iddoc`) VALUES ( idx, iddc);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_doc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_doc`(doc_nombr VARCHAR(45))
BEGIN
INSERT INTO `ampdb`.`conf_docs` ( `nombre`) 
    VALUES (doc_nombr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_empresa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_empresa`( nombre_empres VARCHAR(60), ru VARCHAR(60))
BEGIN
    DECLARE lastid INT;
INSERT INTO `ampdb`.`empresas` (`nombre_empresa`, `rut`) VALUES (nombre_empres, ru);
    SELECT LAST_INSERT_ID() INTO lastid FROM `ampdb`.`empresas` LIMIT 1;
    SELECT lastid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_persona`( nombr1 VARCHAR(25), nombr2 VARCHAR(25), apellid1 VARCHAR(25), apellid2 VARCHAR(25), id VARCHAR(45), ta INT, emai VARCHAR(60), are VARCHAR(25), empres INT,fot VARCHAR(6))
BEGIN
DECLARE lastid INT;
INSERT INTO `ampdb`.`personas` (`nombre1`, `nombre2`, `apellido1`, `apellido2`, `idn`, `tag`, `email`, `area`, `empresa`, `foto`) 
VALUES ( nombr1  , nombr2  , apellid1  , apellid2  , id  , ta , emai , are  , empres  ,fot );
    SELECT LAST_INSERT_ID() INTO lastid FROM `ampdb`.`personas` LIMIT 1;

    SELECT lastid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_subdoc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_subdoc`(subdoc_nombr VARCHAR(45), idconf_doc INT)
BEGIN
INSERT INTO `ampdb`.`conf_documentos_sub` ( `subdoc_nombre`, `idconf_docs`) 
    VALUES (subdoc_nombr, idconf_doc);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_tags`(serial_ VARCHAR(45), estado_ int)
BEGIN
INSERT INTO `ampdb`.`tags` ( `serial`, `estado`) VALUES (serial_, estado_) ON DUPLICATE KEY UPDATE ignoreflag=0, estado=estado_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_tipo_vehiculos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_tipo_vehiculos`(nombre_tipo_ VARCHAR(45))
BEGIN
INSERT INTO `ampdb`.`tipo_vehiculos` (`nombre_tipo`) VALUES (nombre_tipo_);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_tnt_alerts_p` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_tnt_alerts_p`()
BEGIN
UPDATE registro_personas
    SET tipo_evento = (tipo_evento+1000)
    WHERE tipo_evento BETWEEN 6 AND 499;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_tnt_alerts_t` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_tnt_alerts_t`()
BEGIN
UPDATE registro_personas
    SET tipo_evento = (tipo_evento+1000)
    WHERE tipo_evento BETWEEN 4 AND 5;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_tnt_reader` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_tnt_reader`(serial_ VARCHAR(45), zona_a_ INT, zona_b_ INT, isethernet_ INT)
BEGIN
INSERT INTO 
`ampdb`.`rfidreaders` (`serial`, `zona_a`, `zona_b`,`isethernet`) 
VALUES (serial_, zona_a_, zona_b_,isethernet_);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_tnt_setting` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_tnt_setting`(setting_nam VARCHAR(45), setting_des VARCHAR(200), setting_va VARCHAR(200))
BEGIN
INSERT INTO `ampdb`.`tnt_settings` (`setting_name`, `setting_desc`, `setting_val`) 
    VALUES (setting_nam, setting_des, setting_va);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_vehiculo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_vehiculo`( nombr1 VARCHAR(25), nombr2 VARCHAR(25), apellid1 VARCHAR(25), apellid2 VARCHAR(25), id VARCHAR(45), ta INT, are VARCHAR(25), empres INT,fot VARCHAR(6))
BEGIN
DECLARE lastid INT;
INSERT INTO `ampdb`.`personas` (`nombre1`, `nombre2`, `apellido1`, `apellido2`, `idn`, `tag`, `area`, `empresa`, `foto`,`vehiculo`) 
VALUES ( nombr1  , nombr2  , apellid1  , apellid2  , id  , ta  , are  , empres  ,fot,1 );
    SELECT LAST_INSERT_ID() INTO lastid FROM `ampdb`.`personas` LIMIT 1;

    SELECT lastid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_zona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_zona`(numero INT, nombre VARCHAR(45), descripcion VARCHAR(200))
BEGIN
INSERT INTO 
`ampdb`.`zonas` (`numero`, `nombre`, `descripcion`) 
VALUES (numero, nombre, descripcion);
    update zonas set idzonas=numero+1 where 1;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `processBusTag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `processBusTag`(tim INT, ipx VARCHAR(45), idx  VARCHAR(45))
BEGIN
  DECLARE zon INT;
  SELECT zona INTO zon
  from registro_personas 
  LEFT JOIN personas ON personas.idpersonas=registro_personas.id_persona 
  LEFT JOIN busreaders ON busreaders.idbusreaders=personas.busreaderid  
  WHERE ip1=ipx 
  AND (tipo_evento=0 OR tipo_evento=3) 
  AND (
    tiempo BETWEEN (FROM_UNIXTIME(tim)-INTERVAL 30 SECOND) 
    AND            (FROM_UNIXTIME(tim)+INTERVAL 30 SECOND)
) 
  ORDER BY tiempo DESC LIMIT 1; 
  
  IF zon is null THEN
UPDATE busrfid SET score=0 WHERE tagnum=idx;
  ELSE
CALL registro_tag_frombus(idx,zon);
  END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `processBusTagId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `processBusTagId`(tim INT, serialReader VARCHAR(45), idx  VARCHAR(45), readernum  VARCHAR(45))
BEGIN
  DECLARE zon INT;
  SELECT zona INTO zon
  from registro_personas 
  LEFT JOIN personas ON personas.idpersonas=registro_personas.id_persona 
  LEFT JOIN busreaders ON busreaders.idbusreaders=personas.busreaderid  
  WHERE busreaders.`serial`=serialReader  
  AND (tipo_evento=0 OR tipo_evento=3) 
  AND (
    tiempo BETWEEN (FROM_UNIXTIME(tim)-INTERVAL 1 MINUTE) 
    AND            (FROM_UNIXTIME(tim)+INTERVAL 1 MINUTE)
) 
  ORDER BY tiempo DESC LIMIT 1;  
  IF zon is null THEN
UPDATE busrfid SET score=0 WHERE tagnum=idx;
  ELSE
CALL registro_tag_frombusId(idx,zon,readernum);
    UPDATE busrfid SET score=0 WHERE tagnum=idx;
  END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registro_error_tag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_error_tag`(idx INT, error_code INT, zona INT, rfid INT)
BEGIN
INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, error_code, 0, zona, rfid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registro_tag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_tag`(idx VARCHAR(30), zona INT, rfid INT)
proc_label:BEGIN
DECLARE estad, num, aux, rfidx, zonex INT;
    SELECT idtags INTO num FROM tags WHERE `serial`=idx;
    IF zona = 1 THEN
        SELECT zona_a INTO zonex FROM rfidreaders WHERE `serial`=rfid;
    ELSE
        SELECT zona_b INTO zonex FROM rfidreaders WHERE `serial`=rfid;
    END IF;

    if zonex is NULL then
SET zonex=0;
    END IF;
    
    if num is NULL then
INSERT INTO tags(`serial`,estado) values(idx,5);
        SELECT idtags INTO num FROM tags WHERE `serial`=idx;
    END IF;
    
SELECT estado INTO estad FROM tags WHERE idtags=num;
    SET idx = num;

    if estad != 1 AND estad != 4 THEN
          call registro_error_tag(idx, 4, zonex,rfid);
    ELSE
        SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=0;
        IF estad =0 THEN
            SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=1;

            IF estad =0 THEN
                call registro_error_tag(idx,5,zonex,rfid);
            ELSE
                SELECT idpersonas INTO aux FROM personas WHERE tag=idx;
                CALL reg_vehiculo(aux, zonex,  rfid);
            END IF;

        ELSE
            SELECT idasig INTO aux FROM tags WHERE idtags=idx;
            call reg_persona(aux, zonex,  rfid);              

        END IF;
    END IF;
    SELECT idx,estad, num, aux, rfidx, zonex;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registro_tag_frombus` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_tag_frombus`(idx VARCHAR(30), zona INT)
proc_label:BEGIN
DECLARE estad, num, aux, rfid, zonex INT;
    SELECT idtags INTO num FROM tags WHERE `serial`=idx; 
    SET zonex=zona; 
    
    if num is NULL then
INSERT INTO tags(`serial`,estado) values(idx,5);
        SELECT idtags INTO num FROM tags WHERE `serial`=idx;
    END IF;
    
SELECT estado INTO estad FROM tags WHERE idtags=num;
    SET idx = num;

    if estad != 1 AND estad != 4 THEN
          call registro_error_tag(idx, 4, zonex,rfid);
    ELSE
        SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=0;
        IF estad =0 THEN
            SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=1;

            IF estad =0 THEN
                call registro_error_tag(idx,5,zonex,rfid);
            ELSE
                SELECT idpersonas INTO aux FROM personas WHERE tag=idx;
                CALL reg_vehiculo(aux, zonex,  rfid);
            END IF;

        ELSE
            SELECT idasig INTO aux FROM tags WHERE idtags=idx;
            call reg_persona(aux, zonex,  rfid);              

        END IF;
    END IF;
    SELECT idx,estad, num, aux, rfidx, zonex;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registro_tag_frombusId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_tag_frombusId`(idx VARCHAR(30), zona INT, reader_  INT)
proc_label:BEGIN
DECLARE estad, num, aux, rfid, zonex INT;
SET rfid=reader_;
    SELECT idtags INTO num FROM tags WHERE `serial`=idx; 
    SET zonex=zona; 
    
    if num is NULL then
INSERT INTO tags(`serial`,estado) values(idx,5);
        SELECT idtags INTO num FROM tags WHERE `serial`=idx;
    END IF;
    
SELECT estado INTO estad FROM tags WHERE idtags=num;
    SET idx = num;

    if estad != 1 AND estad != 4 THEN
          call registro_error_tag(idx, 4, zonex,rfid);
    ELSE
        SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=0;
        IF estad =0 THEN
            SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=1;

            IF estad =0 THEN
                call registro_error_tag(idx,5,zonex,rfid);
            ELSE
                SELECT idpersonas INTO aux FROM personas WHERE tag=idx;
                CALL reg_vehiculo(aux, zonex,  rfid);
            END IF;

        ELSE
            SELECT idasig INTO aux FROM tags WHERE idtags=idx;
            call reg_persona(aux, zonex,  rfid);              

        END IF;
    END IF;
    SELECT idx,estad, num, aux, rfid, zonex;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registro_tag_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_tag_tm`(idx VARCHAR(30), zona INT, rfid INT, tm INT)
proc_label:BEGIN
DECLARE estad, num, aux, rfidx, zonex INT;
    SELECT idtags INTO num FROM tags WHERE `serial`=idx;
    IF zona = 1 THEN
        SELECT zona_a INTO zonex FROM rfidreaders WHERE `serial`=rfid;
    ELSE
        SELECT zona_b INTO zonex FROM rfidreaders WHERE `serial`=rfid;
    END IF;

    if zonex is NULL then
SET zonex=0;
    END IF;
    
    if num is NULL then
INSERT INTO tags(`serial`,estado) values(idx,5);
        SELECT idtags INTO num FROM tags WHERE `serial`=idx;
    END IF;
    
SELECT estado INTO estad FROM tags WHERE idtags=num;
    SET idx = num;

    if estad != 1 AND estad != 4 THEN
          call registro_error_tag(idx, 4, zonex,rfid);
    ELSE
        SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=0;
        IF estad =0 THEN
            SELECT COUNT(*) INTO estad FROM personas WHERE tag=idx  AND vehiculo=1;

            IF estad =0 THEN
                call registro_error_tag(idx,5,zonex,rfid);
            ELSE
                SELECT idpersonas INTO aux FROM personas WHERE tag=idx;
                CALL reg_vehiculo_tm(aux, zonex,  rfid, tm);
            END IF;

        ELSE
            SELECT idasig INTO aux FROM tags WHERE idtags=idx;
            call reg_persona_tm(aux, zonex,  rfid, tm);              

        END IF;
    END IF;
    SELECT idx,estad, num, aux, rfidx, zonex;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_error_acc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_error_acc`(idx INT, zone INT, rfidreadr INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 6, empr, zone, rfidreadr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_error_acc_vehiculo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_error_acc_vehiculo`(idx INT, zone INT, rfidreadr INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 106, empr, zone, rfidreadr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_in_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_in_persona`( idx INT, idzona INT, rfid INT)
BEGIN
DECLARE empr,ulzon INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
    SELECT ultima_zona INTO ulzon FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas`  
SET 
`ultima_zona`=idzona , 
`ultimo_ingreso`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`, `ulzon`) 
    VALUES (idx, 0, empr, idzona, rfid, ulzon);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_in_persona_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_in_persona_tm`( idx INT, idzona INT, rfid INT, tm INT)
BEGIN
DECLARE empr,ulzon INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
    SELECT ultima_zona INTO ulzon FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas`  
SET 
`ultima_zona`=idzona , 
`ultimo_ingreso`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`, `tiempo`, `ulzon`) 
    VALUES (idx, 0, empr, idzona, rfid, DATE(tm), ulzon);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_in_vehiculo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_in_vehiculo`( idx INT, idzona INT, rfid INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas`  
SET 
`ultima_zona`=idzona , 
`ultimo_ingreso`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 100, empr, idzona, rfid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_in_vehiculo_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_in_vehiculo_tm`( idx INT, idzona INT, rfid INT, tm INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas`  
SET 
`ultima_zona`=idzona , 
`ultimo_ingreso`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`, `tiempo`) 
    VALUES (idx, 100, empr, idzona, rfid, DATE(tm));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_last_v_zone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_last_v_zone`(idx INT, zone INT, rfidreadr INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` SET ultima_zona= zone WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 102, empr, zone, rfidreadr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_last_zone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_last_zone`(idx INT, zone INT, rfidreadr INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` SET ultima_zona= zone WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 2, empr, zone, rfidreadr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_last_zone_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_last_zone_tm`(idx INT, zone INT, rfidreadr INT, tm INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` SET ultima_zona= zone WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`, `tiempo`) 
    VALUES (idx, 2, empr, zone, rfidreadr, DATE(tm));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_out_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_out_persona`( idx INT, rfid INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` 
    SET 
`ultima_zona`=1 , 
`ultima_salida`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 3, empr, 1, rfid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_out_persona_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_out_persona_tm`( idx INT, rfid INT, tm INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` 
    SET 
`ultima_zona`=1 , 
`ultima_salida`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`, `tiempo`) 
    VALUES (idx, 3, empr, 1, rfid, DATE(tm));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_out_vehiculo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_out_vehiculo`( idx INT, rfid INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` 
    SET 
`ultima_zona`=1 , 
`ultima_salida`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`) 
    VALUES (idx, 103, empr, 1, rfid);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_out_vehiculo_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_out_vehiculo_tm`( idx INT, rfid INT, tm INT)
BEGIN
DECLARE empr INT;
    SELECT empresa INTO empr FROM personas WHERE idpersonas=idx;
UPDATE `ampdb`.`personas` 
    SET 
`ultima_zona`=1 , 
`ultima_salida`= CURRENT_TIMESTAMP
WHERE idpersonas=idx;
    INSERT INTO `ampdb`.`registro_personas` (`id_persona`, `tipo_evento`, `empresa`, `zona`, `rfidreader`,`tiempo`) 
    VALUES (idx, 103, empr, 1, rfid, DATE(tm));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_persona`(idx INT, zone INT, rfid INT)
BEGIN
DECLARE last_zone , doc_prob INT;
    
    SELECT count(*) INTO doc_prob
    FROM (
SELECT iddoc 
FROM condic_zonas 
            WHERE idzona= zone 
            AND NOT EXISTS (
SELECT idtipos as iddoc 
                FROM documentos 
                WHERE idpersona= idx AND vencimiento>CURDATE()
)
) t;
    
    IF doc_prob>0 THEN
call reg_error_acc(idx, zone, rfid);
END IF;
      
SELECT 
personas.ultima_zona
INTO last_zone
FROM `ampdb`.`personas`
WHERE idpersonas=idx;

IF zone != last_zone AND zone =1  THEN
CALL reg_out_persona(idx, rfid);
ELSEIF zone != last_zone AND last_zone = 1 THEN
CALL reg_in_persona(idx, zone, rfid);
ELSEIF zone != last_zone AND zone != 1 THEN
CALL reg_last_zone(idx, zone, rfid);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_persona_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_persona_tm`(idx INT, zone INT, rfid INT, tm INT)
BEGIN
DECLARE last_zone , doc_prob INT;
    
    SELECT count(*) INTO doc_prob
    FROM (
SELECT iddoc 
FROM condic_zonas 
            WHERE idzona= zone 
            AND NOT EXISTS (
SELECT idtipos as iddoc 
                FROM documentos 
                WHERE idpersona= idx AND vencimiento>CURDATE()
)
) t;
    
    IF doc_prob>0 THEN
call reg_error_acc(idx, zone, rfid);
END IF;
      
SELECT 
personas.ultima_zona
INTO last_zone
FROM `ampdb`.`personas`
WHERE idpersonas=idx;

IF zone != last_zone AND zone =1  THEN
CALL reg_out_persona_tm(idx, rfid, tm);
ELSEIF zone != last_zone AND last_zone = 1 THEN
CALL reg_in_persona_tm(idx, zone, rfid, tm);
ELSEIF zone != last_zone AND zone != 1 THEN
CALL reg_last_zone_tm(idx, zone, rfid, tm);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_vehiculo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_vehiculo`(idx INT, zone INT, rfid INT)
BEGIN
DECLARE last_zone , doc_prob INT;
    
    SELECT count(*) INTO doc_prob
    FROM (
SELECT idtipos 
FROM documentos 
            WHERE vencimiento <= CURDATE()
) t;
    
    IF doc_prob>0 THEN
call reg_error_acc_vehiculo(idx, zone, rfid);
END IF;
      
SELECT 
personas.ultima_zona
INTO last_zone
FROM `ampdb`.`personas`
WHERE idpersonas=idx;

IF zone != last_zone AND zone =1  THEN
CALL reg_out_vehiculo(idx, rfid);
ELSEIF zone != last_zone AND last_zone = 1 THEN
CALL reg_in_vehiculo(idx, zone, rfid);
ELSEIF zone != last_zone AND zone != 1 THEN
CALL reg_last_v_zone(idx, zone, rfid);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reg_vehiculo_tm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reg_vehiculo_tm`(idx INT, zone INT, rfid INT, tmstmp INT)
BEGIN
DECLARE last_zone , doc_prob INT;
    
    SELECT count(*) INTO doc_prob
    FROM (
SELECT idtipos 
FROM documentos 
            WHERE vencimiento <= CURDATE()
) t;
    
    IF doc_prob>0 THEN
call reg_error_acc_vehiculo(idx, zone, rfid);
END IF;
      
SELECT 
personas.ultima_zona
INTO last_zone
FROM `ampdb`.`personas`
WHERE idpersonas=idx;

IF zone != last_zone AND zone =1  THEN
CALL reg_out_vehiculo(idx, rfid);
ELSEIF zone != last_zone AND last_zone = 1 THEN
CALL reg_in_vehiculo(idx, zone, rfid);
ELSEIF zone != last_zone AND zone != 1 THEN
CALL reg_last_v_zone(idx, zone, rfid);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `resetTagBus` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetTagBus`(tagnum_ VARCHAR(45))
BEGIN
DECLARE last_score INT;
    SELECT 
    busrfid.score
INTO last_score
    FROM `ampdb`.`busrfid`
    WHERE tagnum=tagnum_;

IF last_score IS NULL THEN
INSERT INTO `ampdb`.`busrfid`(tagnum,score) VALUES(tagnum_,0);
ELSE 
UPDATE `ampdb`.`busrfid` SET score=0  WHERE tagnum=tagnum_; 
END IF;
    
    SELECT last_score;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `saveTagBus` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `saveTagBus`(tagnum_ VARCHAR(45), rfidclient VARCHAR(45), _score int,_reader int, tmstmp int)
BEGIN
DECLARE last_score, idreader INT;
SELECT 
busrfid.score
INTO last_score
FROM `ampdb`.`busrfid`
WHERE tagnum=tagnum_;
    
SELECT idrfidreaders INTO idreader FROM rfidreaders WHERE `serial`=_reader;


IF last_score IS NULL THEN
INSERT INTO `ampdb`.`busrfid`(tagnum,readerid,score,last_mod,tagreaderserial) VALUES(tagnum_,rfidclient,_score,FROM_UNIXTIME(tmstmp),idreader);
ELSEIF last_score = 0 THEN 
UPDATE `ampdb`.`busrfid` SET score=_score,readerid=rfidclient,tagreaderserial=idreader, last_mod=FROM_UNIXTIME(tmstmp)  WHERE tagnum=tagnum_;
ELSEIF last_score != _score THEN 
UPDATE `ampdb`.`busrfid` SET score=(score+_score), readerid=rfidclient, tagreaderserial=idreader, last_mod=FROM_UNIXTIME(tmstmp)  WHERE tagnum=tagnum_;
ELSE 
UPDATE `ampdb`.`busrfid` SET readerid=rfidclient, tagreaderserial=idreader , last_mod=FROM_UNIXTIME(tmstmp) WHERE tagnum=tagnum_;
END IF;
SELECT last_score;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setserviceTime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `setserviceTime`(idx INT)
BEGIN
     SELECT modbus_devices_vars.device_id, modbus_devices.address, modbus_devices.mapid FROM modbus_devices_vars LEFT JOIN modbus_devices ON modbus_devices.id = modbus_devices_vars.device_id WHERE alarm=1 AND mapid>0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tag_lecture` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tag_lecture`(idx INT, zona INT, rfid INT)
BEGIN
    DECLARE zon, rf INT;
IF zona=1 THEN
SELECT zona_a INTO zon FROM rfidreaders WHERE `serial`=rfid;
ELSE 
SELECT zona_b INTO zon FROM rfidreaders WHERE `serial`=rfid;
END IF;
SELECT idrfidreaders INTO rf FROM rfidreaders WHERE `serial`=rfid;
    CALL registro_tag(idx, zon, rf);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `turn_off_alarm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `turn_off_alarm`(idx int)
BEGIN
UPDATE registro_personas SET tipo_evento=tipo_evento + 1000 where idregistro_personas=idx AND tipo_evento BETWEEN 4 AND 6;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `turn_off_all_alarms` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `turn_off_all_alarms`()
BEGIN
UPDATE registro_personas SET tipo_evento=tipo_evento + 1000 WHERE tipo_evento BETWEEN 4 AND 6;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_doc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_doc`(idx INT, subdoc INT, vendate date)
BEGIN
INSERT INTO documentos (idtipos,vencimiento,idpersona)
    VALUES (subdoc,vendate,idx)
    ON DUPLICATE key 
UPDATE vencimiento=vendate;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
