-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `execution`
--

DROP TABLE IF EXISTS `execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `execution` (
  `exeid` int(11) NOT NULL AUTO_INCREMENT,
  `appid` int(4) NOT NULL,
  `task` int(4) NOT NULL,
  `value` int(4) NOT NULL,
  `state` int(4) NOT NULL,
  `value_ex1` int(4) DEFAULT NULL,
  PRIMARY KEY (`exeid`)
) ENGINE=InnoDB AUTO_INCREMENT=511 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `execution`
--

LOCK TABLES `execution` WRITE;
/*!40000 ALTER TABLE `execution` DISABLE KEYS */;
INSERT INTO `execution` VALUES (475,1,1,3,1,NULL),(476,1,0,3,1,NULL),(477,1,0,3,1,NULL),(478,1,0,3,1,NULL),(479,1,0,3,1,NULL),(480,1,0,3,1,NULL),(481,1,0,3,1,NULL),(482,1,1,3,1,NULL),(483,1,1,3,1,NULL),(484,1,0,3,1,NULL),(485,1,1,3,1,NULL),(486,1,0,3,1,NULL),(487,1,1,3,1,NULL),(488,1,0,3,1,NULL),(489,1,1,3,1,NULL),(490,1,0,3,1,NULL),(491,1,1,3,1,NULL),(492,1,1,3,1,NULL),(493,1,0,3,1,NULL),(494,1,1,3,1,NULL),(495,1,0,3,1,NULL),(496,1,1,3,1,NULL),(497,1,1,3,1,NULL),(498,1,0,3,1,NULL),(499,1,1,3,1,NULL),(500,1,1,3,1,NULL),(501,1,0,3,1,NULL),(502,1,1,3,1,NULL),(503,1,0,3,1,NULL),(504,1,1,3,1,NULL),(505,1,1,3,1,NULL),(506,1,0,3,1,NULL),(507,1,1,4,1,NULL),(508,1,1,4,1,NULL),(509,1,1,4,1,NULL),(510,1,1,3,1,NULL);
/*!40000 ALTER TABLE `execution` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
