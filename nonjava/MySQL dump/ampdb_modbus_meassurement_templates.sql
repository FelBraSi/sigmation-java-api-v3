-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `modbus_meassurement_templates`
--

DROP TABLE IF EXISTS `modbus_meassurement_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modbus_meassurement_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(45) NOT NULL DEFAULT 'Unknown',
  `function` int(11) NOT NULL DEFAULT '1',
  `startaddress` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `defaultmax` float(10,1) NOT NULL DEFAULT '0.0',
  `defaultmin` float(10,1) NOT NULL DEFAULT '0.0',
  `unit` varchar(45) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `perbit` tinyint(1) NOT NULL DEFAULT '0',
  `specialpar` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `typeindex_idx` (`type_id`),
  CONSTRAINT `typeinxtool` FOREIGN KEY (`type_id`) REFERENCES `modbus_devices_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modbus_meassurement_templates`
--

LOCK TABLES `modbus_meassurement_templates` WRITE;
/*!40000 ALTER TABLE `modbus_meassurement_templates` DISABLE KEYS */;
INSERT INTO `modbus_meassurement_templates` VALUES (1,'V1/V12 Voltage',3,256,1,999999.0,0.0,'0-Vmax',2,0,'11111'),(2,'ejemplo',1,1,1,100000000.0,0.0,'1',2,0,'1');
/*!40000 ALTER TABLE `modbus_meassurement_templates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
