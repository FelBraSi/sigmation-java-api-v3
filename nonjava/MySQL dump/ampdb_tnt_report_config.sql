-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: ampdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tnt_report_config`
--

DROP TABLE IF EXISTS `tnt_report_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnt_report_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_smpt` varchar(200) NOT NULL DEFAULT 'smtp.gmail.com',
  `mail_port` varchar(200) NOT NULL DEFAULT '465',
  `mail_auth` varchar(200) NOT NULL DEFAULT 'true',
  `mail_account_address` varchar(200) NOT NULL DEFAULT 'report_origin@mail.address',
  `mail_report_address` varchar(200) NOT NULL DEFAULT 'report_destination@mail.address',
  `mail_password` varchar(45) NOT NULL DEFAULT 'password',
  `mail_title` varchar(200) NOT NULL DEFAULT 'Sigmation Daily Report',
  `mail_content` varchar(400) NOT NULL DEFAULT 'Automated Content from SIGMATION SERVER - Tagging&Tracking App',
  `hour` varchar(45) NOT NULL DEFAULT '00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tnt_report_config`
--

LOCK TABLES `tnt_report_config` WRITE;
/*!40000 ALTER TABLE `tnt_report_config` DISABLE KEYS */;
INSERT INTO `tnt_report_config` VALUES (2,'smtp.gmail.com','465','true','felipe@sigma-telecom.com','felipe@sigma-telecom.com','sigmati0n','Sigmation Daily Report','Automated Content from SIGMATION SERVER - Tagging&Tracking App','09:30');
/*!40000 ALTER TABLE `tnt_report_config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:06
