-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: config
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_definitions`
--

DROP TABLE IF EXISTS `sys_definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_definitions` (
  `parameter` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL DEFAULT '0',
  `det` varchar(200) DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`parameter`),
  UNIQUE KEY `parameter` (`parameter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_definitions`
--

LOCK TABLES `sys_definitions` WRITE;
/*!40000 ALTER TABLE `sys_definitions` DISABLE KEYS */;
INSERT INTO `sys_definitions` VALUES ('ApiSystem_dram','5','Diagnostico Remoto Amp.','apiSystem'),('ApiSystem_drfp','4','Diagnostico Remoto Fuentes','apiSystem'),('ApiSystem_fsys','6','Firewatcher','apiSystem'),('ApiSystem_mb_dolf','7','DOLF','apiSystem'),('ApiSystem_msg','2','Sistema Mensajes','apiSystem'),('ApiSystem_r_tgsrv','101','Remote Tag Server','apiSystem'),('ApiSystem_tag','1','Sistema Tag','apiSystem'),('ApiSystem_vent','3','Sistema Ventilacion','apiSystem'),('sversion_r_tag_1','101','Remote Tag Server V1.0','hwver'),('sversion_ssa_2','3','Sigma Sudafrica V2.0','hwver'),('sversion_stel_1','0','Sigma Telecom V1.0','hwver'),('sversion_stel_2','1','Sigma Telecom V2.0','hwver'),('sversion_stel_3_wmx','2','Sigma Telecom V3.0 WMX','hwver');
/*!40000 ALTER TABLE `sys_definitions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17  7:38:07
