// -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
// Example of a clock. This is very similar to the text-example,
// except that it shows the time :)
//
// This code is public domain
// (but note, that the led-matrix library this depends on is GPL v2)

#include "led-matrix.h"
#include "threaded-canvas-manipulator.h"
#include "transformer.h"
#include "graphics.h"

#include <assert.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <mysql/mysql.h>


#define DATABASE_NAME		"remotedb"
#define DATABASE_USERNAME	"root"
#define DATABASE_PASSWORD	"orca"
MYSQL *mysql1;


struct Pixel {
    Pixel() : red(0), green(0), blue(0){}
    uint8_t red;
    uint8_t green;
    uint8_t blue;
  };

  struct Image {
    Image() : width(-1), height(-1), image(NULL) {}
    ~Image() { Delete(); }
    void Delete() { delete [] image; Reset(); }
    void Reset() { image = NULL; width = -1; height = -1; }
    inline bool IsValid() { return image && height > 0 && width > 0; }
    const Pixel &getPixel(int x, int y) {
      static Pixel black;
      if (x < 0 || x >= width || y < 0 || y >= height) return black;
      return image[x + width * y];
    }

    int width;
    int height;
    Pixel *image;
  };

  // Read line, skip comments.
  char *ReadLine(FILE *f, char *buffer, size_t len) {
    char *result;
    do {
      result = fgets(buffer, len, f);
    } while (result != NULL && result[0] == '#');
    return result;
  }

  const int scroll_jumps_ =0; 

  // Current image is only manipulated in our thread.
  Image current_image_;

  // New image can be loaded from another thread, then taken over in main thread 
  Image new_image_; 


int32_t horizontal_position_;

using namespace rgb_matrix;
char text_buffer[256];
int msset=0;
Color color(255, 255, 255);
Color bg_color(0, 0, 0);
Color outline_color(0,0,0);



bool LoadPPM(const char *filename) {
    FILE *f = fopen(filename, "r");
    // check if file exists
    if (f == NULL && access(filename, F_OK) == -1) {
      fprintf(stderr, "File \"%s\" doesn't exist\n", filename);
      return false;
    }
    if (f == NULL) return false;
    char header_buf[256];
    const char *line = ReadLine(f, header_buf, sizeof(header_buf));
#define EXIT_WITH_MSG(m) { fprintf(stderr, "%s: %s |%s", filename, m, line); \
      fclose(f); return false; }
    if (sscanf(line, "P6 ") == EOF)
      EXIT_WITH_MSG("Can only handle P6 as PPM type.");
    line = ReadLine(f, header_buf, sizeof(header_buf));
    int new_width, new_height;
    if (!line || sscanf(line, "%d %d ", &new_width, &new_height) != 2)
      EXIT_WITH_MSG("Width/height expected");
    int value;
    line = ReadLine(f, header_buf, sizeof(header_buf));
    if (!line || sscanf(line, "%d ", &value) != 1 || value != 255)
      EXIT_WITH_MSG("Only 255 for maxval allowed.");
    const size_t pixel_count = new_width * new_height;
    Pixel *new_image = new Pixel [ pixel_count ];
    assert(sizeof(Pixel) == 3);   // we make that assumption.
    if (fread(new_image, sizeof(Pixel), pixel_count, f) != pixel_count) {
      line = "";
      EXIT_WITH_MSG("Not enough pixels read.");
    }
#undef EXIT_WITH_MSG
    fclose(f);
    fprintf(stderr, "Read image '%s' with %dx%d\n", filename,
            new_width, new_height);
    horizontal_position_ = 0; 
    new_image_.Delete();  // in case we reload faster than is picked up
    new_image_.image = new_image;
    new_image_.width = new_width;
    new_image_.height = new_height;
    return true;
}

volatile bool interrupt_received = false;
static void InterruptHandler(int signo) {
  interrupt_received = true;
}

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options]\n", progname);
  fprintf(stderr, "Reads text from stdin and displays it. "
          "Empty string: clear screen\n");
  fprintf(stderr, "Options:\n");
  rgb_matrix::PrintMatrixFlags(stderr);
  fprintf(stderr,
          "\t-d <time-format>  : Default '%%H:%%M'. See strftime()\n"
          "\t-f <font-file>    : Use given font.\n"
          "\t-b <brightness>   : Sets brightness percent. Default: 100.\n"
          "\t-x <x-origin>     : X-Origin of displaying text (Default: 0)\n"
          "\t-y <y-origin>     : Y-Origin of displaying text (Default: 0)\n"
          "\t-S <spacing>      : Spacing pixels between letters (Default: 0)\n"
          "\t-C <r,g,b>        : Color. Default 255,255,0\n"
          "\t-B <r,g,b>        : Background-Color. Default 0,0,0\n"
          "\t-O <r,g,b>        : Outline-Color, e.g. to increase contrast.\n"
          "\t-r <bck.brgt.rel> : will divide background colors for this number, to darken image.\n"
          "\t-R <txt.brgt.rel> : will divide text colors for this number, to darken image.\n"
          );

  return 1;
}

static bool parseColor(Color *c, const char *str) {
  return sscanf(str, "%hhu,%hhu,%hhu", &c->r, &c->g, &c->b) == 3;
}

static bool FullSaturation(const Color &c) {
    return (c.r == 0 || c.r == 255)
        && (c.g == 0 || c.g == 255)
        && (c.b == 0 || c.b == 255);
}


void mysqlConnect(){
    
    //initialize MYSQL object for connections
	mysql1 = mysql_init(NULL);

    if(mysql1 == NULL)
    {
        fprintf(stderr, "%s\n", mysql_error(mysql1));
        return;
    }

    //Connect to the database
    if(mysql_real_connect(mysql1, "localhost", DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME, 0, NULL, 0) == NULL)
    {
    	fprintf(stderr, "%s\n", mysql_error(mysql1));
    }
    else
    {
        printf("Database connection successful.\n");
    }
}


void mysql_disconnect (void)
{
    mysql_close(mysql1);
    printf( "Disconnected from database.\n");
}


void getMessage (int text_brightness)
{
   if (mysql1 != NULL)
    {
        if (!mysql_query(mysql1, "SELECT * from message"))
        {
        	MYSQL_RES *result = mysql_store_result(mysql1);
        	if (result != NULL)
        	{ 

        		MYSQL_ROW row;			//An array of strings
        		while( (row = mysql_fetch_row(result)) )
        		{
        			
                        if (strstr(row[1], "null")== NULL) {
                                sprintf(text_buffer,"%s \n",row[1]);
                                color.r = atoi(row[3])/text_brightness;
                                color.g = atoi(row[4])/text_brightness; 
                                color.b = atoi(row[5])/text_brightness;
                                bg_color.r = atoi(row[6]);
                                bg_color.g = atoi(row[7]);
                                bg_color.b = atoi(row[8]);
                                msset=1;
                                return ;
                        } else  msset=0;
                        color.r = (180/text_brightness); 
                        color.g = (180/text_brightness); 
                        color.b = (255/text_brightness);
                        bg_color.r = (0);
                        bg_color.g = (0);
                        bg_color.g = (0);
                        return ;
        	        
        		}
   	            mysql_free_result(result);
        	}
        }
        
    }
}

void getTime(){
    
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(text_buffer," %02d-%02d-%d     %02d:%02d\n", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,   tm.tm_hour, tm.tm_min); //, tm.tm_sec);
}

void setTime(int timeStamp){
    int rcc;
    char localbuffer[60];
    char comm[20] = "sudo date +%s -s @";
    sprintf(localbuffer,"%s%d", comm, timeStamp);
    rcc=system(localbuffer);
    if(rcc==0) {
            printf("time set\n");
    }
    else {
        printf("settimeofday() / failed, "
        "errno = %s\n",strerror(errno));
    }
}


void drawBackground(int brightrelation, FrameCanvas *offscreen, int swapbluetogreen){
    if (brightrelation<1) brightrelation =1;
    for (int x = 0; x < 256; ++x) {
        for (int y = 0; y < 32; ++y) {
          const Pixel &p = new_image_.getPixel(
            x % new_image_.width, y);
          if(swapbluetogreen==0) offscreen->SetPixel(x, y, p.red/brightrelation, p.green/brightrelation, p.blue/brightrelation);
          else offscreen->SetPixel(x, y, p.red/brightrelation, p.blue/brightrelation, p.green/brightrelation);
        }
    }
}

int main(int argc, char *argv[]) {

  mysqlConnect();
  RGBMatrix::Options matrix_options;
  rgb_matrix::RuntimeOptions runtime_opt;
  if (!rgb_matrix::ParseOptionsFromFlags(&argc, &argv,
                                         &matrix_options, &runtime_opt)) {
    return usage(argv[0]);
  }

  const char *time_format = "%H:%M"; 
  bool with_outline = false;
  //matrix_options.chain_length = 4; 

  const char *bdf_font_file = NULL;
  int x_orig = 0;
  int y_orig = 0;
  int brightness = 100;
  int letter_spacing = 0;
  int back_brightness = 1;
  int text_brightness = 1;
char* argtext;
  int opt;

  while ((opt = getopt(argc, argv, "x:y:f:W:C:B:O:b:S:d:r:R:")) != -1) {
    switch (opt) {
    case 'd': time_format = strdup(optarg); break;
    case 'b': brightness = atoi(optarg); break;
    case 'x': x_orig = atoi(optarg); break;
    case 'y': y_orig = atoi(optarg); break;
    case 'W': argtext = optarg; break;
    case 'f': bdf_font_file = strdup(optarg); break;
    case 'S': letter_spacing = atoi(optarg); break;
    case 'r': back_brightness = atoi(optarg); break;
    case 'R': text_brightness = atoi(optarg); break;
    case 'C':
      if (!parseColor(&color, optarg)) {
        fprintf(stderr, "Invalid color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'B':
      if (!parseColor(&bg_color, optarg)) {
        fprintf(stderr, "Invalid background color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'O':
      if (!parseColor(&outline_color, optarg)) {
        fprintf(stderr, "Invalid outline color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      with_outline = true;
      break;
    default:
      return usage(argv[0]);
    }
  }

  if (bdf_font_file == NULL) {
    //fprintf(stderr, "Need to specify BDF font-file with -f\n");
    bdf_font_file = "/var/www/html/sigma10x24.bdf";
    //return usage(argv[0]);
  }

  
  /*
   * Load font. This needs to be a filename with a bdf bitmap font.
   */

  rgb_matrix::Font font;
  if (!font.LoadFont(bdf_font_file)) {
    fprintf(stderr, "Couldn't load font '%s'\n", bdf_font_file);
    return 1;
  }
  rgb_matrix::Font *outline_font = NULL;
  if (with_outline) {
      outline_font = font.CreateOutlineFont();
  }

  if (brightness < 1 || brightness > 100) {
    fprintf(stderr, "Brightness is outside usable range.\n");
    return 1;
  }

  RGBMatrix *matrix = rgb_matrix::CreateMatrixFromOptions(matrix_options,
                                                          runtime_opt);
  if (matrix == NULL)
    return 1;

  matrix->SetBrightness(brightness);
 
  /*
   * Load PPM
   */
  bdf_font_file = "/home/pi/Documents/totrans.ppm";
  LoadPPM (bdf_font_file);

  const int x = x_orig;
  int y = y_orig;

  FrameCanvas *offscreen = matrix->CreateFrameCanvas();

  
  struct timespec next_time;
  next_time.tv_sec = time(NULL);
  next_time.tv_nsec = 0;
  struct tm tm;

  signal(SIGTERM, InterruptHandler);
  signal(SIGINT, InterruptHandler);
  
  drawBackground(1,&*offscreen,0);
      //offscreen = matrix->SwapOnVSync(offscreen);
      // Wait until we're ready to show it.
  clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &next_time, NULL);
      // Atomic swap with double buffer
  offscreen = matrix->SwapOnVSync(offscreen);

  next_time.tv_sec += 1;
       
  sleep(3);
  bdf_font_file = "/home/pi/Documents/totransback.ppm";
  LoadPPM (bdf_font_file);
  matrix->Clear();
  while (!interrupt_received) {
      getMessage(text_brightness);
      if(msset==0) getTime();
      //offscreen->Fill(bg_color.r, bg_color.g, bg_color.b);
      drawBackground(back_brightness,&*offscreen,msset);
      //offscreen = matrix->SwapOnVSync(offscreen);
      if (outline_font) {
          rgb_matrix::DrawText(offscreen, *outline_font,
                               x - 1, y + font.baseline(),
                               outline_color, NULL, text_buffer,
                               letter_spacing - 2);
      }
      rgb_matrix::DrawText(offscreen, font, x+2, y+2 + font.baseline(),
                           color, NULL, text_buffer,
                           letter_spacing);

      // Wait until we're ready to show it.
      clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &next_time, NULL);
      // Atomic swap with double buffer
      offscreen = matrix->SwapOnVSync(offscreen);

      next_time.tv_sec += 1;
      sleep(1);
  }

  // Finished. Shut down the RGB matrix.
  matrix->Clear();
  delete matrix;

  write(STDOUT_FILENO, "\n", 1);  // Create a fresh new line after ^C on screen
  return 0;
}
